-- phpMyAdmin SQL Dump
-- version 4.7.7
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Jul 06, 2018 at 09:48 AM
-- Server version: 10.1.34-MariaDB
-- PHP Version: 5.6.30

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `armeniea_coordinate`
--

-- --------------------------------------------------------

--
-- Table structure for table `t41uf_assets`
--

CREATE TABLE `t41uf_assets` (
  `id` int(10) UNSIGNED NOT NULL COMMENT 'Primary Key',
  `parent_id` int(11) NOT NULL DEFAULT '0' COMMENT 'Nested set parent.',
  `lft` int(11) NOT NULL DEFAULT '0' COMMENT 'Nested set lft.',
  `rgt` int(11) NOT NULL DEFAULT '0' COMMENT 'Nested set rgt.',
  `level` int(10) UNSIGNED NOT NULL COMMENT 'The cached level in the nested tree.',
  `name` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT 'The unique name for the asset.\n',
  `title` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT 'The descriptive title for the asset.',
  `rules` varchar(5120) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT 'JSON encoded access control.'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `t41uf_assets`
--

INSERT INTO `t41uf_assets` (`id`, `parent_id`, `lft`, `rgt`, `level`, `name`, `title`, `rules`) VALUES
(1, 0, 1, 199, 0, 'root.1', 'Root Asset', '{\"core.login.site\":{\"6\":1,\"2\":1},\"core.login.admin\":{\"6\":1},\"core.login.offline\":{\"6\":1},\"core.admin\":{\"8\":1},\"core.manage\":{\"7\":1},\"core.create\":{\"6\":1,\"3\":1},\"core.delete\":{\"6\":1},\"core.edit\":{\"6\":1,\"4\":1},\"core.edit.state\":{\"6\":1,\"5\":1},\"core.edit.own\":{\"6\":1,\"3\":1}}'),
(2, 1, 1, 2, 1, 'com_admin', 'com_admin', '{}'),
(3, 1, 3, 6, 1, 'com_banners', 'com_banners', '{\"core.admin\":{\"7\":1},\"core.manage\":{\"6\":1},\"core.create\":[],\"core.delete\":[],\"core.edit\":[],\"core.edit.state\":[]}'),
(4, 1, 7, 8, 1, 'com_cache', 'com_cache', '{\"core.admin\":{\"7\":1},\"core.manage\":{\"7\":1}}'),
(5, 1, 9, 10, 1, 'com_checkin', 'com_checkin', '{\"core.admin\":{\"7\":1},\"core.manage\":{\"7\":1}}'),
(6, 1, 11, 12, 1, 'com_config', 'com_config', '{}'),
(7, 1, 13, 16, 1, 'com_contact', 'com_contact', '{\"core.admin\":{\"7\":1},\"core.manage\":{\"6\":1},\"core.create\":[],\"core.delete\":[],\"core.edit\":[],\"core.edit.state\":[],\"core.edit.own\":[]}'),
(8, 1, 17, 104, 1, 'com_content', 'com_content', '{\"core.admin\":{\"7\":1},\"core.manage\":{\"6\":1},\"core.create\":{\"3\":1},\"core.delete\":[],\"core.edit\":{\"4\":1},\"core.edit.state\":{\"5\":1},\"core.edit.own\":[]}'),
(9, 1, 105, 106, 1, 'com_cpanel', 'com_cpanel', '{}'),
(10, 1, 107, 108, 1, 'com_installer', 'com_installer', '{\"core.admin\":[],\"core.manage\":{\"7\":0},\"core.delete\":{\"7\":0},\"core.edit.state\":{\"7\":0}}'),
(11, 1, 109, 110, 1, 'com_languages', 'com_languages', '{\"core.admin\":{\"7\":1},\"core.manage\":[],\"core.create\":[],\"core.delete\":[],\"core.edit\":[],\"core.edit.state\":[]}'),
(12, 1, 111, 112, 1, 'com_login', 'com_login', '{}'),
(13, 1, 113, 114, 1, 'com_mailto', 'com_mailto', '{}'),
(14, 1, 115, 116, 1, 'com_massmail', 'com_massmail', '{}'),
(15, 1, 117, 118, 1, 'com_media', 'com_media', '{\"core.admin\":{\"7\":1},\"core.manage\":{\"6\":1},\"core.create\":{\"3\":1},\"core.delete\":{\"5\":1}}'),
(16, 1, 119, 120, 1, 'com_menus', 'com_menus', '{\"core.admin\":{\"7\":1},\"core.manage\":[],\"core.create\":[],\"core.delete\":[],\"core.edit\":[],\"core.edit.state\":[]}'),
(17, 1, 121, 122, 1, 'com_messages', 'com_messages', '{\"core.admin\":{\"7\":1},\"core.manage\":{\"7\":1}}'),
(18, 1, 123, 158, 1, 'com_modules', 'com_modules', '{\"core.admin\":{\"7\":1},\"core.manage\":[],\"core.create\":[],\"core.delete\":[],\"core.edit\":[],\"core.edit.state\":[]}'),
(19, 1, 159, 162, 1, 'com_newsfeeds', 'com_newsfeeds', '{\"core.admin\":{\"7\":1},\"core.manage\":{\"6\":1},\"core.create\":[],\"core.delete\":[],\"core.edit\":[],\"core.edit.state\":[],\"core.edit.own\":[]}'),
(20, 1, 163, 164, 1, 'com_plugins', 'com_plugins', '{\"core.admin\":{\"7\":1},\"core.manage\":[],\"core.edit\":[],\"core.edit.state\":[]}'),
(21, 1, 165, 166, 1, 'com_redirect', 'com_redirect', '{\"core.admin\":{\"7\":1},\"core.manage\":[]}'),
(22, 1, 167, 168, 1, 'com_search', 'com_search', '{\"core.admin\":{\"7\":1},\"core.manage\":{\"6\":1}}'),
(23, 1, 169, 170, 1, 'com_templates', 'com_templates', '{\"core.admin\":{\"7\":1},\"core.manage\":[],\"core.create\":[],\"core.delete\":[],\"core.edit\":[],\"core.edit.state\":[]}'),
(24, 1, 171, 174, 1, 'com_users', 'com_users', '{\"core.admin\":{\"7\":1},\"core.manage\":[],\"core.create\":[],\"core.delete\":[],\"core.edit\":[],\"core.edit.state\":[]}'),
(25, 1, 175, 178, 1, 'com_weblinks', 'com_weblinks', '{\"core.admin\":{\"7\":1},\"core.manage\":{\"6\":1},\"core.create\":{\"3\":1},\"core.delete\":[],\"core.edit\":{\"4\":1},\"core.edit.state\":{\"5\":1},\"core.edit.own\":[]}'),
(26, 1, 179, 180, 1, 'com_wrapper', 'com_wrapper', '{}'),
(27, 8, 18, 21, 2, 'com_content.category.2', 'Uncategorised', '{\"core.create\":[],\"core.delete\":[],\"core.edit\":[],\"core.edit.state\":[],\"core.edit.own\":[]}'),
(28, 3, 4, 5, 2, 'com_banners.category.3', 'Uncategorised', '{\"core.create\":[],\"core.delete\":[],\"core.edit\":[],\"core.edit.state\":[]}'),
(29, 7, 14, 15, 2, 'com_contact.category.4', 'Uncategorised', '{\"core.create\":[],\"core.delete\":[],\"core.edit\":[],\"core.edit.state\":[],\"core.edit.own\":[]}'),
(30, 19, 160, 161, 2, 'com_newsfeeds.category.5', 'Uncategorised', '{\"core.create\":[],\"core.delete\":[],\"core.edit\":[],\"core.edit.state\":[],\"core.edit.own\":[]}'),
(31, 25, 176, 177, 2, 'com_weblinks.category.6', 'Uncategorised', '{\"core.create\":[],\"core.delete\":[],\"core.edit\":[],\"core.edit.state\":[],\"core.edit.own\":[]}'),
(32, 24, 172, 173, 1, 'com_users.category.7', 'Uncategorised', '{\"core.create\":[],\"core.delete\":[],\"core.edit\":[],\"core.edit.state\":[]}'),
(33, 1, 181, 182, 1, 'com_finder', 'com_finder', '{\"core.admin\":{\"7\":1},\"core.manage\":{\"6\":1}}'),
(34, 1, 183, 184, 1, 'com_joomlaupdate', 'com_joomlaupdate', '{\"core.admin\":[],\"core.manage\":[],\"core.delete\":[],\"core.edit.state\":[]}'),
(35, 37, 55, 64, 3, 'com_content.category.8', 'Joomla', '{\"core.create\":{\"6\":1,\"3\":1},\"core.delete\":{\"6\":1},\"core.edit\":{\"6\":1,\"4\":1},\"core.edit.state\":{\"6\":1,\"5\":1},\"core.edit.own\":{\"6\":1,\"3\":1}}'),
(36, 35, 56, 57, 4, 'com_content.article.1', 'The Joomla! Community', '{\"core.delete\":{\"6\":1},\"core.edit\":{\"6\":1,\"4\":1},\"core.edit.state\":{\"6\":1,\"5\":1}}'),
(37, 8, 22, 65, 2, 'com_content.category.9', 'Sample Data Article', '{\"core.create\":{\"6\":1,\"3\":1},\"core.delete\":{\"6\":1},\"core.edit\":{\"6\":1,\"4\":1},\"core.edit.state\":{\"6\":1,\"5\":1},\"core.edit.own\":{\"6\":1,\"3\":1}}'),
(38, 37, 23, 54, 3, 'com_content.category.10', 'Demo', '{\"core.create\":{\"6\":1,\"3\":1},\"core.delete\":{\"6\":1},\"core.edit\":{\"6\":1,\"4\":1},\"core.edit.state\":{\"6\":1,\"5\":1},\"core.edit.own\":{\"6\":1,\"3\":1}}'),
(40, 38, 24, 45, 4, 'com_content.category.11', 'Shortcode', '{\"core.create\":{\"6\":1,\"3\":1},\"core.delete\":{\"6\":1},\"core.edit\":{\"6\":1,\"4\":1},\"core.edit.state\":{\"6\":1,\"5\":1},\"core.edit.own\":{\"6\":1,\"3\":1}}'),
(41, 40, 25, 26, 5, 'com_content.article.3', 'Accordion', '{\"core.delete\":{\"6\":1},\"core.edit\":{\"6\":1,\"4\":1},\"core.edit.state\":{\"6\":1,\"5\":1}}'),
(42, 40, 27, 28, 5, 'com_content.article.4', 'Carousel', '{\"core.delete\":{\"6\":1},\"core.edit\":{\"6\":1,\"4\":1},\"core.edit.state\":{\"6\":1,\"5\":1}}'),
(43, 40, 29, 30, 5, 'com_content.article.5', 'Tab', '{\"core.delete\":{\"6\":1},\"core.edit\":{\"6\":1,\"4\":1},\"core.edit.state\":{\"6\":1,\"5\":1}}'),
(44, 40, 31, 32, 5, 'com_content.article.6', 'Map', '{\"core.delete\":{\"6\":1},\"core.edit\":{\"6\":1,\"4\":1},\"core.edit.state\":{\"6\":1,\"5\":1}}'),
(45, 40, 33, 34, 5, 'com_content.article.7', 'Testimonial ', '{\"core.delete\":{\"6\":1},\"core.edit\":{\"6\":1,\"4\":1},\"core.edit.state\":{\"6\":1,\"5\":1}}'),
(46, 40, 35, 36, 5, 'com_content.article.8', 'Alert', '{\"core.delete\":{\"6\":1},\"core.edit\":{\"6\":1,\"4\":1},\"core.edit.state\":{\"6\":1,\"5\":1}}'),
(47, 40, 37, 38, 5, 'com_content.article.9', 'Button', '{\"core.delete\":{\"6\":1},\"core.edit\":{\"6\":1,\"4\":1},\"core.edit.state\":{\"6\":1,\"5\":1}}'),
(48, 40, 39, 40, 5, 'com_content.article.10', 'Icon', '{\"core.delete\":{\"6\":1},\"core.edit\":{\"6\":1,\"4\":1},\"core.edit.state\":{\"6\":1,\"5\":1}}'),
(49, 40, 41, 42, 5, 'com_content.article.11', 'Column', '{\"core.delete\":{\"6\":1},\"core.edit\":{\"6\":1,\"4\":1},\"core.edit.state\":{\"6\":1,\"5\":1}}'),
(51, 38, 46, 47, 4, 'com_content.article.13', 'Article', '{\"core.delete\":{\"6\":1},\"core.edit\":{\"6\":1,\"4\":1},\"core.edit.state\":{\"6\":1,\"5\":1}}'),
(52, 38, 48, 49, 4, 'com_content.article.14', 'Typography', '{\"core.delete\":{\"6\":1},\"core.edit\":{\"6\":1,\"4\":1},\"core.edit.state\":{\"6\":1,\"5\":1}}'),
(54, 40, 43, 44, 5, 'com_content.article.16', 'Gallery', '{\"core.delete\":{\"6\":1},\"core.edit\":{\"6\":1,\"4\":1},\"core.edit.state\":{\"6\":1,\"5\":1}}'),
(55, 38, 50, 51, 4, 'com_content.article.17', 'Video', '{\"core.delete\":{\"6\":1},\"core.edit\":{\"6\":1,\"4\":1},\"core.edit.state\":{\"6\":1,\"5\":1}}'),
(56, 38, 52, 53, 4, 'com_content.article.18', 'Module Position', '{\"core.delete\":{\"6\":1},\"core.edit\":{\"6\":1,\"4\":1},\"core.edit.state\":{\"6\":1,\"5\":1}}'),
(57, 35, 58, 59, 4, 'com_content.article.19', 'The Joomla Blog', '{\"core.delete\":{\"6\":1},\"core.edit\":{\"6\":1,\"4\":1},\"core.edit.state\":{\"6\":1,\"5\":1}}'),
(58, 35, 60, 61, 4, 'com_content.article.20', 'The Joomla overview', '{\"core.delete\":{\"6\":1},\"core.edit\":{\"6\":1,\"4\":1},\"core.edit.state\":{\"6\":1,\"5\":1}}'),
(59, 35, 62, 63, 4, 'com_content.article.21', 'The Joomla Help', '{\"core.delete\":{\"6\":1},\"core.edit\":{\"6\":1,\"4\":1},\"core.edit.state\":{\"6\":1,\"5\":1}}'),
(60, 8, 66, 89, 2, 'com_content.category.12', 'portfolio', '{\"core.create\":{\"6\":1,\"3\":1},\"core.delete\":{\"6\":1},\"core.edit\":{\"6\":1,\"4\":1},\"core.edit.state\":{\"6\":1,\"5\":1},\"core.edit.own\":{\"6\":1,\"3\":1}}'),
(61, 60, 67, 74, 3, 'com_content.category.13', 'Graphic Design', '{\"core.create\":{\"6\":1,\"3\":1},\"core.delete\":{\"6\":1},\"core.edit\":{\"6\":1,\"4\":1},\"core.edit.state\":{\"6\":1,\"5\":1},\"core.edit.own\":{\"6\":1,\"3\":1}}'),
(62, 60, 75, 82, 3, 'com_content.category.14', '3D Modeling', '{\"core.create\":{\"6\":1,\"3\":1},\"core.delete\":{\"6\":1},\"core.edit\":{\"6\":1,\"4\":1},\"core.edit.state\":{\"6\":1,\"5\":1},\"core.edit.own\":{\"6\":1,\"3\":1}}'),
(63, 60, 83, 88, 3, 'com_content.category.15', 'Architecture', '{\"core.create\":{\"6\":1,\"3\":1},\"core.delete\":{\"6\":1},\"core.edit\":{\"6\":1,\"4\":1},\"core.edit.state\":{\"6\":1,\"5\":1},\"core.edit.own\":{\"6\":1,\"3\":1}}'),
(64, 61, 68, 69, 4, 'com_content.article.22', 'Lorem ipsum dolor sit amet', '{\"core.delete\":{\"6\":1},\"core.edit\":{\"6\":1,\"4\":1},\"core.edit.state\":{\"6\":1,\"5\":1}}'),
(65, 61, 70, 71, 4, 'com_content.article.23', 'Lorem ipsum dolor sit amet', '{\"core.delete\":{\"6\":1},\"core.edit\":{\"6\":1,\"4\":1},\"core.edit.state\":{\"6\":1,\"5\":1}}'),
(66, 62, 76, 77, 4, 'com_content.article.24', 'Lorem ipsum dolor sit amet', '{\"core.delete\":{\"6\":1},\"core.edit\":{\"6\":1,\"4\":1},\"core.edit.state\":{\"6\":1,\"5\":1}}'),
(67, 62, 78, 79, 4, 'com_content.article.25', 'Lorem ipsum dolor sit amet', '{\"core.delete\":{\"6\":1},\"core.edit\":{\"6\":1,\"4\":1},\"core.edit.state\":{\"6\":1,\"5\":1}}'),
(68, 63, 84, 85, 4, 'com_content.article.26', 'Lorem ipsum dolor sit amet', '{\"core.delete\":{\"6\":1},\"core.edit\":{\"6\":1,\"4\":1},\"core.edit.state\":{\"6\":1,\"5\":1}}'),
(69, 63, 86, 87, 4, 'com_content.article.27', 'Me hrashali project', '{\"core.delete\":{\"6\":1},\"core.edit\":{\"6\":1,\"4\":1},\"core.edit.state\":{\"6\":1,\"5\":1}}'),
(70, 61, 72, 73, 4, 'com_content.article.28', 'Lorem ipsum dolor sit amet', '{\"core.delete\":{\"6\":1},\"core.edit\":{\"6\":1,\"4\":1},\"core.edit.state\":{\"6\":1,\"5\":1}}'),
(71, 62, 80, 81, 4, 'com_content.article.29', 'Lorem ipsum dolor sit amet', '{\"core.delete\":{\"6\":1},\"core.edit\":{\"6\":1,\"4\":1},\"core.edit.state\":{\"6\":1,\"5\":1}}'),
(72, 1, 185, 186, 1, 'com_tags', 'com_tags', '{}'),
(73, 1, 187, 188, 1, 'com_contenthistory', 'com_contenthistory', '{}'),
(74, 1, 189, 190, 1, 'com_ajax', 'com_ajax', '{}'),
(75, 1, 191, 192, 1, 'com_postinstall', 'com_postinstall', '{}'),
(92, 18, 124, 125, 2, 'com_modules.module.123', 'SP Portfolio', '{\"core.delete\":{\"6\":1},\"core.edit\":{\"6\":1,\"4\":1},\"core.edit.state\":{\"6\":1,\"5\":1}}'),
(93, 18, 126, 127, 2, 'com_modules.module.128', 'Xeon Pricing Table', '{\"core.delete\":{\"6\":1},\"core.edit\":{\"6\":1,\"4\":1},\"core.edit.state\":{\"6\":1,\"5\":1}}'),
(94, 18, 128, 129, 2, 'com_modules.module.116', 'services Architectural Design', '{\"core.delete\":{\"6\":1},\"core.edit\":{\"6\":1,\"4\":1},\"core.edit.state\":{\"6\":1,\"5\":1}}'),
(95, 18, 130, 131, 2, 'com_modules.module.119', 'service WEB', '{\"core.delete\":{\"6\":1},\"core.edit\":{\"6\":1,\"4\":1},\"core.edit.state\":{\"6\":1,\"5\":1}}'),
(96, 18, 132, 133, 2, 'com_modules.module.117', 'service Modeling', '{\"core.delete\":{\"6\":1},\"core.edit\":{\"6\":1,\"4\":1},\"core.edit.state\":{\"6\":1,\"5\":1}}'),
(97, 18, 134, 135, 2, 'com_modules.module.120', 'service Graphic', '{\"core.delete\":{\"6\":1},\"core.edit\":{\"6\":1,\"4\":1},\"core.edit.state\":{\"6\":1,\"5\":1}}'),
(98, 18, 136, 137, 2, 'com_modules.module.118', 'service Interior', '{\"core.delete\":{\"6\":1},\"core.edit\":{\"6\":1,\"4\":1},\"core.edit.state\":{\"6\":1,\"5\":1}}'),
(99, 18, 138, 139, 2, 'com_modules.module.121', 'service Animation', '{\"core.delete\":{\"6\":1},\"core.edit\":{\"6\":1,\"4\":1},\"core.edit.state\":{\"6\":1,\"5\":1}}'),
(100, 1, 193, 194, 1, 'com_fields', 'com_fields', '{}'),
(101, 1, 195, 196, 1, 'com_associations', 'com_associations', '{}'),
(102, 18, 140, 141, 2, 'com_modules.module.122', 'Xeon Slider', '{}'),
(103, 18, 142, 143, 2, 'com_modules.module.132', 'test ', '{}'),
(104, 18, 144, 145, 2, 'com_modules.module.129', 'AboutUs', '{}'),
(105, 18, 146, 147, 2, 'com_modules.module.131', 'Presets', '{}'),
(106, 18, 148, 149, 2, 'com_modules.module.105', 'Our Address', '{}'),
(107, 18, 150, 151, 2, 'com_modules.module.133', 'video', '{}'),
(108, 18, 152, 153, 2, 'com_modules.module.130', 'Xeon Team', '{}'),
(109, 27, 19, 20, 3, 'com_content.article.30', 'test Gallery', '{}'),
(110, 111, 91, 92, 3, 'com_content.article.31', 'Architectural Design', '{}'),
(111, 8, 90, 103, 2, 'com_content.category.16', 'Services', '{}'),
(112, 111, 93, 94, 3, 'com_content.article.32', '3D modeling', '{}'),
(113, 111, 95, 96, 3, 'com_content.article.33', 'Graphic Design', '{}'),
(114, 111, 97, 98, 3, 'com_content.article.34', 'WEB Design ', '{}'),
(115, 18, 154, 155, 2, 'com_modules.module.134', 'services', '{}'),
(116, 111, 99, 100, 3, 'com_content.article.35', 'Interior', '{}'),
(117, 111, 101, 102, 3, 'com_content.article.36', 'Animation', '{}'),
(118, 18, 156, 157, 2, 'com_modules.module.135', 'Xeon Slider for Architect', '{}'),
(119, 1, 197, 198, 1, 'com_osgallery', 'COM_OSGALLERY', '{}');

-- --------------------------------------------------------

--
-- Table structure for table `t41uf_associations`
--

CREATE TABLE `t41uf_associations` (
  `id` int(11) NOT NULL COMMENT 'A reference to the associated item.',
  `context` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT 'The context of the associated item.',
  `key` char(32) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT 'The key for the association computed from an md5 on associated ids.'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `t41uf_banners`
--

CREATE TABLE `t41uf_banners` (
  `id` int(11) NOT NULL,
  `cid` int(11) NOT NULL DEFAULT '0',
  `type` int(11) NOT NULL DEFAULT '0',
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `alias` varchar(400) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL DEFAULT '',
  `imptotal` int(11) NOT NULL DEFAULT '0',
  `impmade` int(11) NOT NULL DEFAULT '0',
  `clicks` int(11) NOT NULL DEFAULT '0',
  `clickurl` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `state` tinyint(3) NOT NULL DEFAULT '0',
  `catid` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `description` mediumtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `custombannercode` varchar(2048) COLLATE utf8mb4_unicode_ci NOT NULL,
  `sticky` tinyint(1) UNSIGNED NOT NULL DEFAULT '0',
  `ordering` int(11) NOT NULL DEFAULT '0',
  `metakey` mediumtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `params` mediumtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `own_prefix` tinyint(1) NOT NULL DEFAULT '0',
  `metakey_prefix` varchar(400) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `purchase_type` tinyint(4) NOT NULL DEFAULT '-1',
  `track_clicks` tinyint(4) NOT NULL DEFAULT '-1',
  `track_impressions` tinyint(4) NOT NULL DEFAULT '-1',
  `checked_out` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `checked_out_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `publish_up` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `publish_down` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `reset` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `created` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `language` char(7) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `created_by` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `created_by_alias` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `modified` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `modified_by` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `version` int(10) UNSIGNED NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `t41uf_banner_clients`
--

CREATE TABLE `t41uf_banner_clients` (
  `id` int(11) NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `contact` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `extrainfo` mediumtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `state` tinyint(3) NOT NULL DEFAULT '0',
  `checked_out` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `checked_out_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `metakey` mediumtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `own_prefix` tinyint(4) NOT NULL DEFAULT '0',
  `metakey_prefix` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `purchase_type` tinyint(4) NOT NULL DEFAULT '-1',
  `track_clicks` tinyint(4) NOT NULL DEFAULT '-1',
  `track_impressions` tinyint(4) NOT NULL DEFAULT '-1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `t41uf_banner_tracks`
--

CREATE TABLE `t41uf_banner_tracks` (
  `track_date` datetime NOT NULL,
  `track_type` int(10) UNSIGNED NOT NULL,
  `banner_id` int(10) UNSIGNED NOT NULL,
  `count` int(10) UNSIGNED NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `t41uf_categories`
--

CREATE TABLE `t41uf_categories` (
  `id` int(11) NOT NULL,
  `asset_id` int(10) UNSIGNED NOT NULL DEFAULT '0' COMMENT 'FK to the #___assets table.',
  `parent_id` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `lft` int(11) NOT NULL DEFAULT '0',
  `rgt` int(11) NOT NULL DEFAULT '0',
  `level` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `path` varchar(400) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `extension` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `alias` varchar(400) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL DEFAULT '',
  `note` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `description` mediumtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `published` tinyint(1) NOT NULL DEFAULT '0',
  `checked_out` int(11) UNSIGNED NOT NULL DEFAULT '0',
  `checked_out_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `access` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `params` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `metadesc` varchar(1024) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '' COMMENT 'The meta description for the page.',
  `metakey` varchar(1024) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '' COMMENT 'The meta keywords for the page.',
  `metadata` varchar(2048) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '' COMMENT 'JSON encoded metadata properties.',
  `created_user_id` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `created_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `modified_user_id` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `modified_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `hits` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `language` char(7) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `version` int(10) UNSIGNED NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `t41uf_categories`
--

INSERT INTO `t41uf_categories` (`id`, `asset_id`, `parent_id`, `lft`, `rgt`, `level`, `path`, `extension`, `title`, `alias`, `note`, `description`, `published`, `checked_out`, `checked_out_time`, `access`, `params`, `metadesc`, `metakey`, `metadata`, `created_user_id`, `created_time`, `modified_user_id`, `modified_time`, `hits`, `language`, `version`) VALUES
(1, 0, 0, 0, 31, 0, '', 'system', 'ROOT', 'root', '', '', 1, 0, '0000-00-00 00:00:00', 1, '{}', '', '', '', 239, '2018-06-26 11:45:53', 0, '0000-00-00 00:00:00', 0, '*', 1),
(2, 27, 1, 1, 2, 1, 'uncategorised', 'com_content', 'Uncategorised', 'uncategorised', '', '', 1, 0, '0000-00-00 00:00:00', 1, '{\"target\":\"\",\"image\":\"\"}', '', '', '{\"page_title\":\"\",\"author\":\"\",\"robots\":\"\"}', 239, '2018-06-26 11:45:53', 0, '0000-00-00 00:00:00', 0, '*', 1),
(3, 28, 1, 3, 4, 1, 'uncategorised', 'com_banners', 'Uncategorised', 'uncategorised', '', '', 1, 0, '0000-00-00 00:00:00', 1, '{\"target\":\"\",\"image\":\"\",\"foobar\":\"\"}', '', '', '{\"page_title\":\"\",\"author\":\"\",\"robots\":\"\"}', 239, '2018-06-26 11:45:53', 0, '0000-00-00 00:00:00', 0, '*', 1),
(4, 29, 1, 5, 6, 1, 'uncategorised', 'com_contact', 'Uncategorised', 'uncategorised', '', '', 1, 0, '0000-00-00 00:00:00', 1, '{\"target\":\"\",\"image\":\"\"}', '', '', '{\"page_title\":\"\",\"author\":\"\",\"robots\":\"\"}', 239, '2018-06-26 11:45:53', 0, '0000-00-00 00:00:00', 0, '*', 1),
(5, 30, 1, 7, 8, 1, 'uncategorised', 'com_newsfeeds', 'Uncategorised', 'uncategorised', '', '', 1, 0, '0000-00-00 00:00:00', 1, '{\"target\":\"\",\"image\":\"\"}', '', '', '{\"page_title\":\"\",\"author\":\"\",\"robots\":\"\"}', 239, '2018-06-26 11:45:53', 0, '0000-00-00 00:00:00', 0, '*', 1),
(6, 31, 1, 9, 10, 1, 'uncategorised', 'com_weblinks', 'Uncategorised', 'uncategorised', '', '', 1, 0, '0000-00-00 00:00:00', 1, '{\"target\":\"\",\"image\":\"\"}', '', '', '{\"page_title\":\"\",\"author\":\"\",\"robots\":\"\"}', 239, '2018-06-26 11:45:53', 0, '0000-00-00 00:00:00', 0, '*', 1),
(7, 32, 1, 11, 12, 1, 'uncategorised', 'com_users', 'Uncategorised', 'uncategorised', '', '', 1, 0, '0000-00-00 00:00:00', 1, '{\"target\":\"\",\"image\":\"\"}', '', '', '{\"page_title\":\"\",\"author\":\"\",\"robots\":\"\"}', 239, '2018-06-26 11:45:53', 0, '0000-00-00 00:00:00', 0, '*', 1),
(8, 35, 9, 18, 19, 2, 'sample-data-article/joomla', 'com_content', 'Joomla', 'joomla', '', '', 1, 0, '0000-00-00 00:00:00', 1, '{\"category_layout\":\"\",\"image\":\"\"}', '', '', '{\"author\":\"\",\"robots\":\"\"}', 239, '2018-06-26 11:45:53', 239, '2018-06-26 11:45:53', 0, '*', 1),
(9, 37, 1, 13, 20, 1, 'sample-data-article', 'com_content', 'Sample Data Article', 'sample-data-article', '', '', 1, 0, '0000-00-00 00:00:00', 1, '{\"category_layout\":\"\",\"image\":\"\"}', '', '', '{\"author\":\"\",\"robots\":\"\"}', 239, '2018-06-26 11:45:53', 0, '0000-00-00 00:00:00', 0, '*', 1),
(10, 38, 9, 14, 17, 2, 'sample-data-article/demo', 'com_content', 'Demo', 'demo', '', '', 1, 0, '0000-00-00 00:00:00', 1, '{\"category_layout\":\"\",\"image\":\"\"}', '', '', '{\"author\":\"\",\"robots\":\"\"}', 239, '2018-06-26 11:45:53', 0, '0000-00-00 00:00:00', 0, '*', 1),
(11, 40, 10, 15, 16, 3, 'sample-data-article/demo/short-code', 'com_content', 'Shortcode', 'short-code', '', '', 1, 0, '0000-00-00 00:00:00', 1, '{\"category_layout\":\"\",\"image\":\"\"}', '', '', '{\"author\":\"\",\"robots\":\"\"}', 239, '2018-06-26 11:45:53', 0, '0000-00-00 00:00:00', 0, '*', 1),
(12, 60, 1, 21, 28, 1, 'portfolio', 'com_content', 'portfolio', 'portfolio', '', '', 1, 0, '0000-00-00 00:00:00', 1, '{\"category_layout\":\"\",\"image\":\"\"}', '', '', '{\"author\":\"\",\"robots\":\"\"}', 239, '2018-06-26 11:45:53', 239, '2018-06-26 11:45:53', 0, '*', 1),
(13, 61, 12, 24, 25, 2, 'portfolio/design', 'com_content', 'Graphic Design', 'design', '', '', 1, 0, '0000-00-00 00:00:00', 1, '{\"category_layout\":\"\",\"image\":\"\",\"image_alt\":\"\"}', '', '', '{\"author\":\"\",\"robots\":\"\"}', 239, '2018-06-26 11:45:53', 239, '2018-06-27 12:48:26', 0, '*', 1),
(14, 62, 12, 26, 27, 2, 'portfolio/3d', 'com_content', '3D Modeling', '3d', '', '', 1, 0, '0000-00-00 00:00:00', 1, '{\"category_layout\":\"\",\"image\":\"\",\"image_alt\":\"\"}', '', '', '{\"author\":\"\",\"robots\":\"\"}', 239, '2018-06-26 11:45:53', 239, '2018-06-27 12:46:52', 0, '*', 1),
(15, 63, 12, 22, 23, 2, 'portfolio/architecture', 'com_content', 'Architecture', 'architecture', '', '', 1, 0, '0000-00-00 00:00:00', 1, '{\"category_layout\":\"\",\"image\":\"\",\"image_alt\":\"\"}', '', '', '{\"author\":\"\",\"robots\":\"\"}', 239, '2018-06-26 11:45:53', 239, '2018-06-27 12:47:13', 0, '*', 1),
(16, 111, 1, 29, 30, 1, 'services', 'com_content', 'Services', 'services', '', '', 1, 0, '0000-00-00 00:00:00', 1, '{\"category_layout\":\"\",\"image\":\"\",\"image_alt\":\"\"}', '', '', '{\"author\":\"\",\"robots\":\"\"}', 239, '2018-07-04 19:44:10', 0, '2018-07-04 19:44:10', 0, '*', 1);

-- --------------------------------------------------------

--
-- Table structure for table `t41uf_contact_details`
--

CREATE TABLE `t41uf_contact_details` (
  `id` int(11) NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `alias` varchar(400) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL,
  `con_position` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `address` mediumtext COLLATE utf8mb4_unicode_ci,
  `suburb` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `state` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `country` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `postcode` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `telephone` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `fax` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `misc` longtext COLLATE utf8mb4_unicode_ci,
  `image` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email_to` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `default_con` tinyint(1) UNSIGNED NOT NULL DEFAULT '0',
  `published` tinyint(1) NOT NULL DEFAULT '0',
  `checked_out` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `checked_out_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `ordering` int(11) NOT NULL DEFAULT '0',
  `params` mediumtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` int(11) NOT NULL DEFAULT '0',
  `catid` int(11) NOT NULL DEFAULT '0',
  `access` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `mobile` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `webpage` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `sortname1` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `sortname2` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `sortname3` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `language` varchar(7) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `created_by` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `created_by_alias` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `modified` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `modified_by` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `metakey` mediumtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `metadesc` mediumtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `metadata` mediumtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `featured` tinyint(3) UNSIGNED NOT NULL DEFAULT '0' COMMENT 'Set if article is featured.',
  `xreference` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '' COMMENT 'A reference to enable linkages to external data sets.',
  `publish_up` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `publish_down` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `version` int(10) UNSIGNED NOT NULL DEFAULT '1',
  `hits` int(10) UNSIGNED NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `t41uf_contact_details`
--

INSERT INTO `t41uf_contact_details` (`id`, `name`, `alias`, `con_position`, `address`, `suburb`, `state`, `country`, `postcode`, `telephone`, `fax`, `misc`, `image`, `email_to`, `default_con`, `published`, `checked_out`, `checked_out_time`, `ordering`, `params`, `user_id`, `catid`, `access`, `mobile`, `webpage`, `sortname1`, `sortname2`, `sortname3`, `language`, `created`, `created_by`, `created_by_alias`, `modified`, `modified_by`, `metakey`, `metadesc`, `metadata`, `featured`, `xreference`, `publish_up`, `publish_down`, `version`, `hits`) VALUES
(1, 'Contact Name', 'name', 'Position', 'Street Address', 'Suburb', 'State', 'Country', 'Zip Code', 'Telephone', 'Fax', '<p>Information about or by the contact.</p>', '', 'email@email.com', 0, 1, 0, '0000-00-00 00:00:00', 1, '{\"show_contact_category\":\"\",\"show_contact_list\":\"\",\"presentation_style\":\"\",\"show_name\":\"\",\"show_position\":\"\",\"show_email\":\"\",\"show_street_address\":\"\",\"show_suburb\":\"\",\"show_state\":\"\",\"show_postcode\":\"\",\"show_country\":\"\",\"show_telephone\":\"\",\"show_mobile\":\"\",\"show_fax\":\"\",\"show_webpage\":\"\",\"show_misc\":\"\",\"show_image\":\"\",\"allow_vcard\":\"\",\"show_articles\":\"\",\"show_profile\":\"\",\"show_links\":\"\",\"linka_name\":\"Facebook\",\"linka\":\"http:\\/\\/www.facebook.com\\/joomla\",\"linkb_name\":\"Twitter\",\"linkb\":\"http:\\/\\/twitter.com\\/joomla\",\"linkc_name\":\"\",\"linkc\":null,\"linkd_name\":\"\",\"linkd\":null,\"linke_name\":\"\",\"linke\":\"\",\"contact_layout\":\"\",\"show_email_form\":\"\",\"show_email_copy\":\"\",\"banned_email\":\"\",\"banned_subject\":\"\",\"banned_text\":\"\",\"validate_session\":\"\",\"custom_reply\":\"\",\"redirect\":\"\"}', 0, 4, 1, '', '', 'last', 'first', 'middle', '*', '2018-06-26 11:45:53', 239, '', '0000-00-00 00:00:00', 0, '', '', '{\"robots\":\"\",\"rights\":\"\"}', 0, '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 2);

-- --------------------------------------------------------

--
-- Table structure for table `t41uf_content`
--

CREATE TABLE `t41uf_content` (
  `id` int(10) UNSIGNED NOT NULL,
  `asset_id` int(10) UNSIGNED NOT NULL DEFAULT '0' COMMENT 'FK to the #___assets table.',
  `title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `alias` varchar(400) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL DEFAULT '',
  `introtext` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `fulltext` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `state` tinyint(3) NOT NULL DEFAULT '0',
  `catid` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `created` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `created_by` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `created_by_alias` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `modified` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `modified_by` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `checked_out` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `checked_out_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `publish_up` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `publish_down` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `images` mediumtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `urls` mediumtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `attribs` varchar(5120) COLLATE utf8mb4_unicode_ci NOT NULL,
  `version` int(10) UNSIGNED NOT NULL DEFAULT '1',
  `ordering` int(11) NOT NULL DEFAULT '0',
  `metakey` mediumtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `metadesc` mediumtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `access` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `hits` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `metadata` mediumtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `featured` tinyint(3) UNSIGNED NOT NULL DEFAULT '0' COMMENT 'Set if article is featured.',
  `language` char(7) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT 'The language code for the article.',
  `xreference` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT ''
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `t41uf_content`
--

INSERT INTO `t41uf_content` (`id`, `asset_id`, `title`, `alias`, `introtext`, `fulltext`, `state`, `catid`, `created`, `created_by`, `created_by_alias`, `modified`, `modified_by`, `checked_out`, `checked_out_time`, `publish_up`, `publish_down`, `images`, `urls`, `attribs`, `version`, `ordering`, `metakey`, `metadesc`, `access`, `hits`, `metadata`, `featured`, `language`, `xreference`) VALUES
(1, 36, 'The Joomla! Community', 'the-joomla-community', '<p>Joomla! means All Together, and it is a community of people all working and having fun together that makes Joomla! possible. Thousands of people each year participate in the Joomla! community, and we hope you will be one of them.</p>\r\n<p>People with all kinds of skills, of all skill levels and from around the world are welcome to join in. Participate in the <a href=\"http://joomla.org\">Joomla.org</a> family of websites (the<a href=\"http://forum.joomla.org\"> forum </a>is a great place to start). Come to a <a href=\"http://community.joomla.org/events.html\">Joomla! event</a>. Join or start a <a href=\"http://community.joomla.org/user-groups.html\">Joomla! Users Group</a>. Whether you are a developer, site administrator, designer, end user or fan, there are ways for you to participate and contribute.</p>', '', 0, 8, '2018-06-26 11:45:53', 239, '', '2018-06-26 11:45:53', 239, 0, '0000-00-00 00:00:00', '2018-06-26 11:45:53', '0000-00-00 00:00:00', '{\"image_intro\":\"\",\"float_intro\":\"\",\"image_intro_alt\":\"\",\"image_intro_caption\":\"\",\"image_fulltext\":\"\",\"float_fulltext\":\"\",\"image_fulltext_alt\":\"\",\"image_fulltext_caption\":\"\"}', '{\"urla\":null,\"urlatext\":\"\",\"targeta\":\"\",\"urlb\":null,\"urlbtext\":\"\",\"targetb\":\"\",\"urlc\":null,\"urlctext\":\"\",\"targetc\":\"\"}', '{\"show_title\":\"\",\"link_titles\":\"\",\"show_intro\":\"\",\"show_category\":\"\",\"link_category\":\"\",\"show_parent_category\":\"\",\"link_parent_category\":\"\",\"show_author\":\"\",\"link_author\":\"\",\"show_create_date\":\"\",\"show_modify_date\":\"\",\"show_publish_date\":\"\",\"show_item_navigation\":\"\",\"show_icons\":\"\",\"show_print_icon\":\"\",\"show_email_icon\":\"\",\"show_vote\":\"\",\"show_hits\":\"\",\"show_noauth\":\"\",\"urls_position\":\"\",\"alternative_readmore\":\"\",\"article_layout\":\"\",\"show_publishing_options\":\"\",\"show_article_options\":\"\",\"show_urls_images_backend\":\"\",\"show_urls_images_frontend\":\"\"}', 2, 3, '', '', 1, 98, '{\"robots\":\"\",\"author\":\"\",\"rights\":\"\",\"xreference\":\"\"}', 0, '*', ''),
(3, 41, 'Accordion', 'sc-accordion', '<p>[accordion id=\"sc-accordion\"] [accordion_item title=\'Item 1\']Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin ornare consectetur sodales. Nulla luctus cursus mauris at dapibus. Cras ac felis et neque consequat elementum a eget turpis. Aliquam erat volutpat. Integer feugiat sem eu ligula vulputate consequat. Nulla facilisi. Cras vel elit lectus, at fringilla lorem.[/accordion_item] [accordion_item title=\'Item 2\']consectetur adipiscing elit. Proin ornare consectetur sodales. Nulla luctus cursus mauris at dapibus. Cras ac felis et neque consequat elementum a eget turpis. Aliquam erat volutpat. Integer feugiat sem eu ligula vulputate consequat. Nulla facilisi. Cras vel elit lectus, at fringilla lorem.[/accordion_item] [accordion_item title=\'Item 3\'] Proin ornare consectetur sodales. Nulla luctus cursus mauris at dapibus. Cras ac felis et neque consequat elementum a eget turpis. Aliquam erat volutpat. Integer feugiat sem eu ligula vulputate consequat. Nulla facilisi. Cras vel elit lectus, at fringilla lorem.[/accordion_item] [/accordion]</p>\r\n<h3>Get the code</h3>\r\n<pre>[[accordion] <br />    [accordion_item title=\'ITEM_TITLE\']ADD_CONTENT_HERE[/accordion_item]<br />    [accordion_item title=\'ITEM_TITLE\']ADD_CONTENT_HERE[/accordion_item] <br />    [accordion_item title=\'ITEM_TITLE\']ADD_CONTENT_HERE[/accordion_item]<br />[/accordion]]</pre>', '', 1, 11, '2018-06-26 11:45:53', 239, '', '2018-06-26 11:45:53', 239, 0, '0000-00-00 00:00:00', '2018-06-26 11:45:53', '0000-00-00 00:00:00', '{\"image_intro\":\"\",\"float_intro\":\"\",\"image_intro_alt\":\"\",\"image_intro_caption\":\"\",\"image_fulltext\":\"\",\"float_fulltext\":\"\",\"image_fulltext_alt\":\"\",\"image_fulltext_caption\":\"\"}', '{\"urla\":null,\"urlatext\":\"\",\"targeta\":\"\",\"urlb\":null,\"urlbtext\":\"\",\"targetb\":\"\",\"urlc\":null,\"urlctext\":\"\",\"targetc\":\"\"}', '{\"show_title\":\"\",\"link_titles\":\"\",\"show_intro\":\"\",\"show_category\":\"\",\"link_category\":\"\",\"show_parent_category\":\"\",\"link_parent_category\":\"\",\"show_author\":\"\",\"link_author\":\"\",\"show_create_date\":\"\",\"show_modify_date\":\"\",\"show_publish_date\":\"\",\"show_item_navigation\":\"\",\"show_icons\":\"\",\"show_print_icon\":\"\",\"show_email_icon\":\"\",\"show_vote\":\"\",\"show_hits\":\"\",\"show_noauth\":\"\",\"urls_position\":\"\",\"alternative_readmore\":\"\",\"article_layout\":\"\",\"show_publishing_options\":\"\",\"show_article_options\":\"\",\"show_urls_images_backend\":\"\",\"show_urls_images_frontend\":\"\"}', 11, 9, '', '', 1, 48, '{\"robots\":\"\",\"author\":\"\",\"rights\":\"\",\"xreference\":\"\"}', 0, '*', ''),
(4, 42, 'Carousel', 'sc-carousel', '<p>[carousel][carousel_item]<img src=\"images/helix/carousel1.png\" border=\"0\" alt=\"\" width=\"1500\" height=\"550\" style=\"border: 0;\" /> [caption] Powerful templates framework to develop Joomla base website faster! [/caption] [/carousel_item] [carousel_item] <img src=\"images/helix/carousel2.png\" border=\"0\" alt=\"\" width=\"1500\" height=\"550\" style=\"border: 0;\" /> [caption] Powerful templates framework to develop Joomla base website faster! [/caption] [/carousel_item] [carousel_item] <img src=\"images/helix/carousel3.png\" border=\"0\" alt=\"\" width=\"1500\" height=\"550\" style=\"border: 0;\" />[caption] Powerful templates framework to develop Joomla base website faster! [/caption] [/carousel_item] [/carousel]</p>\r\n<h3>Get the code</h3>\r\n<pre>[[carousel]\r\n[carousel_item]add image here [caption]Powerful templates framework to develop Joomla base website faster![/caption][/carousel_item]\r\n[/carousel]]</pre>', '', 1, 11, '2018-06-26 11:45:53', 239, '', '2018-06-26 11:45:53', 239, 0, '0000-00-00 00:00:00', '2018-06-26 11:45:53', '0000-00-00 00:00:00', '{\"image_intro\":\"\",\"float_intro\":\"\",\"image_intro_alt\":\"\",\"image_intro_caption\":\"\",\"image_fulltext\":\"\",\"float_fulltext\":\"\",\"image_fulltext_alt\":\"\",\"image_fulltext_caption\":\"\"}', '{\"urla\":null,\"urlatext\":\"\",\"targeta\":\"\",\"urlb\":null,\"urlbtext\":\"\",\"targetb\":\"\",\"urlc\":null,\"urlctext\":\"\",\"targetc\":\"\"}', '{\"show_title\":\"\",\"link_titles\":\"\",\"show_intro\":\"\",\"show_category\":\"\",\"link_category\":\"\",\"show_parent_category\":\"\",\"link_parent_category\":\"\",\"show_author\":\"\",\"link_author\":\"\",\"show_create_date\":\"\",\"show_modify_date\":\"\",\"show_publish_date\":\"\",\"show_item_navigation\":\"\",\"show_icons\":\"\",\"show_print_icon\":\"\",\"show_email_icon\":\"\",\"show_vote\":\"\",\"show_hits\":\"\",\"show_noauth\":\"\",\"urls_position\":\"\",\"alternative_readmore\":\"\",\"article_layout\":\"\",\"show_publishing_options\":\"\",\"show_article_options\":\"\",\"show_urls_images_backend\":\"\",\"show_urls_images_frontend\":\"\"}', 23, 8, '', '', 1, 80, '{\"robots\":\"\",\"author\":\"\",\"rights\":\"\",\"xreference\":\"\"}', 0, '*', ''),
(5, 43, 'Tab', 'tab', '<p>[row]</p>\r\n<p>[col class=\"span6\"]</p>\r\n<p>[tab id=\"tab1\" class=\"tabbale\" button=\"nav-tabs\"] [tab_item title=\"Tab1\"]Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin ornare consectetur sodales. Nulla luctus cursus mauris at dapibus. Cras ac felis et neque consequat elementum a eget turpis. Aliquam erat volutpat. Integer feugiat sem eu ligula vulputate consequat. Nulla facilisi. Cras vel elit lectus, at fringilla lorem.[/tab_item] [tab_item title=\"Tab2\"]consectetur adipiscing elit. Proin ornare consectetur sodales. Nulla luctus cursus mauris at dapibus. Cras ac felis et neque consequat elementum a eget turpis. Aliquam erat volutpat. Integer feugiat sem eu ligula vulputate consequat. Nulla facilisi. Cras vel elit lectus, at fringilla lorem.[/tab_item] [tab_item title=\"Tab3\"]consectetur adipiscing elit. Proin ornare consectetur sodales. Nulla luctus cursus mauris at dapibus. Cras ac felis et neque consequat elementum a eget turpis. Aliquam erat volutpat. Integer feugiat sem eu ligula vulputate consequat. Nulla facilisi. Cras vel elit lectus, at fringilla lorem.[/tab_item][/tab]</p>\r\n<p>[/col]</p>\r\n<p>[col class=\"span6\"]</p>\r\n<p>[tab id=\"tab2\" class=\"tabbale\" button=\"nav-pills\"] [tab_item title=\"Tab1\"]Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin ornare consectetur sodales. Nulla luctus cursus mauris at dapibus. Cras ac felis et neque consequat elementum a eget turpis. Aliquam erat volutpat. Integer feugiat sem eu ligula vulputate consequat. Nulla facilisi. Cras vel elit lectus, at fringilla lorem.[/tab_item] [tab_item title=\"Tab2\"]consectetur adipiscing elit. Proin ornare consectetur sodales. Nulla luctus cursus mauris at dapibus. Cras ac felis et neque consequat elementum a eget turpis. Aliquam erat volutpat. Integer feugiat sem eu ligula vulputate consequat. Nulla facilisi. Cras vel elit lectus, at fringilla lorem.[/tab_item] [tab_item title=\"Tab3\"]consectetur adipiscing elit. Proin ornare consectetur sodales. Nulla luctus cursus mauris at dapibus. Cras ac felis et neque consequat elementum a eget turpis. Aliquam erat volutpat. Integer feugiat sem eu ligula vulputate consequat. Nulla facilisi. Cras vel elit lectus, at fringilla lorem.[/tab_item][/tab]</p>\r\n<p>[/col]</p>\r\n<p>[/row]</p>\r\n<h3>Get the code</h3>\r\n<pre>[[tab] \r\n[tab_item title=\"ITEM_TITLE\"]ADD_CONTENT_HERE[/tab_item ]\r\n[tab_item title=\"ITEM_TITLE\"]ADD_CONTENT_HERE[/tab_item ] \r\n[tab_item title=\"ITEM_TITLE\"]ADD_CONTENT_HERE[/tab_item ]\r\n[/tab]]\r\n</pre>', '', 1, 11, '2018-06-26 11:45:53', 239, '', '2018-06-26 11:45:53', 239, 0, '0000-00-00 00:00:00', '2018-06-26 11:45:53', '0000-00-00 00:00:00', '{\"image_intro\":\"\",\"float_intro\":\"\",\"image_intro_alt\":\"\",\"image_intro_caption\":\"\",\"image_fulltext\":\"\",\"float_fulltext\":\"\",\"image_fulltext_alt\":\"\",\"image_fulltext_caption\":\"\"}', '{\"urla\":null,\"urlatext\":\"\",\"targeta\":\"\",\"urlb\":null,\"urlbtext\":\"\",\"targetb\":\"\",\"urlc\":null,\"urlctext\":\"\",\"targetc\":\"\"}', '{\"show_title\":\"\",\"link_titles\":\"\",\"show_intro\":\"\",\"show_category\":\"\",\"link_category\":\"\",\"show_parent_category\":\"\",\"link_parent_category\":\"\",\"show_author\":\"\",\"link_author\":\"\",\"show_create_date\":\"\",\"show_modify_date\":\"\",\"show_publish_date\":\"\",\"show_item_navigation\":\"\",\"show_icons\":\"\",\"show_print_icon\":\"\",\"show_email_icon\":\"\",\"show_vote\":\"\",\"show_hits\":\"\",\"show_noauth\":\"\",\"urls_position\":\"\",\"alternative_readmore\":\"\",\"article_layout\":\"\",\"show_publishing_options\":\"\",\"show_article_options\":\"\",\"show_urls_images_backend\":\"\",\"show_urls_images_frontend\":\"\"}', 9, 7, '', '', 1, 53, '{\"robots\":\"\",\"author\":\"\",\"rights\":\"\",\"xreference\":\"\"}', 0, '*', ''),
(6, 44, 'Map', 'sc-map', '<p>[spmap lat=\"23.743193\" lng=\"90.388281\" zoom=\"8\" maptype=\"ROADMAP\"]</p>\r\n<h3>Get the code</h3>\r\n<pre>[[spmap lat=\"LATITUDE\" lng=\"LONGITUDE\" zoom=\"VALUE 1 to 10\"]]</pre>', '', 1, 11, '2018-06-26 11:45:53', 239, '', '2018-06-26 11:45:53', 239, 0, '0000-00-00 00:00:00', '2018-06-26 11:45:53', '0000-00-00 00:00:00', '{\"image_intro\":\"\",\"float_intro\":\"\",\"image_intro_alt\":\"\",\"image_intro_caption\":\"\",\"image_fulltext\":\"\",\"float_fulltext\":\"\",\"image_fulltext_alt\":\"\",\"image_fulltext_caption\":\"\"}', '{\"urla\":null,\"urlatext\":\"\",\"targeta\":\"\",\"urlb\":null,\"urlbtext\":\"\",\"targetb\":\"\",\"urlc\":null,\"urlctext\":\"\",\"targetc\":\"\"}', '{\"show_title\":\"\",\"link_titles\":\"\",\"show_intro\":\"\",\"show_category\":\"\",\"link_category\":\"\",\"show_parent_category\":\"\",\"link_parent_category\":\"\",\"show_author\":\"\",\"link_author\":\"\",\"show_create_date\":\"\",\"show_modify_date\":\"\",\"show_publish_date\":\"\",\"show_item_navigation\":\"\",\"show_icons\":\"\",\"show_print_icon\":\"\",\"show_email_icon\":\"\",\"show_vote\":\"\",\"show_hits\":\"\",\"show_noauth\":\"\",\"urls_position\":\"\",\"alternative_readmore\":\"\",\"article_layout\":\"\",\"show_publishing_options\":\"\",\"show_article_options\":\"\",\"show_urls_images_backend\":\"\",\"show_urls_images_frontend\":\"\"}', 9, 6, '', '', 1, 35, '{\"robots\":\"\",\"author\":\"\",\"rights\":\"\",\"xreference\":\"\"}', 0, '*', ''),
(7, 45, 'Testimonial ', 'testimonial', '<p>[testimonial name=\"John Doe\" email=\"jakirhasaneng@gmail.com\" company=\"joomshaper\" designation=\"Developer\"]Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum venenatis, felis a semper mollis, mauris mi suscipit dui, non laoreet diam enim et turpis. Sed imperdiet ultrices felis, at ultricies tellus consequat a. Proin condimentum porttitor eros, vitae facilisis sapien rhoncus vitae. Aliquam dapibus elit non metus posuere blandit. Phasellus a aliquam urna. Aliquam ac massa tellus, a semper odio. In hac habitasse platea dictumst. Integer tincidunt, nisi quis congue consectetur, lacus augue scelerisque enim, eu vehicula neque tortor ac risus. Nunc mollis interdum iaculis. [/testimonial]</p>\r\n<h3>Get the code</h3>\r\n<pre>[[testimonial name=\"AUTHOR_NAME\" email=\"AUTHOR_EMAIL\" company=\"AUTHOR_COMPANY\" designation=\"AUTHOR_DESIGNATION\"]ADD_CONTENT_HERE[/testimonial]]</pre>', '', 1, 11, '2018-06-26 11:45:53', 239, '', '2018-06-26 11:45:53', 239, 0, '0000-00-00 00:00:00', '2018-06-26 11:45:53', '0000-00-00 00:00:00', '{\"image_intro\":\"\",\"float_intro\":\"\",\"image_intro_alt\":\"\",\"image_intro_caption\":\"\",\"image_fulltext\":\"\",\"float_fulltext\":\"\",\"image_fulltext_alt\":\"\",\"image_fulltext_caption\":\"\"}', '{\"urla\":null,\"urlatext\":\"\",\"targeta\":\"\",\"urlb\":null,\"urlbtext\":\"\",\"targetb\":\"\",\"urlc\":null,\"urlctext\":\"\",\"targetc\":\"\"}', '{\"show_title\":\"\",\"link_titles\":\"\",\"show_intro\":\"\",\"show_category\":\"\",\"link_category\":\"\",\"show_parent_category\":\"\",\"link_parent_category\":\"\",\"show_author\":\"\",\"link_author\":\"\",\"show_create_date\":\"\",\"show_modify_date\":\"\",\"show_publish_date\":\"\",\"show_item_navigation\":\"\",\"show_icons\":\"\",\"show_print_icon\":\"\",\"show_email_icon\":\"\",\"show_vote\":\"\",\"show_hits\":\"\",\"show_noauth\":\"\",\"urls_position\":\"\",\"alternative_readmore\":\"\",\"article_layout\":\"\",\"show_publishing_options\":\"\",\"show_article_options\":\"\",\"show_urls_images_backend\":\"\",\"show_urls_images_frontend\":\"\"}', 8, 5, '', '', 1, 32, '{\"robots\":\"\",\"author\":\"\",\"rights\":\"\",\"xreference\":\"\"}', 0, '*', ''),
(8, 46, 'Alert', 'sc-alert', '<p>[alert type=\"alert\"]</p>\r\n<h4>Warning!</h4>\r\n<p>Best check yo self, you\'re not looking too good. Nulla vitae elit libero, a pharetra augue. [/alert] [alert type=\"error\" style=\"width:85%\"]</p>\r\n<h4>Error or danger!</h4>\r\n<p>Oh snap! Change a few things up and try submitting again.[/alert] [alert type=\"success\" style=\"width:75%\"]</p>\r\n<h4>success!</h4>\r\n<p>Well done! You successfully read this important alert message. [/alert] [alert type=\"info\" style=\"width:65%\"]</p>\r\n<h4>Information!</h4>\r\n<p>Heads up! This alert needs your attention, but it\'s not super important. [/alert]</p>', '', 1, 11, '2018-06-26 11:45:53', 239, '', '2018-06-26 11:45:53', 239, 0, '0000-00-00 00:00:00', '2018-06-26 11:45:53', '0000-00-00 00:00:00', '{\"image_intro\":\"\",\"float_intro\":\"\",\"image_intro_alt\":\"\",\"image_intro_caption\":\"\",\"image_fulltext\":\"\",\"float_fulltext\":\"\",\"image_fulltext_alt\":\"\",\"image_fulltext_caption\":\"\"}', '{\"urla\":null,\"urlatext\":\"\",\"targeta\":\"\",\"urlb\":null,\"urlbtext\":\"\",\"targetb\":\"\",\"urlc\":null,\"urlctext\":\"\",\"targetc\":\"\"}', '{\"show_title\":\"\",\"link_titles\":\"\",\"show_intro\":\"\",\"show_category\":\"\",\"link_category\":\"\",\"show_parent_category\":\"\",\"link_parent_category\":\"\",\"show_author\":\"\",\"link_author\":\"\",\"show_create_date\":\"\",\"show_modify_date\":\"\",\"show_publish_date\":\"\",\"show_item_navigation\":\"\",\"show_icons\":\"\",\"show_print_icon\":\"\",\"show_email_icon\":\"\",\"show_vote\":\"\",\"show_hits\":\"\",\"show_noauth\":\"\",\"urls_position\":\"\",\"alternative_readmore\":\"\",\"article_layout\":\"\",\"show_publishing_options\":\"\",\"show_article_options\":\"\",\"show_urls_images_backend\":\"\",\"show_urls_images_frontend\":\"\"}', 6, 4, '', '', 1, 47, '{\"robots\":\"\",\"author\":\"\",\"rights\":\"\",\"xreference\":\"\"}', 0, '*', ''),
(9, 47, 'Button', 'sc-button', '<p>[row] [col class=\"span6\"] [button type=\"default\" size=\"\" link=\"#\"]Default[/button]</p>\r\n<p>[button type=\"primary\" size=\"\" link=\"#\"]Primary[/button]</p>\r\n<p>[button type=\"info\" size=\"\" link=\"#\"]Info[/button]</p>\r\n<p>[button type=\"success\" size=\"\" link=\"#\"]Success[/button]</p>\r\n<p>[button type=\"warning\" size=\"\" link=\"#\"]Warning[/button]</p>\r\n<p>[button type=\"danger\" size=\"\" link=\"#\"]danger[/button]</p>\r\n<p>[button type=\"inverse\" size=\"\" link=\"#\"]Inverse[/button]</p>\r\n<p>[button type=\"block btn-primary\" size=\"large\" link=\"#\"]Block level button[/button] [/col] [col class=\"span6\"] [button type=\"default\" size=\"large\" link=\"#\"]Large button[/button]</p>\r\n<p>[button type=\"primary\" size=\"large\" link=\"#\"]Large button[/button]</p>\r\n<p>[button type=\"default\" size=\"\" link=\"#\"]Default button[/button]</p>\r\n<p>[button type=\"primary\" size=\"\" link=\"#\"]Default button[/button]</p>\r\n<p>[button type=\"default\" size=\"small\" link=\"#\"]Small button[/button]</p>\r\n<p>[button type=\"primary\" size=\"small\" link=\"#\"]Small button[/button]</p>\r\n<p>[button type=\"default\" size=\"mini\" link=\"#\"]Mini button[/button]</p>\r\n<p>[button type=\"primary\" size=\"mini\" link=\"#\"]Mini button[/button] [/col] [/row]</p>\r\n<h3>Get the code</h3>\r\n<pre>[[button type=\"BUTTON_TYPE\"]...[/button]]\r\n</pre>', '', 1, 11, '2018-06-26 11:45:53', 239, '', '2018-06-26 11:45:53', 239, 0, '0000-00-00 00:00:00', '2018-06-26 11:45:53', '0000-00-00 00:00:00', '{\"image_intro\":\"\",\"float_intro\":\"\",\"image_intro_alt\":\"\",\"image_intro_caption\":\"\",\"image_fulltext\":\"\",\"float_fulltext\":\"\",\"image_fulltext_alt\":\"\",\"image_fulltext_caption\":\"\"}', '{\"urla\":null,\"urlatext\":\"\",\"targeta\":\"\",\"urlb\":null,\"urlbtext\":\"\",\"targetb\":\"\",\"urlc\":null,\"urlctext\":\"\",\"targetc\":\"\"}', '{\"show_title\":\"\",\"link_titles\":\"\",\"show_intro\":\"\",\"show_category\":\"\",\"link_category\":\"\",\"show_parent_category\":\"\",\"link_parent_category\":\"\",\"show_author\":\"\",\"link_author\":\"\",\"show_create_date\":\"\",\"show_modify_date\":\"\",\"show_publish_date\":\"\",\"show_item_navigation\":\"\",\"show_icons\":\"\",\"show_print_icon\":\"\",\"show_email_icon\":\"\",\"show_vote\":\"\",\"show_hits\":\"\",\"show_noauth\":\"\",\"urls_position\":\"\",\"alternative_readmore\":\"\",\"article_layout\":\"\",\"show_publishing_options\":\"\",\"show_article_options\":\"\",\"show_urls_images_backend\":\"\",\"show_urls_images_frontend\":\"\"}', 4, 3, '', '', 1, 27, '{\"robots\":\"\",\"author\":\"\",\"rights\":\"\",\"xreference\":\"\"}', 0, '*', ''),
(10, 48, 'Icon', 'sc-icon', '<p>[fontawesome_icons]</p>', '', 1, 11, '2018-06-26 11:45:53', 239, '', '2018-06-26 11:45:53', 239, 0, '0000-00-00 00:00:00', '2018-06-26 11:45:53', '0000-00-00 00:00:00', '{\"image_intro\":\"\",\"float_intro\":\"\",\"image_intro_alt\":\"\",\"image_intro_caption\":\"\",\"image_fulltext\":\"\",\"float_fulltext\":\"\",\"image_fulltext_alt\":\"\",\"image_fulltext_caption\":\"\"}', '{\"urla\":false,\"urlatext\":\"\",\"targeta\":\"\",\"urlb\":false,\"urlbtext\":\"\",\"targetb\":\"\",\"urlc\":false,\"urlctext\":\"\",\"targetc\":\"\"}', '{\"show_title\":\"\",\"link_titles\":\"\",\"show_intro\":\"\",\"show_category\":\"\",\"link_category\":\"\",\"show_parent_category\":\"\",\"link_parent_category\":\"\",\"show_author\":\"\",\"link_author\":\"\",\"show_create_date\":\"\",\"show_modify_date\":\"\",\"show_publish_date\":\"\",\"show_item_navigation\":\"\",\"show_icons\":\"\",\"show_print_icon\":\"\",\"show_email_icon\":\"\",\"show_vote\":\"\",\"show_hits\":\"\",\"show_noauth\":\"\",\"urls_position\":\"\",\"alternative_readmore\":\"\",\"article_layout\":\"\",\"show_publishing_options\":\"\",\"show_article_options\":\"\",\"show_urls_images_backend\":\"\",\"show_urls_images_frontend\":\"\"}', 12, 2, '', '', 1, 49, '{\"robots\":\"\",\"author\":\"\",\"rights\":\"\",\"xreference\":\"\"}', 0, '*', ''),
(11, 49, 'Column', 'sc-column', '<p>[row class=\"show-grid\"] [col class=\"span4\"] Cras justo odio, dapibus ac facilisis in, egestas eget quam. Donec id elit non mi porta gravida at eget metus. Nullam id dolor id nibh ultricies vehicula ut id.[/col] [col class=\"span4\"] Cras justo odio, dapibus ac facilisis in, egestas eget quam. Donec id elit non mi porta gravida at eget metus. Nullam id dolor id nibh ultricies vehicula ut id.[/col] [col class=\"span4\"] Cras justo odio, dapibus ac facilisis in, egestas eget quam. Donec id elit non mi porta gravida at eget metus. Nullam id dolor id nibh ultricies vehicula ut id.[/col] [/row]</p>\r\n<h3>Get the code</h3>\r\n<pre>[[row id=\"ROW_ID\" class=\"ROW_CLASS\"]\r\n[col class=\"span4\"]ADD_CONTENT_HERE[/col]\r\n[col class=\"span4\"]ADD_CONTENT_HERE[/col]\r\n[col class=\"span4\"]ADD_CONTENT_HERE[/col]\r\n[/row]]\r\n</pre>\r\n<p>[row class=\"show-grid\"] [col class=\"span8\"] Cras justo odio, dapibus ac facilisis in, egestas eget quam. Donec id elit non mi porta gravida at eget metus. Nullam id dolor id nibh ultricies vehicula ut id. Cras justo odio, dapibus ac facilisis in, egestas eget quam. Donec id elit non mi porta gravida at eget metus. Nullam id dolor id nibh ultricies vehicula ut id. Nullam id dolor id nibh ultricies vehicula ut id. Donec id elit non mi porta gravida at eget metus.[/col] [col class=\"span4\"] Cras justo odio, dapibus ac facilisis in, egestas eget quam. Donec id elit non mi porta gravida at eget metus. Nullam id dolor id nibh ultricies vehicula ut id.[/col] [/row]</p>\r\n<h3>Get the code</h3>\r\n<pre>[[row id=\"ROW_ID\" class=\"ROW_CLASS\"]\r\n[col class=\"span8\"]ADD_CONTENT_HERE[/col]\r\n[col class=\"span4\"]ADD_CONTENT_HERE[/col]\r\n[/row]]\r\n</pre>', '', 1, 11, '2018-06-26 11:45:53', 239, '', '2018-06-26 11:45:53', 239, 0, '0000-00-00 00:00:00', '2018-06-26 11:45:53', '0000-00-00 00:00:00', '{\"image_intro\":\"\",\"float_intro\":\"\",\"image_intro_alt\":\"\",\"image_intro_caption\":\"\",\"image_fulltext\":\"\",\"float_fulltext\":\"\",\"image_fulltext_alt\":\"\",\"image_fulltext_caption\":\"\"}', '{\"urla\":null,\"urlatext\":\"\",\"targeta\":\"\",\"urlb\":null,\"urlbtext\":\"\",\"targetb\":\"\",\"urlc\":null,\"urlctext\":\"\",\"targetc\":\"\"}', '{\"show_title\":\"\",\"link_titles\":\"\",\"show_intro\":\"\",\"show_category\":\"\",\"link_category\":\"\",\"show_parent_category\":\"\",\"link_parent_category\":\"\",\"show_author\":\"\",\"link_author\":\"\",\"show_create_date\":\"\",\"show_modify_date\":\"\",\"show_publish_date\":\"\",\"show_item_navigation\":\"\",\"show_icons\":\"\",\"show_print_icon\":\"\",\"show_email_icon\":\"\",\"show_vote\":\"\",\"show_hits\":\"\",\"show_noauth\":\"\",\"urls_position\":\"\",\"alternative_readmore\":\"\",\"article_layout\":\"\",\"show_publishing_options\":\"\",\"show_article_options\":\"\",\"show_urls_images_backend\":\"\",\"show_urls_images_frontend\":\"\"}', 10, 1, '', '', 1, 36, '{\"robots\":\"\",\"author\":\"\",\"rights\":\"\",\"xreference\":\"\"}', 0, '*', ''),
(13, 51, 'Article', 'article', '<ul>\r\n<li>This is a sample <strong>unordered list</strong></li>\r\n</ul>\r\n<ul class=\"arrow\">\r\n<li>ul with class <strong>arrow</strong></li>\r\n</ul>\r\n<ul class=\"arrow-2\">\r\n<li>ul with class <strong>arrow-2</strong></li>\r\n</ul>\r\n<ul class=\"star\">\r\n<li>ul with class <strong>star</strong></li>\r\n</ul>\r\n<ul class=\"rss\">\r\n<li>ul with class <strong>rss</strong></li>\r\n</ul>', '', 1, 10, '2018-06-26 11:45:53', 239, '', '0000-00-00 00:00:00', 0, 0, '0000-00-00 00:00:00', '2018-06-26 11:45:53', '0000-00-00 00:00:00', '{\"image_intro\":\"\",\"float_intro\":\"\",\"image_intro_alt\":\"\",\"image_intro_caption\":\"\",\"image_fulltext\":\"\",\"float_fulltext\":\"\",\"image_fulltext_alt\":\"\",\"image_fulltext_caption\":\"\"}', '{\"urla\":null,\"urlatext\":\"\",\"targeta\":\"\",\"urlb\":null,\"urlbtext\":\"\",\"targetb\":\"\",\"urlc\":null,\"urlctext\":\"\",\"targetc\":\"\"}', '{\"show_title\":\"\",\"link_titles\":\"\",\"show_intro\":\"\",\"show_category\":\"\",\"link_category\":\"\",\"show_parent_category\":\"\",\"link_parent_category\":\"\",\"show_author\":\"\",\"link_author\":\"\",\"show_create_date\":\"\",\"show_modify_date\":\"\",\"show_publish_date\":\"\",\"show_item_navigation\":\"\",\"show_icons\":\"\",\"show_print_icon\":\"\",\"show_email_icon\":\"\",\"show_vote\":\"\",\"show_hits\":\"\",\"show_noauth\":\"\",\"urls_position\":\"\",\"alternative_readmore\":\"\",\"article_layout\":\"\",\"show_publishing_options\":\"\",\"show_article_options\":\"\",\"show_urls_images_backend\":\"\",\"show_urls_images_frontend\":\"\"}', 1, 3, '', '', 1, 7, '{\"robots\":\"\",\"author\":\"\",\"rights\":\"\",\"xreference\":\"\"}', 0, '*', ''),
(14, 52, 'Typography', 'typgraphy', '<p>[row]</p>\r\n<p>[col class=\"span6\"]</p>\r\n<h1>h1. Heading 1</h1>\r\n<h2>h2. Heading 2</h2>\r\n<h3>h3. Heading 3</h3>\r\n<h4>h4. Heading 4</h4>\r\n<h5>h5. Heading 5</h5>\r\n<h6>h6. Heading 6</h6>\r\n<p>[/col]</p>\r\n<p>[col class=\"span6\"]</p>\r\n<h3>Dropcap</h3>\r\n<p>[dropcap]This is a Magazine Style Drop Cap. Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.[/dropcap]</p>\r\n<pre>[[dropcap]Dropcap Texts[/dropcap]]</pre>\r\n<p>[/col]</p>\r\n<p>[/row]</p>\r\n<p>[row]</p>\r\n<h3>Lists example</h3>\r\n<p>[col class=\"span6\"]</p>\r\n<ul>\r\n<li>This is a sample <strong>unordered list</strong></li>\r\n</ul>\r\n<ul class=\"arrow\">\r\n<li>ul with class <strong>arrow</strong></li>\r\n</ul>\r\n<ul class=\"arrow-double\">\r\n<li>ul with class <strong>arrow-double</strong></li>\r\n</ul>\r\n<ul class=\"tick\">\r\n<li>ul with class <strong>tick</strong></li>\r\n</ul>\r\n<ul class=\"cross\">\r\n<li>ul with class <strong>cross</strong></li>\r\n</ul>\r\n<ul class=\"star\">\r\n<li>ul with class <strong>star</strong></li>\r\n</ul>\r\n<ul class=\"rss\">\r\n<li>ul with class <strong>rss</strong></li>\r\n</ul>\r\n<p>[/col]</p>\r\n<p>[col class=\"span6\"]</p>\r\n<ol>\r\n<li>This is a sample <strong>ordered list</strong></li>\r\n</ol>\r\n<ul class=\"arrow\">\r\n<li>ul with class <strong>arrow</strong></li>\r\n</ul>\r\n<ul class=\"arrow-double\">\r\n<li>ul with class <strong>arrow-double</strong></li>\r\n</ul>\r\n<ul class=\"tick\">\r\n<li>ul with class <strong>tick</strong></li>\r\n</ul>\r\n<ul class=\"cross\">\r\n<li>ul with class <strong>cross</strong></li>\r\n</ul>\r\n<ul class=\"star\">\r\n<li>ul with class <strong>star</strong></li>\r\n</ul>\r\n<ul class=\"rss\">\r\n<li>ul with class <strong>rss</strong></li>\r\n</ul>\r\n<p>[/col]</p>\r\n<p>[/row]</p>\r\n<h3>Block Number</h3>\r\n<p>[row][col class=\"span4\"] [blocknumber type=\"circle\" text=\"01\" color=\"#FFF\" background=\"#34bcf5\"]Lorem ipsum dolor sit amet consectetuer rutrum dignissim et neque id.[/blocknumber]</p>\r\n<p>[blocknumber type=\"circle\" text=\"02\" color=\"#FFF\" background=\"#aacb24\"]Lorem ipsum dolor sit amet consectetuer rutrum dignissim et neque id.[/blocknumber]</p>\r\n<p>[blocknumber type=\"circle\" text=\"03\" color=\"#FFF\" background=\"#f16a10\"]Lorem ipsum dolor sit amet consectetuer rutrum dignissim et neque id.[/blocknumber]</p>\r\n<pre>[[blocknumber type=\"circle\" text=\"01\" color=\"#FFF\" background=\"#f16a10\"]Circle Block Number[/blocknumber]]</pre>\r\n<p>[/col]</p>\r\n<p>[col class=\"span4\"] [blocknumber type=\"rounded\" text=\"01\" color=\"#FFF\" background=\"#7d2828\"]Lorem ipsum dolor sit amet consectetuer rutrum dignissim et neque id.[/blocknumber]</p>\r\n<p>[blocknumber type=\"rounded\" text=\"02\" color=\"#FFF\" background=\"#d80000\"]Lorem ipsum dolor sit amet consectetuer rutrum dignissim et neque id.[/blocknumber]</p>\r\n<p>[blocknumber type=\"rounded\" text=\"03\" color=\"#FFF\" background=\"#329491\"]Lorem ipsum dolor sit amet consectetuer rutrum dignissim et neque id.[/blocknumber]</p>\r\n<pre>[[blocknumber type=\"rounded\" text=\"01\" color=\"#FFF\" background=\"#329491\"]Rounded Block Number[/blocknumber]]</pre>\r\n<p>[/col]</p>\r\n<p>[col class=\"span4\"] [blocknumber text=\"01\" color=\"#FFF\" background=\"#999\"]Lorem ipsum dolor sit amet consectetuer rutrum dignissim et neque id.[/blocknumber]</p>\r\n<p>[blocknumber text=\"02\" color=\"#FFF\" background=\"#666\"]Lorem ipsum dolor sit amet consectetuer rutrum dignissim et neque id.[/blocknumber]</p>\r\n<p>[blocknumber text=\"03\" color=\"#FFF\" background=\"#000\"]Lorem ipsum dolor sit amet consectetuer rutrum dignissim et neque id.[/blocknumber]</p>\r\n<pre>[[blocknumber text=\"01\" color=\"#FFF\" background=\"#999\"]Normal Block Number[/blocknumber]]</pre>\r\n<p>[/col]<span style=\"line-height: 1.3em;\">[/row]</span></p>\r\n<h3>Block Examples </h3>\r\n<p>[row][col class=\"span4\"]</p>\r\n<p>[block color=\"#FFF\" background=\"#34bcf5\"]Lorem ipsum dolor sit amet, sic genero nomine Piscatore mihi. Dicis Deducitur potest flens praemio quod non dum veniens indica enim.[/block]</p>\r\n<p>[block color=\"#FFF\" background=\"#aacb24\"]Lorem ipsum dolor sit amet, sic genero nomine Piscatore mihi. Dicis Deducitur potest flens praemio quod non dum veniens indica enim.[/block]</p>\r\n<p>[block color=\"#FFF\" background=\"#f16a10\"]Lorem ipsum dolor sit amet, sic genero nomine Piscatore mihi. Dicis Deducitur potest flens praemio quod non dum veniens indica enim.[/block]</p>\r\n<pre>[[block color=\"#FFF\" background=\"#f16a10\"]Content[/block]]</pre>\r\n<p>[/col]</p>\r\n<p>[col class=\"span4\"]</p>\r\n<p>[block type=\"rounded\" color=\"#FFF\" background=\"#7d2828\"]Lorem ipsum dolor sit amet, sic genero nomine Piscatore mihi. Dicis Deducitur potest flens praemio quod non dum veniens indica enim.[/block]</p>\r\n<p>[block type=\"rounded\" color=\"#FFF\" background=\"#329491\"]Lorem ipsum dolor sit amet, sic genero nomine Piscatore mihi. Dicis Deducitur potest flens praemio quod non dum veniens indica enim.[/block]</p>\r\n<p>[block type=\"rounded\" color=\"#FFF\" background=\"#000000\"]Lorem ipsum dolor sit amet, sic genero nomine Piscatore mihi. Dicis Deducitur potest flens praemio quod non dum veniens indica enim.[/block]</p>\r\n<pre>[[block type=\"rounded\" color=\"#FFF\" background=\"#000000\"]Lorem ipsum dolor[/block]]</pre>\r\n<p>[/col]</p>\r\n<p>[col class=\"span4\"]</p>\r\n<p>[block border=\"1px dashed #CCC\" padding=\"14px 15px\"]Lorem ipsum dolor sit amet, sic genero nomine Piscatore mihi. Dicis Deducitur potest flens praemio quod non dum veniens indica enim.[/block]</p>\r\n<p>[block border=\"3px solid #34bcf5\" padding=\"12px 15px\"]Lorem ipsum dolor sit amet, sic genero nomine Piscatore mihi. Dicis Deducitur potest flens praemio quod non dum veniens indica enim.[/block]</p>\r\n<p>[block type=\"rounded\" border=\"5px solid #aacb24\" padding=\"10px 15px\"]Lorem ipsum dolor sit amet, sic genero nomine Piscatore mihi. Dicis Deducitur potest flens praemio quod non dum veniens indica enim.[/block]</p>\r\n<pre>[[block border=\"5px solid #aacb24\" padding=\"10px 15px\"]Lorem ipsum dolor[/block]]</pre>\r\n<p>[/col]</p>\r\n<p>[/row]</p>\r\n<h3>Bubble Examples</h3>\r\n<p>[row][col class=\"span4\"]</p>\r\n<p><span style=\"line-height: 1.3em;\">[bubble author=\"Betty D. Steward\"]Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed ligula orci, ullamcorper vitae sodales venenatis, feugiat et felis. Donec non dui velit, a posuere dui.[/bubble]</span></p>\r\n<p><span style=\"line-height: 1.3em;\">[/col][col class=\"span4\"]</span></p>\r\n<p><span style=\"line-height: 1.3em;\">[bubble color=\"#FFF\" background=\"#736357\" author=\"Barbara J. Pennebaker\"]Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed ligula orci, ullamcorper vitae sodales venenatis, feugiat et felis. Donec non dui velit, a posuere dui.[/bubble]</span></p>\r\n<p><span style=\"line-height: 1.3em;\">[/col]</span><span style=\"line-height: 1.3em;\">[col class=\"span4\"]</span></p>\r\n<p><span style=\"line-height: 1.3em;\">[bubble background=\"transparent\" border=\"3px solid #ccc\" author=\"Chad M. Simmons\"]Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed ligula orci, ullamcorper vitae sodales venenatis, feugiat et felis. Donec non dui.[/bubble]</span></p>\r\n<p><span>[/col]</span></p>\r\n<p><span style=\"line-height: 1.3em;\">[/row]</span></p>\r\n<p><span style=\"line-height: 1.3em;\">[row][col class=\"span12\"]</span></p>\r\n<p> </p>\r\n<pre>[[bubble background=\"#FFF\" color=\"#666\" border=\"3px solid #ccc\" author=\"Chad M. Simmons\"]Lorem ipsum dolor sit amet.[/bubble]]</pre>\r\n<p><span style=\"line-height: 1.3em;\">[/col]</span><span style=\"line-height: 1.3em;\">[/row]</span></p>', '', 1, 10, '2018-06-26 11:45:53', 239, '', '2018-07-05 12:00:11', 239, 0, '0000-00-00 00:00:00', '2018-06-26 11:45:53', '0000-00-00 00:00:00', '{\"image_intro\":\"\",\"float_intro\":\"\",\"image_intro_alt\":\"\",\"image_intro_caption\":\"\",\"image_fulltext\":\"\",\"float_fulltext\":\"\",\"image_fulltext_alt\":\"\",\"image_fulltext_caption\":\"\"}', '{\"urla\":false,\"urlatext\":\"\",\"targeta\":\"\",\"urlb\":false,\"urlbtext\":\"\",\"targetb\":\"\",\"urlc\":false,\"urlctext\":\"\",\"targetc\":\"\"}', '{\"article_layout\":\"\",\"show_title\":\"\",\"link_titles\":\"\",\"show_tags\":\"\",\"show_intro\":\"\",\"info_block_position\":\"\",\"info_block_show_title\":\"\",\"show_category\":\"\",\"link_category\":\"\",\"show_parent_category\":\"\",\"link_parent_category\":\"\",\"show_associations\":\"\",\"show_author\":\"\",\"link_author\":\"\",\"show_create_date\":\"\",\"show_modify_date\":\"\",\"show_publish_date\":\"\",\"show_item_navigation\":\"\",\"show_icons\":\"\",\"show_print_icon\":\"\",\"show_email_icon\":\"\",\"show_vote\":\"\",\"show_hits\":\"\",\"show_noauth\":\"\",\"urls_position\":\"\",\"alternative_readmore\":\"\",\"article_page_title\":\"\",\"show_publishing_options\":\"\",\"show_article_options\":\"\",\"show_urls_images_backend\":\"\",\"show_urls_images_frontend\":\"\"}', 92, 2, '', '', 1, 518, '{\"robots\":\"\",\"author\":\"\",\"rights\":\"\",\"xreference\":\"\"}', 0, '*', ''),
(16, 54, 'Gallery', 'gallery', '<p>[gallery columns=\"4\" filter=\"yes\"]</p>\r\n<p>[gallery_item tag=\"joomla\" src=\"images/helix/gallery/300x200.png\" /]</p>\r\n<p>[gallery_item tag=\"wordpress\" src=\"images/helix/gallery/300x200.png\" /]</p>\r\n<p>[gallery_item tag=\"joomla, wordpress\" src=\"images/helix/gallery/300x200.png\" /]</p>\r\n<p>[gallery_item tag=\"magento\" src=\"images/helix/gallery/300x200.png\" /]</p>\r\n<p>[gallery_item tag=\"opencart\" src=\"images/helix/gallery/300x200.png\" /]</p>\r\n<p>[gallery_item tag=\"magento, opencart\" src=\"images/helix/gallery/300x200.png\" /]</p>\r\n<p>[gallery_item tag=\"joomla, magento, wordpress, opencart\" src=\"images/helix/gallery/300x200.png\" /]</p>\r\n<p>[gallery_item tag=\"drupal\" src=\"images/helix/gallery/300x200.png\" /]</p>\r\n<p>[/gallery]</p>\r\n<h3>Get the code</h3>\r\n<pre>[[gallery columns=\"4\" filter=\"yes\"]\r\n[gallery_item tag=\"joomla\" src=\"images/helix/gallery/300x200.png\"/]\r\n[gallery_item tag=\"wordpress\" src=\"images/helix/gallery/300x200.png\"/]\r\n[gallery_item tag=\"joomla, wordpress\" src=\"images/helix/gallery/300x200.png\"/]\r\n[gallery_item tag=\"magento\" src=\"images/helix/gallery/300x200.png\"/]\r\n[gallery_item tag=\"opencart\" src=\"images/helix/gallery/300x200.png\"/]\r\n[gallery_item tag=\"magento, opencart\" src=\"images/helix/gallery/300x200.png\"/]\r\n[gallery_item tag=\"joomla, magento, wordpress, opencart\" src=\"images/helix/gallery/300x200.png\"/]\r\n[gallery_item tag=\"drupal\" src=\"images/helix/gallery/300x200.png\"/]\r\n[/gallery]]\r\n</pre>', '', 0, 11, '2018-06-26 11:45:53', 239, '', '2018-06-26 11:45:53', 239, 0, '0000-00-00 00:00:00', '2018-06-26 11:45:53', '0000-00-00 00:00:00', '{\"image_intro\":\"\",\"float_intro\":\"\",\"image_intro_alt\":\"\",\"image_intro_caption\":\"\",\"image_fulltext\":\"\",\"float_fulltext\":\"\",\"image_fulltext_alt\":\"\",\"image_fulltext_caption\":\"\"}', '{\"urla\":null,\"urlatext\":\"\",\"targeta\":\"\",\"urlb\":null,\"urlbtext\":\"\",\"targetb\":\"\",\"urlc\":null,\"urlctext\":\"\",\"targetc\":\"\"}', '{\"show_title\":\"\",\"link_titles\":\"\",\"show_intro\":\"\",\"show_category\":\"\",\"link_category\":\"\",\"show_parent_category\":\"\",\"link_parent_category\":\"\",\"show_author\":\"\",\"link_author\":\"\",\"show_create_date\":\"\",\"show_modify_date\":\"\",\"show_publish_date\":\"\",\"show_item_navigation\":\"\",\"show_icons\":\"\",\"show_print_icon\":\"\",\"show_email_icon\":\"\",\"show_vote\":\"\",\"show_hits\":\"\",\"show_noauth\":\"\",\"urls_position\":\"\",\"alternative_readmore\":\"\",\"article_layout\":\"\",\"show_publishing_options\":\"\",\"show_article_options\":\"\",\"show_urls_images_backend\":\"\",\"show_urls_images_frontend\":\"\"}', 29, 0, '', '', 1, 109, '{\"robots\":\"\",\"author\":\"\",\"rights\":\"\",\"xreference\":\"\"}', 0, '*', ''),
(17, 55, 'Video', 'video', '<p>[row]</p>\r\n<p>[col class=\"span6\"]</p>\r\n<p>[spvideo]https://www.youtube.com/watch?v=vb2eObvmvdI[/spvideo]</p>\r\n<h3>Get the code</h3>\r\n<pre>[[spvideo]http://www.youtube.com/watch?v=vb2eObvmvdI[/spvideo]]</pre>\r\n<p>[/col]</p>\r\n<p>[col class=\"span6\"]</p>\r\n<p>[spvideo]http://vimeo.com/3701346[/spvideo]</p>\r\n<h3>Get the code</h3>\r\n<pre>[[spvideo]http://vimeo.com/3701346[/spvideo]]</pre>\r\n<p>[/col]</p>\r\n<p>[/row]</p>', '', 0, 10, '2018-06-26 11:45:53', 239, '', '2018-06-26 11:45:53', 239, 0, '0000-00-00 00:00:00', '2018-06-26 11:45:53', '0000-00-00 00:00:00', '{\"image_intro\":\"\",\"float_intro\":\"\",\"image_intro_alt\":\"\",\"image_intro_caption\":\"\",\"image_fulltext\":\"\",\"float_fulltext\":\"\",\"image_fulltext_alt\":\"\",\"image_fulltext_caption\":\"\"}', '{\"urla\":null,\"urlatext\":\"\",\"targeta\":\"\",\"urlb\":null,\"urlbtext\":\"\",\"targetb\":\"\",\"urlc\":null,\"urlctext\":\"\",\"targetc\":\"\"}', '{\"show_title\":\"\",\"link_titles\":\"\",\"show_intro\":\"\",\"show_category\":\"\",\"link_category\":\"\",\"show_parent_category\":\"\",\"link_parent_category\":\"\",\"show_author\":\"\",\"link_author\":\"\",\"show_create_date\":\"\",\"show_modify_date\":\"\",\"show_publish_date\":\"\",\"show_item_navigation\":\"\",\"show_icons\":\"\",\"show_print_icon\":\"\",\"show_email_icon\":\"\",\"show_vote\":\"\",\"show_hits\":\"\",\"show_noauth\":\"\",\"urls_position\":\"\",\"alternative_readmore\":\"\",\"article_layout\":\"\",\"show_publishing_options\":\"\",\"show_article_options\":\"\",\"show_urls_images_backend\":\"\",\"show_urls_images_frontend\":\"\"}', 34, 1, '', '', 1, 107, '{\"robots\":\"\",\"author\":\"\",\"rights\":\"\",\"xreference\":\"\"}', 0, '*', ''),
(18, 56, 'Module Position', 'module-position', '<table class=\"table table-bordered table-striped center\">\r\n<thead>\r\n<tr><th colspan=\"6\">\r\n<h4>Header</h4>\r\n</th></tr>\r\n</thead>\r\n<tbody>\r\n<tr>\r\n<td>logo</td>\r\n<td>menu</td>\r\n<td>search</td>\r\n</tr>\r\n</tbody>\r\n</table>\r\n<table class=\"table table-bordered table-striped center\">\r\n<thead>\r\n<tr><th colspan=\"6\">\r\n<h4>feature</h4>\r\n</th></tr>\r\n</thead>\r\n</table>\r\n<table class=\"table table-bordered table-striped center\">\r\n<thead>\r\n<tr><th colspan=\"6\">\r\n<h4>Users</h4>\r\n</th></tr>\r\n</thead>\r\n<tbody>\r\n<tr>\r\n<td>user1</td>\r\n<td>user2</td>\r\n<td>user3</td>\r\n<td>user4</td>\r\n</tr>\r\n</tbody>\r\n</table>\r\n<table class=\"table table-bordered table-striped center\">\r\n<thead>\r\n<tr><th colspan=\"6\">\r\n<h4>Main Body</h4>\r\n</th></tr>\r\n</thead>\r\n<tbody>\r\n<tr>\r\n<td>left</td>\r\n<td>component</td>\r\n<td>right</td>\r\n</tr>\r\n</tbody>\r\n</table>\r\n<table class=\"table table-bordered table-striped center\">\r\n<thead>\r\n<tr><th colspan=\"6\">\r\n<h4>breadcrumb</h4>\r\n</th></tr>\r\n</thead>\r\n</table>\r\n<table class=\"table table-bordered table-striped center\">\r\n<thead>\r\n<tr><th colspan=\"6\">\r\n<h4>Bottom</h4>\r\n</th></tr>\r\n</thead>\r\n<tbody>\r\n<tr>\r\n<td>bottom1</td>\r\n<td>bottom2</td>\r\n<td>bottom3</td>\r\n<td>bottom4</td>\r\n</tr>\r\n</tbody>\r\n</table>\r\n<table class=\"table table-bordered table-striped center\">\r\n<thead>\r\n<tr><th colspan=\"6\">\r\n<h4>Footer</h4>\r\n</th></tr>\r\n</thead>\r\n<tbody>\r\n<tr>\r\n<td>footer2</td>\r\n<td>footer1</td>\r\n</tr>\r\n</tbody>\r\n</table>', '', 0, 10, '2018-06-26 11:45:53', 239, '', '2018-06-26 11:45:53', 239, 0, '0000-00-00 00:00:00', '2018-06-26 11:45:53', '0000-00-00 00:00:00', '{\"image_intro\":\"\",\"float_intro\":\"\",\"image_intro_alt\":\"\",\"image_intro_caption\":\"\",\"image_fulltext\":\"\",\"float_fulltext\":\"\",\"image_fulltext_alt\":\"\",\"image_fulltext_caption\":\"\"}', '{\"urla\":null,\"urlatext\":\"\",\"targeta\":\"\",\"urlb\":null,\"urlbtext\":\"\",\"targetb\":\"\",\"urlc\":null,\"urlctext\":\"\",\"targetc\":\"\"}', '{\"show_title\":\"\",\"link_titles\":\"\",\"show_intro\":\"\",\"show_category\":\"\",\"link_category\":\"\",\"show_parent_category\":\"\",\"link_parent_category\":\"\",\"show_author\":\"\",\"link_author\":\"\",\"show_create_date\":\"\",\"show_modify_date\":\"\",\"show_publish_date\":\"\",\"show_item_navigation\":\"\",\"show_icons\":\"\",\"show_print_icon\":\"\",\"show_email_icon\":\"\",\"show_vote\":\"\",\"show_hits\":\"\",\"show_noauth\":\"\",\"urls_position\":\"\",\"alternative_readmore\":\"\",\"article_layout\":\"\",\"show_publishing_options\":\"\",\"show_article_options\":\"\",\"show_urls_images_backend\":\"\",\"show_urls_images_frontend\":\"\"}', 22, 0, '', '', 1, 73, '{\"robots\":\"\",\"author\":\"\",\"rights\":\"\",\"xreference\":\"\"}', 0, '*', ''),
(19, 57, 'The Joomla Blog', 'the-joomla-blog', '<p>Joomla! means All Together, and it is a community of people all working and having fun together that makes Joomla! possible. Thousands of people each year participate in the Joomla! community, and we hope you will be one of them.</p>\r\n<p>People with all kinds of skills, of all skill levels and from around the world are welcome to join in. Participate in the <a href=\"http://joomla.org\">Joomla.org</a> family of websites (the<a href=\"http://forum.joomla.org\"> forum </a>is a great place to start). Come to a <a href=\"http://community.joomla.org/events.html\">Joomla! event</a>. Join or start a <a href=\"http://community.joomla.org/user-groups.html\">Joomla! Users Group</a>. Whether you are a developer, site administrator, designer, end user or fan, there are ways for you to participate and contribute.</p>', '', 0, 8, '2018-06-26 11:45:53', 239, '', '2018-06-26 11:45:53', 239, 0, '0000-00-00 00:00:00', '2018-06-26 11:45:53', '0000-00-00 00:00:00', '{\"image_intro\":\"\",\"float_intro\":\"\",\"image_intro_alt\":\"\",\"image_intro_caption\":\"\",\"image_fulltext\":\"\",\"float_fulltext\":\"\",\"image_fulltext_alt\":\"\",\"image_fulltext_caption\":\"\"}', '{\"urla\":null,\"urlatext\":\"\",\"targeta\":\"\",\"urlb\":null,\"urlbtext\":\"\",\"targetb\":\"\",\"urlc\":null,\"urlctext\":\"\",\"targetc\":\"\"}', '{\"show_title\":\"\",\"link_titles\":\"\",\"show_intro\":\"\",\"show_category\":\"\",\"link_category\":\"\",\"show_parent_category\":\"\",\"link_parent_category\":\"\",\"show_author\":\"\",\"link_author\":\"\",\"show_create_date\":\"\",\"show_modify_date\":\"\",\"show_publish_date\":\"\",\"show_item_navigation\":\"\",\"show_icons\":\"\",\"show_print_icon\":\"\",\"show_email_icon\":\"\",\"show_vote\":\"\",\"show_hits\":\"\",\"show_noauth\":\"\",\"urls_position\":\"\",\"alternative_readmore\":\"\",\"article_layout\":\"\",\"show_publishing_options\":\"\",\"show_article_options\":\"\",\"show_urls_images_backend\":\"\",\"show_urls_images_frontend\":\"\"}', 2, 2, '', '', 1, 2, '{\"robots\":\"\",\"author\":\"\",\"rights\":\"\",\"xreference\":\"\"}', 0, '*', ''),
(20, 58, 'The Joomla overview', 'the-joomla-overview', '<p>Joomla! means All Together, and it is a community of people all working and having fun together that makes Joomla! possible. Thousands of people each year participate in the Joomla! community, and we hope you will be one of them.</p>\r\n<p>People with all kinds of skills, of all skill levels and from around the world are welcome to join in. Participate in the <a href=\"http://joomla.org\">Joomla.org</a> family of websites (the<a href=\"http://forum.joomla.org\"> forum </a>is a great place to start). Come to a <a href=\"http://community.joomla.org/events.html\">Joomla! event</a>. Join or start a <a href=\"http://community.joomla.org/user-groups.html\">Joomla! Users Group</a>. Whether you are a developer, site administrator, designer, end user or fan, there are ways for you to participate and contribute.</p>', '', 0, 8, '2018-06-26 11:45:53', 239, '', '2018-06-26 11:45:53', 239, 0, '0000-00-00 00:00:00', '2018-06-26 11:45:53', '0000-00-00 00:00:00', '{\"image_intro\":\"\",\"float_intro\":\"\",\"image_intro_alt\":\"\",\"image_intro_caption\":\"\",\"image_fulltext\":\"\",\"float_fulltext\":\"\",\"image_fulltext_alt\":\"\",\"image_fulltext_caption\":\"\"}', '{\"urla\":null,\"urlatext\":\"\",\"targeta\":\"\",\"urlb\":null,\"urlbtext\":\"\",\"targetb\":\"\",\"urlc\":null,\"urlctext\":\"\",\"targetc\":\"\"}', '{\"show_title\":\"\",\"link_titles\":\"\",\"show_intro\":\"\",\"show_category\":\"\",\"link_category\":\"\",\"show_parent_category\":\"\",\"link_parent_category\":\"\",\"show_author\":\"\",\"link_author\":\"\",\"show_create_date\":\"\",\"show_modify_date\":\"\",\"show_publish_date\":\"\",\"show_item_navigation\":\"\",\"show_icons\":\"\",\"show_print_icon\":\"\",\"show_email_icon\":\"\",\"show_vote\":\"\",\"show_hits\":\"\",\"show_noauth\":\"\",\"urls_position\":\"\",\"alternative_readmore\":\"\",\"article_layout\":\"\",\"show_publishing_options\":\"\",\"show_article_options\":\"\",\"show_urls_images_backend\":\"\",\"show_urls_images_frontend\":\"\"}', 2, 1, '', '', 1, 0, '{\"robots\":\"\",\"author\":\"\",\"rights\":\"\",\"xreference\":\"\"}', 0, '*', ''),
(21, 59, 'The Joomla Help', 'the-joomla-help', '<p>Joomla! means All Together, and it is a community of people all working and having fun together that makes Joomla! possible. Thousands of people each year participate in the Joomla! community, and we hope you will be one of them.</p>\r\n<p>People with all kinds of skills, of all skill levels and from around the world are welcome to join in. Participate in the <a href=\"http://joomla.org\">Joomla.org</a> family of websites (the<a href=\"http://forum.joomla.org\"> forum </a>is a great place to start). Come to a <a href=\"http://community.joomla.org/events.html\">Joomla! event</a>. Join or start a <a href=\"http://community.joomla.org/user-groups.html\">Joomla! Users Group</a>. Whether you are a developer, site administrator, designer, end user or fan, there are ways for you to participate and contribute.</p>', '', 0, 8, '2018-06-26 11:45:53', 239, '', '2018-06-26 11:45:53', 239, 0, '0000-00-00 00:00:00', '2018-06-26 11:45:53', '0000-00-00 00:00:00', '{\"image_intro\":\"\",\"float_intro\":\"\",\"image_intro_alt\":\"\",\"image_intro_caption\":\"\",\"image_fulltext\":\"\",\"float_fulltext\":\"\",\"image_fulltext_alt\":\"\",\"image_fulltext_caption\":\"\"}', '{\"urla\":null,\"urlatext\":\"\",\"targeta\":\"\",\"urlb\":null,\"urlbtext\":\"\",\"targetb\":\"\",\"urlc\":null,\"urlctext\":\"\",\"targetc\":\"\"}', '{\"show_title\":\"\",\"link_titles\":\"\",\"show_intro\":\"\",\"show_category\":\"\",\"link_category\":\"\",\"show_parent_category\":\"\",\"link_parent_category\":\"\",\"show_author\":\"\",\"link_author\":\"\",\"show_create_date\":\"\",\"show_modify_date\":\"\",\"show_publish_date\":\"\",\"show_item_navigation\":\"\",\"show_icons\":\"\",\"show_print_icon\":\"\",\"show_email_icon\":\"\",\"show_vote\":\"\",\"show_hits\":\"\",\"show_noauth\":\"\",\"urls_position\":\"\",\"alternative_readmore\":\"\",\"article_layout\":\"\",\"show_publishing_options\":\"\",\"show_article_options\":\"\",\"show_urls_images_backend\":\"\",\"show_urls_images_frontend\":\"\"}', 2, 0, '', '', 1, 0, '{\"robots\":\"\",\"author\":\"\",\"rights\":\"\",\"xreference\":\"\"}', 0, '*', '');
INSERT INTO `t41uf_content` (`id`, `asset_id`, `title`, `alias`, `introtext`, `fulltext`, `state`, `catid`, `created`, `created_by`, `created_by_alias`, `modified`, `modified_by`, `checked_out`, `checked_out_time`, `publish_up`, `publish_down`, `images`, `urls`, `attribs`, `version`, `ordering`, `metakey`, `metadesc`, `access`, `hits`, `metadata`, `featured`, `language`, `xreference`) VALUES
(22, 64, 'Lorem ipsum dolor sit amet', 'lorem-ipsum-dolor-sit-amet', '<p><span>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut aliquip ex ea commodo consequat.</span></p>', '', 1, 13, '2018-06-26 11:45:53', 239, '', '0000-00-00 00:00:00', 0, 0, '0000-00-00 00:00:00', '2018-06-26 11:45:53', '0000-00-00 00:00:00', '{\"image_intro\":\"images\\/xeon\\/portfolio\\/item1.jpg\",\"float_intro\":\"\",\"image_intro_alt\":\"\",\"image_intro_caption\":\"\",\"image_fulltext\":\"images\\/xeon\\/portfolio\\/item1.jpg\",\"float_fulltext\":\"\",\"image_fulltext_alt\":\"\",\"image_fulltext_caption\":\"\"}', '{\"urla\":false,\"urlatext\":\"\",\"targeta\":\"\",\"urlb\":false,\"urlbtext\":\"\",\"targetb\":\"\",\"urlc\":false,\"urlctext\":\"\",\"targetc\":\"\"}', '{\"show_title\":\"\",\"link_titles\":\"\",\"show_intro\":\"\",\"show_category\":\"\",\"link_category\":\"\",\"show_parent_category\":\"\",\"link_parent_category\":\"\",\"show_author\":\"\",\"link_author\":\"\",\"show_create_date\":\"\",\"show_modify_date\":\"\",\"show_publish_date\":\"\",\"show_item_navigation\":\"\",\"show_icons\":\"\",\"show_print_icon\":\"\",\"show_email_icon\":\"\",\"show_vote\":\"\",\"show_hits\":\"\",\"show_noauth\":\"\",\"urls_position\":\"\",\"alternative_readmore\":\"\",\"article_layout\":\"\",\"show_publishing_options\":\"\",\"show_article_options\":\"\",\"show_urls_images_backend\":\"\",\"show_urls_images_frontend\":\"\"}', 1, 2, '', '', 1, 1, '{\"robots\":\"\",\"author\":\"\",\"rights\":\"\",\"xreference\":\"\"}', 0, '*', ''),
(23, 65, 'Lorem ipsum dolor sit amet', 'lorem-ipsum-dolor-sit-amet2', '<p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut aliquip ex ea commodo consequat.</p>', '', 1, 13, '2018-06-26 11:45:53', 239, '', '0000-00-00 00:00:00', 0, 0, '0000-00-00 00:00:00', '2018-06-26 11:45:53', '0000-00-00 00:00:00', '{\"image_intro\":\"images\\/xeon\\/portfolio\\/item2.jpg\",\"float_intro\":\"\",\"image_intro_alt\":\"\",\"image_intro_caption\":\"\",\"image_fulltext\":\"images\\/xeon\\/portfolio\\/item2.jpg\",\"float_fulltext\":\"\",\"image_fulltext_alt\":\"\",\"image_fulltext_caption\":\"\"}', '{\"urla\":false,\"urlatext\":\"\",\"targeta\":\"\",\"urlb\":false,\"urlbtext\":\"\",\"targetb\":\"\",\"urlc\":false,\"urlctext\":\"\",\"targetc\":\"\"}', '{\"show_title\":\"\",\"link_titles\":\"\",\"show_intro\":\"\",\"show_category\":\"\",\"link_category\":\"\",\"show_parent_category\":\"\",\"link_parent_category\":\"\",\"show_author\":\"\",\"link_author\":\"\",\"show_create_date\":\"\",\"show_modify_date\":\"\",\"show_publish_date\":\"\",\"show_item_navigation\":\"\",\"show_icons\":\"\",\"show_print_icon\":\"\",\"show_email_icon\":\"\",\"show_vote\":\"\",\"show_hits\":\"\",\"show_noauth\":\"\",\"urls_position\":\"\",\"alternative_readmore\":\"\",\"article_layout\":\"\",\"show_publishing_options\":\"\",\"show_article_options\":\"\",\"show_urls_images_backend\":\"\",\"show_urls_images_frontend\":\"\"}', 1, 1, '', '', 1, 3, '{\"robots\":\"\",\"author\":\"\",\"rights\":\"\",\"xreference\":\"\"}', 0, '*', ''),
(24, 66, 'Lorem ipsum dolor sit amet', 'lorem-ipsum-dolor-sit-amet3', '<p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut aliquip ex ea commodo consequat.</p>', '', 1, 14, '2018-06-26 11:45:53', 239, '', '0000-00-00 00:00:00', 0, 0, '0000-00-00 00:00:00', '2018-06-26 11:45:53', '0000-00-00 00:00:00', '{\"image_intro\":\"images\\/xeon\\/portfolio\\/item3.jpg\",\"float_intro\":\"\",\"image_intro_alt\":\"\",\"image_intro_caption\":\"\",\"image_fulltext\":\"images\\/xeon\\/portfolio\\/item3.jpg\",\"float_fulltext\":\"\",\"image_fulltext_alt\":\"\",\"image_fulltext_caption\":\"\"}', '{\"urla\":false,\"urlatext\":\"\",\"targeta\":\"\",\"urlb\":false,\"urlbtext\":\"\",\"targetb\":\"\",\"urlc\":false,\"urlctext\":\"\",\"targetc\":\"\"}', '{\"show_title\":\"\",\"link_titles\":\"\",\"show_intro\":\"\",\"show_category\":\"\",\"link_category\":\"\",\"show_parent_category\":\"\",\"link_parent_category\":\"\",\"show_author\":\"\",\"link_author\":\"\",\"show_create_date\":\"\",\"show_modify_date\":\"\",\"show_publish_date\":\"\",\"show_item_navigation\":\"\",\"show_icons\":\"\",\"show_print_icon\":\"\",\"show_email_icon\":\"\",\"show_vote\":\"\",\"show_hits\":\"\",\"show_noauth\":\"\",\"urls_position\":\"\",\"alternative_readmore\":\"\",\"article_layout\":\"\",\"show_publishing_options\":\"\",\"show_article_options\":\"\",\"show_urls_images_backend\":\"\",\"show_urls_images_frontend\":\"\"}', 1, 2, '', '', 1, 1, '{\"robots\":\"\",\"author\":\"\",\"rights\":\"\",\"xreference\":\"\"}', 0, '*', ''),
(25, 67, 'Lorem ipsum dolor sit amet', 'lorem-ipsum-dolor-sit-amet4', '<p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut aliquip ex ea commodo consequat.</p>', '', 1, 14, '2018-06-26 11:45:53', 239, '', '0000-00-00 00:00:00', 0, 0, '0000-00-00 00:00:00', '2018-06-26 11:45:53', '0000-00-00 00:00:00', '{\"image_intro\":\"images\\/xeon\\/portfolio\\/item4.jpg\",\"float_intro\":\"\",\"image_intro_alt\":\"\",\"image_intro_caption\":\"\",\"image_fulltext\":\"images\\/xeon\\/portfolio\\/item4.jpg\",\"float_fulltext\":\"\",\"image_fulltext_alt\":\"\",\"image_fulltext_caption\":\"\"}', '{\"urla\":false,\"urlatext\":\"\",\"targeta\":\"\",\"urlb\":false,\"urlbtext\":\"\",\"targetb\":\"\",\"urlc\":false,\"urlctext\":\"\",\"targetc\":\"\"}', '{\"show_title\":\"\",\"link_titles\":\"\",\"show_intro\":\"\",\"show_category\":\"\",\"link_category\":\"\",\"show_parent_category\":\"\",\"link_parent_category\":\"\",\"show_author\":\"\",\"link_author\":\"\",\"show_create_date\":\"\",\"show_modify_date\":\"\",\"show_publish_date\":\"\",\"show_item_navigation\":\"\",\"show_icons\":\"\",\"show_print_icon\":\"\",\"show_email_icon\":\"\",\"show_vote\":\"\",\"show_hits\":\"\",\"show_noauth\":\"\",\"urls_position\":\"\",\"alternative_readmore\":\"\",\"article_layout\":\"\",\"show_publishing_options\":\"\",\"show_article_options\":\"\",\"show_urls_images_backend\":\"\",\"show_urls_images_frontend\":\"\"}', 1, 1, '', '', 1, 6, '{\"robots\":\"\",\"author\":\"\",\"rights\":\"\",\"xreference\":\"\"}', 0, '*', ''),
(26, 68, 'Lorem ipsum dolor sit amet', 'lorem-ipsum-dolor-sit-amet5', '<p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut aliquip ex ea commodo consequat.</p>', '', 1, 15, '2018-06-26 11:45:53', 239, '', '0000-00-00 00:00:00', 0, 0, '0000-00-00 00:00:00', '2018-06-26 11:45:53', '0000-00-00 00:00:00', '{\"image_intro\":\"images\\/xeon\\/portfolio\\/item5.jpg\",\"float_intro\":\"\",\"image_intro_alt\":\"\",\"image_intro_caption\":\"\",\"image_fulltext\":\"images\\/xeon\\/portfolio\\/item5.jpg\",\"float_fulltext\":\"\",\"image_fulltext_alt\":\"\",\"image_fulltext_caption\":\"\"}', '{\"urla\":false,\"urlatext\":\"\",\"targeta\":\"\",\"urlb\":false,\"urlbtext\":\"\",\"targetb\":\"\",\"urlc\":false,\"urlctext\":\"\",\"targetc\":\"\"}', '{\"show_title\":\"\",\"link_titles\":\"\",\"show_intro\":\"\",\"show_category\":\"\",\"link_category\":\"\",\"show_parent_category\":\"\",\"link_parent_category\":\"\",\"show_author\":\"\",\"link_author\":\"\",\"show_create_date\":\"\",\"show_modify_date\":\"\",\"show_publish_date\":\"\",\"show_item_navigation\":\"\",\"show_icons\":\"\",\"show_print_icon\":\"\",\"show_email_icon\":\"\",\"show_vote\":\"\",\"show_hits\":\"\",\"show_noauth\":\"\",\"urls_position\":\"\",\"alternative_readmore\":\"\",\"article_layout\":\"\",\"show_publishing_options\":\"\",\"show_article_options\":\"\",\"show_urls_images_backend\":\"\",\"show_urls_images_frontend\":\"\"}', 1, 1, '', '', 1, 1, '{\"robots\":\"\",\"author\":\"\",\"rights\":\"\",\"xreference\":\"\"}', 0, '*', ''),
(27, 69, 'Me hrashali project', 'lorem-ipsum-dolor-sit-amet6', '<p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut aliquip ex ea commodo consequat.</p>', '', 1, 15, '2018-06-26 11:45:53', 239, '', '2018-06-26 12:33:07', 239, 0, '0000-00-00 00:00:00', '2018-06-26 11:45:53', '0000-00-00 00:00:00', '{\"image_intro\":\"images\\/xeon\\/portfolio\\/item6.jpg\",\"float_intro\":\"\",\"image_intro_alt\":\"\",\"image_intro_caption\":\"\",\"image_fulltext\":\"images\\/xeon\\/portfolio\\/item6.jpg\",\"float_fulltext\":\"\",\"image_fulltext_alt\":\"\",\"image_fulltext_caption\":\"\"}', '{\"urla\":false,\"urlatext\":\"\",\"targeta\":\"\",\"urlb\":false,\"urlbtext\":\"\",\"targetb\":\"\",\"urlc\":false,\"urlctext\":\"\",\"targetc\":\"\"}', '{\"article_layout\":\"\",\"show_title\":\"\",\"link_titles\":\"\",\"show_tags\":\"\",\"show_intro\":\"\",\"info_block_position\":\"\",\"info_block_show_title\":\"\",\"show_category\":\"\",\"link_category\":\"\",\"show_parent_category\":\"\",\"link_parent_category\":\"\",\"show_associations\":\"\",\"show_author\":\"\",\"link_author\":\"\",\"show_create_date\":\"\",\"show_modify_date\":\"\",\"show_publish_date\":\"\",\"show_item_navigation\":\"\",\"show_icons\":\"\",\"show_print_icon\":\"\",\"show_email_icon\":\"\",\"show_vote\":\"\",\"show_hits\":\"\",\"show_noauth\":\"\",\"urls_position\":\"\",\"alternative_readmore\":\"\",\"article_page_title\":\"\",\"show_publishing_options\":\"\",\"show_article_options\":\"\",\"show_urls_images_backend\":\"\",\"show_urls_images_frontend\":\"\"}', 3, 0, '', '', 1, 5, '{\"robots\":\"\",\"author\":\"\",\"rights\":\"\",\"xreference\":\"\"}', 0, '*', ''),
(28, 70, 'Lorem ipsum dolor sit amet', 'lorem-ipsum-dolor-sit-amet7', '<p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut aliquip ex ea commodo consequat.</p>', '', 1, 13, '2018-06-26 11:45:53', 239, '', '2018-06-26 11:45:53', 239, 0, '0000-00-00 00:00:00', '2018-06-26 11:45:53', '0000-00-00 00:00:00', '{\"image_intro\":\"images\\/xeon\\/portfolio\\/item3.jpg\",\"float_intro\":\"\",\"image_intro_alt\":\"\",\"image_intro_caption\":\"\",\"image_fulltext\":\"images\\/xeon\\/portfolio\\/item3.jpg\",\"float_fulltext\":\"\",\"image_fulltext_alt\":\"\",\"image_fulltext_caption\":\"\"}', '{\"urla\":false,\"urlatext\":\"\",\"targeta\":\"\",\"urlb\":false,\"urlbtext\":\"\",\"targetb\":\"\",\"urlc\":false,\"urlctext\":\"\",\"targetc\":\"\"}', '{\"show_title\":\"\",\"link_titles\":\"\",\"show_intro\":\"\",\"show_category\":\"\",\"link_category\":\"\",\"show_parent_category\":\"\",\"link_parent_category\":\"\",\"show_author\":\"\",\"link_author\":\"\",\"show_create_date\":\"\",\"show_modify_date\":\"\",\"show_publish_date\":\"\",\"show_item_navigation\":\"\",\"show_icons\":\"\",\"show_print_icon\":\"\",\"show_email_icon\":\"\",\"show_vote\":\"\",\"show_hits\":\"\",\"show_noauth\":\"\",\"urls_position\":\"\",\"alternative_readmore\":\"\",\"article_layout\":\"\",\"show_publishing_options\":\"\",\"show_article_options\":\"\",\"show_urls_images_backend\":\"\",\"show_urls_images_frontend\":\"\"}', 2, 0, '', '', 1, 12, '{\"robots\":\"\",\"author\":\"\",\"rights\":\"\",\"xreference\":\"\"}', 0, '*', ''),
(29, 71, 'Lorem ipsum dolor sit amet', 'lorem-ipsum-dolor-sit-amet8', '<p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut aliquip ex ea commodo consequat.</p>', '', 1, 14, '2018-06-26 11:45:53', 239, '', '2018-06-26 11:45:53', 239, 0, '0000-00-00 00:00:00', '2018-06-26 11:45:53', '0000-00-00 00:00:00', '{\"image_intro\":\"images\\/xeon\\/portfolio\\/item6.jpg\",\"float_intro\":\"\",\"image_intro_alt\":\"\",\"image_intro_caption\":\"\",\"image_fulltext\":\"images\\/xeon\\/portfolio\\/item6.jpg\",\"float_fulltext\":\"\",\"image_fulltext_alt\":\"\",\"image_fulltext_caption\":\"\"}', '{\"urla\":false,\"urlatext\":\"\",\"targeta\":\"\",\"urlb\":false,\"urlbtext\":\"\",\"targetb\":\"\",\"urlc\":false,\"urlctext\":\"\",\"targetc\":\"\"}', '{\"show_title\":\"\",\"link_titles\":\"\",\"show_intro\":\"\",\"show_category\":\"\",\"link_category\":\"\",\"show_parent_category\":\"\",\"link_parent_category\":\"\",\"show_author\":\"\",\"link_author\":\"\",\"show_create_date\":\"\",\"show_modify_date\":\"\",\"show_publish_date\":\"\",\"show_item_navigation\":\"\",\"show_icons\":\"\",\"show_print_icon\":\"\",\"show_email_icon\":\"\",\"show_vote\":\"\",\"show_hits\":\"\",\"show_noauth\":\"\",\"urls_position\":\"\",\"alternative_readmore\":\"\",\"article_layout\":\"\",\"show_publishing_options\":\"\",\"show_article_options\":\"\",\"show_urls_images_backend\":\"\",\"show_urls_images_frontend\":\"\"}', 2, 0, '', '', 1, 2, '{\"robots\":\"\",\"author\":\"\",\"rights\":\"\",\"xreference\":\"\"}', 0, '*', ''),
(30, 109, 'test Gallery', 'test-gallery', '[gallery columns=\"2\" filter=\"no\"]\r\n[gallery_item tag=\"joomla\" src=\"/images/helix/gallery/300x200.png\"/]\r\n[gallery_item tag=\"wordpress\" src=\"/images/helix/gallery/300x200.png\"/]\r\n\r\n[/gallery]', '', -2, 2, '2018-07-04 19:06:34', 239, '', '2018-07-04 19:24:06', 239, 0, '0000-00-00 00:00:00', '2018-07-04 19:06:34', '0000-00-00 00:00:00', '{\"image_intro\":\"\",\"float_intro\":\"\",\"image_intro_alt\":\"\",\"image_intro_caption\":\"\",\"image_fulltext\":\"\",\"float_fulltext\":\"\",\"image_fulltext_alt\":\"\",\"image_fulltext_caption\":\"\"}', '{\"urla\":false,\"urlatext\":\"\",\"targeta\":\"\",\"urlb\":false,\"urlbtext\":\"\",\"targetb\":\"\",\"urlc\":false,\"urlctext\":\"\",\"targetc\":\"\"}', '{\"article_layout\":\"\",\"show_title\":\"\",\"link_titles\":\"\",\"show_tags\":\"\",\"show_intro\":\"\",\"info_block_position\":\"\",\"info_block_show_title\":\"\",\"show_category\":\"\",\"link_category\":\"\",\"show_parent_category\":\"\",\"link_parent_category\":\"\",\"show_associations\":\"\",\"show_author\":\"\",\"link_author\":\"\",\"show_create_date\":\"\",\"show_modify_date\":\"\",\"show_publish_date\":\"\",\"show_item_navigation\":\"\",\"show_icons\":\"\",\"show_print_icon\":\"\",\"show_email_icon\":\"\",\"show_vote\":\"\",\"show_hits\":\"\",\"show_noauth\":\"\",\"urls_position\":\"\",\"alternative_readmore\":\"\",\"article_page_title\":\"\",\"show_publishing_options\":\"\",\"show_article_options\":\"\",\"show_urls_images_backend\":\"\",\"show_urls_images_frontend\":\"\"}', 3, 0, '', '', 1, 3, '{\"robots\":\"\",\"author\":\"\",\"rights\":\"\",\"xreference\":\"\"}', 0, '*', ''),
(31, 110, 'Architectural Design', 'architect', '[row id=\"ROW_ID\" class=\"show-grid\"]\r\n[col class=\"span4\"]Architectura\r\n\r\nPellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Vestibulum tortor quam, feugiat vitae.\r\n\r\nInterior design\r\n\r\nPellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Vestibulum tortor quam, feugiat vitae.\r\n\r\nVideo Animation[/col]\r\n[col class=\"span4\"]Architectura\r\n\r\nPellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Vestibulum tortor quam, feugiat vitae.\r\n\r\nInterior design\r\n\r\nPellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Vestibulum tortor quam, feugiat vitae.\r\n\r\nVideo Animation[/col]\r\n[col class=\"span4\"]Architectura\r\n\r\nPellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Vestibulum tortor quam, feugiat vitae.\r\n\r\nInterior design\r\n\r\nPellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Vestibulum tortor quam, feugiat vitae.\r\n\r\nVideo Animation[/col]\r\n[/row]\r\n\r\n\r\n{os-gal-1}\r\n\r\n\r\nPellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Vestibulum tortor quam, feugiat vitae.\r\n\r\n', '', 1, 16, '2018-07-04 19:43:21', 239, '', '2018-07-05 15:07:12', 239, 239, '2018-07-05 15:07:12', '2018-07-04 19:43:21', '0000-00-00 00:00:00', '{\"image_intro\":\"images\\/xeon\\/portfolio\\/item1.jpg\",\"float_intro\":\"\",\"image_intro_alt\":\"\",\"image_intro_caption\":\"\",\"image_fulltext\":\"\",\"float_fulltext\":\"\",\"image_fulltext_alt\":\"\",\"image_fulltext_caption\":\"\"}', '{\"urla\":false,\"urlatext\":\"\",\"targeta\":\"\",\"urlb\":false,\"urlbtext\":\"\",\"targetb\":\"\",\"urlc\":false,\"urlctext\":\"\",\"targetc\":\"\"}', '{\"article_layout\":\"\",\"show_title\":\"\",\"link_titles\":\"\",\"show_tags\":\"\",\"show_intro\":\"\",\"info_block_position\":\"\",\"info_block_show_title\":\"\",\"show_category\":\"\",\"link_category\":\"\",\"show_parent_category\":\"\",\"link_parent_category\":\"\",\"show_associations\":\"\",\"show_author\":\"\",\"link_author\":\"\",\"show_create_date\":\"\",\"show_modify_date\":\"\",\"show_publish_date\":\"\",\"show_item_navigation\":\"\",\"show_icons\":\"\",\"show_print_icon\":\"\",\"show_email_icon\":\"\",\"show_vote\":\"\",\"show_hits\":\"\",\"show_noauth\":\"\",\"urls_position\":\"\",\"alternative_readmore\":\"\",\"article_page_title\":\"\",\"show_publishing_options\":\"\",\"show_article_options\":\"\",\"show_urls_images_backend\":\"\",\"show_urls_images_frontend\":\"\"}', 12, 5, '', '', 1, 52, '{\"robots\":\"\",\"author\":\"\",\"rights\":\"\",\"xreference\":\"\"}', 0, '*', ''),
(32, 112, '3D modeling', '3d', '3D\r\n\r\nPellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Vestibulum tortor quam, feugiat vitae.\r\n\r\nInterior design\r\n\r\nPellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Vestibulum tortor quam, feugiat vitae.\r\n\r\nVideo Animation\r\n\r\nPellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Vestibulum tortor quam, feugiat vitae.\r\n', '', 1, 16, '2018-07-04 19:43:21', 239, '', '2018-07-05 13:01:06', 239, 0, '0000-00-00 00:00:00', '2018-07-04 19:43:21', '0000-00-00 00:00:00', '{\"image_intro\":\"images\\/xeon\\/portfolio\\/item2.jpg\",\"float_intro\":\"\",\"image_intro_alt\":\"\",\"image_intro_caption\":\"\",\"image_fulltext\":\"\",\"float_fulltext\":\"\",\"image_fulltext_alt\":\"\",\"image_fulltext_caption\":\"\"}', '{\"urla\":false,\"urlatext\":\"\",\"targeta\":\"\",\"urlb\":false,\"urlbtext\":\"\",\"targetb\":\"\",\"urlc\":false,\"urlctext\":\"\",\"targetc\":\"\"}', '{\"article_layout\":\"\",\"show_title\":\"\",\"link_titles\":\"\",\"show_tags\":\"\",\"show_intro\":\"\",\"info_block_position\":\"\",\"info_block_show_title\":\"\",\"show_category\":\"\",\"link_category\":\"\",\"show_parent_category\":\"\",\"link_parent_category\":\"\",\"show_associations\":\"\",\"show_author\":\"\",\"link_author\":\"\",\"show_create_date\":\"\",\"show_modify_date\":\"\",\"show_publish_date\":\"\",\"show_item_navigation\":\"\",\"show_icons\":\"\",\"show_print_icon\":\"\",\"show_email_icon\":\"\",\"show_vote\":\"\",\"show_hits\":\"\",\"show_noauth\":\"\",\"urls_position\":\"\",\"alternative_readmore\":\"\",\"article_page_title\":\"\",\"show_publishing_options\":\"\",\"show_article_options\":\"\",\"show_urls_images_backend\":\"\",\"show_urls_images_frontend\":\"\"}', 4, 4, '', '', 1, 4, '{\"robots\":\"\",\"author\":\"\",\"rights\":\"\",\"xreference\":\"\"}', 0, '*', ''),
(33, 113, 'Graphic Design', 'graphic', 'Graphic design\r\n\r\nPellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Vestibulum tortor quam, feugiat vitae.\r\n\r\nInterior design\r\n\r\nPellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Vestibulum tortor quam, feugiat vitae.\r\n\r\n\r\nVideo Animation\r\n\r\n\r\nPellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Vestibulum tortor quam, feugiat vitae.\r\n', '', 1, 16, '2018-07-04 19:43:21', 239, '', '2018-07-05 14:26:55', 239, 0, '0000-00-00 00:00:00', '2018-07-04 19:43:21', '0000-00-00 00:00:00', '{\"image_intro\":\"images\\/xeon\\/portfolio\\/item3.jpg\",\"float_intro\":\"\",\"image_intro_alt\":\"\",\"image_intro_caption\":\"\",\"image_fulltext\":\"\",\"float_fulltext\":\"\",\"image_fulltext_alt\":\"\",\"image_fulltext_caption\":\"\"}', '{\"urla\":false,\"urlatext\":\"\",\"targeta\":\"\",\"urlb\":false,\"urlbtext\":\"\",\"targetb\":\"\",\"urlc\":false,\"urlctext\":\"\",\"targetc\":\"\"}', '{\"article_layout\":\"\",\"show_title\":\"\",\"link_titles\":\"\",\"show_tags\":\"\",\"show_intro\":\"\",\"info_block_position\":\"\",\"info_block_show_title\":\"\",\"show_category\":\"\",\"link_category\":\"\",\"show_parent_category\":\"\",\"link_parent_category\":\"\",\"show_associations\":\"\",\"show_author\":\"\",\"link_author\":\"\",\"show_create_date\":\"\",\"show_modify_date\":\"\",\"show_publish_date\":\"\",\"show_item_navigation\":\"\",\"show_icons\":\"\",\"show_print_icon\":\"\",\"show_email_icon\":\"\",\"show_vote\":\"\",\"show_hits\":\"\",\"show_noauth\":\"\",\"urls_position\":\"\",\"alternative_readmore\":\"\",\"article_page_title\":\"\",\"show_publishing_options\":\"\",\"show_article_options\":\"\",\"show_urls_images_backend\":\"\",\"show_urls_images_frontend\":\"\"}', 9, 3, '', '', 1, 9, '{\"robots\":\"\",\"author\":\"\",\"rights\":\"\",\"xreference\":\"\"}', 0, '*', ''),
(34, 114, 'WEB Design ', 'web', 'WEB design\r\n\r\nPellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Vestibulum tortor quam, feugiat vitae.\r\n\r\nInterior design\r\n\r\nPellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Vestibulum tortor quam, feugiat vitae.\r\n\r\nVideo Animation\r\n\r\nPellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Vestibulum tortor quam, feugiat vitae.\r\n', '', 1, 16, '2018-07-04 19:43:21', 239, '', '2018-07-05 12:48:42', 239, 0, '0000-00-00 00:00:00', '2018-07-04 19:43:21', '0000-00-00 00:00:00', '{\"image_intro\":\"images\\/xeon\\/portfolio\\/item4.jpg\",\"float_intro\":\"\",\"image_intro_alt\":\"\",\"image_intro_caption\":\"\",\"image_fulltext\":\"\",\"float_fulltext\":\"\",\"image_fulltext_alt\":\"\",\"image_fulltext_caption\":\"\"}', '{\"urla\":false,\"urlatext\":\"\",\"targeta\":\"\",\"urlb\":false,\"urlbtext\":\"\",\"targetb\":\"\",\"urlc\":false,\"urlctext\":\"\",\"targetc\":\"\"}', '{\"article_layout\":\"\",\"show_title\":\"\",\"link_titles\":\"\",\"show_tags\":\"\",\"show_intro\":\"\",\"info_block_position\":\"\",\"info_block_show_title\":\"\",\"show_category\":\"\",\"link_category\":\"\",\"show_parent_category\":\"\",\"link_parent_category\":\"\",\"show_associations\":\"\",\"show_author\":\"\",\"link_author\":\"\",\"show_create_date\":\"\",\"show_modify_date\":\"\",\"show_publish_date\":\"\",\"show_item_navigation\":\"\",\"show_icons\":\"\",\"show_print_icon\":\"\",\"show_email_icon\":\"\",\"show_vote\":\"\",\"show_hits\":\"\",\"show_noauth\":\"\",\"urls_position\":\"\",\"alternative_readmore\":\"\",\"article_page_title\":\"\",\"show_publishing_options\":\"\",\"show_article_options\":\"\",\"show_urls_images_backend\":\"\",\"show_urls_images_frontend\":\"\"}', 6, 2, '', '', 1, 5, '{\"robots\":\"\",\"author\":\"\",\"rights\":\"\",\"xreference\":\"\"}', 0, '*', ''),
(35, 116, 'Interior', 'interior', 'Interior\r\n\r\nPellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Vestibulum tortor quam, feugiat vitae.\r\n\r\nInterior design\r\n\r\nPellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Vestibulum tortor quam, feugiat vitae.\r\n\r\nVideo Animation\r\n\r\nPellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Vestibulum tortor quam, feugiat vitae.\r\n', '', 1, 16, '2018-07-04 19:43:21', 239, '', '2018-07-05 13:04:19', 239, 0, '0000-00-00 00:00:00', '2018-07-04 19:43:21', '0000-00-00 00:00:00', '{\"image_intro\":\"images\\/xeon\\/portfolio\\/item1.jpg\",\"float_intro\":\"\",\"image_intro_alt\":\"\",\"image_intro_caption\":\"\",\"image_fulltext\":\"\",\"float_fulltext\":\"\",\"image_fulltext_alt\":\"\",\"image_fulltext_caption\":\"\"}', '{\"urla\":false,\"urlatext\":\"\",\"targeta\":\"\",\"urlb\":false,\"urlbtext\":\"\",\"targetb\":\"\",\"urlc\":false,\"urlctext\":\"\",\"targetc\":\"\"}', '{\"article_layout\":\"\",\"show_title\":\"\",\"link_titles\":\"\",\"show_tags\":\"\",\"show_intro\":\"\",\"info_block_position\":\"\",\"info_block_show_title\":\"\",\"show_category\":\"\",\"link_category\":\"\",\"show_parent_category\":\"\",\"link_parent_category\":\"\",\"show_associations\":\"\",\"show_author\":\"\",\"link_author\":\"\",\"show_create_date\":\"\",\"show_modify_date\":\"\",\"show_publish_date\":\"\",\"show_item_navigation\":\"\",\"show_icons\":\"\",\"show_print_icon\":\"\",\"show_email_icon\":\"\",\"show_vote\":\"\",\"show_hits\":\"\",\"show_noauth\":\"\",\"urls_position\":\"\",\"alternative_readmore\":\"\",\"article_page_title\":\"\",\"show_publishing_options\":\"\",\"show_article_options\":\"\",\"show_urls_images_backend\":\"\",\"show_urls_images_frontend\":\"\"}', 2, 1, '', '', 1, 1, '{\"robots\":\"\",\"author\":\"\",\"rights\":\"\",\"xreference\":\"\"}', 0, '*', ''),
(36, 117, 'Animation', 'animation', 'Animation\r\n\r\nPellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Vestibulum tortor quam, feugiat vitae.\r\n\r\nInterior design\r\n\r\nPellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Vestibulum tortor quam, feugiat vitae.\r\n\r\nVideo Animation\r\n\r\nPellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Vestibulum tortor quam, feugiat vitae.\r\n', '', 1, 16, '2018-07-04 19:43:21', 239, '', '2018-07-05 13:07:50', 239, 0, '0000-00-00 00:00:00', '2018-07-04 19:43:21', '0000-00-00 00:00:00', '{\"image_intro\":\"images\\/xeon\\/portfolio\\/item2.jpg\",\"float_intro\":\"\",\"image_intro_alt\":\"\",\"image_intro_caption\":\"\",\"image_fulltext\":\"\",\"float_fulltext\":\"\",\"image_fulltext_alt\":\"\",\"image_fulltext_caption\":\"\"}', '{\"urla\":false,\"urlatext\":\"\",\"targeta\":\"\",\"urlb\":false,\"urlbtext\":\"\",\"targetb\":\"\",\"urlc\":false,\"urlctext\":\"\",\"targetc\":\"\"}', '{\"article_layout\":\"\",\"show_title\":\"\",\"link_titles\":\"\",\"show_tags\":\"\",\"show_intro\":\"\",\"info_block_position\":\"\",\"info_block_show_title\":\"\",\"show_category\":\"\",\"link_category\":\"\",\"show_parent_category\":\"\",\"link_parent_category\":\"\",\"show_associations\":\"\",\"show_author\":\"\",\"link_author\":\"\",\"show_create_date\":\"\",\"show_modify_date\":\"\",\"show_publish_date\":\"\",\"show_item_navigation\":\"\",\"show_icons\":\"\",\"show_print_icon\":\"\",\"show_email_icon\":\"\",\"show_vote\":\"\",\"show_hits\":\"\",\"show_noauth\":\"\",\"urls_position\":\"\",\"alternative_readmore\":\"\",\"article_page_title\":\"\",\"show_publishing_options\":\"\",\"show_article_options\":\"\",\"show_urls_images_backend\":\"\",\"show_urls_images_frontend\":\"\"}', 2, 0, '', '', 1, 2, '{\"robots\":\"\",\"author\":\"\",\"rights\":\"\",\"xreference\":\"\"}', 0, '*', '');

-- --------------------------------------------------------

--
-- Table structure for table `t41uf_contentitem_tag_map`
--

CREATE TABLE `t41uf_contentitem_tag_map` (
  `type_alias` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `core_content_id` int(10) UNSIGNED NOT NULL COMMENT 'PK from the core content table',
  `content_item_id` int(11) NOT NULL COMMENT 'PK from the content type table',
  `tag_id` int(10) UNSIGNED NOT NULL COMMENT 'PK from the tag table',
  `tag_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT 'Date of most recent save for this tag-item',
  `type_id` mediumint(8) NOT NULL COMMENT 'PK from the content_type table'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci COMMENT='Maps items from content tables to tags';

-- --------------------------------------------------------

--
-- Table structure for table `t41uf_content_frontpage`
--

CREATE TABLE `t41uf_content_frontpage` (
  `content_id` int(11) NOT NULL DEFAULT '0',
  `ordering` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `t41uf_content_frontpage`
--

INSERT INTO `t41uf_content_frontpage` (`content_id`, `ordering`) VALUES
(12, 2),
(15, 1);

-- --------------------------------------------------------

--
-- Table structure for table `t41uf_content_rating`
--

CREATE TABLE `t41uf_content_rating` (
  `content_id` int(11) NOT NULL DEFAULT '0',
  `rating_sum` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `rating_count` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `lastip` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT ''
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `t41uf_content_types`
--

CREATE TABLE `t41uf_content_types` (
  `type_id` int(10) UNSIGNED NOT NULL,
  `type_title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `type_alias` varchar(400) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `table` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `rules` mediumtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `field_mappings` mediumtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `router` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `content_history_options` varchar(5120) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT 'JSON string for com_contenthistory options'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `t41uf_content_types`
--

INSERT INTO `t41uf_content_types` (`type_id`, `type_title`, `type_alias`, `table`, `rules`, `field_mappings`, `router`, `content_history_options`) VALUES
(1, 'Article', 'com_content.article', '{\"special\":{\"dbtable\":\"#___content\",\"key\":\"id\",\"type\":\"Content\",\"prefix\":\"JTable\",\"config\":\"array()\"},\"common\":{\"dbtable\":\"#___ucm_content\",\"key\":\"ucm_id\",\"type\":\"Corecontent\",\"prefix\":\"JTable\",\"config\":\"array()\"}}', '', '{\"common\":{\"core_content_item_id\":\"id\",\"core_title\":\"title\",\"core_state\":\"state\",\"core_alias\":\"alias\",\"core_created_time\":\"created\",\"core_modified_time\":\"modified\",\"core_body\":\"introtext\", \"core_hits\":\"hits\",\"core_publish_up\":\"publish_up\",\"core_publish_down\":\"publish_down\",\"core_access\":\"access\", \"core_params\":\"attribs\", \"core_featured\":\"featured\", \"core_metadata\":\"metadata\", \"core_language\":\"language\", \"core_images\":\"images\", \"core_urls\":\"urls\", \"core_version\":\"version\", \"core_ordering\":\"ordering\", \"core_metakey\":\"metakey\", \"core_metadesc\":\"metadesc\", \"core_catid\":\"catid\", \"core_xreference\":\"xreference\", \"asset_id\":\"asset_id\"}, \"special\": {\"fulltext\":\"fulltext\"}}', 'ContentHelperRoute::getArticleRoute', '{\"formFile\":\"administrator\\/components\\/com_content\\/models\\/forms\\/article.xml\", \"hideFields\":[\"asset_id\",\"checked_out\",\"checked_out_time\",\"version\"],\"ignoreChanges\":[\"modified_by\", \"modified\", \"checked_out\", \"checked_out_time\", \"version\", \"hits\"],\"convertToInt\":[\"publish_up\", \"publish_down\", \"featured\", \"ordering\"],\"displayLookup\":[{\"sourceColumn\":\"catid\",\"targetTable\":\"#___categories\",\"targetColumn\":\"id\",\"displayColumn\":\"title\"},{\"sourceColumn\":\"created_by\",\"targetTable\":\"#___users\",\"targetColumn\":\"id\",\"displayColumn\":\"name\"},{\"sourceColumn\":\"access\",\"targetTable\":\"#___viewlevels\",\"targetColumn\":\"id\",\"displayColumn\":\"title\"},{\"sourceColumn\":\"modified_by\",\"targetTable\":\"#___users\",\"targetColumn\":\"id\",\"displayColumn\":\"name\"} ]}'),
(2, 'Weblink', 'com_weblinks.weblink', '{\"special\":{\"dbtable\":\"#___weblinks\",\"key\":\"id\",\"type\":\"Weblink\",\"prefix\":\"WeblinksTable\",\"config\":\"array()\"},\"common\":{\"dbtable\":\"#___ucm_content\",\"key\":\"ucm_id\",\"type\":\"Corecontent\",\"prefix\":\"JTable\",\"config\":\"array()\"}}', '', '{\"common\":{\"core_content_item_id\":\"id\",\"core_title\":\"title\",\"core_state\":\"state\",\"core_alias\":\"alias\",\"core_created_time\":\"created\",\"core_modified_time\":\"modified\",\"core_body\":\"description\", \"core_hits\":\"hits\",\"core_publish_up\":\"publish_up\",\"core_publish_down\":\"publish_down\",\"core_access\":\"access\", \"core_params\":\"params\", \"core_featured\":\"featured\", \"core_metadata\":\"metadata\", \"core_language\":\"language\", \"core_images\":\"images\", \"core_urls\":\"url\", \"core_version\":\"version\", \"core_ordering\":\"ordering\", \"core_metakey\":\"metakey\", \"core_metadesc\":\"metadesc\", \"core_catid\":\"catid\", \"core_xreference\":\"xreference\", \"asset_id\":\"null\"}, \"special\": {}}', 'WeblinksHelperRoute::getWeblinkRoute', '{\"formFile\":\"administrator\\/components\\/com_weblinks\\/models\\/forms\\/weblink.xml\", \"hideFields\":[\"asset_id\",\"checked_out\",\"checked_out_time\",\"version\",\"featured\",\"images\"], \"ignoreChanges\":[\"modified_by\", \"modified\", \"checked_out\", \"checked_out_time\", \"version\", \"hits\"], \"convertToInt\":[\"publish_up\", \"publish_down\", \"featured\", \"ordering\"], \"displayLookup\":[{\"sourceColumn\":\"catid\",\"targetTable\":\"#___categories\",\"targetColumn\":\"id\",\"displayColumn\":\"title\"},{\"sourceColumn\":\"created_by\",\"targetTable\":\"#___users\",\"targetColumn\":\"id\",\"displayColumn\":\"name\"},{\"sourceColumn\":\"access\",\"targetTable\":\"#___viewlevels\",\"targetColumn\":\"id\",\"displayColumn\":\"title\"},{\"sourceColumn\":\"modified_by\",\"targetTable\":\"#___users\",\"targetColumn\":\"id\",\"displayColumn\":\"name\"} ]}'),
(3, 'Contact', 'com_contact.contact', '{\"special\":{\"dbtable\":\"#___contact_details\",\"key\":\"id\",\"type\":\"Contact\",\"prefix\":\"ContactTable\",\"config\":\"array()\"},\"common\":{\"dbtable\":\"#___ucm_content\",\"key\":\"ucm_id\",\"type\":\"Corecontent\",\"prefix\":\"JTable\",\"config\":\"array()\"}}', '', '{\"common\":{\"core_content_item_id\":\"id\",\"core_title\":\"name\",\"core_state\":\"published\",\"core_alias\":\"alias\",\"core_created_time\":\"created\",\"core_modified_time\":\"modified\",\"core_body\":\"address\", \"core_hits\":\"hits\",\"core_publish_up\":\"publish_up\",\"core_publish_down\":\"publish_down\",\"core_access\":\"access\", \"core_params\":\"params\", \"core_featured\":\"featured\", \"core_metadata\":\"metadata\", \"core_language\":\"language\", \"core_images\":\"image\", \"core_urls\":\"webpage\", \"core_version\":\"version\", \"core_ordering\":\"ordering\", \"core_metakey\":\"metakey\", \"core_metadesc\":\"metadesc\", \"core_catid\":\"catid\", \"core_xreference\":\"xreference\", \"asset_id\":\"null\"}, \"special\": {\"con_position\":\"con_position\",\"suburb\":\"suburb\",\"state\":\"state\",\"country\":\"country\",\"postcode\":\"postcode\",\"telephone\":\"telephone\",\"fax\":\"fax\",\"misc\":\"misc\",\"email_to\":\"email_to\",\"default_con\":\"default_con\",\"user_id\":\"user_id\",\"mobile\":\"mobile\",\"sortname1\":\"sortname1\",\"sortname2\":\"sortname2\",\"sortname3\":\"sortname3\"}}', 'ContactHelperRoute::getContactRoute', '{\"formFile\":\"administrator\\/components\\/com_contact\\/models\\/forms\\/contact.xml\",\"hideFields\":[\"default_con\",\"checked_out\",\"checked_out_time\",\"version\",\"xreference\"],\"ignoreChanges\":[\"modified_by\", \"modified\", \"checked_out\", \"checked_out_time\", \"version\", \"hits\"],\"convertToInt\":[\"publish_up\", \"publish_down\", \"featured\", \"ordering\"], \"displayLookup\":[ {\"sourceColumn\":\"created_by\",\"targetTable\":\"#___users\",\"targetColumn\":\"id\",\"displayColumn\":\"name\"},{\"sourceColumn\":\"catid\",\"targetTable\":\"#___categories\",\"targetColumn\":\"id\",\"displayColumn\":\"title\"},{\"sourceColumn\":\"modified_by\",\"targetTable\":\"#___users\",\"targetColumn\":\"id\",\"displayColumn\":\"name\"},{\"sourceColumn\":\"access\",\"targetTable\":\"#___viewlevels\",\"targetColumn\":\"id\",\"displayColumn\":\"title\"},{\"sourceColumn\":\"user_id\",\"targetTable\":\"#___users\",\"targetColumn\":\"id\",\"displayColumn\":\"name\"} ] }'),
(4, 'Newsfeed', 'com_newsfeeds.newsfeed', '{\"special\":{\"dbtable\":\"#___newsfeeds\",\"key\":\"id\",\"type\":\"Newsfeed\",\"prefix\":\"NewsfeedsTable\",\"config\":\"array()\"},\"common\":{\"dbtable\":\"#___ucm_content\",\"key\":\"ucm_id\",\"type\":\"Corecontent\",\"prefix\":\"JTable\",\"config\":\"array()\"}}', '', '{\"common\":{\"core_content_item_id\":\"id\",\"core_title\":\"name\",\"core_state\":\"published\",\"core_alias\":\"alias\",\"core_created_time\":\"created\",\"core_modified_time\":\"modified\",\"core_body\":\"description\", \"core_hits\":\"hits\",\"core_publish_up\":\"publish_up\",\"core_publish_down\":\"publish_down\",\"core_access\":\"access\", \"core_params\":\"params\", \"core_featured\":\"featured\", \"core_metadata\":\"metadata\", \"core_language\":\"language\", \"core_images\":\"images\", \"core_urls\":\"link\", \"core_version\":\"version\", \"core_ordering\":\"ordering\", \"core_metakey\":\"metakey\", \"core_metadesc\":\"metadesc\", \"core_catid\":\"catid\", \"core_xreference\":\"xreference\", \"asset_id\":\"null\"}, \"special\": {\"numarticles\":\"numarticles\",\"cache_time\":\"cache_time\",\"rtl\":\"rtl\"}}', 'NewsfeedsHelperRoute::getNewsfeedRoute', '{\"formFile\":\"administrator\\/components\\/com_newsfeeds\\/models\\/forms\\/newsfeed.xml\",\"hideFields\":[\"asset_id\",\"checked_out\",\"checked_out_time\",\"version\"],\"ignoreChanges\":[\"modified_by\", \"modified\", \"checked_out\", \"checked_out_time\", \"version\", \"hits\"],\"convertToInt\":[\"publish_up\", \"publish_down\", \"featured\", \"ordering\"],\"displayLookup\":[{\"sourceColumn\":\"catid\",\"targetTable\":\"#___categories\",\"targetColumn\":\"id\",\"displayColumn\":\"title\"},{\"sourceColumn\":\"created_by\",\"targetTable\":\"#___users\",\"targetColumn\":\"id\",\"displayColumn\":\"name\"},{\"sourceColumn\":\"access\",\"targetTable\":\"#___viewlevels\",\"targetColumn\":\"id\",\"displayColumn\":\"title\"},{\"sourceColumn\":\"modified_by\",\"targetTable\":\"#___users\",\"targetColumn\":\"id\",\"displayColumn\":\"name\"}]}'),
(5, 'User', 'com_users.user', '{\"special\":{\"dbtable\":\"#___users\",\"key\":\"id\",\"type\":\"User\",\"prefix\":\"JTable\",\"config\":\"array()\"},\"common\":{\"dbtable\":\"#___ucm_content\",\"key\":\"ucm_id\",\"type\":\"Corecontent\",\"prefix\":\"JTable\",\"config\":\"array()\"}}', '', '{\"common\":{\"core_content_item_id\":\"id\",\"core_title\":\"name\",\"core_state\":\"null\",\"core_alias\":\"username\",\"core_created_time\":\"registerdate\",\"core_modified_time\":\"lastvisitDate\",\"core_body\":\"null\", \"core_hits\":\"null\",\"core_publish_up\":\"null\",\"core_publish_down\":\"null\",\"access\":\"null\", \"core_params\":\"params\", \"core_featured\":\"null\", \"core_metadata\":\"null\", \"core_language\":\"null\", \"core_images\":\"null\", \"core_urls\":\"null\", \"core_version\":\"null\", \"core_ordering\":\"null\", \"core_metakey\":\"null\", \"core_metadesc\":\"null\", \"core_catid\":\"null\", \"core_xreference\":\"null\", \"asset_id\":\"null\"}, \"special\": {}}', 'UsersHelperRoute::getUserRoute', ''),
(6, 'Article Category', 'com_content.category', '{\"special\":{\"dbtable\":\"#___categories\",\"key\":\"id\",\"type\":\"Category\",\"prefix\":\"JTable\",\"config\":\"array()\"},\"common\":{\"dbtable\":\"#___ucm_content\",\"key\":\"ucm_id\",\"type\":\"Corecontent\",\"prefix\":\"JTable\",\"config\":\"array()\"}}', '', '{\"common\":{\"core_content_item_id\":\"id\",\"core_title\":\"title\",\"core_state\":\"published\",\"core_alias\":\"alias\",\"core_created_time\":\"created_time\",\"core_modified_time\":\"modified_time\",\"core_body\":\"description\", \"core_hits\":\"hits\",\"core_publish_up\":\"null\",\"core_publish_down\":\"null\",\"core_access\":\"access\", \"core_params\":\"params\", \"core_featured\":\"null\", \"core_metadata\":\"metadata\", \"core_language\":\"language\", \"core_images\":\"null\", \"core_urls\":\"null\", \"core_version\":\"version\", \"core_ordering\":\"null\", \"core_metakey\":\"metakey\", \"core_metadesc\":\"metadesc\", \"core_catid\":\"parent_id\", \"core_xreference\":\"null\", \"asset_id\":\"asset_id\"}, \"special\": {\"parent_id\":\"parent_id\",\"lft\":\"lft\",\"rgt\":\"rgt\",\"level\":\"level\",\"path\":\"path\",\"extension\":\"extension\",\"note\":\"note\"}}', 'ContentHelperRoute::getCategoryRoute', '{\"formFile\":\"administrator\\/components\\/com_categories\\/models\\/forms\\/category.xml\", \"hideFields\":[\"asset_id\",\"checked_out\",\"checked_out_time\",\"version\",\"lft\",\"rgt\",\"level\",\"path\",\"extension\"], \"ignoreChanges\":[\"modified_user_id\", \"modified_time\", \"checked_out\", \"checked_out_time\", \"version\", \"hits\", \"path\"],\"convertToInt\":[\"publish_up\", \"publish_down\"], \"displayLookup\":[{\"sourceColumn\":\"created_user_id\",\"targetTable\":\"#___users\",\"targetColumn\":\"id\",\"displayColumn\":\"name\"},{\"sourceColumn\":\"access\",\"targetTable\":\"#___viewlevels\",\"targetColumn\":\"id\",\"displayColumn\":\"title\"},{\"sourceColumn\":\"modified_user_id\",\"targetTable\":\"#___users\",\"targetColumn\":\"id\",\"displayColumn\":\"name\"},{\"sourceColumn\":\"parent_id\",\"targetTable\":\"#___categories\",\"targetColumn\":\"id\",\"displayColumn\":\"title\"}]}'),
(7, 'Contact Category', 'com_contact.category', '{\"special\":{\"dbtable\":\"#___categories\",\"key\":\"id\",\"type\":\"Category\",\"prefix\":\"JTable\",\"config\":\"array()\"},\"common\":{\"dbtable\":\"#___ucm_content\",\"key\":\"ucm_id\",\"type\":\"Corecontent\",\"prefix\":\"JTable\",\"config\":\"array()\"}}', '', '{\"common\":{\"core_content_item_id\":\"id\",\"core_title\":\"title\",\"core_state\":\"published\",\"core_alias\":\"alias\",\"core_created_time\":\"created_time\",\"core_modified_time\":\"modified_time\",\"core_body\":\"description\", \"core_hits\":\"hits\",\"core_publish_up\":\"null\",\"core_publish_down\":\"null\",\"core_access\":\"access\", \"core_params\":\"params\", \"core_featured\":\"null\", \"core_metadata\":\"metadata\", \"core_language\":\"language\", \"core_images\":\"null\", \"core_urls\":\"null\", \"core_version\":\"version\", \"core_ordering\":\"null\", \"core_metakey\":\"metakey\", \"core_metadesc\":\"metadesc\", \"core_catid\":\"parent_id\", \"core_xreference\":\"null\", \"asset_id\":\"asset_id\"}, \"special\": {\"parent_id\":\"parent_id\",\"lft\":\"lft\",\"rgt\":\"rgt\",\"level\":\"level\",\"path\":\"path\",\"extension\":\"extension\",\"note\":\"note\"}}', 'ContactHelperRoute::getCategoryRoute', '{\"formFile\":\"administrator\\/components\\/com_categories\\/models\\/forms\\/category.xml\", \"hideFields\":[\"asset_id\",\"checked_out\",\"checked_out_time\",\"version\",\"lft\",\"rgt\",\"level\",\"path\",\"extension\"], \"ignoreChanges\":[\"modified_user_id\", \"modified_time\", \"checked_out\", \"checked_out_time\", \"version\", \"hits\", \"path\"],\"convertToInt\":[\"publish_up\", \"publish_down\"], \"displayLookup\":[{\"sourceColumn\":\"created_user_id\",\"targetTable\":\"#___users\",\"targetColumn\":\"id\",\"displayColumn\":\"name\"},{\"sourceColumn\":\"access\",\"targetTable\":\"#___viewlevels\",\"targetColumn\":\"id\",\"displayColumn\":\"title\"},{\"sourceColumn\":\"modified_user_id\",\"targetTable\":\"#___users\",\"targetColumn\":\"id\",\"displayColumn\":\"name\"},{\"sourceColumn\":\"parent_id\",\"targetTable\":\"#___categories\",\"targetColumn\":\"id\",\"displayColumn\":\"title\"}]}'),
(8, 'Newsfeeds Category', 'com_newsfeeds.category', '{\"special\":{\"dbtable\":\"#___categories\",\"key\":\"id\",\"type\":\"Category\",\"prefix\":\"JTable\",\"config\":\"array()\"},\"common\":{\"dbtable\":\"#___ucm_content\",\"key\":\"ucm_id\",\"type\":\"Corecontent\",\"prefix\":\"JTable\",\"config\":\"array()\"}}', '', '{\"common\":{\"core_content_item_id\":\"id\",\"core_title\":\"title\",\"core_state\":\"published\",\"core_alias\":\"alias\",\"core_created_time\":\"created_time\",\"core_modified_time\":\"modified_time\",\"core_body\":\"description\", \"core_hits\":\"hits\",\"core_publish_up\":\"null\",\"core_publish_down\":\"null\",\"core_access\":\"access\", \"core_params\":\"params\", \"core_featured\":\"null\", \"core_metadata\":\"metadata\", \"core_language\":\"language\", \"core_images\":\"null\", \"core_urls\":\"null\", \"core_version\":\"version\", \"core_ordering\":\"null\", \"core_metakey\":\"metakey\", \"core_metadesc\":\"metadesc\", \"core_catid\":\"parent_id\", \"core_xreference\":\"null\", \"asset_id\":\"asset_id\"}, \"special\": {\"parent_id\":\"parent_id\",\"lft\":\"lft\",\"rgt\":\"rgt\",\"level\":\"level\",\"path\":\"path\",\"extension\":\"extension\",\"note\":\"note\"}}', 'NewsfeedsHelperRoute::getCategoryRoute', '{\"formFile\":\"administrator\\/components\\/com_categories\\/models\\/forms\\/category.xml\", \"hideFields\":[\"asset_id\",\"checked_out\",\"checked_out_time\",\"version\",\"lft\",\"rgt\",\"level\",\"path\",\"extension\"], \"ignoreChanges\":[\"modified_user_id\", \"modified_time\", \"checked_out\", \"checked_out_time\", \"version\", \"hits\", \"path\"],\"convertToInt\":[\"publish_up\", \"publish_down\"], \"displayLookup\":[{\"sourceColumn\":\"created_user_id\",\"targetTable\":\"#___users\",\"targetColumn\":\"id\",\"displayColumn\":\"name\"},{\"sourceColumn\":\"access\",\"targetTable\":\"#___viewlevels\",\"targetColumn\":\"id\",\"displayColumn\":\"title\"},{\"sourceColumn\":\"modified_user_id\",\"targetTable\":\"#___users\",\"targetColumn\":\"id\",\"displayColumn\":\"name\"},{\"sourceColumn\":\"parent_id\",\"targetTable\":\"#___categories\",\"targetColumn\":\"id\",\"displayColumn\":\"title\"}]}'),
(9, 'Weblinks Category', 'com_weblinks.category', '{\"special\":{\"dbtable\":\"#___categories\",\"key\":\"id\",\"type\":\"Category\",\"prefix\":\"JTable\",\"config\":\"array()\"},\"common\":{\"dbtable\":\"#___ucm_content\",\"key\":\"ucm_id\",\"type\":\"Corecontent\",\"prefix\":\"JTable\",\"config\":\"array()\"}}', '', '{\"common\":{\"core_content_item_id\":\"id\",\"core_title\":\"title\",\"core_state\":\"published\",\"core_alias\":\"alias\",\"core_created_time\":\"created_time\",\"core_modified_time\":\"modified_time\",\"core_body\":\"description\", \"core_hits\":\"hits\",\"core_publish_up\":\"null\",\"core_publish_down\":\"null\",\"core_access\":\"access\", \"core_params\":\"params\", \"core_featured\":\"null\", \"core_metadata\":\"metadata\", \"core_language\":\"language\", \"core_images\":\"null\", \"core_urls\":\"null\", \"core_version\":\"version\", \"core_ordering\":\"null\", \"core_metakey\":\"metakey\", \"core_metadesc\":\"metadesc\", \"core_catid\":\"parent_id\", \"core_xreference\":\"null\", \"asset_id\":\"asset_id\"}, \"special\": {\"parent_id\":\"parent_id\",\"lft\":\"lft\",\"rgt\":\"rgt\",\"level\":\"level\",\"path\":\"path\",\"extension\":\"extension\",\"note\":\"note\"}}', 'WeblinksHelperRoute::getCategoryRoute', '{\"formFile\":\"administrator\\/components\\/com_categories\\/models\\/forms\\/category.xml\", \"hideFields\":[\"asset_id\",\"checked_out\",\"checked_out_time\",\"version\",\"lft\",\"rgt\",\"level\",\"path\",\"extension\"], \"ignoreChanges\":[\"modified_user_id\", \"modified_time\", \"checked_out\", \"checked_out_time\", \"version\", \"hits\", \"path\"],\"convertToInt\":[\"publish_up\", \"publish_down\"], \"displayLookup\":[{\"sourceColumn\":\"created_user_id\",\"targetTable\":\"#___users\",\"targetColumn\":\"id\",\"displayColumn\":\"name\"},{\"sourceColumn\":\"access\",\"targetTable\":\"#___viewlevels\",\"targetColumn\":\"id\",\"displayColumn\":\"title\"},{\"sourceColumn\":\"modified_user_id\",\"targetTable\":\"#___users\",\"targetColumn\":\"id\",\"displayColumn\":\"name\"},{\"sourceColumn\":\"parent_id\",\"targetTable\":\"#___categories\",\"targetColumn\":\"id\",\"displayColumn\":\"title\"}]}'),
(10, 'Tag', 'com_tags.tag', '{\"special\":{\"dbtable\":\"#___tags\",\"key\":\"tag_id\",\"type\":\"Tag\",\"prefix\":\"TagsTable\",\"config\":\"array()\"},\"common\":{\"dbtable\":\"#___ucm_content\",\"key\":\"ucm_id\",\"type\":\"Corecontent\",\"prefix\":\"JTable\",\"config\":\"array()\"}}', '', '{\"common\":{\"core_content_item_id\":\"id\",\"core_title\":\"title\",\"core_state\":\"published\",\"core_alias\":\"alias\",\"core_created_time\":\"created_time\",\"core_modified_time\":\"modified_time\",\"core_body\":\"description\", \"core_hits\":\"hits\",\"core_publish_up\":\"null\",\"core_publish_down\":\"null\",\"core_access\":\"access\", \"core_params\":\"params\", \"core_featured\":\"featured\", \"core_metadata\":\"metadata\", \"core_language\":\"language\", \"core_images\":\"images\", \"core_urls\":\"urls\", \"core_version\":\"version\", \"core_ordering\":\"null\", \"core_metakey\":\"metakey\", \"core_metadesc\":\"metadesc\", \"core_catid\":\"null\", \"core_xreference\":\"null\", \"asset_id\":\"null\"}, \"special\": {\"parent_id\":\"parent_id\",\"lft\":\"lft\",\"rgt\":\"rgt\",\"level\":\"level\",\"path\":\"path\"}}', 'TagsHelperRoute::getTagRoute', '{\"formFile\":\"administrator\\/components\\/com_tags\\/models\\/forms\\/tag.xml\", \"hideFields\":[\"checked_out\",\"checked_out_time\",\"version\", \"lft\", \"rgt\", \"level\", \"path\", \"urls\", \"publish_up\", \"publish_down\"],\"ignoreChanges\":[\"modified_user_id\", \"modified_time\", \"checked_out\", \"checked_out_time\", \"version\", \"hits\", \"path\"],\"convertToInt\":[\"publish_up\", \"publish_down\"], \"displayLookup\":[{\"sourceColumn\":\"created_user_id\",\"targetTable\":\"#___users\",\"targetColumn\":\"id\",\"displayColumn\":\"name\"}, {\"sourceColumn\":\"access\",\"targetTable\":\"#___viewlevels\",\"targetColumn\":\"id\",\"displayColumn\":\"title\"}, {\"sourceColumn\":\"modified_user_id\",\"targetTable\":\"#___users\",\"targetColumn\":\"id\",\"displayColumn\":\"name\"}]}'),
(10000, 'Banner', 'com_banners.banner', '{\"special\":{\"dbtable\":\"#___banners\",\"key\":\"id\",\"type\":\"Banner\",\"prefix\":\"BannersTable\",\"config\":\"array()\"},\"common\":{\"dbtable\":\"#___ucm_content\",\"key\":\"ucm_id\",\"type\":\"Corecontent\",\"prefix\":\"JTable\",\"config\":\"array()\"}}', '', '{\"common\":{\"core_content_item_id\":\"id\",\"core_title\":\"name\",\"core_state\":\"published\",\"core_alias\":\"alias\",\"core_created_time\":\"created\",\"core_modified_time\":\"modified\",\"core_body\":\"description\", \"core_hits\":\"null\",\"core_publish_up\":\"publish_up\",\"core_publish_down\":\"publish_down\",\"core_access\":\"access\", \"core_params\":\"params\", \"core_featured\":\"null\", \"core_metadata\":\"metadata\", \"core_language\":\"language\", \"core_images\":\"images\", \"core_urls\":\"link\", \"core_version\":\"version\", \"core_ordering\":\"ordering\", \"core_metakey\":\"metakey\", \"core_metadesc\":\"metadesc\", \"core_catid\":\"catid\", \"core_xreference\":\"null\", \"asset_id\":\"null\"}, \"special\":{\"imptotal\":\"imptotal\", \"impmade\":\"impmade\", \"clicks\":\"clicks\", \"clickurl\":\"clickurl\", \"custombannercode\":\"custombannercode\", \"cid\":\"cid\", \"purchase_type\":\"purchase_type\", \"track_impressions\":\"track_impressions\", \"track_clicks\":\"track_clicks\"}}', '', '{\"formFile\":\"administrator\\/components\\/com_banners\\/models\\/forms\\/banner.xml\", \"hideFields\":[\"checked_out\",\"checked_out_time\",\"version\", \"reset\"],\"ignoreChanges\":[\"modified_by\", \"modified\", \"checked_out\", \"checked_out_time\", \"version\", \"imptotal\", \"impmade\", \"reset\"], \"convertToInt\":[\"publish_up\", \"publish_down\", \"ordering\"], \"displayLookup\":[{\"sourceColumn\":\"catid\",\"targetTable\":\"#___categories\",\"targetColumn\":\"id\",\"displayColumn\":\"title\"}, {\"sourceColumn\":\"cid\",\"targetTable\":\"#___banner_clients\",\"targetColumn\":\"id\",\"displayColumn\":\"name\"}, {\"sourceColumn\":\"created_by\",\"targetTable\":\"#___users\",\"targetColumn\":\"id\",\"displayColumn\":\"name\"},{\"sourceColumn\":\"modified_by\",\"targetTable\":\"#___users\",\"targetColumn\":\"id\",\"displayColumn\":\"name\"}]}'),
(10001, 'Banners Category', 'com_banners.category', '{\"special\":{\"dbtable\":\"#___categories\",\"key\":\"id\",\"type\":\"Category\",\"prefix\":\"JTable\",\"config\":\"array()\"},\"common\":{\"dbtable\":\"#___ucm_content\",\"key\":\"ucm_id\",\"type\":\"Corecontent\",\"prefix\":\"JTable\",\"config\":\"array()\"}}', '', '{\"common\":{\"core_content_item_id\":\"id\",\"core_title\":\"title\",\"core_state\":\"published\",\"core_alias\":\"alias\",\"core_created_time\":\"created_time\",\"core_modified_time\":\"modified_time\",\"core_body\":\"description\", \"core_hits\":\"hits\",\"core_publish_up\":\"null\",\"core_publish_down\":\"null\",\"core_access\":\"access\", \"core_params\":\"params\", \"core_featured\":\"null\", \"core_metadata\":\"metadata\", \"core_language\":\"language\", \"core_images\":\"null\", \"core_urls\":\"null\", \"core_version\":\"version\", \"core_ordering\":\"null\", \"core_metakey\":\"metakey\", \"core_metadesc\":\"metadesc\", \"core_catid\":\"parent_id\", \"core_xreference\":\"null\", \"asset_id\":\"asset_id\"}, \"special\": {\"parent_id\":\"parent_id\",\"lft\":\"lft\",\"rgt\":\"rgt\",\"level\":\"level\",\"path\":\"path\",\"extension\":\"extension\",\"note\":\"note\"}}', '', '{\"formFile\":\"administrator\\/components\\/com_categories\\/models\\/forms\\/category.xml\", \"hideFields\":[\"asset_id\",\"checked_out\",\"checked_out_time\",\"version\",\"lft\",\"rgt\",\"level\",\"path\",\"extension\"], \"ignoreChanges\":[\"modified_user_id\", \"modified_time\", \"checked_out\", \"checked_out_time\", \"version\", \"hits\", \"path\"], \"convertToInt\":[\"publish_up\", \"publish_down\"], \"displayLookup\":[{\"sourceColumn\":\"created_user_id\",\"targetTable\":\"#___users\",\"targetColumn\":\"id\",\"displayColumn\":\"name\"},{\"sourceColumn\":\"access\",\"targetTable\":\"#___viewlevels\",\"targetColumn\":\"id\",\"displayColumn\":\"title\"},{\"sourceColumn\":\"modified_user_id\",\"targetTable\":\"#___users\",\"targetColumn\":\"id\",\"displayColumn\":\"name\"},{\"sourceColumn\":\"parent_id\",\"targetTable\":\"#___categories\",\"targetColumn\":\"id\",\"displayColumn\":\"title\"}]}'),
(10002, 'Banner Client', 'com_banners.client', '{\"special\":{\"dbtable\":\"#___banner_clients\",\"key\":\"id\",\"type\":\"Client\",\"prefix\":\"BannersTable\"}}', '', '', '', '{\"formFile\":\"administrator\\/components\\/com_banners\\/models\\/forms\\/client.xml\", \"hideFields\":[\"checked_out\",\"checked_out_time\"], \"ignoreChanges\":[\"checked_out\", \"checked_out_time\"], \"convertToInt\":[], \"displayLookup\":[]}'),
(10003, 'User Notes', 'com_users.note', '{\"special\":{\"dbtable\":\"#___user_notes\",\"key\":\"id\",\"type\":\"Note\",\"prefix\":\"UsersTable\"}}', '', '', '', '{\"formFile\":\"administrator\\/components\\/com_users\\/models\\/forms\\/note.xml\", \"hideFields\":[\"checked_out\",\"checked_out_time\", \"publish_up\", \"publish_down\"],\"ignoreChanges\":[\"modified_user_id\", \"modified_time\", \"checked_out\", \"checked_out_time\"], \"convertToInt\":[\"publish_up\", \"publish_down\"],\"displayLookup\":[{\"sourceColumn\":\"catid\",\"targetTable\":\"#___categories\",\"targetColumn\":\"id\",\"displayColumn\":\"title\"}, {\"sourceColumn\":\"created_user_id\",\"targetTable\":\"#___users\",\"targetColumn\":\"id\",\"displayColumn\":\"name\"}, {\"sourceColumn\":\"user_id\",\"targetTable\":\"#___users\",\"targetColumn\":\"id\",\"displayColumn\":\"name\"}, {\"sourceColumn\":\"modified_user_id\",\"targetTable\":\"#___users\",\"targetColumn\":\"id\",\"displayColumn\":\"name\"}]}'),
(10004, 'User Notes Category', 'com_users.category', '{\"special\":{\"dbtable\":\"#___categories\",\"key\":\"id\",\"type\":\"Category\",\"prefix\":\"JTable\",\"config\":\"array()\"},\"common\":{\"dbtable\":\"#___ucm_content\",\"key\":\"ucm_id\",\"type\":\"Corecontent\",\"prefix\":\"JTable\",\"config\":\"array()\"}}', '', '{\"common\":{\"core_content_item_id\":\"id\",\"core_title\":\"title\",\"core_state\":\"published\",\"core_alias\":\"alias\",\"core_created_time\":\"created_time\",\"core_modified_time\":\"modified_time\",\"core_body\":\"description\", \"core_hits\":\"hits\",\"core_publish_up\":\"null\",\"core_publish_down\":\"null\",\"core_access\":\"access\", \"core_params\":\"params\", \"core_featured\":\"null\", \"core_metadata\":\"metadata\", \"core_language\":\"language\", \"core_images\":\"null\", \"core_urls\":\"null\", \"core_version\":\"version\", \"core_ordering\":\"null\", \"core_metakey\":\"metakey\", \"core_metadesc\":\"metadesc\", \"core_catid\":\"parent_id\", \"core_xreference\":\"null\", \"asset_id\":\"asset_id\"}, \"special\":{\"parent_id\":\"parent_id\",\"lft\":\"lft\",\"rgt\":\"rgt\",\"level\":\"level\",\"path\":\"path\",\"extension\":\"extension\",\"note\":\"note\"}}', '', '{\"formFile\":\"administrator\\/components\\/com_categories\\/models\\/forms\\/category.xml\", \"hideFields\":[\"checked_out\",\"checked_out_time\",\"version\",\"lft\",\"rgt\",\"level\",\"path\",\"extension\"], \"ignoreChanges\":[\"modified_user_id\", \"modified_time\", \"checked_out\", \"checked_out_time\", \"version\", \"hits\", \"path\"], \"convertToInt\":[\"publish_up\", \"publish_down\"], \"displayLookup\":[{\"sourceColumn\":\"created_user_id\",\"targetTable\":\"#___users\",\"targetColumn\":\"id\",\"displayColumn\":\"name\"}, {\"sourceColumn\":\"access\",\"targetTable\":\"#___viewlevels\",\"targetColumn\":\"id\",\"displayColumn\":\"title\"},{\"sourceColumn\":\"modified_user_id\",\"targetTable\":\"#___users\",\"targetColumn\":\"id\",\"displayColumn\":\"name\"},{\"sourceColumn\":\"parent_id\",\"targetTable\":\"#___categories\",\"targetColumn\":\"id\",\"displayColumn\":\"title\"}]}');

-- --------------------------------------------------------

--
-- Table structure for table `t41uf_core_log_searches`
--

CREATE TABLE `t41uf_core_log_searches` (
  `search_term` varchar(128) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `hits` int(10) UNSIGNED NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `t41uf_extensions`
--

CREATE TABLE `t41uf_extensions` (
  `extension_id` int(11) NOT NULL,
  `package_id` int(11) NOT NULL DEFAULT '0' COMMENT 'Parent package ID for extensions installed as a package.',
  `name` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `type` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL,
  `element` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `folder` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `client_id` tinyint(3) NOT NULL,
  `enabled` tinyint(3) NOT NULL DEFAULT '0',
  `access` int(10) UNSIGNED NOT NULL DEFAULT '1',
  `protected` tinyint(3) NOT NULL DEFAULT '0',
  `manifest_cache` mediumtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `params` mediumtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `custom_data` mediumtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `system_data` mediumtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `checked_out` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `checked_out_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `ordering` int(11) DEFAULT '0',
  `state` int(11) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `t41uf_extensions`
--

INSERT INTO `t41uf_extensions` (`extension_id`, `package_id`, `name`, `type`, `element`, `folder`, `client_id`, `enabled`, `access`, `protected`, `manifest_cache`, `params`, `custom_data`, `system_data`, `checked_out`, `checked_out_time`, `ordering`, `state`) VALUES
(1, 0, 'com_mailto', 'component', 'com_mailto', '', 0, 1, 1, 1, '{\"name\":\"com_mailto\",\"type\":\"component\",\"creationDate\":\"April 2006\",\"author\":\"Joomla! Project\",\"copyright\":\"(C) 2005 - 2017 Open Source Matters. All rights reserved.\\t\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.0.0\",\"description\":\"COM_MAILTO_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"mailto\"}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(2, 0, 'com_wrapper', 'component', 'com_wrapper', '', 0, 1, 1, 1, '{\"name\":\"com_wrapper\",\"type\":\"component\",\"creationDate\":\"April 2006\",\"author\":\"Joomla! Project\",\"copyright\":\"(C) 2005 - 2017 Open Source Matters. All rights reserved.\\n\\t\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.0.0\",\"description\":\"COM_WRAPPER_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"wrapper\"}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(3, 0, 'com_admin', 'component', 'com_admin', '', 1, 1, 1, 1, '{\"name\":\"com_admin\",\"type\":\"component\",\"creationDate\":\"April 2006\",\"author\":\"Joomla! Project\",\"copyright\":\"(C) 2005 - 2017 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.0.0\",\"description\":\"COM_ADMIN_XML_DESCRIPTION\",\"group\":\"\"}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(4, 0, 'com_banners', 'component', 'com_banners', '', 1, 1, 1, 0, '{\"name\":\"com_banners\",\"type\":\"component\",\"creationDate\":\"April 2006\",\"author\":\"Joomla! Project\",\"copyright\":\"(C) 2005 - 2017 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.0.0\",\"description\":\"COM_BANNERS_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"banners\"}', '{\"purchase_type\":\"3\",\"track_impressions\":\"0\",\"track_clicks\":\"0\",\"metakey_prefix\":\"\"}', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(5, 0, 'com_cache', 'component', 'com_cache', '', 1, 1, 1, 1, '{\"name\":\"com_cache\",\"type\":\"component\",\"creationDate\":\"April 2006\",\"author\":\"Joomla! Project\",\"copyright\":\"(C) 2005 - 2017 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.0.0\",\"description\":\"COM_CACHE_XML_DESCRIPTION\",\"group\":\"\"}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(6, 0, 'com_categories', 'component', 'com_categories', '', 1, 1, 1, 1, '{\"name\":\"com_categories\",\"type\":\"component\",\"creationDate\":\"December 2007\",\"author\":\"Joomla! Project\",\"copyright\":\"(C) 2005 - 2017 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.0.0\",\"description\":\"COM_CATEGORIES_XML_DESCRIPTION\",\"group\":\"\"}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(7, 0, 'com_checkin', 'component', 'com_checkin', '', 1, 1, 1, 1, '{\"name\":\"com_checkin\",\"type\":\"component\",\"creationDate\":\"April 2006\",\"author\":\"Joomla! Project\",\"copyright\":\"(C) 2005 - 2017 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.0.0\",\"description\":\"COM_CHECKIN_XML_DESCRIPTION\",\"group\":\"\"}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(8, 0, 'com_contact', 'component', 'com_contact', '', 1, 1, 1, 0, '{\"name\":\"com_contact\",\"type\":\"component\",\"creationDate\":\"April 2006\",\"author\":\"Joomla! Project\",\"copyright\":\"(C) 2005 - 2017 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.0.0\",\"description\":\"COM_CONTACT_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"contact\"}', '{\"show_contact_category\":\"hide\",\"show_contact_list\":\"0\",\"presentation_style\":\"sliders\",\"show_name\":\"1\",\"show_position\":\"1\",\"show_email\":\"0\",\"show_street_address\":\"1\",\"show_suburb\":\"1\",\"show_state\":\"1\",\"show_postcode\":\"1\",\"show_country\":\"1\",\"show_telephone\":\"1\",\"show_mobile\":\"1\",\"show_fax\":\"1\",\"show_webpage\":\"1\",\"show_misc\":\"1\",\"show_image\":\"1\",\"image\":\"\",\"allow_vcard\":\"0\",\"show_articles\":\"0\",\"show_profile\":\"0\",\"show_links\":\"0\",\"linka_name\":\"\",\"linkb_name\":\"\",\"linkc_name\":\"\",\"linkd_name\":\"\",\"linke_name\":\"\",\"contact_icons\":\"0\",\"icon_address\":\"\",\"icon_email\":\"\",\"icon_telephone\":\"\",\"icon_mobile\":\"\",\"icon_fax\":\"\",\"icon_misc\":\"\",\"show_headings\":\"1\",\"show_position_headings\":\"1\",\"show_email_headings\":\"0\",\"show_telephone_headings\":\"1\",\"show_mobile_headings\":\"0\",\"show_fax_headings\":\"0\",\"allow_vcard_headings\":\"0\",\"show_suburb_headings\":\"1\",\"show_state_headings\":\"1\",\"show_country_headings\":\"1\",\"show_email_form\":\"1\",\"show_email_copy\":\"1\",\"banned_email\":\"\",\"banned_subject\":\"\",\"banned_text\":\"\",\"validate_session\":\"1\",\"custom_reply\":\"0\",\"redirect\":\"\",\"show_category_crumb\":\"0\",\"metakey\":\"\",\"metadesc\":\"\",\"robots\":\"\",\"author\":\"\",\"rights\":\"\",\"xreference\":\"\"}', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(9, 0, 'com_cpanel', 'component', 'com_cpanel', '', 1, 1, 1, 1, '{\"name\":\"com_cpanel\",\"type\":\"component\",\"creationDate\":\"April 2006\",\"author\":\"Joomla! Project\",\"copyright\":\"(C) 2005 - 2017 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.0.0\",\"description\":\"COM_CPANEL_XML_DESCRIPTION\",\"group\":\"\"}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(10, 0, 'com_installer', 'component', 'com_installer', '', 1, 1, 1, 1, '{\"name\":\"com_installer\",\"type\":\"component\",\"creationDate\":\"April 2006\",\"author\":\"Joomla! Project\",\"copyright\":\"(C) 2005 - 2017 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.0.0\",\"description\":\"COM_INSTALLER_XML_DESCRIPTION\",\"group\":\"\"}', '{}', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(11, 0, 'com_languages', 'component', 'com_languages', '', 1, 1, 1, 1, '{\"name\":\"com_languages\",\"type\":\"component\",\"creationDate\":\"April 2006\",\"author\":\"Joomla! Project\",\"copyright\":\"(C) 2005 - 2017 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.0.0\",\"description\":\"COM_LANGUAGES_XML_DESCRIPTION\",\"group\":\"\"}', '{\"administrator\":\"en-GB\",\"site\":\"en-GB\"}', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(12, 0, 'com_login', 'component', 'com_login', '', 1, 1, 1, 1, '{\"name\":\"com_login\",\"type\":\"component\",\"creationDate\":\"April 2006\",\"author\":\"Joomla! Project\",\"copyright\":\"(C) 2005 - 2017 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.0.0\",\"description\":\"COM_LOGIN_XML_DESCRIPTION\",\"group\":\"\"}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(13, 0, 'com_media', 'component', 'com_media', '', 1, 1, 0, 1, '{\"name\":\"com_media\",\"type\":\"component\",\"creationDate\":\"April 2006\",\"author\":\"Joomla! Project\",\"copyright\":\"(C) 2005 - 2017 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.0.0\",\"description\":\"COM_MEDIA_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"media\"}', '{\"upload_extensions\":\"bmp,csv,doc,gif,ico,jpg,jpeg,odg,odp,ods,odt,pdf,png,ppt,swf,txt,xcf,xls,BMP,CSV,DOC,GIF,ICO,JPG,JPEG,ODG,ODP,ODS,ODT,PDF,PNG,PPT,SWF,TXT,XCF,XLS\",\"upload_maxsize\":\"10\",\"file_path\":\"images\",\"image_path\":\"images\",\"restrict_uploads\":\"1\",\"allowed_media_usergroup\":\"3\",\"check_mime\":\"1\",\"image_extensions\":\"bmp,gif,jpg,png\",\"ignore_extensions\":\"\",\"upload_mime\":\"image\\/jpeg,image\\/gif,image\\/png,image\\/bmp,application\\/x-shockwave-flash,application\\/msword,application\\/excel,application\\/pdf,application\\/powerpoint,text\\/plain,application\\/x-zip\",\"upload_mime_illegal\":\"text\\/html\",\"enable_flash\":\"0\"}', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(14, 0, 'com_menus', 'component', 'com_menus', '', 1, 1, 1, 1, '{\"name\":\"com_menus\",\"type\":\"component\",\"creationDate\":\"April 2006\",\"author\":\"Joomla! Project\",\"copyright\":\"(C) 2005 - 2017 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.0.0\",\"description\":\"COM_MENUS_XML_DESCRIPTION\",\"group\":\"\"}', '{}', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(15, 0, 'com_messages', 'component', 'com_messages', '', 1, 1, 1, 1, '{\"name\":\"com_messages\",\"type\":\"component\",\"creationDate\":\"April 2006\",\"author\":\"Joomla! Project\",\"copyright\":\"(C) 2005 - 2017 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.0.0\",\"description\":\"COM_MESSAGES_XML_DESCRIPTION\",\"group\":\"\"}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(16, 0, 'com_modules', 'component', 'com_modules', '', 1, 1, 1, 1, '{\"name\":\"com_modules\",\"type\":\"component\",\"creationDate\":\"April 2006\",\"author\":\"Joomla! Project\",\"copyright\":\"(C) 2005 - 2017 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.0.0\",\"description\":\"COM_MODULES_XML_DESCRIPTION\",\"group\":\"\"}', '{}', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(17, 0, 'com_newsfeeds', 'component', 'com_newsfeeds', '', 1, 1, 1, 0, '{\"name\":\"com_newsfeeds\",\"type\":\"component\",\"creationDate\":\"April 2006\",\"author\":\"Joomla! Project\",\"copyright\":\"(C) 2005 - 2017 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.0.0\",\"description\":\"COM_NEWSFEEDS_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"newsfeeds\"}', '{\"show_feed_image\":\"1\",\"show_feed_description\":\"1\",\"show_item_description\":\"1\",\"feed_word_count\":\"0\",\"show_headings\":\"1\",\"show_name\":\"1\",\"show_articles\":\"0\",\"show_link\":\"1\",\"show_description\":\"1\",\"show_description_image\":\"1\",\"display_num\":\"\",\"show_pagination_limit\":\"1\",\"show_pagination\":\"1\",\"show_pagination_results\":\"1\",\"show_cat_items\":\"1\"}', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(18, 0, 'com_plugins', 'component', 'com_plugins', '', 1, 1, 1, 1, '{\"name\":\"com_plugins\",\"type\":\"component\",\"creationDate\":\"April 2006\",\"author\":\"Joomla! Project\",\"copyright\":\"(C) 2005 - 2017 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.0.0\",\"description\":\"COM_PLUGINS_XML_DESCRIPTION\",\"group\":\"\"}', '{}', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(19, 0, 'com_search', 'component', 'com_search', '', 1, 1, 1, 0, '{\"name\":\"com_search\",\"type\":\"component\",\"creationDate\":\"April 2006\",\"author\":\"Joomla! Project\",\"copyright\":\"(C) 2005 - 2017 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.0.0\",\"description\":\"COM_SEARCH_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"search\"}', '{\"enabled\":\"0\",\"show_date\":\"1\"}', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(20, 0, 'com_templates', 'component', 'com_templates', '', 1, 1, 1, 1, '{\"name\":\"com_templates\",\"type\":\"component\",\"creationDate\":\"April 2006\",\"author\":\"Joomla! Project\",\"copyright\":\"(C) 2005 - 2017 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.0.0\",\"description\":\"COM_TEMPLATES_XML_DESCRIPTION\",\"group\":\"\"}', '{\"template_positions_display\":\"1\",\"upload_limit\":\"2\",\"image_formats\":\"gif,bmp,jpg,jpeg,png\",\"source_formats\":\"txt,less,ini,xml,js,php,css\",\"font_formats\":\"woff,ttf,otf\",\"compressed_formats\":\"zip\"}', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(21, 801, 'com_weblinks', 'component', 'com_weblinks', '', 1, 1, 1, 0, '{\"name\":\"com_weblinks\",\"type\":\"component\",\"creationDate\":\"2017-03-08\",\"author\":\"Joomla! Project\",\"copyright\":\"(C) 2005 - 2017 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.6.0\",\"description\":\"COM_WEBLINKS_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"weblinks\"}', '{\"show_comp_description\":\"1\",\"comp_description\":\"\",\"show_link_hits\":\"1\",\"show_link_description\":\"1\",\"show_other_cats\":\"0\",\"show_headings\":\"0\",\"show_numbers\":\"0\",\"show_report\":\"1\",\"count_clicks\":\"1\",\"target\":\"0\",\"link_icons\":\"\"}', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(22, 0, 'com_content', 'component', 'com_content', '', 1, 1, 0, 1, '{\"name\":\"com_content\",\"type\":\"component\",\"creationDate\":\"April 2006\",\"author\":\"Joomla! Project\",\"copyright\":\"(C) 2005 - 2017 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.0.0\",\"description\":\"COM_CONTENT_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"content\"}', '{\"article_layout\":\"_:default\",\"show_title\":\"1\",\"link_titles\":\"1\",\"show_intro\":\"1\",\"show_category\":\"1\",\"link_category\":\"1\",\"show_parent_category\":\"0\",\"link_parent_category\":\"0\",\"show_author\":\"1\",\"link_author\":\"0\",\"show_create_date\":\"1\",\"show_modify_date\":\"0\",\"show_publish_date\":\"0\",\"show_item_navigation\":\"0\",\"show_vote\":\"0\",\"show_readmore\":\"1\",\"show_readmore_title\":\"1\",\"readmore_limit\":\"100\",\"show_icons\":\"1\",\"show_print_icon\":\"1\",\"show_email_icon\":\"1\",\"show_hits\":\"0\",\"show_noauth\":\"0\",\"urls_position\":\"0\",\"show_publishing_options\":\"1\",\"show_article_options\":\"1\",\"show_urls_images_frontend\":\"0\",\"show_urls_images_backend\":\"1\",\"targeta\":0,\"targetb\":0,\"targetc\":0,\"float_intro\":\"left\",\"float_fulltext\":\"left\",\"category_layout\":\"_:blog\",\"show_category_title\":\"0\",\"show_description\":\"0\",\"show_description_image\":\"0\",\"maxLevel\":\"1\",\"show_empty_categories\":\"0\",\"show_no_articles\":\"1\",\"show_subcat_desc\":\"1\",\"show_cat_num_articles\":\"0\",\"show_base_description\":\"1\",\"maxLevelcat\":\"-1\",\"show_empty_categories_cat\":\"0\",\"show_subcat_desc_cat\":\"1\",\"show_cat_num_articles_cat\":\"1\",\"num_leading_articles\":\"1\",\"num_intro_articles\":\"4\",\"num_columns\":\"2\",\"num_links\":\"4\",\"multi_column_order\":\"0\",\"show_subcategory_content\":\"0\",\"show_pagination_limit\":\"1\",\"filter_field\":\"hide\",\"show_headings\":\"1\",\"list_show_date\":\"0\",\"date_format\":\"\",\"list_show_hits\":\"1\",\"list_show_author\":\"1\",\"orderby_pri\":\"order\",\"orderby_sec\":\"rdate\",\"order_date\":\"published\",\"show_pagination\":\"2\",\"show_pagination_results\":\"1\",\"show_feed_link\":\"1\",\"feed_summary\":\"0\",\"feed_show_readmore\":\"0\"}', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(23, 0, 'com_config', 'component', 'com_config', '', 1, 1, 0, 1, '{\"name\":\"com_config\",\"type\":\"component\",\"creationDate\":\"April 2006\",\"author\":\"Joomla! Project\",\"copyright\":\"(C) 2005 - 2017 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.0.0\",\"description\":\"COM_CONFIG_XML_DESCRIPTION\",\"group\":\"\"}', '{\"filters\":{\"1\":{\"filter_type\":\"NH\",\"filter_tags\":\"\",\"filter_attributes\":\"\"},\"6\":{\"filter_type\":\"BL\",\"filter_tags\":\"\",\"filter_attributes\":\"\"},\"7\":{\"filter_type\":\"NONE\",\"filter_tags\":\"\",\"filter_attributes\":\"\"},\"2\":{\"filter_type\":\"NH\",\"filter_tags\":\"\",\"filter_attributes\":\"\"},\"3\":{\"filter_type\":\"BL\",\"filter_tags\":\"\",\"filter_attributes\":\"\"},\"4\":{\"filter_type\":\"BL\",\"filter_tags\":\"\",\"filter_attributes\":\"\"},\"5\":{\"filter_type\":\"BL\",\"filter_tags\":\"\",\"filter_attributes\":\"\"},\"8\":{\"filter_type\":\"NONE\",\"filter_tags\":\"\",\"filter_attributes\":\"\"}}}', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(24, 0, 'com_redirect', 'component', 'com_redirect', '', 1, 1, 0, 1, '{\"name\":\"com_redirect\",\"type\":\"component\",\"creationDate\":\"April 2006\",\"author\":\"Joomla! Project\",\"copyright\":\"(C) 2005 - 2017 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.0.0\",\"description\":\"COM_REDIRECT_XML_DESCRIPTION\",\"group\":\"\"}', '{}', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(25, 0, 'com_users', 'component', 'com_users', '', 1, 1, 0, 1, '{\"name\":\"com_users\",\"type\":\"component\",\"creationDate\":\"April 2006\",\"author\":\"Joomla! Project\",\"copyright\":\"(C) 2005 - 2017 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.0.0\",\"description\":\"COM_USERS_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"users\"}', '{\"allowUserRegistration\":\"1\",\"new_usertype\":\"2\",\"useractivation\":\"1\",\"frontend_userparams\":\"1\",\"mailSubjectPrefix\":\"\",\"mailBodySuffix\":\"\"}', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(27, 0, 'com_finder', 'component', 'com_finder', '', 1, 1, 0, 0, '{\"name\":\"com_finder\",\"type\":\"component\",\"creationDate\":\"August 2011\",\"author\":\"Joomla! Project\",\"copyright\":\"(C) 2005 - 2017 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.0.0\",\"description\":\"COM_FINDER_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"finder\"}', '{\"show_description\":\"1\",\"description_length\":255,\"allow_empty_query\":\"0\",\"show_url\":\"1\",\"show_advanced\":\"1\",\"expand_advanced\":\"0\",\"show_date_filters\":\"0\",\"highlight_terms\":\"1\",\"opensearch_name\":\"\",\"opensearch_description\":\"\",\"batch_size\":\"50\",\"memory_table_limit\":30000,\"title_multiplier\":\"1.7\",\"text_multiplier\":\"0.7\",\"meta_multiplier\":\"1.2\",\"path_multiplier\":\"2.0\",\"misc_multiplier\":\"0.3\",\"stemmer\":\"snowball\"}', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(28, 0, 'com_joomlaupdate', 'component', 'com_joomlaupdate', '', 1, 1, 0, 1, '{\"name\":\"com_joomlaupdate\",\"type\":\"component\",\"creationDate\":\"February 2012\",\"author\":\"Joomla! Project\",\"copyright\":\"(C) 2005 - 2017 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.6.2\",\"description\":\"COM_JOOMLAUPDATE_XML_DESCRIPTION\",\"group\":\"\"}', '{}', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(29, 0, 'com_tags', 'component', 'com_tags', '', 1, 1, 1, 1, '{\"name\":\"com_tags\",\"type\":\"component\",\"creationDate\":\"December 2013\",\"author\":\"Joomla! Project\",\"copyright\":\"(C) 2005 - 2017 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.1.0\",\"description\":\"COM_TAGS_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"tags\"}', '{}', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(30, 0, 'com_contenthistory', 'component', 'com_contenthistory', '', 1, 1, 1, 0, '{\"name\":\"com_contenthistory\",\"type\":\"component\",\"creationDate\":\"May 2013\",\"author\":\"Joomla! Project\",\"copyright\":\"(C) 2005 - 2017 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.2.0\",\"description\":\"COM_CONTENTHISTORY_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"contenthistory\"}', '{}', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(31, 0, 'com_ajax', 'component', 'com_ajax', '', 1, 1, 1, 1, '{\"name\":\"com_ajax\",\"type\":\"component\",\"creationDate\":\"August 2013\",\"author\":\"Joomla! Project\",\"copyright\":\"(C) 2005 - 2017 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.2.0\",\"description\":\"COM_AJAX_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"ajax\"}', '{}', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(32, 0, 'com_postinstall', 'component', 'com_postinstall', '', 1, 1, 1, 1, '{\"name\":\"com_postinstall\",\"type\":\"component\",\"creationDate\":\"September 2013\",\"author\":\"Joomla! Project\",\"copyright\":\"(C) 2005 - 2017 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.2.0\",\"description\":\"COM_POSTINSTALL_XML_DESCRIPTION\",\"group\":\"\"}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(33, 0, 'com_fields', 'component', 'com_fields', '', 1, 1, 1, 0, '{\"name\":\"com_fields\",\"type\":\"component\",\"creationDate\":\"March 2016\",\"author\":\"Joomla! Project\",\"copyright\":\"(C) 2005 - 2017 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.7.0\",\"description\":\"COM_FIELDS_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"fields\"}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(34, 0, 'com_associations', 'component', 'com_associations', '', 1, 1, 1, 0, '{\"name\":\"com_associations\",\"type\":\"component\",\"creationDate\":\"Januar 2017\",\"author\":\"Joomla! Project\",\"copyright\":\"(C) 2005 - 2017 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.7.0\",\"description\":\"COM_ASSOCIATIONS_XML_DESCRIPTION\",\"group\":\"\"}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(102, 0, 'LIB_PHPUTF8', 'library', 'phputf8', '', 0, 1, 1, 1, '{\"name\":\"LIB_PHPUTF8\",\"type\":\"library\",\"creationDate\":\"2006\",\"author\":\"Harry Fuecks\",\"copyright\":\"Copyright various authors\",\"authorEmail\":\"hfuecks@gmail.com\",\"authorUrl\":\"http:\\/\\/sourceforge.net\\/projects\\/phputf8\",\"version\":\"0.5\",\"description\":\"LIB_PHPUTF8_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"phputf8\"}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(103, 0, 'LIB_JOOMLA', 'library', 'joomla', '', 0, 1, 1, 1, '{\"name\":\"LIB_JOOMLA\",\"type\":\"library\",\"creationDate\":\"2008\",\"author\":\"Joomla! Project\",\"copyright\":\"Copyright (C) 2005 - 2017 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"https:\\/\\/www.joomla.org\",\"version\":\"13.1\",\"description\":\"LIB_JOOMLA_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"joomla\"}', '{\"mediaversion\":\"de9136c0e3886dafac9eaac77602b8c0\"}', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(104, 0, 'LIB_IDNA', 'library', 'idna_convert', '', 0, 1, 1, 1, '{\"name\":\"LIB_IDNA\",\"type\":\"library\",\"creationDate\":\"2004\",\"author\":\"phlyLabs\",\"copyright\":\"2004-2011 phlyLabs Berlin, http:\\/\\/phlylabs.de\",\"authorEmail\":\"phlymail@phlylabs.de\",\"authorUrl\":\"http:\\/\\/phlylabs.de\",\"version\":\"0.8.0\",\"description\":\"LIB_IDNA_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"idna_convert\"}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(105, 0, 'FOF', 'library', 'fof', '', 0, 1, 1, 1, '{\"name\":\"FOF\",\"type\":\"library\",\"creationDate\":\"2015-04-22 13:15:32\",\"author\":\"Nicholas K. Dionysopoulos \\/ Akeeba Ltd\",\"copyright\":\"(C)2011-2015 Nicholas K. Dionysopoulos\",\"authorEmail\":\"nicholas@akeebabackup.com\",\"authorUrl\":\"https:\\/\\/www.akeebabackup.com\",\"version\":\"2.4.3\",\"description\":\"LIB_FOF_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"fof\"}', '{}', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(106, 0, 'LIB_PHPASS', 'library', 'phpass', '', 0, 1, 1, 1, '{\"name\":\"LIB_PHPASS\",\"type\":\"library\",\"creationDate\":\"2004-2006\",\"author\":\"Solar Designer\",\"copyright\":\"\",\"authorEmail\":\"solar@openwall.com\",\"authorUrl\":\"http:\\/\\/www.openwall.com\\/phpass\\/\",\"version\":\"0.3\",\"description\":\"LIB_PHPASS_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"phpass\"}', '{}', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(200, 0, 'mod_articles_archive', 'module', 'mod_articles_archive', '', 0, 1, 1, 0, '{\"name\":\"mod_articles_archive\",\"type\":\"module\",\"creationDate\":\"July 2006\",\"author\":\"Joomla! Project\",\"copyright\":\"Copyright (C) 2005 - 2017 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.0.0\",\"description\":\"MOD_ARTICLES_ARCHIVE_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"mod_articles_archive\"}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(201, 0, 'mod_articles_latest', 'module', 'mod_articles_latest', '', 0, 1, 1, 0, '{\"name\":\"mod_articles_latest\",\"type\":\"module\",\"creationDate\":\"July 2004\",\"author\":\"Joomla! Project\",\"copyright\":\"Copyright (C) 2005 - 2017 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.0.0\",\"description\":\"MOD_LATEST_NEWS_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"mod_articles_latest\"}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(202, 0, 'mod_articles_popular', 'module', 'mod_articles_popular', '', 0, 1, 1, 0, '{\"name\":\"mod_articles_popular\",\"type\":\"module\",\"creationDate\":\"July 2006\",\"author\":\"Joomla! Project\",\"copyright\":\"Copyright (C) 2005 - 2017 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.0.0\",\"description\":\"MOD_POPULAR_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"mod_articles_popular\"}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(203, 0, 'mod_banners', 'module', 'mod_banners', '', 0, 1, 1, 0, '{\"name\":\"mod_banners\",\"type\":\"module\",\"creationDate\":\"July 2006\",\"author\":\"Joomla! Project\",\"copyright\":\"Copyright (C) 2005 - 2017 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.0.0\",\"description\":\"MOD_BANNERS_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"mod_banners\"}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(204, 0, 'mod_breadcrumbs', 'module', 'mod_breadcrumbs', '', 0, 1, 1, 1, '{\"name\":\"mod_breadcrumbs\",\"type\":\"module\",\"creationDate\":\"July 2006\",\"author\":\"Joomla! Project\",\"copyright\":\"Copyright (C) 2005 - 2017 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.0.0\",\"description\":\"MOD_BREADCRUMBS_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"mod_breadcrumbs\"}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(205, 0, 'mod_custom', 'module', 'mod_custom', '', 0, 1, 1, 1, '{\"name\":\"mod_custom\",\"type\":\"module\",\"creationDate\":\"July 2004\",\"author\":\"Joomla! Project\",\"copyright\":\"Copyright (C) 2005 - 2017 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.0.0\",\"description\":\"MOD_CUSTOM_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"mod_custom\"}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(206, 0, 'mod_feed', 'module', 'mod_feed', '', 0, 1, 1, 0, '{\"name\":\"mod_feed\",\"type\":\"module\",\"creationDate\":\"July 2005\",\"author\":\"Joomla! Project\",\"copyright\":\"Copyright (C) 2005 - 2017 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.0.0\",\"description\":\"MOD_FEED_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"mod_feed\"}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(207, 0, 'mod_footer', 'module', 'mod_footer', '', 0, 1, 1, 0, '{\"name\":\"mod_footer\",\"type\":\"module\",\"creationDate\":\"July 2006\",\"author\":\"Joomla! Project\",\"copyright\":\"Copyright (C) 2005 - 2017 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.0.0\",\"description\":\"MOD_FOOTER_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"mod_footer\"}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(208, 0, 'mod_login', 'module', 'mod_login', '', 0, 1, 1, 1, '{\"name\":\"mod_login\",\"type\":\"module\",\"creationDate\":\"July 2006\",\"author\":\"Joomla! Project\",\"copyright\":\"Copyright (C) 2005 - 2017 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.0.0\",\"description\":\"MOD_LOGIN_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"mod_login\"}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(209, 0, 'mod_menu', 'module', 'mod_menu', '', 0, 1, 1, 1, '{\"name\":\"mod_menu\",\"type\":\"module\",\"creationDate\":\"July 2004\",\"author\":\"Joomla! Project\",\"copyright\":\"Copyright (C) 2005 - 2017 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.0.0\",\"description\":\"MOD_MENU_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"mod_menu\"}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(210, 0, 'mod_articles_news', 'module', 'mod_articles_news', '', 0, 1, 1, 0, '{\"name\":\"mod_articles_news\",\"type\":\"module\",\"creationDate\":\"July 2006\",\"author\":\"Joomla! Project\",\"copyright\":\"Copyright (C) 2005 - 2017 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.0.0\",\"description\":\"MOD_ARTICLES_NEWS_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"mod_articles_news\"}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(211, 0, 'mod_random_image', 'module', 'mod_random_image', '', 0, 1, 1, 0, '{\"name\":\"mod_random_image\",\"type\":\"module\",\"creationDate\":\"July 2006\",\"author\":\"Joomla! Project\",\"copyright\":\"Copyright (C) 2005 - 2017 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.0.0\",\"description\":\"MOD_RANDOM_IMAGE_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"mod_random_image\"}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(212, 0, 'mod_related_items', 'module', 'mod_related_items', '', 0, 1, 1, 0, '{\"name\":\"mod_related_items\",\"type\":\"module\",\"creationDate\":\"July 2004\",\"author\":\"Joomla! Project\",\"copyright\":\"Copyright (C) 2005 - 2017 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.0.0\",\"description\":\"MOD_RELATED_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"mod_related_items\"}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(213, 0, 'mod_search', 'module', 'mod_search', '', 0, 1, 1, 0, '{\"name\":\"mod_search\",\"type\":\"module\",\"creationDate\":\"July 2004\",\"author\":\"Joomla! Project\",\"copyright\":\"Copyright (C) 2005 - 2017 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.0.0\",\"description\":\"MOD_SEARCH_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"mod_search\"}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(214, 0, 'mod_stats', 'module', 'mod_stats', '', 0, 1, 1, 0, '{\"name\":\"mod_stats\",\"type\":\"module\",\"creationDate\":\"July 2004\",\"author\":\"Joomla! Project\",\"copyright\":\"Copyright (C) 2005 - 2017 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.0.0\",\"description\":\"MOD_STATS_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"mod_stats\"}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(215, 0, 'mod_syndicate', 'module', 'mod_syndicate', '', 0, 1, 1, 1, '{\"name\":\"mod_syndicate\",\"type\":\"module\",\"creationDate\":\"May 2006\",\"author\":\"Joomla! Project\",\"copyright\":\"Copyright (C) 2005 - 2017 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.0.0\",\"description\":\"MOD_SYNDICATE_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"mod_syndicate\"}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(216, 0, 'mod_users_latest', 'module', 'mod_users_latest', '', 0, 1, 1, 0, '{\"name\":\"mod_users_latest\",\"type\":\"module\",\"creationDate\":\"December 2009\",\"author\":\"Joomla! Project\",\"copyright\":\"Copyright (C) 2005 - 2017 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.0.0\",\"description\":\"MOD_USERS_LATEST_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"mod_users_latest\"}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(217, 801, 'mod_weblinks', 'module', 'mod_weblinks', '', 0, 1, 1, 0, '{\"name\":\"mod_weblinks\",\"type\":\"module\",\"creationDate\":\"2017-03-08\",\"author\":\"Joomla! Project\",\"copyright\":\"Copyright (C) 2005 - 2017 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.6.0\",\"description\":\"MOD_WEBLINKS_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"mod_weblinks\"}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(218, 0, 'mod_whosonline', 'module', 'mod_whosonline', '', 0, 1, 1, 0, '{\"name\":\"mod_whosonline\",\"type\":\"module\",\"creationDate\":\"July 2004\",\"author\":\"Joomla! Project\",\"copyright\":\"Copyright (C) 2005 - 2017 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.0.0\",\"description\":\"MOD_WHOSONLINE_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"mod_whosonline\"}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(219, 0, 'mod_wrapper', 'module', 'mod_wrapper', '', 0, 1, 1, 0, '{\"name\":\"mod_wrapper\",\"type\":\"module\",\"creationDate\":\"October 2004\",\"author\":\"Joomla! Project\",\"copyright\":\"Copyright (C) 2005 - 2017 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.0.0\",\"description\":\"MOD_WRAPPER_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"mod_wrapper\"}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(220, 0, 'mod_articles_category', 'module', 'mod_articles_category', '', 0, 1, 1, 0, '{\"name\":\"mod_articles_category\",\"type\":\"module\",\"creationDate\":\"February 2010\",\"author\":\"Joomla! Project\",\"copyright\":\"Copyright (C) 2005 - 2017 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.0.0\",\"description\":\"MOD_ARTICLES_CATEGORY_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"mod_articles_category\"}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(221, 0, 'mod_articles_categories', 'module', 'mod_articles_categories', '', 0, 1, 1, 0, '{\"name\":\"mod_articles_categories\",\"type\":\"module\",\"creationDate\":\"February 2010\",\"author\":\"Joomla! Project\",\"copyright\":\"Copyright (C) 2005 - 2017 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.0.0\",\"description\":\"MOD_ARTICLES_CATEGORIES_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"mod_articles_categories\"}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(222, 0, 'mod_languages', 'module', 'mod_languages', '', 0, 1, 1, 1, '{\"name\":\"mod_languages\",\"type\":\"module\",\"creationDate\":\"February 2010\",\"author\":\"Joomla! Project\",\"copyright\":\"Copyright (C) 2005 - 2017 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.5.0\",\"description\":\"MOD_LANGUAGES_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"mod_languages\"}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(223, 0, 'mod_finder', 'module', 'mod_finder', '', 0, 1, 0, 0, '{\"name\":\"mod_finder\",\"type\":\"module\",\"creationDate\":\"August 2011\",\"author\":\"Joomla! Project\",\"copyright\":\"(C) 2005 - 2017 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.0.0\",\"description\":\"MOD_FINDER_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"mod_finder\"}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(300, 0, 'mod_custom', 'module', 'mod_custom', '', 1, 1, 1, 1, '{\"name\":\"mod_custom\",\"type\":\"module\",\"creationDate\":\"July 2004\",\"author\":\"Joomla! Project\",\"copyright\":\"Copyright (C) 2005 - 2017 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.0.0\",\"description\":\"MOD_CUSTOM_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"mod_custom\"}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(301, 0, 'mod_feed', 'module', 'mod_feed', '', 1, 1, 1, 0, '{\"name\":\"mod_feed\",\"type\":\"module\",\"creationDate\":\"July 2005\",\"author\":\"Joomla! Project\",\"copyright\":\"Copyright (C) 2005 - 2017 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.0.0\",\"description\":\"MOD_FEED_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"mod_feed\"}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(302, 0, 'mod_latest', 'module', 'mod_latest', '', 1, 1, 1, 0, '{\"name\":\"mod_latest\",\"type\":\"module\",\"creationDate\":\"July 2004\",\"author\":\"Joomla! Project\",\"copyright\":\"Copyright (C) 2005 - 2017 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.0.0\",\"description\":\"MOD_LATEST_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"mod_latest\"}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(303, 0, 'mod_logged', 'module', 'mod_logged', '', 1, 1, 1, 0, '{\"name\":\"mod_logged\",\"type\":\"module\",\"creationDate\":\"January 2005\",\"author\":\"Joomla! Project\",\"copyright\":\"Copyright (C) 2005 - 2017 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.0.0\",\"description\":\"MOD_LOGGED_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"mod_logged\"}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(304, 0, 'mod_login', 'module', 'mod_login', '', 1, 1, 1, 1, '{\"name\":\"mod_login\",\"type\":\"module\",\"creationDate\":\"March 2005\",\"author\":\"Joomla! Project\",\"copyright\":\"Copyright (C) 2005 - 2017 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.0.0\",\"description\":\"MOD_LOGIN_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"mod_login\"}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(305, 0, 'mod_menu', 'module', 'mod_menu', '', 1, 1, 1, 0, '{\"name\":\"mod_menu\",\"type\":\"module\",\"creationDate\":\"March 2006\",\"author\":\"Joomla! Project\",\"copyright\":\"Copyright (C) 2005 - 2017 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.0.0\",\"description\":\"MOD_MENU_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"mod_menu\"}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(307, 0, 'mod_popular', 'module', 'mod_popular', '', 1, 1, 1, 0, '{\"name\":\"mod_popular\",\"type\":\"module\",\"creationDate\":\"July 2004\",\"author\":\"Joomla! Project\",\"copyright\":\"Copyright (C) 2005 - 2017 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.0.0\",\"description\":\"MOD_POPULAR_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"mod_popular\"}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(308, 0, 'mod_quickicon', 'module', 'mod_quickicon', '', 1, 1, 1, 1, '{\"name\":\"mod_quickicon\",\"type\":\"module\",\"creationDate\":\"Nov 2005\",\"author\":\"Joomla! Project\",\"copyright\":\"Copyright (C) 2005 - 2017 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.0.0\",\"description\":\"MOD_QUICKICON_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"mod_quickicon\"}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(309, 0, 'mod_status', 'module', 'mod_status', '', 1, 1, 1, 0, '{\"name\":\"mod_status\",\"type\":\"module\",\"creationDate\":\"Feb 2006\",\"author\":\"Joomla! Project\",\"copyright\":\"(C) 2005 - 2017 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.0.0\",\"description\":\"MOD_STATUS_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"mod_status\"}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(310, 0, 'mod_submenu', 'module', 'mod_submenu', '', 1, 1, 1, 0, '{\"name\":\"mod_submenu\",\"type\":\"module\",\"creationDate\":\"Feb 2006\",\"author\":\"Joomla! Project\",\"copyright\":\"Copyright (C) 2005 - 2017 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.0.0\",\"description\":\"MOD_SUBMENU_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"mod_submenu\"}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(311, 0, 'mod_title', 'module', 'mod_title', '', 1, 1, 1, 0, '{\"name\":\"mod_title\",\"type\":\"module\",\"creationDate\":\"Nov 2005\",\"author\":\"Joomla! Project\",\"copyright\":\"Copyright (C) 2005 - 2017 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.0.0\",\"description\":\"MOD_TITLE_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"mod_title\"}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(312, 0, 'mod_toolbar', 'module', 'mod_toolbar', '', 1, 1, 1, 1, '{\"name\":\"mod_toolbar\",\"type\":\"module\",\"creationDate\":\"Nov 2005\",\"author\":\"Joomla! Project\",\"copyright\":\"Copyright (C) 2005 - 2017 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.0.0\",\"description\":\"MOD_TOOLBAR_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"mod_toolbar\"}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(313, 0, 'mod_multilangstatus', 'module', 'mod_multilangstatus', '', 1, 1, 1, 0, '{\"name\":\"mod_multilangstatus\",\"type\":\"module\",\"creationDate\":\"September 2011\",\"author\":\"Joomla! Project\",\"copyright\":\"Copyright (C) 2005 - 2017 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.0.0\",\"description\":\"MOD_MULTILANGSTATUS_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"mod_multilangstatus\"}', '{\"cache\":\"0\"}', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(314, 0, 'mod_version', 'module', 'mod_version', '', 1, 1, 1, 0, '{\"name\":\"mod_version\",\"type\":\"module\",\"creationDate\":\"January 2012\",\"author\":\"Joomla! Project\",\"copyright\":\"Copyright (C) 2005 - 2017 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.0.0\",\"description\":\"MOD_VERSION_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"mod_version\"}', '{\"format\":\"short\",\"product\":\"1\",\"cache\":\"0\"}', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(315, 0, 'mod_stats_admin', 'module', 'mod_stats_admin', '', 1, 1, 1, 0, '{\"name\":\"mod_stats_admin\",\"type\":\"module\",\"creationDate\":\"July 2004\",\"author\":\"Joomla! Project\",\"copyright\":\"Copyright (C) 2005 - 2017 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.0.0\",\"description\":\"MOD_STATS_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"mod_stats_admin\"}', '{\"serverinfo\":\"0\",\"siteinfo\":\"0\",\"counter\":\"0\",\"increase\":\"0\",\"cache\":\"1\",\"cache_time\":\"900\",\"cachemode\":\"static\"}', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(316, 0, 'mod_tags_popular', 'module', 'mod_tags_popular', '', 0, 1, 1, 0, '{\"name\":\"mod_tags_popular\",\"type\":\"module\",\"creationDate\":\"January 2013\",\"author\":\"Joomla! Project\",\"copyright\":\"Copyright (C) 2005 - 2017 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.1.0\",\"description\":\"MOD_TAGS_POPULAR_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"mod_tags_popular\"}', '{\"maximum\":\"5\",\"timeframe\":\"alltime\",\"owncache\":\"1\"}', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(317, 0, 'mod_tags_similar', 'module', 'mod_tags_similar', '', 0, 1, 1, 0, '{\"name\":\"mod_tags_similar\",\"type\":\"module\",\"creationDate\":\"January 2013\",\"author\":\"Joomla! Project\",\"copyright\":\"Copyright (C) 2005 - 2017 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.1.0\",\"description\":\"MOD_TAGS_SIMILAR_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"mod_tags_similar\"}', '{\"maximum\":\"5\",\"matchtype\":\"any\",\"owncache\":\"1\"}', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(318, 0, 'mod_sampledata', 'module', 'mod_sampledata', '', 1, 0, 1, 0, '{\"name\":\"mod_sampledata\",\"type\":\"module\",\"creationDate\":\"July 2017\",\"author\":\"Joomla! Project\",\"copyright\":\"Copyright (C) 2005 - 2017 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.8.0\",\"description\":\"MOD_SAMPLEDATA_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"mod_sampledata\"}', '{}', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(400, 0, 'plg_authentication_gmail', 'plugin', 'gmail', 'authentication', 0, 0, 1, 0, '{\"name\":\"plg_authentication_gmail\",\"type\":\"plugin\",\"creationDate\":\"February 2006\",\"author\":\"Joomla! Project\",\"copyright\":\"Copyright (C) 2005 - 2017 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.0.0\",\"description\":\"PLG_GMAIL_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"gmail\"}', '{\"applysuffix\":\"0\",\"suffix\":\"\",\"verifypeer\":\"1\",\"user_blacklist\":\"\"}', '', '', 0, '0000-00-00 00:00:00', 1, 0),
(401, 0, 'plg_authentication_joomla', 'plugin', 'joomla', 'authentication', 0, 1, 1, 1, '{\"name\":\"plg_authentication_joomla\",\"type\":\"plugin\",\"creationDate\":\"November 2005\",\"author\":\"Joomla! Project\",\"copyright\":\"Copyright (C) 2005 - 2017 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.0.0\",\"description\":\"PLG_AUTH_JOOMLA_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"joomla\"}', '{}', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(402, 0, 'plg_authentication_ldap', 'plugin', 'ldap', 'authentication', 0, 0, 1, 0, '{\"name\":\"plg_authentication_ldap\",\"type\":\"plugin\",\"creationDate\":\"November 2005\",\"author\":\"Joomla! Project\",\"copyright\":\"Copyright (C) 2005 - 2017 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.0.0\",\"description\":\"PLG_LDAP_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"ldap\"}', '{\"host\":\"\",\"port\":\"389\",\"use_ldapV3\":\"0\",\"negotiate_tls\":\"0\",\"no_referrals\":\"0\",\"auth_method\":\"bind\",\"base_dn\":\"\",\"search_string\":\"\",\"users_dn\":\"\",\"username\":\"admin\",\"password\":\"bobby7\",\"ldap_fullname\":\"fullName\",\"ldap_email\":\"mail\",\"ldap_uid\":\"uid\"}', '', '', 0, '0000-00-00 00:00:00', 3, 0),
(403, 0, 'plg_content_contact', 'plugin', 'contact', 'content', 0, 1, 1, 0, '{\"name\":\"plg_content_contact\",\"type\":\"plugin\",\"creationDate\":\"January 2014\",\"author\":\"Joomla! Project\",\"copyright\":\"Copyright (C) 2005 - 2017 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.2.2\",\"description\":\"PLG_CONTENT_CONTACT_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"contact\"}', '', '', '', 0, '0000-00-00 00:00:00', 1, 0),
(404, 0, 'plg_content_emailcloak', 'plugin', 'emailcloak', 'content', 0, 1, 1, 0, '{\"name\":\"plg_content_emailcloak\",\"type\":\"plugin\",\"creationDate\":\"November 2005\",\"author\":\"Joomla! Project\",\"copyright\":\"Copyright (C) 2005 - 2017 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.0.0\",\"description\":\"PLG_CONTENT_EMAILCLOAK_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"emailcloak\"}', '{\"mode\":\"1\"}', '', '', 0, '0000-00-00 00:00:00', 1, 0),
(405, 0, 'plg_content_geshi', 'plugin', 'geshi', 'content', 0, 0, 1, 0, '{\"legacy\":false,\"name\":\"plg_content_geshi\",\"type\":\"plugin\",\"creationDate\":\"November 2005\",\"author\":\"Joomla! Project\",\"copyright\":\"Copyright (C) 2005 - 2013 Open Source Matters. All rights reserved.\",\"authorEmail\":\"\",\"authorUrl\":\"qbnz.com\\/highlighter\",\"version\":\"2.5.0\",\"description\":\"PLG_CONTENT_GESHI_XML_DESCRIPTION\",\"group\":\"\"}', '{}', '', '', 0, '0000-00-00 00:00:00', 2, 0),
(406, 0, 'plg_content_loadmodule', 'plugin', 'loadmodule', 'content', 0, 1, 1, 0, '{\"name\":\"plg_content_loadmodule\",\"type\":\"plugin\",\"creationDate\":\"November 2005\",\"author\":\"Joomla! Project\",\"copyright\":\"Copyright (C) 2005 - 2017 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.0.0\",\"description\":\"PLG_LOADMODULE_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"loadmodule\"}', '{\"style\":\"xhtml\"}', '', '', 0, '2011-09-18 15:22:50', 0, 0);
INSERT INTO `t41uf_extensions` (`extension_id`, `package_id`, `name`, `type`, `element`, `folder`, `client_id`, `enabled`, `access`, `protected`, `manifest_cache`, `params`, `custom_data`, `system_data`, `checked_out`, `checked_out_time`, `ordering`, `state`) VALUES
(407, 0, 'plg_content_pagebreak', 'plugin', 'pagebreak', 'content', 0, 1, 1, 0, '{\"name\":\"plg_content_pagebreak\",\"type\":\"plugin\",\"creationDate\":\"November 2005\",\"author\":\"Joomla! Project\",\"copyright\":\"Copyright (C) 2005 - 2017 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.0.0\",\"description\":\"PLG_CONTENT_PAGEBREAK_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"pagebreak\"}', '{\"title\":\"1\",\"multipage_toc\":\"1\",\"showall\":\"1\"}', '', '', 0, '0000-00-00 00:00:00', 4, 0),
(408, 0, 'plg_content_pagenavigation', 'plugin', 'pagenavigation', 'content', 0, 1, 1, 0, '{\"name\":\"plg_content_pagenavigation\",\"type\":\"plugin\",\"creationDate\":\"January 2006\",\"author\":\"Joomla! Project\",\"copyright\":\"Copyright (C) 2005 - 2017 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.0.0\",\"description\":\"PLG_PAGENAVIGATION_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"pagenavigation\"}', '{\"position\":\"1\",\"relative\":\"1\",\"display\":\"0\"}', '', '', 0, '0000-00-00 00:00:00', 5, 0),
(409, 0, 'plg_content_vote', 'plugin', 'vote', 'content', 0, 1, 1, 0, '{\"name\":\"plg_content_vote\",\"type\":\"plugin\",\"creationDate\":\"November 2005\",\"author\":\"Joomla! Project\",\"copyright\":\"Copyright (C) 2005 - 2017 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.0.0\",\"description\":\"PLG_VOTE_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"vote\"}', '{}', '', '', 0, '0000-00-00 00:00:00', 6, 0),
(410, 0, 'plg_editors_codemirror', 'plugin', 'codemirror', 'editors', 0, 1, 1, 1, '{\"name\":\"plg_editors_codemirror\",\"type\":\"plugin\",\"creationDate\":\"28 March 2011\",\"author\":\"Marijn Haverbeke\",\"copyright\":\"Copyright (C) 2014 - 2017 by Marijn Haverbeke <marijnh@gmail.com> and others\",\"authorEmail\":\"marijnh@gmail.com\",\"authorUrl\":\"http:\\/\\/codemirror.net\\/\",\"version\":\"5.30.0\",\"description\":\"PLG_CODEMIRROR_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"codemirror\"}', '{\"lineNumbers\":\"1\",\"codeFolding\":\"1\",\"markerGutter\":\"0\",\"lineWrapping\":\"1\",\"activeLine\":\"1\",\"selectionMatches\":\"0\",\"matchTags\":\"1\",\"matchBrackets\":\"1\",\"autoCloseTags\":\"1\",\"autoCloseBrackets\":\"1\",\"autoFocus\":\"1\",\"vimKeyBinding\":\"0\",\"fullScreen\":\"F10\",\"basePath\":\"media\\/editors\\/codemirror\\/\",\"modePath\":\"media\\/editors\\/codemirror\\/mode\\/%N\\/%N\",\"theme\":\"dracula\",\"activeLineColor\":\"#a4c2eb\",\"highlightMatchColor\":\"#fa542f\",\"fontFamily\":\"0\",\"fontSize\":13,\"lineHeight\":\"1.2\",\"scrollbarStyle\":\"native\"}', '', '', 0, '0000-00-00 00:00:00', 1, 0),
(411, 0, 'plg_editors_none', 'plugin', 'none', 'editors', 0, 1, 1, 1, '{\"name\":\"plg_editors_none\",\"type\":\"plugin\",\"creationDate\":\"September 2005\",\"author\":\"Joomla! Project\",\"copyright\":\"Copyright (C) 2005 - 2017 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.0.0\",\"description\":\"PLG_NONE_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"none\"}', '{}', '', '', 0, '0000-00-00 00:00:00', 2, 0),
(412, 0, 'plg_editors_tinymce', 'plugin', 'tinymce', 'editors', 0, 1, 1, 0, '{\"name\":\"plg_editors_tinymce\",\"type\":\"plugin\",\"creationDate\":\"2005-2017\",\"author\":\"Ephox Corporation\",\"copyright\":\"Ephox Corporation\",\"authorEmail\":\"N\\/A\",\"authorUrl\":\"http:\\/\\/www.tinymce.com\",\"version\":\"4.5.7\",\"description\":\"PLG_TINY_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"tinymce\"}', '{\"mode\":\"2\",\"skin\":\"0\",\"entity_encoding\":\"raw\",\"lang_mode\":\"0\",\"lang_code\":\"en\",\"text_direction\":\"ltr\",\"content_css\":\"1\",\"content_css_custom\":\"\",\"relative_urls\":\"1\",\"newlines\":\"0\",\"invalid_elements\":\"script,applet,iframe\",\"extended_elements\":\"\",\"toolbar\":\"top\",\"toolbar_align\":\"left\",\"html_height\":\"550\",\"html_width\":\"750\",\"resizing\":\"true\",\"resize_horizontal\":\"false\",\"element_path\":\"1\",\"fonts\":\"1\",\"paste\":\"1\",\"searchreplace\":\"1\",\"insertdate\":\"1\",\"format_date\":\"%Y-%m-%d\",\"inserttime\":\"1\",\"format_time\":\"%H:%M:%S\",\"colors\":\"1\",\"table\":\"1\",\"smilies\":\"1\",\"media\":\"1\",\"hr\":\"1\",\"directionality\":\"1\",\"fullscreen\":\"1\",\"style\":\"1\",\"layer\":\"1\",\"xhtmlxtras\":\"1\",\"visualchars\":\"1\",\"visualblocks\":\"1\",\"nonbreaking\":\"1\",\"template\":\"1\",\"blockquote\":\"1\",\"wordcount\":\"1\",\"advimage\":\"1\",\"advlink\":\"1\",\"advlist\":\"1\",\"autosave\":\"1\",\"contextmenu\":\"1\",\"inlinepopups\":\"1\",\"custom_plugin\":\"\",\"custom_button\":\"\"}', '', '', 0, '0000-00-00 00:00:00', 3, 0),
(413, 0, 'plg_editors-xtd_article', 'plugin', 'article', 'editors-xtd', 0, 1, 1, 0, '{\"name\":\"plg_editors-xtd_article\",\"type\":\"plugin\",\"creationDate\":\"October 2009\",\"author\":\"Joomla! Project\",\"copyright\":\"Copyright (C) 2005 - 2017 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.0.0\",\"description\":\"PLG_ARTICLE_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"article\"}', '{}', '', '', 0, '0000-00-00 00:00:00', 1, 0),
(414, 0, 'plg_editors-xtd_image', 'plugin', 'image', 'editors-xtd', 0, 1, 1, 0, '{\"name\":\"plg_editors-xtd_image\",\"type\":\"plugin\",\"creationDate\":\"August 2004\",\"author\":\"Joomla! Project\",\"copyright\":\"Copyright (C) 2005 - 2017 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.0.0\",\"description\":\"PLG_IMAGE_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"image\"}', '{}', '', '', 0, '0000-00-00 00:00:00', 2, 0),
(415, 0, 'plg_editors-xtd_pagebreak', 'plugin', 'pagebreak', 'editors-xtd', 0, 1, 1, 0, '{\"name\":\"plg_editors-xtd_pagebreak\",\"type\":\"plugin\",\"creationDate\":\"August 2004\",\"author\":\"Joomla! Project\",\"copyright\":\"Copyright (C) 2005 - 2017 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.0.0\",\"description\":\"PLG_EDITORSXTD_PAGEBREAK_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"pagebreak\"}', '{}', '', '', 0, '0000-00-00 00:00:00', 3, 0),
(416, 0, 'plg_editors-xtd_readmore', 'plugin', 'readmore', 'editors-xtd', 0, 1, 1, 0, '{\"name\":\"plg_editors-xtd_readmore\",\"type\":\"plugin\",\"creationDate\":\"March 2006\",\"author\":\"Joomla! Project\",\"copyright\":\"Copyright (C) 2005 - 2017 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.0.0\",\"description\":\"PLG_READMORE_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"readmore\"}', '{}', '', '', 0, '0000-00-00 00:00:00', 4, 0),
(417, 0, 'plg_search_categories', 'plugin', 'categories', 'search', 0, 1, 1, 0, '{\"name\":\"plg_search_categories\",\"type\":\"plugin\",\"creationDate\":\"November 2005\",\"author\":\"Joomla! Project\",\"copyright\":\"Copyright (C) 2005 - 2017 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.0.0\",\"description\":\"PLG_SEARCH_CATEGORIES_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"categories\"}', '{\"search_limit\":\"50\",\"search_content\":\"1\",\"search_archived\":\"1\"}', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(418, 0, 'plg_search_contacts', 'plugin', 'contacts', 'search', 0, 1, 1, 0, '{\"name\":\"plg_search_contacts\",\"type\":\"plugin\",\"creationDate\":\"November 2005\",\"author\":\"Joomla! Project\",\"copyright\":\"Copyright (C) 2005 - 2017 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.0.0\",\"description\":\"PLG_SEARCH_CONTACTS_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"contacts\"}', '{\"search_limit\":\"50\",\"search_content\":\"1\",\"search_archived\":\"1\"}', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(419, 0, 'plg_search_content', 'plugin', 'content', 'search', 0, 1, 1, 0, '{\"name\":\"plg_search_content\",\"type\":\"plugin\",\"creationDate\":\"November 2005\",\"author\":\"Joomla! Project\",\"copyright\":\"Copyright (C) 2005 - 2017 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.0.0\",\"description\":\"PLG_SEARCH_CONTENT_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"content\"}', '{\"search_limit\":\"50\",\"search_content\":\"1\",\"search_archived\":\"1\"}', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(420, 0, 'plg_search_newsfeeds', 'plugin', 'newsfeeds', 'search', 0, 1, 1, 0, '{\"name\":\"plg_search_newsfeeds\",\"type\":\"plugin\",\"creationDate\":\"November 2005\",\"author\":\"Joomla! Project\",\"copyright\":\"Copyright (C) 2005 - 2017 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.0.0\",\"description\":\"PLG_SEARCH_NEWSFEEDS_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"newsfeeds\"}', '{\"search_limit\":\"50\",\"search_content\":\"1\",\"search_archived\":\"1\"}', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(421, 801, 'plg_search_weblinks', 'plugin', 'weblinks', 'search', 0, 1, 1, 0, '{\"name\":\"plg_search_weblinks\",\"type\":\"plugin\",\"creationDate\":\"2017-03-08\",\"author\":\"Joomla! Project\",\"copyright\":\"Copyright (C) 2005 - 2017 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.6.0\",\"description\":\"PLG_SEARCH_WEBLINKS_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"weblinks\"}', '{\"search_limit\":\"50\",\"search_content\":\"1\",\"search_archived\":\"1\"}', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(422, 0, 'plg_system_languagefilter', 'plugin', 'languagefilter', 'system', 0, 0, 1, 1, '{\"name\":\"plg_system_languagefilter\",\"type\":\"plugin\",\"creationDate\":\"July 2010\",\"author\":\"Joomla! Project\",\"copyright\":\"Copyright (C) 2005 - 2017 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.0.0\",\"description\":\"PLG_SYSTEM_LANGUAGEFILTER_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"languagefilter\"}', '{}', '', '', 0, '0000-00-00 00:00:00', 1, 0),
(423, 0, 'plg_system_p3p', 'plugin', 'p3p', 'system', 0, 1, 1, 0, '{\"name\":\"plg_system_p3p\",\"type\":\"plugin\",\"creationDate\":\"September 2010\",\"author\":\"Joomla! Project\",\"copyright\":\"Copyright (C) 2005 - 2017 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.0.0\",\"description\":\"PLG_P3P_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"p3p\"}', '{\"headers\":\"NOI ADM DEV PSAi COM NAV OUR OTRo STP IND DEM\"}', '', '', 0, '0000-00-00 00:00:00', 2, 0),
(424, 0, 'plg_system_cache', 'plugin', 'cache', 'system', 0, 0, 1, 1, '{\"name\":\"plg_system_cache\",\"type\":\"plugin\",\"creationDate\":\"February 2007\",\"author\":\"Joomla! Project\",\"copyright\":\"Copyright (C) 2005 - 2017 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.0.0\",\"description\":\"PLG_CACHE_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"cache\"}', '{\"browsercache\":\"0\",\"cachetime\":\"15\"}', '', '', 0, '0000-00-00 00:00:00', 9, 0),
(425, 0, 'plg_system_debug', 'plugin', 'debug', 'system', 0, 1, 1, 0, '{\"name\":\"plg_system_debug\",\"type\":\"plugin\",\"creationDate\":\"December 2006\",\"author\":\"Joomla! Project\",\"copyright\":\"Copyright (C) 2005 - 2017 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.0.0\",\"description\":\"PLG_DEBUG_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"debug\"}', '{\"profile\":\"1\",\"queries\":\"1\",\"memory\":\"1\",\"language_files\":\"1\",\"language_strings\":\"1\",\"strip-first\":\"1\",\"strip-prefix\":\"\",\"strip-suffix\":\"\"}', '', '', 0, '0000-00-00 00:00:00', 4, 0),
(426, 0, 'plg_system_log', 'plugin', 'log', 'system', 0, 1, 1, 1, '{\"name\":\"plg_system_log\",\"type\":\"plugin\",\"creationDate\":\"April 2007\",\"author\":\"Joomla! Project\",\"copyright\":\"Copyright (C) 2005 - 2017 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.0.0\",\"description\":\"PLG_LOG_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"log\"}', '{}', '', '', 0, '0000-00-00 00:00:00', 5, 0),
(427, 0, 'plg_system_redirect', 'plugin', 'redirect', 'system', 0, 1, 1, 1, '{\"name\":\"plg_system_redirect\",\"type\":\"plugin\",\"creationDate\":\"April 2009\",\"author\":\"Joomla! Project\",\"copyright\":\"Copyright (C) 2005 - 2017 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.0.0\",\"description\":\"PLG_SYSTEM_REDIRECT_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"redirect\"}', '{}', '', '', 0, '0000-00-00 00:00:00', 6, 0),
(428, 0, 'plg_system_remember', 'plugin', 'remember', 'system', 0, 1, 1, 1, '{\"name\":\"plg_system_remember\",\"type\":\"plugin\",\"creationDate\":\"April 2007\",\"author\":\"Joomla! Project\",\"copyright\":\"Copyright (C) 2005 - 2017 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.0.0\",\"description\":\"PLG_REMEMBER_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"remember\"}', '{}', '', '', 0, '0000-00-00 00:00:00', 7, 0),
(429, 0, 'plg_system_sef', 'plugin', 'sef', 'system', 0, 1, 1, 0, '{\"name\":\"plg_system_sef\",\"type\":\"plugin\",\"creationDate\":\"December 2007\",\"author\":\"Joomla! Project\",\"copyright\":\"Copyright (C) 2005 - 2017 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.0.0\",\"description\":\"PLG_SEF_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"sef\"}', '{}', '', '', 0, '0000-00-00 00:00:00', 8, 0),
(430, 0, 'plg_system_logout', 'plugin', 'logout', 'system', 0, 1, 1, 1, '{\"name\":\"plg_system_logout\",\"type\":\"plugin\",\"creationDate\":\"April 2009\",\"author\":\"Joomla! Project\",\"copyright\":\"Copyright (C) 2005 - 2017 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.0.0\",\"description\":\"PLG_SYSTEM_LOGOUT_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"logout\"}', '{}', '', '', 0, '0000-00-00 00:00:00', 3, 0),
(431, 0, 'plg_user_contactcreator', 'plugin', 'contactcreator', 'user', 0, 0, 1, 0, '{\"name\":\"plg_user_contactcreator\",\"type\":\"plugin\",\"creationDate\":\"August 2009\",\"author\":\"Joomla! Project\",\"copyright\":\"(C) 2005 - 2017 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.0.0\",\"description\":\"PLG_CONTACTCREATOR_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"contactcreator\"}', '{\"autowebpage\":\"\",\"category\":\"34\",\"autopublish\":\"0\"}', '', '', 0, '0000-00-00 00:00:00', 1, 0),
(432, 0, 'plg_user_joomla', 'plugin', 'joomla', 'user', 0, 1, 1, 0, '{\"name\":\"plg_user_joomla\",\"type\":\"plugin\",\"creationDate\":\"December 2006\",\"author\":\"Joomla! Project\",\"copyright\":\"(C) 2005 - 2017 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.0.0\",\"description\":\"PLG_USER_JOOMLA_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"joomla\"}', '{\"autoregister\":\"1\"}', '', '', 0, '0000-00-00 00:00:00', 2, 0),
(433, 0, 'plg_user_profile', 'plugin', 'profile', 'user', 0, 0, 1, 0, '{\"name\":\"plg_user_profile\",\"type\":\"plugin\",\"creationDate\":\"January 2008\",\"author\":\"Joomla! Project\",\"copyright\":\"(C) 2005 - 2017 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.0.0\",\"description\":\"PLG_USER_PROFILE_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"profile\"}', '{\"register-require_address1\":\"1\",\"register-require_address2\":\"1\",\"register-require_city\":\"1\",\"register-require_region\":\"1\",\"register-require_country\":\"1\",\"register-require_postal_code\":\"1\",\"register-require_phone\":\"1\",\"register-require_website\":\"1\",\"register-require_favoritebook\":\"1\",\"register-require_aboutme\":\"1\",\"register-require_tos\":\"1\",\"register-require_dob\":\"1\",\"profile-require_address1\":\"1\",\"profile-require_address2\":\"1\",\"profile-require_city\":\"1\",\"profile-require_region\":\"1\",\"profile-require_country\":\"1\",\"profile-require_postal_code\":\"1\",\"profile-require_phone\":\"1\",\"profile-require_website\":\"1\",\"profile-require_favoritebook\":\"1\",\"profile-require_aboutme\":\"1\",\"profile-require_tos\":\"1\",\"profile-require_dob\":\"1\"}', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(434, 0, 'plg_extension_joomla', 'plugin', 'joomla', 'extension', 0, 1, 1, 1, '{\"name\":\"plg_extension_joomla\",\"type\":\"plugin\",\"creationDate\":\"May 2010\",\"author\":\"Joomla! Project\",\"copyright\":\"Copyright (C) 2005 - 2017 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.0.0\",\"description\":\"PLG_EXTENSION_JOOMLA_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"joomla\"}', '{}', '', '', 0, '0000-00-00 00:00:00', 1, 0),
(435, 0, 'plg_content_joomla', 'plugin', 'joomla', 'content', 0, 1, 1, 0, '{\"name\":\"plg_content_joomla\",\"type\":\"plugin\",\"creationDate\":\"November 2010\",\"author\":\"Joomla! Project\",\"copyright\":\"Copyright (C) 2005 - 2017 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.0.0\",\"description\":\"PLG_CONTENT_JOOMLA_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"joomla\"}', '{}', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(436, 0, 'plg_system_languagecode', 'plugin', 'languagecode', 'system', 0, 0, 1, 0, '{\"name\":\"plg_system_languagecode\",\"type\":\"plugin\",\"creationDate\":\"November 2011\",\"author\":\"Joomla! Project\",\"copyright\":\"Copyright (C) 2005 - 2017 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.0.0\",\"description\":\"PLG_SYSTEM_LANGUAGECODE_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"languagecode\"}', '{}', '', '', 0, '0000-00-00 00:00:00', 10, 0),
(437, 0, 'plg_quickicon_joomlaupdate', 'plugin', 'joomlaupdate', 'quickicon', 0, 1, 1, 1, '{\"name\":\"plg_quickicon_joomlaupdate\",\"type\":\"plugin\",\"creationDate\":\"August 2011\",\"author\":\"Joomla! Project\",\"copyright\":\"Copyright (C) 2005 - 2017 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.0.0\",\"description\":\"PLG_QUICKICON_JOOMLAUPDATE_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"joomlaupdate\"}', '{}', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(438, 0, 'plg_quickicon_extensionupdate', 'plugin', 'extensionupdate', 'quickicon', 0, 1, 1, 1, '{\"name\":\"plg_quickicon_extensionupdate\",\"type\":\"plugin\",\"creationDate\":\"August 2011\",\"author\":\"Joomla! Project\",\"copyright\":\"Copyright (C) 2005 - 2017 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.0.0\",\"description\":\"PLG_QUICKICON_EXTENSIONUPDATE_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"extensionupdate\"}', '{}', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(439, 0, 'plg_captcha_recaptcha', 'plugin', 'recaptcha', 'captcha', 0, 1, 1, 0, '{\"name\":\"plg_captcha_recaptcha\",\"type\":\"plugin\",\"creationDate\":\"December 2011\",\"author\":\"Joomla! Project\",\"copyright\":\"Copyright (C) 2005 - 2017 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.4.0\",\"description\":\"PLG_CAPTCHA_RECAPTCHA_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"recaptcha\"}', '{\"public_key\":\"\",\"private_key\":\"\",\"theme\":\"clean\"}', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(440, 0, 'plg_system_highlight', 'plugin', 'highlight', 'system', 0, 1, 1, 0, '{\"name\":\"plg_system_highlight\",\"type\":\"plugin\",\"creationDate\":\"August 2011\",\"author\":\"Joomla! Project\",\"copyright\":\"(C) 2005 - 2017 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.0.0\",\"description\":\"PLG_SYSTEM_HIGHLIGHT_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"highlight\"}', '{}', '', '', 0, '0000-00-00 00:00:00', 7, 0),
(441, 0, 'plg_content_finder', 'plugin', 'finder', 'content', 0, 0, 1, 0, '{\"name\":\"plg_content_finder\",\"type\":\"plugin\",\"creationDate\":\"December 2011\",\"author\":\"Joomla! Project\",\"copyright\":\"Copyright (C) 2005 - 2017 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.0.0\",\"description\":\"PLG_CONTENT_FINDER_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"finder\"}', '{}', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(442, 0, 'plg_finder_categories', 'plugin', 'categories', 'finder', 0, 1, 1, 0, '{\"name\":\"plg_finder_categories\",\"type\":\"plugin\",\"creationDate\":\"August 2011\",\"author\":\"Joomla! Project\",\"copyright\":\"(C) 2005 - 2017 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.0.0\",\"description\":\"PLG_FINDER_CATEGORIES_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"categories\"}', '{}', '', '', 0, '0000-00-00 00:00:00', 1, 0),
(443, 0, 'plg_finder_contacts', 'plugin', 'contacts', 'finder', 0, 1, 1, 0, '{\"name\":\"plg_finder_contacts\",\"type\":\"plugin\",\"creationDate\":\"August 2011\",\"author\":\"Joomla! Project\",\"copyright\":\"(C) 2005 - 2017 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.0.0\",\"description\":\"PLG_FINDER_CONTACTS_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"contacts\"}', '{}', '', '', 0, '0000-00-00 00:00:00', 2, 0),
(444, 0, 'plg_finder_content', 'plugin', 'content', 'finder', 0, 1, 1, 0, '{\"name\":\"plg_finder_content\",\"type\":\"plugin\",\"creationDate\":\"August 2011\",\"author\":\"Joomla! Project\",\"copyright\":\"(C) 2005 - 2017 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.0.0\",\"description\":\"PLG_FINDER_CONTENT_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"content\"}', '{}', '', '', 0, '0000-00-00 00:00:00', 3, 0),
(445, 0, 'plg_finder_newsfeeds', 'plugin', 'newsfeeds', 'finder', 0, 1, 1, 0, '{\"name\":\"plg_finder_newsfeeds\",\"type\":\"plugin\",\"creationDate\":\"August 2011\",\"author\":\"Joomla! Project\",\"copyright\":\"(C) 2005 - 2017 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.0.0\",\"description\":\"PLG_FINDER_NEWSFEEDS_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"newsfeeds\"}', '{}', '', '', 0, '0000-00-00 00:00:00', 4, 0),
(446, 801, 'plg_finder_weblinks', 'plugin', 'weblinks', 'finder', 0, 1, 1, 0, '{\"name\":\"plg_finder_weblinks\",\"type\":\"plugin\",\"creationDate\":\"2017-03-08\",\"author\":\"Joomla! Project\",\"copyright\":\"(C) 2005 - 2017 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.6.0\",\"description\":\"PLG_FINDER_WEBLINKS_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"weblinks\"}', '{}', '', '', 0, '0000-00-00 00:00:00', 5, 0),
(447, 0, 'plg_finder_tags', 'plugin', 'tags', 'finder', 0, 1, 1, 0, '{\"name\":\"plg_finder_tags\",\"type\":\"plugin\",\"creationDate\":\"February 2013\",\"author\":\"Joomla! Project\",\"copyright\":\"(C) 2005 - 2017 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.0.0\",\"description\":\"PLG_FINDER_TAGS_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"tags\"}', '{}', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(448, 0, 'plg_twofactorauth_totp', 'plugin', 'totp', 'twofactorauth', 0, 0, 1, 0, '{\"name\":\"plg_twofactorauth_totp\",\"type\":\"plugin\",\"creationDate\":\"August 2013\",\"author\":\"Joomla! Project\",\"copyright\":\"Copyright (C) 2005 - 2017 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.2.0\",\"description\":\"PLG_TWOFACTORAUTH_TOTP_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"totp\"}', '{}', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(449, 0, 'plg_authentication_cookie', 'plugin', 'cookie', 'authentication', 0, 1, 1, 0, '{\"name\":\"plg_authentication_cookie\",\"type\":\"plugin\",\"creationDate\":\"July 2013\",\"author\":\"Joomla! Project\",\"copyright\":\"Copyright (C) 2005 - 2017 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.0.0\",\"description\":\"PLG_AUTH_COOKIE_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"cookie\"}', '{}', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(450, 0, 'plg_twofactorauth_yubikey', 'plugin', 'yubikey', 'twofactorauth', 0, 0, 1, 0, '{\"name\":\"plg_twofactorauth_yubikey\",\"type\":\"plugin\",\"creationDate\":\"September 2013\",\"author\":\"Joomla! Project\",\"copyright\":\"Copyright (C) 2005 - 2017 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.2.0\",\"description\":\"PLG_TWOFACTORAUTH_YUBIKEY_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"yubikey\"}', '{}', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(451, 0, 'plg_search_tags', 'plugin', 'tags', 'search', 0, 0, 1, 0, '{\"name\":\"plg_search_tags\",\"type\":\"plugin\",\"creationDate\":\"March 2014\",\"author\":\"Joomla! Project\",\"copyright\":\"Copyright (C) 2005 - 2017 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.0.0\",\"description\":\"PLG_SEARCH_TAGS_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"tags\"}', '{\"search_limit\":\"50\",\"show_tagged_items\":\"1\"}', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(452, 0, 'plg_system_updatenotification', 'plugin', 'updatenotification', 'system', 0, 1, 1, 0, '{\"name\":\"plg_system_updatenotification\",\"type\":\"plugin\",\"creationDate\":\"May 2015\",\"author\":\"Joomla! Project\",\"copyright\":\"Copyright (C) 2005 - 2017 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.5.0\",\"description\":\"PLG_SYSTEM_UPDATENOTIFICATION_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"updatenotification\"}', '{\"lastrun\":1530858378}', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(453, 0, 'plg_editors-xtd_module', 'plugin', 'module', 'editors-xtd', 0, 1, 1, 0, '{\"name\":\"plg_editors-xtd_module\",\"type\":\"plugin\",\"creationDate\":\"October 2015\",\"author\":\"Joomla! Project\",\"copyright\":\"Copyright (C) 2005 - 2017 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.5.0\",\"description\":\"PLG_MODULE_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"module\"}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(454, 0, 'plg_system_stats', 'plugin', 'stats', 'system', 0, 1, 1, 0, '{\"name\":\"plg_system_stats\",\"type\":\"plugin\",\"creationDate\":\"November 2013\",\"author\":\"Joomla! Project\",\"copyright\":\"Copyright (C) 2005 - 2017 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.5.0\",\"description\":\"PLG_SYSTEM_STATS_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"stats\"}', '{\"mode\":1,\"lastrun\":1530858439,\"unique_id\":\"8f175840a96feb81c784a54b1484790814df4ff2\",\"interval\":12}', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(455, 0, 'plg_installer_packageinstaller', 'plugin', 'packageinstaller', 'installer', 0, 1, 1, 1, '{\"name\":\"plg_installer_packageinstaller\",\"type\":\"plugin\",\"creationDate\":\"May 2016\",\"author\":\"Joomla! Project\",\"copyright\":\"Copyright (C) 2005 - 2017 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.6.0\",\"description\":\"PLG_INSTALLER_PACKAGEINSTALLER_PLUGIN_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"packageinstaller\"}', '', '', '', 0, '0000-00-00 00:00:00', 1, 0),
(456, 0, 'PLG_INSTALLER_FOLDERINSTALLER', 'plugin', 'folderinstaller', 'installer', 0, 1, 1, 1, '{\"name\":\"PLG_INSTALLER_FOLDERINSTALLER\",\"type\":\"plugin\",\"creationDate\":\"May 2016\",\"author\":\"Joomla! Project\",\"copyright\":\"Copyright (C) 2005 - 2017 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.6.0\",\"description\":\"PLG_INSTALLER_FOLDERINSTALLER_PLUGIN_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"folderinstaller\"}', '', '', '', 0, '0000-00-00 00:00:00', 2, 0),
(457, 0, 'PLG_INSTALLER_URLINSTALLER', 'plugin', 'urlinstaller', 'installer', 0, 1, 1, 1, '{\"name\":\"PLG_INSTALLER_URLINSTALLER\",\"type\":\"plugin\",\"creationDate\":\"May 2016\",\"author\":\"Joomla! Project\",\"copyright\":\"Copyright (C) 2005 - 2017 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.6.0\",\"description\":\"PLG_INSTALLER_URLINSTALLER_PLUGIN_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"urlinstaller\"}', '', '', '', 0, '0000-00-00 00:00:00', 3, 0),
(458, 0, 'plg_quickicon_phpversioncheck', 'plugin', 'phpversioncheck', 'quickicon', 0, 1, 1, 1, '{\"name\":\"plg_quickicon_phpversioncheck\",\"type\":\"plugin\",\"creationDate\":\"August 2016\",\"author\":\"Joomla! Project\",\"copyright\":\"Copyright (C) 2005 - 2017 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.7.0\",\"description\":\"PLG_QUICKICON_PHPVERSIONCHECK_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"phpversioncheck\"}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(459, 0, 'plg_editors-xtd_menu', 'plugin', 'menu', 'editors-xtd', 0, 1, 1, 0, '{\"name\":\"plg_editors-xtd_menu\",\"type\":\"plugin\",\"creationDate\":\"August 2016\",\"author\":\"Joomla! Project\",\"copyright\":\"Copyright (C) 2005 - 2017 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.7.0\",\"description\":\"PLG_EDITORS-XTD_MENU_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"menu\"}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(460, 0, 'plg_editors-xtd_contact', 'plugin', 'contact', 'editors-xtd', 0, 1, 1, 0, '{\"name\":\"plg_editors-xtd_contact\",\"type\":\"plugin\",\"creationDate\":\"October 2016\",\"author\":\"Joomla! Project\",\"copyright\":\"Copyright (C) 2005 - 2017 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.7.0\",\"description\":\"PLG_EDITORS-XTD_CONTACT_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"contact\"}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(461, 0, 'plg_system_fields', 'plugin', 'fields', 'system', 0, 1, 1, 0, '{\"name\":\"plg_system_fields\",\"type\":\"plugin\",\"creationDate\":\"March 2016\",\"author\":\"Joomla! Project\",\"copyright\":\"Copyright (C) 2005 - 2017 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.7.0\",\"description\":\"PLG_SYSTEM_FIELDS_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"fields\"}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(462, 0, 'plg_fields_calendar', 'plugin', 'calendar', 'fields', 0, 1, 1, 0, '{\"name\":\"plg_fields_calendar\",\"type\":\"plugin\",\"creationDate\":\"March 2016\",\"author\":\"Joomla! Project\",\"copyright\":\"Copyright (C) 2005 - 2017 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.7.0\",\"description\":\"PLG_FIELDS_CALENDAR_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"calendar\"}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(463, 0, 'plg_fields_checkboxes', 'plugin', 'checkboxes', 'fields', 0, 1, 1, 0, '{\"name\":\"plg_fields_checkboxes\",\"type\":\"plugin\",\"creationDate\":\"March 2016\",\"author\":\"Joomla! Project\",\"copyright\":\"Copyright (C) 2005 - 2017 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.7.0\",\"description\":\"PLG_FIELDS_CHECKBOXES_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"checkboxes\"}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(464, 0, 'plg_fields_color', 'plugin', 'color', 'fields', 0, 1, 1, 0, '{\"name\":\"plg_fields_color\",\"type\":\"plugin\",\"creationDate\":\"March 2016\",\"author\":\"Joomla! Project\",\"copyright\":\"Copyright (C) 2005 - 2017 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.7.0\",\"description\":\"PLG_FIELDS_COLOR_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"color\"}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(465, 0, 'plg_fields_editor', 'plugin', 'editor', 'fields', 0, 1, 1, 0, '{\"name\":\"plg_fields_editor\",\"type\":\"plugin\",\"creationDate\":\"March 2016\",\"author\":\"Joomla! Project\",\"copyright\":\"Copyright (C) 2005 - 2017 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.7.0\",\"description\":\"PLG_FIELDS_EDITOR_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"editor\"}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(466, 0, 'plg_fields_imagelist', 'plugin', 'imagelist', 'fields', 0, 1, 1, 0, '{\"name\":\"plg_fields_imagelist\",\"type\":\"plugin\",\"creationDate\":\"March 2016\",\"author\":\"Joomla! Project\",\"copyright\":\"Copyright (C) 2005 - 2017 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.7.0\",\"description\":\"PLG_FIELDS_IMAGELIST_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"imagelist\"}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(467, 0, 'plg_fields_integer', 'plugin', 'integer', 'fields', 0, 1, 1, 0, '{\"name\":\"plg_fields_integer\",\"type\":\"plugin\",\"creationDate\":\"March 2016\",\"author\":\"Joomla! Project\",\"copyright\":\"Copyright (C) 2005 - 2017 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.7.0\",\"description\":\"PLG_FIELDS_INTEGER_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"integer\"}', '{\"multiple\":\"0\",\"first\":\"1\",\"last\":\"100\",\"step\":\"1\"}', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(468, 0, 'plg_fields_list', 'plugin', 'list', 'fields', 0, 1, 1, 0, '{\"name\":\"plg_fields_list\",\"type\":\"plugin\",\"creationDate\":\"March 2016\",\"author\":\"Joomla! Project\",\"copyright\":\"Copyright (C) 2005 - 2017 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.7.0\",\"description\":\"PLG_FIELDS_LIST_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"list\"}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(469, 0, 'plg_fields_media', 'plugin', 'media', 'fields', 0, 1, 1, 0, '{\"name\":\"plg_fields_media\",\"type\":\"plugin\",\"creationDate\":\"March 2016\",\"author\":\"Joomla! Project\",\"copyright\":\"Copyright (C) 2005 - 2017 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.7.0\",\"description\":\"PLG_FIELDS_MEDIA_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"media\"}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(470, 0, 'plg_fields_radio', 'plugin', 'radio', 'fields', 0, 1, 1, 0, '{\"name\":\"plg_fields_radio\",\"type\":\"plugin\",\"creationDate\":\"March 2016\",\"author\":\"Joomla! Project\",\"copyright\":\"Copyright (C) 2005 - 2017 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.7.0\",\"description\":\"PLG_FIELDS_RADIO_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"radio\"}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(471, 0, 'plg_fields_sql', 'plugin', 'sql', 'fields', 0, 1, 1, 0, '{\"name\":\"plg_fields_sql\",\"type\":\"plugin\",\"creationDate\":\"March 2016\",\"author\":\"Joomla! Project\",\"copyright\":\"Copyright (C) 2005 - 2017 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.7.0\",\"description\":\"PLG_FIELDS_SQL_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"sql\"}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(472, 0, 'plg_fields_text', 'plugin', 'text', 'fields', 0, 1, 1, 0, '{\"name\":\"plg_fields_text\",\"type\":\"plugin\",\"creationDate\":\"March 2016\",\"author\":\"Joomla! Project\",\"copyright\":\"Copyright (C) 2005 - 2017 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.7.0\",\"description\":\"PLG_FIELDS_TEXT_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"text\"}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(473, 0, 'plg_fields_textarea', 'plugin', 'textarea', 'fields', 0, 1, 1, 0, '{\"name\":\"plg_fields_textarea\",\"type\":\"plugin\",\"creationDate\":\"March 2016\",\"author\":\"Joomla! Project\",\"copyright\":\"Copyright (C) 2005 - 2017 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.7.0\",\"description\":\"PLG_FIELDS_TEXTAREA_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"textarea\"}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(474, 0, 'plg_fields_url', 'plugin', 'url', 'fields', 0, 1, 1, 0, '{\"name\":\"plg_fields_url\",\"type\":\"plugin\",\"creationDate\":\"March 2016\",\"author\":\"Joomla! Project\",\"copyright\":\"Copyright (C) 2005 - 2017 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.7.0\",\"description\":\"PLG_FIELDS_URL_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"url\"}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(475, 0, 'plg_fields_user', 'plugin', 'user', 'fields', 0, 1, 1, 0, '{\"name\":\"plg_fields_user\",\"type\":\"plugin\",\"creationDate\":\"March 2016\",\"author\":\"Joomla! Project\",\"copyright\":\"Copyright (C) 2005 - 2017 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.7.0\",\"description\":\"PLG_FIELDS_USER_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"user\"}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(476, 0, 'plg_fields_usergrouplist', 'plugin', 'usergrouplist', 'fields', 0, 1, 1, 0, '{\"name\":\"plg_fields_usergrouplist\",\"type\":\"plugin\",\"creationDate\":\"March 2016\",\"author\":\"Joomla! Project\",\"copyright\":\"Copyright (C) 2005 - 2017 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.7.0\",\"description\":\"PLG_FIELDS_USERGROUPLIST_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"usergrouplist\"}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(477, 0, 'plg_content_fields', 'plugin', 'fields', 'content', 0, 1, 1, 0, '{\"name\":\"plg_content_fields\",\"type\":\"plugin\",\"creationDate\":\"February 2017\",\"author\":\"Joomla! Project\",\"copyright\":\"Copyright (C) 2005 - 2017 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.7.0\",\"description\":\"PLG_CONTENT_FIELDS_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"fields\"}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(478, 0, 'plg_editors-xtd_fields', 'plugin', 'fields', 'editors-xtd', 0, 1, 1, 0, '{\"name\":\"plg_editors-xtd_fields\",\"type\":\"plugin\",\"creationDate\":\"February 2017\",\"author\":\"Joomla! Project\",\"copyright\":\"Copyright (C) 2005 - 2017 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.7.0\",\"description\":\"PLG_EDITORS-XTD_FIELDS_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"fields\"}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(479, 0, 'plg_sampledata_blog', 'plugin', 'blog', 'sampledata', 0, 0, 1, 0, '{\"name\":\"plg_sampledata_blog\",\"type\":\"plugin\",\"creationDate\":\"July 2017\",\"author\":\"Joomla! Project\",\"copyright\":\"Copyright (C) 2005 - 2017 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.8.0\",\"description\":\"PLG_SAMPLEDATA_BLOG_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"blog\"}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(600, 802, 'English (en-GB)', 'language', 'en-GB', '', 0, 1, 1, 1, '{\"name\":\"English (en-GB)\",\"type\":\"language\",\"creationDate\":\"November 2017\",\"author\":\"Joomla! Project\",\"copyright\":\"Copyright (C) 2005 - 2017 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.8.2\",\"description\":\"en-GB site language\",\"group\":\"\"}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(601, 802, 'English (en-GB)', 'language', 'en-GB', '', 1, 1, 1, 1, '{\"name\":\"English (en-GB)\",\"type\":\"language\",\"creationDate\":\"November 2017\",\"author\":\"Joomla! Project\",\"copyright\":\"Copyright (C) 2005 - 2017 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.8.2\",\"description\":\"en-GB administrator language\",\"group\":\"\"}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(700, 0, 'files_joomla', 'file', 'joomla', '', 0, 1, 1, 1, '{\"name\":\"files_joomla\",\"type\":\"file\",\"creationDate\":\"June 2018\",\"author\":\"Joomla! Project\",\"copyright\":\"(C) 2005 - 2018 Open Source Matters. All rights reserved\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.8.10\",\"description\":\"FILES_JOOMLA_XML_DESCRIPTION\",\"group\":\"\"}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(801, 0, 'pkg_weblinks', 'package', 'pkg_weblinks', '', 0, 1, 1, 0, '{\"name\":\"pkg_weblinks\",\"type\":\"package\",\"creationDate\":\"2017-03-08\",\"author\":\"Joomla! Project\",\"copyright\":\"(C) 2005 - 2017 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.6.0\",\"description\":\"PKG_WEBLINKS_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"pkg_weblinks\"}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(802, 0, 'English (en-GB) Language Pack', 'package', 'pkg_en-GB', '', 0, 1, 1, 1, '{\"name\":\"English (en-GB) Language Pack\",\"type\":\"package\",\"creationDate\":\"November 2017\",\"author\":\"Joomla! Project\",\"copyright\":\"Copyright (C) 2005 - 2017 Open Source Matters, Inc. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.8.2.1\",\"description\":\"en-GB language pack\",\"group\":\"\",\"filename\":\"pkg_en-GB\"}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(10000, 0, 'System - Helix Framework', 'plugin', 'helix', 'system', 0, 1, 1, 0, '{\"name\":\"System - Helix Framework\",\"type\":\"plugin\",\"creationDate\":\"March 2011\",\"author\":\"JoomShaper.com\",\"copyright\":\"Copyright (C) 2010 - 2017 JoomShaper. All rights reserved.\",\"authorEmail\":\"support@joomshaper.com\",\"authorUrl\":\"www.joomshaper.com\",\"version\":\"2.5\",\"description\":\"Helix Framework - JoomShaper Template Framework for Joomla 2.5 and 3.X\",\"group\":\"\",\"filename\":\"helix\"}', '{}', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(10004, 0, 'shaper_xeon', 'template', 'shaper_xeon', '', 0, 1, 1, 0, '{\"name\":\"shaper_xeon\",\"type\":\"template\",\"creationDate\":\"Nov 2013\",\"author\":\"JoomShaper.com\",\"copyright\":\"Copyright (C) 2010 - 2015 JoomShaper.com. All rights reserved.\",\"authorEmail\":\"support@joomshaper.com\",\"authorUrl\":\"http:\\/\\/www.joomshaper.com\",\"version\":\"1.4\",\"description\":\"\\n\\t\\t\\n\\t\\t\\tShaper Xeon is the first free complete One Page Modern Template in Joomla. Its perfect to promote \\n            your Portfolio or Business releated work. \\n            This free powerful tempalte comes with Xeon Slider, SP Portfolio, Dynamic Xeon Pricing Table and Xeon Team Carousel module. \\n\\t\\t\\t<h3>Key Features<\\/h3>\\n\\t\\t\\t<ul class=\\\"arrow\\\">\\n\\t\\t\\t\\t<li>Unlimited module positions with the power of unique layout builder<\\/li>\\n\\t\\t\\t\\t<li>Powered by Bootstrap and compatible with Joomla 2.5 and 3.0<\\/li>\\n\\t\\t\\t\\t<li>Three (3) presets with presets manager<\\/li>\\n\\t\\t\\t\\t<li>Uses latest web technologies like html5 and css3<\\/li>\\n\\t\\t\\t\\t<li>Tons of shortcodes in built<\\/li>\\n\\t\\t\\t\\t<li>Built with LESS CSS<\\/li>\\n\\t\\t\\t\\t<li>Megamenu, RTL Support, CSS and JS compression and many more...<\\/li>\\n\\t\\t\\t<\\/ul>\\n\\t\\t\\n\\t\",\"group\":\"\",\"filename\":\"templateDetails\"}', '{\"layout_width\":\"1170\",\"layout_type\":\"responsive\",\"logo_type\":\"image\",\"logo_position\":\"logo\",\"logo_type_text\":\"Helix\",\"logo_type_slogan\":\"Joomla! Templates Framework\",\"logo_width\":\"130\",\"logo_height\":\"50\",\"footer_position\":\"footer1\",\"showcp\":\"1\",\"copyright\":\"Copyright \\u00a9  {year} Shaper Helix - II Demo. All Rights Reserved.\",\"show_helix_logo\":\"1\",\"jcredit\":\"1\",\"credit_link\":\"1\",\"credit_text\":\"Designed by {JoomShaper}\",\"validator\":\"0\",\"showtop\":\"1\",\"totop_position\":\"footer2\",\"preset\":\"preset1\",\"preset1_header\":\"#f8f8f8\",\"preset1_bg\":\"#f2f2f2\",\"preset1_text\":\"#666666\",\"preset1_link\":\"#22b8f0\",\"preset2_header\":\"#eeeeee\",\"preset2_bg\":\"#f5f5f5\",\"preset2_text\":\"#444444\",\"preset2_link\":\"#6d7f1b\",\"preset3_header\":\"#e5ddd5\",\"preset3_bg\":\"#f2f2f2\",\"preset3_text\":\"#333333\",\"preset3_link\":\"#aa824a\",\"menu\":\"mainmenu\",\"menutype\":\"mega\",\"menu_col_width\":\"200\",\"show_menu_image\":\"1\",\"menu_image_position\":\"1\",\"submenu_position\":\"0\",\"init_x\":\"0\",\"init_y\":\"0\",\"sub_x\":\"0\",\"sub_y\":\"0\",\"body_font\":\"\",\"header_font\":\"\",\"header_selectors\":\"\",\"other_font\":\"\",\"cache_time\":\"60\",\"compress_css\":\"0\",\"compress_js\":\"0\",\"enable_ga\":\"0\",\"ga_code\":\"\",\"loadjquery\":\"0\",\"loadfromcdn\":\"0\",\"lessoption\":\"1\",\"hide_component_area\":\"0\"}', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(10005, 0, 'Xeon Slider', 'module', 'mod_xeon_slider', '', 0, 1, 0, 0, '{\"legacy\":false,\"name\":\"Xeon Slider\",\"type\":\"module\",\"creationDate\":\"Nov 2013\",\"author\":\"JoomShaper.com\",\"copyright\":\"Copyright (C) 2010 - 2013 JoomShaper. All rights reserved.\",\"authorEmail\":\"support@joomshaper.com\",\"authorUrl\":\"www.joomshaper.com\",\"version\":\"1.0\",\"description\":\"Slideshow for Xeon Template\",\"group\":\"\"}', '{\"cache\":\"1\",\"cache_time\":\"900\",\"cachemode\":\"static\"}', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(10006, 0, 'SP Portfolio', 'module', 'mod_sp_portfolio', '', 0, 1, 0, 0, '{\"name\":\"SP Portfolio\",\"type\":\"module\",\"creationDate\":\"Jul 2013\",\"author\":\"JoomShaper.com\",\"copyright\":\"Copyright (C) 2010 - 2013 JoomShaper.com. All rights reserved.\",\"authorEmail\":\"support@joomshaper.com\",\"authorUrl\":\"www.joomshaper.com\",\"version\":\"1.4\",\"description\":\"Portfolio moudue for Joomla\",\"group\":\"\",\"filename\":\"mod_sp_portfolio\"}', '{\"catid\":\"\",\"limit\":\"1\",\"column\":\"1\",\"ajaxlimit\":\"2\",\"orderby\":\"a.created\",\"ordering\":\"DESC\",\"show_featured\":\"\",\"module_layout\":\"\",\"show_title\":\"1\",\"linked_title\":\"1\",\"show_category\":\"1\",\"show_url\":\"1\",\"show_introtext\":\"1\",\"show_readmore\":\"1\",\"ajax_loader\":\"1\",\"show_filter\":\"1\",\"load_jquery\":\"1\",\"moduleclass_sfx\":\"\",\"cache\":\"1\",\"cache_time\":\"900\",\"cachemode\":\"itemid\"}', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(10007, 0, 'SP Quick Contact', 'module', 'mod_sp_quickcontact', '', 0, 1, 0, 0, '{\"name\":\"SP Quick Contact\",\"type\":\"module\",\"creationDate\":\"Aug 2011\",\"author\":\"JoomShaper.com\",\"copyright\":\"Copyright (C) 2010 - 2012 JoomShaper.com. All rights reserved.\",\"authorEmail\":\"support@joomshaper.com\",\"authorUrl\":\"www.joomshaper.com\",\"version\":\"1.4\",\"description\":\"SP Quick Contact - Ajax based Quick Contact Module for Joomla!\",\"group\":\"\",\"filename\":\"mod_sp_quickcontact\"}', '{\"email\":\"\",\"success\":\"Email was sent successfully.\",\"failed\":\"Email could not be sent.\",\"cache\":\"1\",\"cache_time\":\"900\",\"cachemode\":\"itemid\"}', '', '', 0, '0000-00-00 00:00:00', 0, 0);
INSERT INTO `t41uf_extensions` (`extension_id`, `package_id`, `name`, `type`, `element`, `folder`, `client_id`, `enabled`, `access`, `protected`, `manifest_cache`, `params`, `custom_data`, `system_data`, `checked_out`, `checked_out_time`, `ordering`, `state`) VALUES
(10008, 0, 'Xeon Pricing Table', 'module', 'mod_xeon_pricing_table', '', 0, 1, 0, 0, '{\"name\":\"Xeon Pricing Table\",\"type\":\"module\",\"creationDate\":\"Nov 2013\",\"author\":\"JoomShaper.com\",\"copyright\":\"Copyright (C) 2010 - 2013 JoomShaper. All rights reserved.\",\"authorEmail\":\"support@joomshaper.com\",\"authorUrl\":\"www.joomshaper.com\",\"version\":\"1.0\",\"description\":\"Pricing Table for Xeon Template\",\"group\":\"\"}', '{\"cache\":\"1\",\"cache_time\":\"900\",\"cachemode\":\"static\"}', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(10009, 0, 'Xeon Team', 'module', 'mod_xeon_team', '', 0, 1, 0, 0, '{\"name\":\"Xeon Team\",\"type\":\"module\",\"creationDate\":\"Nov 2013\",\"author\":\"JoomShaper.com\",\"copyright\":\"Copyright (C) 2010 - 2013 JoomShaper. All rights reserved.\",\"authorEmail\":\"support@joomshaper.com\",\"authorUrl\":\"www.joomshaper.com\",\"version\":\"1.0\",\"description\":\"Team Module for Xeon Template\",\"group\":\"\"}', '{\"cache\":\"1\",\"cache_time\":\"900\",\"cachemode\":\"static\"}', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(10010, 0, 'isis', 'template', 'isis', '', 1, 1, 1, 0, '{\"name\":\"isis\",\"type\":\"template\",\"creationDate\":\"3\\/30\\/2012\",\"author\":\"Kyle Ledbetter\",\"copyright\":\"Copyright (C) 2005 - 2017 Open Source Matters, Inc. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"\",\"version\":\"1.0\",\"description\":\"TPL_ISIS_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"templateDetails\"}', '{\"templateColor\":\"\",\"logoFile\":\"\"}', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(10013, 0, 'plg_installer_webinstaller', 'plugin', 'webinstaller', 'installer', 0, 1, 1, 0, '{\"name\":\"plg_installer_webinstaller\",\"type\":\"plugin\",\"creationDate\":\"28 April 2017\",\"author\":\"Joomla! Project\",\"copyright\":\"Copyright (C) 2013-2017 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"1.1.1\",\"description\":\"PLG_INSTALLER_WEBINSTALLER_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"webinstaller\"}', '{\"tab_position\":\"0\"}', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(10015, 0, 'Helix V2 Shortcode Generator', 'plugin', 'helix_shortcode', 'editors-xtd', 0, 1, 1, 0, '{\"name\":\"Helix V2 Shortcode Generator\",\"type\":\"plugin\",\"creationDate\":\"June 2014\",\"author\":\"JoomShaper.com\",\"copyright\":\"Copyright (C) 2010 - 2017 JoomShaper.com. All rights reserved.\",\"authorEmail\":\"support@joomshaper.com\",\"authorUrl\":\"http:\\/\\/www.joomshaper.com\",\"version\":\"2.5\",\"description\":\"Helix V2 Shortcode Generator\",\"group\":\"\",\"filename\":\"helix_shortcode\"}', '{}', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(10016, 0, 'beez3', 'template', 'beez3', '', 0, 1, 1, 0, '{\"name\":\"beez3\",\"type\":\"template\",\"creationDate\":\"25 November 2009\",\"author\":\"Angie Radtke\",\"copyright\":\"Copyright (C) 2005 - 2017 Open Source Matters, Inc. All rights reserved.\",\"authorEmail\":\"a.radtke@derauftritt.de\",\"authorUrl\":\"http:\\/\\/www.der-auftritt.de\",\"version\":\"3.1.0\",\"description\":\"TPL_BEEZ3_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"templateDetails\"}', '{\"wrapperSmall\":\"53\",\"wrapperLarge\":\"72\",\"sitetitle\":\"\",\"sitedescription\":\"\",\"navposition\":\"center\",\"templatecolor\":\"nature\",\"backgroundcolor\":\"#eee\"}', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(10017, 0, 'hathor', 'template', 'hathor', '', 1, 1, 1, 0, '{\"name\":\"hathor\",\"type\":\"template\",\"creationDate\":\"May 2010\",\"author\":\"Andrea Tarr\",\"copyright\":\"Copyright (C) 2005 - 2017 Open Source Matters, Inc. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"\",\"version\":\"3.0.0\",\"description\":\"TPL_HATHOR_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"templateDetails\"}', '{\"showSiteName\":\"0\",\"logoFile\":\"\",\"colourChoice\":\"0\",\"boldText\":\"0\"}', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(10018, 10020, 'COM_OSGALLERY', 'component', 'com_osgallery', '', 1, 1, 0, 0, '{\"name\":\"COM_OSGALLERY\",\"type\":\"component\",\"creationDate\":\"May 2018\",\"author\":\"Andrey Kvasnevskiy, Roman Akoev, Dmitriy Smirnov, Buchastuy Sergey\",\"copyright\":\"Ordasoft - Andrey Kvasnevskiy\",\"authorEmail\":\"akbet@mail.ru,akoevroman@gmail.com, dmitriiua21@gmail.com\",\"authorUrl\":\"http:\\/\\/www.ordasoft.com\",\"version\":\"4.5.3 Light\",\"description\":\"<img style=\\\"width:515px; display: block; margin: 0 auto;\\\" src=\\\"..\\/administrator\\/components\\/com_osgallery\\/assets\\/images\\/os-image-gallery.png\\\">\",\"group\":\"\",\"filename\":\"osgallery\"}', '{}', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(10019, 10020, 'OS Gallery System', 'plugin', 'osgallery_system', 'system', 0, 1, 1, 0, '{\"name\":\"OS Gallery System\",\"type\":\"plugin\",\"creationDate\":\"June 2017\",\"author\":\"www.ordasoft.com \\/Roman Akoev(akoevroman@gmail.com)\",\"copyright\":\"Copyright \\u00a9 ordasoft.com. All rights reserved.\",\"authorEmail\":\"support@ordasoft.com\",\"authorUrl\":\"http:\\/\\/www.ordasoft.com\",\"version\":\"3.2.6\",\"description\":\"System plugin for OS Gallery.\",\"group\":\"\",\"filename\":\"osgallery_system\"}', '{}', '', '', 0, '0000-00-00 00:00:00', 9999, 0),
(10020, 0, 'OS Gallery PKG Pro', 'package', 'pkg_OSGalleryPKG', '', 0, 1, 1, 0, '{\"name\":\"OS Gallery PKG Pro\",\"type\":\"package\",\"creationDate\":\"June 2018\",\"author\":\"www.ordasoft.com \\/Roman Akoev(akoevroman@gmail.com),Andrey Kvasnevskiy(akbet@mail.ru)\",\"copyright\":\"OrdaSoft 2018\",\"authorEmail\":\"support@ordasoft.com\",\"authorUrl\":\"\",\"version\":\"4.5.7 Light\",\"description\":\"<img style=\\\"width:515px; display: block; margin: 0 auto;\\\" src=\\\"..\\/administrator\\/components\\/com_osgallery\\/assets\\/images\\/os-image-gallery.png\\\">\",\"group\":\"\",\"filename\":\"pkg_OSGalleryPKG\"}', '{}', '', '', 0, '0000-00-00 00:00:00', 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `t41uf_fields`
--

CREATE TABLE `t41uf_fields` (
  `id` int(10) UNSIGNED NOT NULL,
  `asset_id` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `context` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `group_id` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `label` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `default_value` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `type` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'text',
  `note` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `description` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `state` tinyint(1) NOT NULL DEFAULT '0',
  `required` tinyint(1) NOT NULL DEFAULT '0',
  `checked_out` int(11) NOT NULL DEFAULT '0',
  `checked_out_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `ordering` int(11) NOT NULL DEFAULT '0',
  `params` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `fieldparams` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `language` char(7) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `created_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `created_user_id` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `modified_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `modified_by` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `access` int(11) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `t41uf_fields_categories`
--

CREATE TABLE `t41uf_fields_categories` (
  `field_id` int(11) NOT NULL DEFAULT '0',
  `category_id` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `t41uf_fields_groups`
--

CREATE TABLE `t41uf_fields_groups` (
  `id` int(10) UNSIGNED NOT NULL,
  `asset_id` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `context` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `note` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `description` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `state` tinyint(1) NOT NULL DEFAULT '0',
  `checked_out` int(11) NOT NULL DEFAULT '0',
  `checked_out_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `ordering` int(11) NOT NULL DEFAULT '0',
  `params` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `language` char(7) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `created` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `created_by` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `modified` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `modified_by` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `access` int(11) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `t41uf_fields_values`
--

CREATE TABLE `t41uf_fields_values` (
  `field_id` int(10) UNSIGNED NOT NULL,
  `item_id` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT 'Allow references to items which have strings as ids, eg. none db systems.',
  `value` text COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `t41uf_finder_filters`
--

CREATE TABLE `t41uf_finder_filters` (
  `filter_id` int(10) UNSIGNED NOT NULL,
  `title` varchar(255) NOT NULL,
  `alias` varchar(255) NOT NULL,
  `state` tinyint(1) NOT NULL DEFAULT '1',
  `created` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `created_by` int(10) UNSIGNED NOT NULL,
  `created_by_alias` varchar(255) NOT NULL,
  `modified` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `modified_by` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `checked_out` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `checked_out_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `map_count` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `data` text NOT NULL,
  `params` mediumtext
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `t41uf_finder_links`
--

CREATE TABLE `t41uf_finder_links` (
  `link_id` int(10) UNSIGNED NOT NULL,
  `url` varchar(255) NOT NULL,
  `route` varchar(255) NOT NULL,
  `title` varchar(400) DEFAULT NULL,
  `description` text,
  `indexdate` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `md5sum` varchar(32) DEFAULT NULL,
  `published` tinyint(1) NOT NULL DEFAULT '1',
  `state` int(5) DEFAULT '1',
  `access` int(5) DEFAULT '0',
  `language` varchar(8) NOT NULL,
  `publish_start_date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `publish_end_date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `start_date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `end_date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `list_price` double UNSIGNED NOT NULL DEFAULT '0',
  `sale_price` double UNSIGNED NOT NULL DEFAULT '0',
  `type_id` int(11) NOT NULL,
  `object` mediumblob NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `t41uf_finder_links_terms0`
--

CREATE TABLE `t41uf_finder_links_terms0` (
  `link_id` int(10) UNSIGNED NOT NULL,
  `term_id` int(10) UNSIGNED NOT NULL,
  `weight` float UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `t41uf_finder_links_terms1`
--

CREATE TABLE `t41uf_finder_links_terms1` (
  `link_id` int(10) UNSIGNED NOT NULL,
  `term_id` int(10) UNSIGNED NOT NULL,
  `weight` float UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `t41uf_finder_links_terms2`
--

CREATE TABLE `t41uf_finder_links_terms2` (
  `link_id` int(10) UNSIGNED NOT NULL,
  `term_id` int(10) UNSIGNED NOT NULL,
  `weight` float UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `t41uf_finder_links_terms3`
--

CREATE TABLE `t41uf_finder_links_terms3` (
  `link_id` int(10) UNSIGNED NOT NULL,
  `term_id` int(10) UNSIGNED NOT NULL,
  `weight` float UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `t41uf_finder_links_terms4`
--

CREATE TABLE `t41uf_finder_links_terms4` (
  `link_id` int(10) UNSIGNED NOT NULL,
  `term_id` int(10) UNSIGNED NOT NULL,
  `weight` float UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `t41uf_finder_links_terms5`
--

CREATE TABLE `t41uf_finder_links_terms5` (
  `link_id` int(10) UNSIGNED NOT NULL,
  `term_id` int(10) UNSIGNED NOT NULL,
  `weight` float UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `t41uf_finder_links_terms6`
--

CREATE TABLE `t41uf_finder_links_terms6` (
  `link_id` int(10) UNSIGNED NOT NULL,
  `term_id` int(10) UNSIGNED NOT NULL,
  `weight` float UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `t41uf_finder_links_terms7`
--

CREATE TABLE `t41uf_finder_links_terms7` (
  `link_id` int(10) UNSIGNED NOT NULL,
  `term_id` int(10) UNSIGNED NOT NULL,
  `weight` float UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `t41uf_finder_links_terms8`
--

CREATE TABLE `t41uf_finder_links_terms8` (
  `link_id` int(10) UNSIGNED NOT NULL,
  `term_id` int(10) UNSIGNED NOT NULL,
  `weight` float UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `t41uf_finder_links_terms9`
--

CREATE TABLE `t41uf_finder_links_terms9` (
  `link_id` int(10) UNSIGNED NOT NULL,
  `term_id` int(10) UNSIGNED NOT NULL,
  `weight` float UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `t41uf_finder_links_termsa`
--

CREATE TABLE `t41uf_finder_links_termsa` (
  `link_id` int(10) UNSIGNED NOT NULL,
  `term_id` int(10) UNSIGNED NOT NULL,
  `weight` float UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `t41uf_finder_links_termsb`
--

CREATE TABLE `t41uf_finder_links_termsb` (
  `link_id` int(10) UNSIGNED NOT NULL,
  `term_id` int(10) UNSIGNED NOT NULL,
  `weight` float UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `t41uf_finder_links_termsc`
--

CREATE TABLE `t41uf_finder_links_termsc` (
  `link_id` int(10) UNSIGNED NOT NULL,
  `term_id` int(10) UNSIGNED NOT NULL,
  `weight` float UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `t41uf_finder_links_termsd`
--

CREATE TABLE `t41uf_finder_links_termsd` (
  `link_id` int(10) UNSIGNED NOT NULL,
  `term_id` int(10) UNSIGNED NOT NULL,
  `weight` float UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `t41uf_finder_links_termse`
--

CREATE TABLE `t41uf_finder_links_termse` (
  `link_id` int(10) UNSIGNED NOT NULL,
  `term_id` int(10) UNSIGNED NOT NULL,
  `weight` float UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `t41uf_finder_links_termsf`
--

CREATE TABLE `t41uf_finder_links_termsf` (
  `link_id` int(10) UNSIGNED NOT NULL,
  `term_id` int(10) UNSIGNED NOT NULL,
  `weight` float UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `t41uf_finder_taxonomy`
--

CREATE TABLE `t41uf_finder_taxonomy` (
  `id` int(10) UNSIGNED NOT NULL,
  `parent_id` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `title` varchar(255) NOT NULL,
  `state` tinyint(1) UNSIGNED NOT NULL DEFAULT '1',
  `access` tinyint(1) UNSIGNED NOT NULL DEFAULT '0',
  `ordering` tinyint(1) UNSIGNED NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `t41uf_finder_taxonomy`
--

INSERT INTO `t41uf_finder_taxonomy` (`id`, `parent_id`, `title`, `state`, `access`, `ordering`) VALUES
(1, 0, 'ROOT', 0, 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `t41uf_finder_taxonomy_map`
--

CREATE TABLE `t41uf_finder_taxonomy_map` (
  `link_id` int(10) UNSIGNED NOT NULL,
  `node_id` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `t41uf_finder_terms`
--

CREATE TABLE `t41uf_finder_terms` (
  `term_id` int(10) UNSIGNED NOT NULL,
  `term` varchar(75) NOT NULL,
  `stem` varchar(75) NOT NULL,
  `common` tinyint(1) UNSIGNED NOT NULL DEFAULT '0',
  `phrase` tinyint(1) UNSIGNED NOT NULL DEFAULT '0',
  `weight` float UNSIGNED NOT NULL DEFAULT '0',
  `soundex` varchar(75) NOT NULL,
  `links` int(10) NOT NULL DEFAULT '0',
  `language` char(3) NOT NULL DEFAULT ''
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `t41uf_finder_terms_common`
--

CREATE TABLE `t41uf_finder_terms_common` (
  `term` varchar(75) NOT NULL,
  `language` varchar(3) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `t41uf_finder_terms_common`
--

INSERT INTO `t41uf_finder_terms_common` (`term`, `language`) VALUES
('a', 'en'),
('about', 'en'),
('after', 'en'),
('ago', 'en'),
('all', 'en'),
('am', 'en'),
('an', 'en'),
('and', 'en'),
('any', 'en'),
('are', 'en'),
('aren\'t', 'en'),
('as', 'en'),
('at', 'en'),
('be', 'en'),
('but', 'en'),
('by', 'en'),
('for', 'en'),
('from', 'en'),
('get', 'en'),
('go', 'en'),
('how', 'en'),
('if', 'en'),
('in', 'en'),
('into', 'en'),
('is', 'en'),
('isn\'t', 'en'),
('it', 'en'),
('its', 'en'),
('me', 'en'),
('more', 'en'),
('most', 'en'),
('must', 'en'),
('my', 'en'),
('new', 'en'),
('no', 'en'),
('none', 'en'),
('not', 'en'),
('nothing', 'en'),
('of', 'en'),
('off', 'en'),
('often', 'en'),
('old', 'en'),
('on', 'en'),
('onc', 'en'),
('once', 'en'),
('only', 'en'),
('or', 'en'),
('other', 'en'),
('our', 'en'),
('ours', 'en'),
('out', 'en'),
('over', 'en'),
('page', 'en'),
('she', 'en'),
('should', 'en'),
('small', 'en'),
('so', 'en'),
('some', 'en'),
('than', 'en'),
('thank', 'en'),
('that', 'en'),
('the', 'en'),
('their', 'en'),
('theirs', 'en'),
('them', 'en'),
('then', 'en'),
('there', 'en'),
('these', 'en'),
('they', 'en'),
('this', 'en'),
('those', 'en'),
('thus', 'en'),
('time', 'en'),
('times', 'en'),
('to', 'en'),
('too', 'en'),
('true', 'en'),
('under', 'en'),
('until', 'en'),
('up', 'en'),
('upon', 'en'),
('use', 'en'),
('user', 'en'),
('users', 'en'),
('version', 'en'),
('very', 'en'),
('via', 'en'),
('want', 'en'),
('was', 'en'),
('way', 'en'),
('were', 'en'),
('what', 'en'),
('when', 'en'),
('where', 'en'),
('which', 'en'),
('who', 'en'),
('whom', 'en'),
('whose', 'en'),
('why', 'en'),
('wide', 'en'),
('will', 'en'),
('with', 'en'),
('within', 'en'),
('without', 'en'),
('would', 'en'),
('yes', 'en'),
('yet', 'en'),
('you', 'en'),
('your', 'en'),
('yours', 'en');

-- --------------------------------------------------------

--
-- Table structure for table `t41uf_finder_tokens`
--

CREATE TABLE `t41uf_finder_tokens` (
  `term` varchar(75) NOT NULL,
  `stem` varchar(75) NOT NULL,
  `common` tinyint(1) UNSIGNED NOT NULL DEFAULT '0',
  `phrase` tinyint(1) UNSIGNED NOT NULL DEFAULT '0',
  `weight` float UNSIGNED NOT NULL DEFAULT '1',
  `context` tinyint(1) UNSIGNED NOT NULL DEFAULT '2',
  `language` char(3) NOT NULL DEFAULT ''
) ENGINE=MEMORY DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `t41uf_finder_tokens_aggregate`
--

CREATE TABLE `t41uf_finder_tokens_aggregate` (
  `term_id` int(10) UNSIGNED NOT NULL,
  `map_suffix` char(1) NOT NULL,
  `term` varchar(75) NOT NULL,
  `stem` varchar(75) NOT NULL,
  `common` tinyint(1) UNSIGNED NOT NULL DEFAULT '0',
  `phrase` tinyint(1) UNSIGNED NOT NULL DEFAULT '0',
  `term_weight` float UNSIGNED NOT NULL,
  `context` tinyint(1) UNSIGNED NOT NULL DEFAULT '2',
  `context_weight` float UNSIGNED NOT NULL,
  `total_weight` float UNSIGNED NOT NULL,
  `language` char(3) NOT NULL DEFAULT ''
) ENGINE=MEMORY DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `t41uf_finder_types`
--

CREATE TABLE `t41uf_finder_types` (
  `id` int(10) UNSIGNED NOT NULL,
  `title` varchar(100) NOT NULL,
  `mime` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `t41uf_languages`
--

CREATE TABLE `t41uf_languages` (
  `lang_id` int(11) UNSIGNED NOT NULL,
  `asset_id` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `lang_code` char(7) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL,
  `title` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `title_native` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `sef` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `image` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` varchar(512) COLLATE utf8mb4_unicode_ci NOT NULL,
  `metakey` mediumtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `metadesc` mediumtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `sitename` varchar(1024) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `published` int(11) NOT NULL DEFAULT '0',
  `access` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `ordering` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `t41uf_languages`
--

INSERT INTO `t41uf_languages` (`lang_id`, `asset_id`, `lang_code`, `title`, `title_native`, `sef`, `image`, `description`, `metakey`, `metadesc`, `sitename`, `published`, `access`, `ordering`) VALUES
(1, 0, 'en-GB', 'English (UK)', 'English (UK)', 'en', 'en', '', '', '', '', 1, 0, 1);

-- --------------------------------------------------------

--
-- Table structure for table `t41uf_menu`
--

CREATE TABLE `t41uf_menu` (
  `id` int(11) NOT NULL,
  `menutype` varchar(24) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT 'The type of menu this item belongs to. FK to #___menu_types.menutype',
  `title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT 'The display title of the menu item.',
  `alias` varchar(400) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL COMMENT 'The SEF alias of the menu item.',
  `note` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `path` varchar(1024) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT 'The computed path of the menu item based on the alias field.',
  `link` varchar(1024) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT 'The actually link the menu item refers to.',
  `type` varchar(16) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT 'The type of link: Component, URL, Alias, Separator',
  `published` tinyint(4) NOT NULL DEFAULT '0' COMMENT 'The published state of the menu link.',
  `parent_id` int(10) UNSIGNED NOT NULL DEFAULT '1' COMMENT 'The parent menu item in the menu tree.',
  `level` int(10) UNSIGNED NOT NULL DEFAULT '0' COMMENT 'The relative level in the tree.',
  `component_id` int(10) UNSIGNED NOT NULL DEFAULT '0' COMMENT 'FK to #___extensions.id',
  `checked_out` int(10) UNSIGNED NOT NULL DEFAULT '0' COMMENT 'FK to #___users.id',
  `checked_out_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00' COMMENT 'The time the menu item was checked out.',
  `browserNav` tinyint(4) NOT NULL DEFAULT '0' COMMENT 'The click behaviour of the link.',
  `access` int(10) UNSIGNED NOT NULL DEFAULT '0' COMMENT 'The access level required to view the menu item.',
  `img` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT 'The image of the menu item.',
  `template_style_id` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `params` mediumtext COLLATE utf8mb4_unicode_ci NOT NULL COMMENT 'JSON encoded data for the menu item.',
  `lft` int(11) NOT NULL DEFAULT '0' COMMENT 'Nested set lft.',
  `rgt` int(11) NOT NULL DEFAULT '0' COMMENT 'Nested set rgt.',
  `home` tinyint(3) UNSIGNED NOT NULL DEFAULT '0' COMMENT 'Indicates if this menu item is the home or default page.',
  `language` char(7) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `client_id` tinyint(4) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `t41uf_menu`
--

INSERT INTO `t41uf_menu` (`id`, `menutype`, `title`, `alias`, `note`, `path`, `link`, `type`, `published`, `parent_id`, `level`, `component_id`, `checked_out`, `checked_out_time`, `browserNav`, `access`, `img`, `template_style_id`, `params`, `lft`, `rgt`, `home`, `language`, `client_id`) VALUES
(1, '', 'Menu_Item_Root', 'root', '', '', '', '', 1, 0, 0, 0, 0, '0000-00-00 00:00:00', 0, 0, '', 0, '', 0, 181, 0, '*', 0),
(2, 'main', 'com_banners', 'Banners', '', 'Banners', 'index.php?option=com_banners', 'component', 1, 1, 1, 4, 0, '0000-00-00 00:00:00', 0, 0, 'class:banners', 0, '', 1, 10, 0, '*', 1),
(3, 'main', 'com_banners', 'Banners', '', 'Banners/Banners', 'index.php?option=com_banners', 'component', 1, 2, 2, 4, 0, '0000-00-00 00:00:00', 0, 0, 'class:banners', 0, '', 2, 3, 0, '*', 1),
(4, 'main', 'com_banners_categories', 'Categories', '', 'Banners/Categories', 'index.php?option=com_categories&extension=com_banners', 'component', 1, 2, 2, 6, 0, '0000-00-00 00:00:00', 0, 0, 'class:banners-cat', 0, '', 4, 5, 0, '*', 1),
(5, 'main', 'com_banners_clients', 'Clients', '', 'Banners/Clients', 'index.php?option=com_banners&view=clients', 'component', 1, 2, 2, 4, 0, '0000-00-00 00:00:00', 0, 0, 'class:banners-clients', 0, '', 6, 7, 0, '*', 1),
(6, 'main', 'com_banners_tracks', 'Tracks', '', 'Banners/Tracks', 'index.php?option=com_banners&view=tracks', 'component', 1, 2, 2, 4, 0, '0000-00-00 00:00:00', 0, 0, 'class:banners-tracks', 0, '', 8, 9, 0, '*', 1),
(7, 'main', 'com_contact', 'Contacts', '', 'Contacts', 'index.php?option=com_contact', 'component', 1, 1, 1, 8, 0, '0000-00-00 00:00:00', 0, 0, 'class:contact', 0, '', 139, 144, 0, '*', 1),
(8, 'main', 'com_contact_contacts', 'Contacts', '', 'Contacts/Contacts', 'index.php?option=com_contact', 'component', 1, 7, 2, 8, 0, '0000-00-00 00:00:00', 0, 0, 'class:contact', 0, '', 140, 141, 0, '*', 1),
(9, 'main', 'com_contact_categories', 'Categories', '', 'Contacts/Categories', 'index.php?option=com_categories&extension=com_contact', 'component', 1, 7, 2, 6, 0, '0000-00-00 00:00:00', 0, 0, 'class:contact-cat', 0, '', 142, 143, 0, '*', 1),
(10, 'main', 'com_messages', 'Messaging', '', 'Messaging', 'index.php?option=com_messages', 'component', 1, 1, 1, 15, 0, '0000-00-00 00:00:00', 0, 0, 'class:messages', 0, '', 145, 148, 0, '*', 1),
(11, 'main', 'com_messages_add', 'New Private Message', '', 'Messaging/New Private Message', 'index.php?option=com_messages&task=message.add', 'component', 1, 10, 2, 15, 0, '0000-00-00 00:00:00', 0, 0, 'class:messages-add', 0, '', 146, 147, 0, '*', 1),
(13, 'main', 'com_newsfeeds', 'News Feeds', '', 'News Feeds', 'index.php?option=com_newsfeeds', 'component', 1, 1, 1, 17, 0, '0000-00-00 00:00:00', 0, 0, 'class:newsfeeds', 0, '', 149, 154, 0, '*', 1),
(14, 'main', 'com_newsfeeds_feeds', 'Feeds', '', 'News Feeds/Feeds', 'index.php?option=com_newsfeeds', 'component', 1, 13, 2, 17, 0, '0000-00-00 00:00:00', 0, 0, 'class:newsfeeds', 0, '', 150, 151, 0, '*', 1),
(15, 'main', 'com_newsfeeds_categories', 'Categories', '', 'News Feeds/Categories', 'index.php?option=com_categories&extension=com_newsfeeds', 'component', 1, 13, 2, 6, 0, '0000-00-00 00:00:00', 0, 0, 'class:newsfeeds-cat', 0, '', 152, 153, 0, '*', 1),
(16, 'main', 'com_redirect', 'Redirect', '', 'Redirect', 'index.php?option=com_redirect', 'component', 1, 1, 1, 24, 0, '0000-00-00 00:00:00', 0, 0, 'class:redirect', 0, '', 161, 162, 0, '*', 1),
(17, 'main', 'com_search', 'Basic Search', '', 'Basic Search', 'index.php?option=com_search', 'component', 1, 1, 1, 19, 0, '0000-00-00 00:00:00', 0, 0, 'class:search', 0, '', 157, 158, 0, '*', 1),
(21, 'main', 'com_finder', 'Smart Search', '', 'Smart Search', 'index.php?option=com_finder', 'component', 1, 1, 1, 27, 0, '0000-00-00 00:00:00', 0, 0, 'class:finder', 0, '', 155, 156, 0, '*', 1),
(22, 'main', 'com_joomlaupdate', 'Joomla! Update', '', 'Joomla! Update', 'index.php?option=com_joomlaupdate', 'component', 1, 1, 1, 28, 0, '0000-00-00 00:00:00', 0, 0, 'class:joomlaupdate', 0, '', 159, 160, 0, '*', 1),
(101, 'mainmenu', 'Home', 'home', '', 'home', 'index.php?option=com_content&view=featured', 'component', 1, 1, 1, 22, 0, '0000-00-00 00:00:00', 0, 1, ' ', 0, '{\"featured_categories\":[\"\"],\"layout_type\":\"blog\",\"num_leading_articles\":\"\",\"num_intro_articles\":\"\",\"num_columns\":\"\",\"num_links\":\"\",\"multi_column_order\":\"\",\"orderby_pri\":\"\",\"orderby_sec\":\"\",\"order_date\":\"\",\"show_pagination\":\"\",\"show_pagination_results\":\"\",\"show_title\":\"\",\"link_titles\":\"\",\"show_intro\":\"\",\"info_block_position\":\"\",\"info_block_show_title\":\"\",\"show_category\":\"0\",\"link_category\":\"0\",\"show_parent_category\":\"0\",\"link_parent_category\":\"0\",\"show_associations\":\"\",\"show_author\":\"0\",\"link_author\":\"0\",\"show_create_date\":\"0\",\"show_modify_date\":\"0\",\"show_publish_date\":\"0\",\"show_item_navigation\":\"0\",\"show_vote\":\"0\",\"show_readmore\":\"\",\"show_readmore_title\":\"\",\"show_icons\":\"0\",\"show_print_icon\":\"0\",\"show_email_icon\":\"0\",\"show_hits\":\"0\",\"show_tags\":\"\",\"show_noauth\":\"\",\"show_feed_link\":\"\",\"feed_summary\":\"\",\"menu-anchor_title\":\"\",\"menu-anchor_css\":\"\",\"menu_image\":\"\",\"menu_image_css\":\"\",\"menu_text\":1,\"menu_show\":1,\"page_title\":\"Coordinate\",\"show_page_heading\":\"0\",\"page_heading\":\"\",\"pageclass_sfx\":\"\",\"menu-meta_description\":\"\",\"menu-meta_keywords\":\"\",\"robots\":\"\",\"secure\":0,\"showmenutitle\":\"1\",\"desc\":\"\",\"cols\":\"1\",\"group\":\"0\",\"showgrouptitle\":\"1\",\"cwidth\":\"\",\"colxw\":\"\",\"class\":\"home\",\"subcontent\":\"normal\"}', 11, 12, 1, '*', 0),
(102, 'mainmenu', 'Features', 'item', '', 'item', '#', 'url', 0, 1, 1, 0, 0, '0000-00-00 00:00:00', 0, 1, '', 0, '{\"menu-anchor_title\":\"\",\"menu-anchor_css\":\"\",\"menu_image\":\"\",\"menu_text\":1,\"showmenutitle\":\"1\",\"desc\":\"\",\"cols\":\"3\",\"group\":\"0\",\"showgrouptitle\":\"1\",\"cwidth\":\"500\",\"colxw\":\"250\\r\\n250\\r\\n500\",\"class\":\"\",\"subcontent\":\"normal\"}', 43, 120, 0, '*', 0),
(103, 'mainmenu', 'Typography', 'typgraphy', '', 'item/2013-01-31-06-54-32/typgraphy', 'index.php?option=com_content&view=article&id=14', 'component', 0, 106, 3, 22, 0, '0000-00-00 00:00:00', 0, 1, '', 0, '{\"show_title\":\"\",\"link_titles\":\"\",\"show_intro\":\"\",\"info_block_position\":\"\",\"show_category\":\"\",\"link_category\":\"\",\"show_parent_category\":\"\",\"link_parent_category\":\"\",\"show_author\":\"\",\"link_author\":\"\",\"show_create_date\":\"\",\"show_modify_date\":\"\",\"show_publish_date\":\"\",\"show_item_navigation\":\"\",\"show_vote\":\"\",\"show_tags\":\"\",\"show_icons\":\"\",\"show_print_icon\":\"\",\"show_email_icon\":\"\",\"show_hits\":\"\",\"show_noauth\":\"\",\"urls_position\":\"\",\"menu-anchor_title\":\"\",\"menu-anchor_css\":\"\",\"menu_image\":\"\",\"menu_text\":1,\"page_title\":\"\",\"show_page_heading\":0,\"page_heading\":\"\",\"pageclass_sfx\":\"\",\"menu-meta_description\":\"\",\"menu-meta_keywords\":\"\",\"robots\":\"\",\"secure\":0,\"showmenutitle\":\"1\",\"desc\":\"\",\"cols\":\"1\",\"group\":\"0\",\"showgrouptitle\":\"1\",\"cwidth\":\"\",\"colxw\":\"\",\"class\":\"no-scroll\",\"subcontent\":\"normal\"}', 45, 46, 0, '*', 0),
(104, 'mainmenu', 'Module Position', 'module-position', '', 'item/2013-01-31-06-54-32/module-position', 'index.php?option=com_content&view=article&id=18', 'component', 0, 106, 3, 22, 0, '0000-00-00 00:00:00', 0, 1, '', 0, '{\"show_title\":\"\",\"link_titles\":\"\",\"show_intro\":\"\",\"info_block_position\":\"\",\"show_category\":\"\",\"link_category\":\"\",\"show_parent_category\":\"\",\"link_parent_category\":\"\",\"show_author\":\"\",\"link_author\":\"\",\"show_create_date\":\"\",\"show_modify_date\":\"\",\"show_publish_date\":\"\",\"show_item_navigation\":\"\",\"show_vote\":\"\",\"show_tags\":\"\",\"show_icons\":\"\",\"show_print_icon\":\"\",\"show_email_icon\":\"\",\"show_hits\":\"\",\"show_noauth\":\"\",\"urls_position\":\"\",\"menu-anchor_title\":\"\",\"menu-anchor_css\":\"\",\"menu_image\":\"\",\"menu_text\":1,\"page_title\":\"\",\"show_page_heading\":0,\"page_heading\":\"\",\"pageclass_sfx\":\"\",\"menu-meta_description\":\"\",\"menu-meta_keywords\":\"\",\"robots\":\"\",\"secure\":0,\"showmenutitle\":\"1\",\"desc\":\"\",\"cols\":\"1\",\"group\":\"0\",\"showgrouptitle\":\"1\",\"cwidth\":\"\",\"colxw\":\"\",\"class\":\"no-scroll\",\"subcontent\":\"normal\"}', 47, 48, 0, '*', 0),
(105, 'mainmenu', 'Module Variations', '2013-01-31-06-39-54', '', 'item/2013-01-31-06-54-32/2013-01-31-06-39-54', 'index.php?option=com_content&view=article&id=2', 'component', -2, 106, 3, 22, 0, '0000-00-00 00:00:00', 0, 1, '', 0, '{\"show_title\":\"\",\"link_titles\":\"\",\"show_intro\":\"\",\"show_category\":\"\",\"link_category\":\"\",\"show_parent_category\":\"\",\"link_parent_category\":\"\",\"show_author\":\"\",\"link_author\":\"\",\"show_create_date\":\"\",\"show_modify_date\":\"\",\"show_publish_date\":\"\",\"show_item_navigation\":\"\",\"show_vote\":\"\",\"show_icons\":\"\",\"show_print_icon\":\"\",\"show_email_icon\":\"\",\"show_hits\":\"\",\"show_noauth\":\"\",\"urls_position\":\"\",\"menu-anchor_title\":\"\",\"menu-anchor_css\":\"\",\"menu_image\":\"\",\"menu_text\":1,\"page_title\":\"\",\"show_page_heading\":0,\"page_heading\":\"\",\"pageclass_sfx\":\"\",\"menu-meta_description\":\"\",\"menu-meta_keywords\":\"\",\"robots\":\"\",\"secure\":0,\"showmenutitle\":\"1\",\"desc\":\"\",\"cols\":\"1\",\"group\":\"0\",\"showgrouptitle\":\"1\",\"cwidth\":\"\",\"colxw\":\"\",\"class\":\"\",\"subcontent\":\"normal\"}', 49, 50, 0, '*', 0),
(106, 'mainmenu', 'Key Features', '2013-01-31-06-54-32', '', 'item/2013-01-31-06-54-32', '#', 'url', 0, 102, 2, 0, 0, '0000-00-00 00:00:00', 0, 1, '', 0, '{\"menu-anchor_title\":\"\",\"menu-anchor_css\":\"\",\"menu_image\":\"\",\"menu_text\":1,\"showmenutitle\":\"1\",\"desc\":\"\",\"cols\":\"1\",\"group\":\"1\",\"showgrouptitle\":\"1\",\"cwidth\":\"\",\"colxw\":\"\",\"class\":\"\",\"subcontent\":\"normal\"}', 44, 81, 0, '*', 0),
(107, 'mainmenu', 'Shortcode', 'shortcode', '', 'item/2013-01-31-06-54-32/shortcode', '#', 'url', 0, 106, 3, 0, 0, '0000-00-00 00:00:00', 0, 1, '', 0, '{\"menu-anchor_title\":\"\",\"menu-anchor_css\":\"\",\"menu_image\":\"\",\"menu_text\":1,\"showmenutitle\":\"1\",\"desc\":\"\",\"cols\":\"1\",\"group\":\"0\",\"showgrouptitle\":\"1\",\"cwidth\":\"\",\"colxw\":\"\",\"class\":\"\",\"subcontent\":\"normal\"}', 57, 80, 0, '*', 0),
(108, 'mainmenu', 'Menu', '2013-01-31-07-00-49', '', 'item/2013-01-31-07-00-49', '#', 'url', 0, 102, 2, 0, 0, '0000-00-00 00:00:00', 0, 1, '', 0, '{\"menu-anchor_title\":\"\",\"menu-anchor_css\":\"\",\"menu_image\":\"\",\"menu_text\":1,\"showmenutitle\":\"1\",\"desc\":\"\",\"cols\":\"1\",\"group\":\"1\",\"showgrouptitle\":\"1\",\"cwidth\":\"\",\"colxw\":\"\",\"class\":\"\",\"subcontent\":\"normal\"}', 82, 117, 0, '*', 0),
(109, 'mainmenu', 'Mega Menu', '2013-01-31-07-04-54', '', 'item/2013-01-31-07-00-49/2013-01-31-07-04-54', '?menu=mega', 'url', 0, 108, 3, 0, 0, '0000-00-00 00:00:00', 0, 1, '', 0, '{\"menu-anchor_title\":\"\",\"menu-anchor_css\":\"\",\"menu_image\":\"\",\"menu_text\":1,\"showmenutitle\":\"1\",\"desc\":\"\",\"cols\":\"1\",\"group\":\"0\",\"showgrouptitle\":\"1\",\"cwidth\":\"\",\"colxw\":\"\",\"class\":\"\",\"subcontent\":\"normal\"}', 83, 84, 0, '*', 0),
(110, 'mainmenu', 'Dropline Menu', '2013-01-31-07-06-22', '', 'item/2013-01-31-07-00-49/2013-01-31-07-06-22', '?menu=drop', 'url', 0, 108, 3, 0, 0, '0000-00-00 00:00:00', 0, 1, '', 0, '{\"menu-anchor_title\":\"\",\"menu-anchor_css\":\"\",\"menu_image\":\"\",\"menu_text\":1,\"showmenutitle\":\"1\",\"desc\":\"\",\"cols\":\"1\",\"group\":\"0\",\"showgrouptitle\":\"1\",\"cwidth\":\"\",\"colxw\":\"\",\"class\":\"\",\"subcontent\":\"normal\"}', 85, 86, 0, '*', 0),
(112, 'mainmenu', 'Split Menu', '2013-01-31-07-08-55', '', 'item/2013-01-31-07-00-49/2013-01-31-07-08-55', '?menu=split', 'url', 0, 108, 3, 0, 0, '0000-00-00 00:00:00', 0, 1, '', 0, '{\"menu-anchor_title\":\"\",\"menu-anchor_css\":\"\",\"menu_image\":\"\",\"menu_text\":1,\"showmenutitle\":\"1\",\"desc\":\"\",\"cols\":\"1\",\"group\":\"0\",\"showgrouptitle\":\"1\",\"cwidth\":\"\",\"colxw\":\"\",\"class\":\"\",\"subcontent\":\"normal\"}', 87, 88, 0, '*', 0),
(113, 'mainmenu', 'RTL Demos', '2013-01-31-07-09-06', '', 'item/2013-01-31-06-54-32/2013-01-31-07-09-06', '#', 'url', 0, 106, 3, 0, 0, '0000-00-00 00:00:00', 0, 1, '', 0, '{\"menu-anchor_title\":\"\",\"menu-anchor_css\":\"\",\"menu_image\":\"\",\"menu_text\":1,\"showmenutitle\":\"1\",\"desc\":\"\",\"cols\":\"1\",\"group\":\"0\",\"showgrouptitle\":\"1\",\"cwidth\":\"\",\"colxw\":\"\",\"class\":\"\",\"subcontent\":\"normal\"}', 51, 56, 0, '*', 0),
(114, 'mainmenu', 'LTR Language', '2013-01-31-07-09-56', '', 'item/2013-01-31-06-54-32/2013-01-31-07-09-06/2013-01-31-07-09-56', '?direction=ltr', 'url', 0, 113, 4, 0, 0, '0000-00-00 00:00:00', 0, 1, '', 0, '{\"menu-anchor_title\":\"\",\"menu-anchor_css\":\"\",\"menu_image\":\"\",\"menu_text\":1,\"showmenutitle\":\"1\",\"desc\":\"\",\"cols\":\"1\",\"group\":\"0\",\"showgrouptitle\":\"1\",\"cwidth\":\"\",\"colxw\":\"\",\"class\":\"no-scroll\",\"subcontent\":\"normal\"}', 52, 53, 0, '*', 0),
(115, 'mainmenu', 'RTL Language', '2013-01-31-07-10-29', '', 'item/2013-01-31-06-54-32/2013-01-31-07-09-06/2013-01-31-07-10-29', '?direction=rtl', 'url', 0, 113, 4, 0, 0, '0000-00-00 00:00:00', 0, 1, '', 0, '{\"menu-anchor_title\":\"\",\"menu-anchor_css\":\"\",\"menu_image\":\"\",\"menu_text\":1,\"showmenutitle\":\"1\",\"desc\":\"\",\"cols\":\"1\",\"group\":\"0\",\"showgrouptitle\":\"1\",\"cwidth\":\"\",\"colxw\":\"\",\"class\":\"no-scroll\",\"subcontent\":\"normal\"}', 54, 55, 0, '*', 0),
(116, 'mainmenu', 'Custom Module', '2013-01-31-07-13-54', '', 'item/2013-01-31-07-13-54', '#', 'url', 0, 102, 2, 0, 0, '0000-00-00 00:00:00', 0, 1, '', 0, '{\"menu-anchor_title\":\"\",\"menu-anchor_css\":\"\",\"menu_image\":\"\",\"menu_text\":1,\"showmenutitle\":\"1\",\"desc\":\"\",\"cols\":\"1\",\"group\":\"1\",\"showgrouptitle\":\"1\",\"cwidth\":\"\",\"colxw\":\"\",\"class\":\"\",\"subcontent\":\"modules\",\"mods\":[\"87\"]}', 118, 119, 0, '*', 0),
(117, 'mainmenu', 'Presets', '2013-01-31-07-20-51', '', '2013-01-31-07-20-51', '#', 'url', -2, 1, 1, 0, 0, '0000-00-00 00:00:00', 0, 1, '', 0, '{\"menu-anchor_title\":\"\",\"menu-anchor_css\":\"\",\"menu_image\":\"\",\"menu_text\":1,\"showmenutitle\":\"1\",\"desc\":\"\",\"cols\":\"1\",\"group\":\"0\",\"showgrouptitle\":\"1\",\"cwidth\":\"\",\"colxw\":\"\",\"class\":\"\",\"subcontent\":\"normal\"}', 29, 36, 0, '*', 0),
(118, 'mainmenu', 'Preset1', '2013-01-31-07-21-59', '', '2013-01-31-07-20-51/2013-01-31-07-21-59', '?preset=preset1', 'url', -2, 117, 2, 0, 0, '0000-00-00 00:00:00', 0, 1, '', 0, '{\"menu-anchor_title\":\"\",\"menu-anchor_css\":\"\",\"menu_image\":\"\",\"menu_text\":1,\"showmenutitle\":\"1\",\"desc\":\"\",\"cols\":\"1\",\"group\":\"0\",\"showgrouptitle\":\"1\",\"cwidth\":\"\",\"colxw\":\"\",\"class\":\"\",\"subcontent\":\"normal\"}', 30, 31, 0, '*', 0),
(119, 'mainmenu', 'Menu Example', '2013-01-31-07-23-52', '', 'item/2013-01-31-07-00-49/2013-01-31-07-23-52', '#', 'url', 0, 108, 3, 0, 0, '0000-00-00 00:00:00', 0, 1, '', 0, '{\"menu-anchor_title\":\"\",\"menu-anchor_css\":\"\",\"menu_image\":\"\",\"menu_text\":1,\"showmenutitle\":\"1\",\"desc\":\"\",\"cols\":\"2\",\"group\":\"0\",\"showgrouptitle\":\"1\",\"cwidth\":\"\",\"colxw\":\"\",\"class\":\"\",\"subcontent\":\"normal\"}', 89, 116, 0, '*', 0),
(120, 'mainmenu', 'Preset2', '2013-01-31-07-24-34', '', '2013-01-31-07-20-51/2013-01-31-07-24-34', '?preset=preset2', 'url', -2, 117, 2, 0, 0, '0000-00-00 00:00:00', 0, 1, '', 0, '{\"menu-anchor_title\":\"\",\"menu-anchor_css\":\"\",\"menu_image\":\"\",\"menu_text\":1,\"showmenutitle\":\"1\",\"desc\":\"\",\"cols\":\"1\",\"group\":\"0\",\"showgrouptitle\":\"1\",\"cwidth\":\"\",\"colxw\":\"\",\"class\":\"\",\"subcontent\":\"normal\"}', 32, 33, 0, '*', 0),
(121, 'mainmenu', 'Preset3', '2013-01-31-07-25-04', '', '2013-01-31-07-20-51/2013-01-31-07-25-04', '?preset=preset3', 'url', -2, 117, 2, 0, 0, '0000-00-00 00:00:00', 0, 1, '', 0, '{\"menu-anchor_title\":\"\",\"menu-anchor_css\":\"\",\"menu_image\":\"\",\"menu_text\":1,\"showmenutitle\":\"1\",\"desc\":\"\",\"cols\":\"1\",\"group\":\"0\",\"showgrouptitle\":\"1\",\"cwidth\":\"\",\"colxw\":\"\",\"class\":\"\",\"subcontent\":\"normal\"}', 34, 35, 0, '*', 0),
(123, 'mainmenu', 'Joomla!', 'joomla', '', 'joomla', 'index.php?option=com_content&view=article&id=1', 'component', 0, 1, 1, 22, 0, '0000-00-00 00:00:00', 0, 1, '', 0, '{\"show_title\":\"\",\"link_titles\":\"\",\"show_intro\":\"\",\"show_category\":\"\",\"link_category\":\"\",\"show_parent_category\":\"\",\"link_parent_category\":\"\",\"show_author\":\"\",\"link_author\":\"\",\"show_create_date\":\"\",\"show_modify_date\":\"\",\"show_publish_date\":\"\",\"show_item_navigation\":\"\",\"show_vote\":\"\",\"show_icons\":\"\",\"show_print_icon\":\"\",\"show_email_icon\":\"\",\"show_hits\":\"\",\"show_noauth\":\"\",\"urls_position\":\"\",\"menu-anchor_title\":\"\",\"menu-anchor_css\":\"\",\"menu_image\":\"\",\"menu_text\":1,\"page_title\":\"\",\"show_page_heading\":0,\"page_heading\":\"\",\"pageclass_sfx\":\"\",\"menu-meta_description\":\"\",\"menu-meta_keywords\":\"\",\"robots\":\"\",\"secure\":0,\"showmenutitle\":\"1\",\"desc\":\"\",\"cols\":\"1\",\"group\":\"0\",\"showgrouptitle\":\"1\",\"cwidth\":\"\",\"colxw\":\"\",\"class\":\"\",\"subcontent\":\"normal\"}', 125, 138, 0, '*', 0),
(124, 'mainmenu', 'Group2', '2013-01-31-07-29-22', '', 'item/2013-01-31-07-00-49/2013-01-31-07-23-52/2013-01-31-07-29-22', '#', 'url', 0, 119, 4, 0, 0, '0000-00-00 00:00:00', 0, 1, '', 0, '{\"menu-anchor_title\":\"\",\"menu-anchor_css\":\"\",\"menu_image\":\"\",\"menu_text\":1,\"showmenutitle\":\"1\",\"desc\":\"\",\"cols\":\"1\",\"group\":\"1\",\"showgrouptitle\":\"1\",\"cwidth\":\"\",\"colxw\":\"\",\"class\":\"\",\"subcontent\":\"normal\"}', 98, 115, 0, '*', 0),
(125, 'mainmenu', 'Child Item', '2013-01-31-07-32-05', '', 'item/2013-01-31-07-00-49/2013-01-31-07-23-52/2013-01-31-07-29-22/2013-01-31-07-32-05', '#', 'url', 0, 124, 5, 0, 0, '0000-00-00 00:00:00', 0, 1, '', 0, '{\"menu-anchor_title\":\"\",\"menu-anchor_css\":\"\",\"menu_image\":\"\",\"menu_text\":1,\"showmenutitle\":\"1\",\"desc\":\"\",\"cols\":\"1\",\"group\":\"0\",\"showgrouptitle\":\"1\",\"cwidth\":\"\",\"colxw\":\"\",\"class\":\"\",\"subcontent\":\"normal\"}', 99, 100, 0, '*', 0),
(126, 'mainmenu', 'Child Item', '2013-01-31-07-32-48', '', 'item/2013-01-31-07-00-49/2013-01-31-07-23-52/2013-01-31-07-29-22/2013-01-31-07-32-48', '#', 'url', 0, 124, 5, 0, 0, '0000-00-00 00:00:00', 0, 1, '', 0, '{\"menu-anchor_title\":\"\",\"menu-anchor_css\":\"\",\"menu_image\":\"\",\"menu_text\":1,\"showmenutitle\":\"1\",\"desc\":\"\",\"cols\":\"1\",\"group\":\"0\",\"showgrouptitle\":\"1\",\"cwidth\":\"\",\"colxw\":\"\",\"class\":\"\",\"subcontent\":\"normal\"}', 101, 102, 0, '*', 0),
(127, 'mainmenu', 'Child Item', '2013-01-31-07-33-25', '', 'item/2013-01-31-07-00-49/2013-01-31-07-23-52/2013-01-31-07-29-22/2013-01-31-07-33-25', '#', 'url', 0, 124, 5, 0, 0, '0000-00-00 00:00:00', 0, 1, '', 0, '{\"menu-anchor_title\":\"\",\"menu-anchor_css\":\"\",\"menu_image\":\"\",\"menu_text\":1,\"showmenutitle\":\"1\",\"desc\":\"\",\"cols\":\"1\",\"group\":\"0\",\"showgrouptitle\":\"1\",\"cwidth\":\"\",\"colxw\":\"\",\"class\":\"\",\"subcontent\":\"normal\"}', 103, 114, 0, '*', 0),
(128, 'mainmenu', 'Group1', '2013-01-31-07-34-16', '', 'item/2013-01-31-07-00-49/2013-01-31-07-23-52/2013-01-31-07-34-16', '#', 'url', 0, 119, 4, 0, 0, '0000-00-00 00:00:00', 0, 1, '', 0, '{\"menu-anchor_title\":\"\",\"menu-anchor_css\":\"\",\"menu_image\":\"\",\"menu_text\":1,\"showmenutitle\":\"1\",\"desc\":\"\",\"cols\":\"1\",\"group\":\"1\",\"showgrouptitle\":\"1\",\"cwidth\":\"\",\"colxw\":\"\",\"class\":\"\",\"subcontent\":\"normal\"}', 90, 97, 0, '*', 0),
(129, 'mainmenu', 'Child Item', '2013-01-31-07-35-10', '', 'item/2013-01-31-07-00-49/2013-01-31-07-23-52/2013-01-31-07-34-16/2013-01-31-07-35-10', '#', 'url', 0, 128, 5, 0, 0, '0000-00-00 00:00:00', 0, 1, '', 0, '{\"menu-anchor_title\":\"\",\"menu-anchor_css\":\"\",\"menu_image\":\"images\\/folder.png\",\"menu_text\":1,\"showmenutitle\":\"1\",\"desc\":\"\",\"cols\":\"1\",\"group\":\"0\",\"showgrouptitle\":\"1\",\"cwidth\":\"\",\"colxw\":\"\",\"class\":\"\",\"subcontent\":\"normal\"}', 91, 92, 0, '*', 0),
(130, 'mainmenu', 'Child Item', '2013-01-31-07-38-19', '', 'item/2013-01-31-07-00-49/2013-01-31-07-23-52/2013-01-31-07-34-16/2013-01-31-07-38-19', '#', 'url', 0, 128, 5, 0, 0, '0000-00-00 00:00:00', 0, 1, '', 0, '{\"menu-anchor_title\":\"\",\"menu-anchor_css\":\"\",\"menu_image\":\"\",\"menu_text\":1,\"showmenutitle\":\"1\",\"desc\":\"\",\"cols\":\"1\",\"group\":\"0\",\"showgrouptitle\":\"1\",\"cwidth\":\"\",\"colxw\":\"\",\"class\":\"\",\"subcontent\":\"normal\"}', 93, 94, 0, '*', 0),
(131, 'mainmenu', 'Child Item', '2013-01-31-07-38-54', '', 'item/2013-01-31-07-00-49/2013-01-31-07-23-52/2013-01-31-07-34-16/2013-01-31-07-38-54', '#', 'url', 0, 128, 5, 0, 0, '0000-00-00 00:00:00', 0, 1, '', 0, '{\"menu-anchor_title\":\"\",\"menu-anchor_css\":\"\",\"menu_image\":\"\",\"menu_text\":1,\"showmenutitle\":\"1\",\"desc\":\"\",\"cols\":\"1\",\"group\":\"0\",\"showgrouptitle\":\"1\",\"cwidth\":\"\",\"colxw\":\"\",\"class\":\"\",\"subcontent\":\"normal\"}', 95, 96, 0, '*', 0),
(132, 'mainmenu', 'Category Blog', 'category-blog', '', 'joomla/category-blog', 'index.php?option=com_content&view=category&layout=blog&id=8', 'component', 0, 123, 2, 22, 0, '0000-00-00 00:00:00', 0, 1, '', 0, '{\"layout_type\":\"blog\",\"show_category_heading_title_text\":\"\",\"show_category_title\":\"\",\"show_description\":\"\",\"show_description_image\":\"\",\"maxLevel\":\"\",\"show_empty_categories\":\"\",\"show_no_articles\":\"\",\"show_subcat_desc\":\"\",\"show_cat_num_articles\":\"\",\"page_subheading\":\"\",\"num_leading_articles\":\"\",\"num_intro_articles\":\"\",\"num_columns\":\"\",\"num_links\":\"\",\"multi_column_order\":\"\",\"show_subcategory_content\":\"\",\"orderby_pri\":\"\",\"orderby_sec\":\"\",\"order_date\":\"\",\"show_pagination\":\"\",\"show_pagination_results\":\"\",\"show_title\":\"\",\"link_titles\":\"\",\"show_intro\":\"\",\"info_block_position\":\"\",\"show_category\":\"\",\"link_category\":\"\",\"show_parent_category\":\"\",\"link_parent_category\":\"\",\"show_author\":\"\",\"link_author\":\"\",\"show_create_date\":\"\",\"show_modify_date\":\"\",\"show_publish_date\":\"\",\"show_item_navigation\":\"\",\"show_vote\":\"\",\"show_readmore\":\"\",\"show_readmore_title\":\"\",\"show_icons\":\"\",\"show_print_icon\":\"\",\"show_email_icon\":\"\",\"show_hits\":\"\",\"show_noauth\":\"\",\"show_feed_link\":\"\",\"feed_summary\":\"\",\"menu-anchor_title\":\"\",\"menu-anchor_css\":\"\",\"menu_image\":\"\",\"menu_text\":1,\"page_title\":\"\",\"show_page_heading\":0,\"page_heading\":\"\",\"pageclass_sfx\":\"\",\"menu-meta_description\":\"\",\"menu-meta_keywords\":\"\",\"robots\":\"\",\"secure\":0,\"showmenutitle\":\"1\",\"desc\":\"\",\"cols\":\"1\",\"group\":\"0\",\"showgrouptitle\":\"1\",\"cwidth\":\"\",\"colxw\":\"\",\"class\":\"no-scroll\",\"subcontent\":\"normal\"}', 126, 127, 0, '*', 0),
(133, 'mainmenu', 'Single Article', 'single-article', '', 'joomla/single-article', 'index.php?option=com_content&view=article&id=1', 'component', 0, 123, 2, 22, 0, '0000-00-00 00:00:00', 0, 1, '', 0, '{\"show_title\":\"\",\"link_titles\":\"\",\"show_intro\":\"\",\"info_block_position\":\"\",\"show_category\":\"\",\"link_category\":\"\",\"show_parent_category\":\"\",\"link_parent_category\":\"\",\"show_author\":\"\",\"link_author\":\"\",\"show_create_date\":\"\",\"show_modify_date\":\"\",\"show_publish_date\":\"\",\"show_item_navigation\":\"\",\"show_vote\":\"\",\"show_tags\":\"\",\"show_icons\":\"\",\"show_print_icon\":\"\",\"show_email_icon\":\"\",\"show_hits\":\"\",\"show_noauth\":\"\",\"urls_position\":\"\",\"menu-anchor_title\":\"\",\"menu-anchor_css\":\"\",\"menu_image\":\"\",\"menu_text\":1,\"page_title\":\"\",\"show_page_heading\":0,\"page_heading\":\"\",\"pageclass_sfx\":\"\",\"menu-meta_description\":\"\",\"menu-meta_keywords\":\"\",\"robots\":\"\",\"secure\":0,\"showmenutitle\":\"1\",\"desc\":\"\",\"cols\":\"1\",\"group\":\"0\",\"showgrouptitle\":\"1\",\"cwidth\":\"\",\"colxw\":\"\",\"class\":\"no-scroll\",\"subcontent\":\"normal\"}', 128, 129, 0, '*', 0),
(134, 'mainmenu', 'Child Item', '2013-01-31-07-41-53', '', 'item/2013-01-31-07-00-49/2013-01-31-07-23-52/2013-01-31-07-29-22/2013-01-31-07-33-25/2013-01-31-07-41-53', '#', 'url', 0, 127, 6, 0, 0, '0000-00-00 00:00:00', 0, 1, '', 0, '{\"menu-anchor_title\":\"\",\"menu-anchor_css\":\"\",\"menu_image\":\"\",\"menu_text\":1,\"showmenutitle\":\"1\",\"desc\":\"\",\"cols\":\"1\",\"group\":\"0\",\"showgrouptitle\":\"1\",\"cwidth\":\"\",\"colxw\":\"\",\"class\":\"\",\"subcontent\":\"normal\"}', 104, 105, 0, '*', 0),
(135, 'mainmenu', 'Child Item', '2013-01-31-07-42-32', '', 'item/2013-01-31-07-00-49/2013-01-31-07-23-52/2013-01-31-07-29-22/2013-01-31-07-33-25/2013-01-31-07-42-32', '#', 'url', 0, 127, 6, 0, 0, '0000-00-00 00:00:00', 0, 1, '', 0, '{\"menu-anchor_title\":\"\",\"menu-anchor_css\":\"\",\"menu_image\":\"\",\"menu_text\":1,\"showmenutitle\":\"1\",\"desc\":\"\",\"cols\":\"1\",\"group\":\"0\",\"showgrouptitle\":\"1\",\"cwidth\":\"\",\"colxw\":\"\",\"class\":\"\",\"subcontent\":\"normal\"}', 106, 107, 0, '*', 0),
(136, 'mainmenu', 'Child Item', '2013-01-31-07-42-56', '', 'item/2013-01-31-07-00-49/2013-01-31-07-23-52/2013-01-31-07-29-22/2013-01-31-07-33-25/2013-01-31-07-42-56', '#', 'url', 0, 127, 6, 0, 0, '0000-00-00 00:00:00', 0, 1, '', 0, '{\"menu-anchor_title\":\"\",\"menu-anchor_css\":\"\",\"menu_image\":\"\",\"menu_text\":1,\"showmenutitle\":\"1\",\"desc\":\"\",\"cols\":\"1\",\"group\":\"0\",\"showgrouptitle\":\"1\",\"cwidth\":\"\",\"colxw\":\"\",\"class\":\"\",\"subcontent\":\"normal\"}', 108, 113, 0, '*', 0),
(137, 'mainmenu', 'Child Item', '2013-01-31-07-43-43', '', 'item/2013-01-31-07-00-49/2013-01-31-07-23-52/2013-01-31-07-29-22/2013-01-31-07-33-25/2013-01-31-07-42-56/2013-01-31-07-43-43', '#', 'url', 0, 136, 7, 0, 0, '0000-00-00 00:00:00', 0, 1, '', 0, '{\"menu-anchor_title\":\"\",\"menu-anchor_css\":\"\",\"menu_image\":\"\",\"menu_text\":1,\"showmenutitle\":\"1\",\"desc\":\"\",\"cols\":\"1\",\"group\":\"0\",\"showgrouptitle\":\"1\",\"cwidth\":\"\",\"colxw\":\"\",\"class\":\"\",\"subcontent\":\"normal\"}', 109, 110, 0, '*', 0),
(138, 'mainmenu', 'Child Item', '2013-01-31-07-44-11', '', 'item/2013-01-31-07-00-49/2013-01-31-07-23-52/2013-01-31-07-29-22/2013-01-31-07-33-25/2013-01-31-07-42-56/2013-01-31-07-44-11', '#', 'url', 0, 136, 7, 0, 0, '0000-00-00 00:00:00', 0, 1, '', 0, '{\"menu-anchor_title\":\"\",\"menu-anchor_css\":\"\",\"menu_image\":\"\",\"menu_text\":1,\"showmenutitle\":\"1\",\"desc\":\"\",\"cols\":\"1\",\"group\":\"0\",\"showgrouptitle\":\"1\",\"cwidth\":\"\",\"colxw\":\"\",\"class\":\"\",\"subcontent\":\"normal\"}', 111, 112, 0, '*', 0),
(139, 'mainmenu', 'Contact', 'contact', '', 'joomla/contact', 'index.php?option=com_contact&view=contact&id=1', 'component', 0, 123, 2, 8, 0, '0000-00-00 00:00:00', 0, 1, '', 0, '{\"presentation_style\":\"\",\"show_contact_category\":\"\",\"show_contact_list\":\"\",\"show_tags\":\"\",\"show_name\":\"\",\"show_position\":\"\",\"show_email\":\"\",\"show_street_address\":\"\",\"show_suburb\":\"\",\"show_state\":\"\",\"show_postcode\":\"\",\"show_country\":\"\",\"show_telephone\":\"\",\"show_mobile\":\"\",\"show_fax\":\"\",\"show_webpage\":\"\",\"show_misc\":\"\",\"show_image\":\"\",\"allow_vcard\":\"\",\"show_articles\":\"\",\"show_links\":\"\",\"linka_name\":\"\",\"linkb_name\":\"\",\"linkc_name\":\"\",\"linkd_name\":\"\",\"linke_name\":\"\",\"show_email_form\":\"\",\"show_email_copy\":\"\",\"banned_email\":\"\",\"banned_subject\":\"\",\"banned_text\":\"\",\"validate_session\":\"\",\"custom_reply\":\"\",\"redirect\":\"\",\"menu-anchor_title\":\"\",\"menu-anchor_css\":\"\",\"menu_image\":\"\",\"menu_text\":1,\"page_title\":\"\",\"show_page_heading\":0,\"page_heading\":\"\",\"pageclass_sfx\":\"\",\"menu-meta_description\":\"\",\"menu-meta_keywords\":\"\",\"robots\":\"\",\"secure\":0,\"showmenutitle\":\"1\",\"desc\":\"\",\"cols\":\"1\",\"group\":\"0\",\"showgrouptitle\":\"1\",\"cwidth\":\"\",\"colxw\":\"\",\"class\":\"no-scroll\",\"subcontent\":\"normal\"}', 130, 131, 0, '*', 0),
(140, 'mainmenu', 'Login', 'login', '', 'joomla/login', 'index.php?option=com_users&view=login', 'component', 0, 123, 2, 25, 0, '0000-00-00 00:00:00', 0, 1, '', 0, '{\"login_redirect_url\":\"\",\"logindescription_show\":\"1\",\"login_description\":\"\",\"login_image\":\"\",\"logout_redirect_url\":\"\",\"logoutdescription_show\":\"1\",\"logout_description\":\"\",\"logout_image\":\"\",\"menu-anchor_title\":\"\",\"menu-anchor_css\":\"\",\"menu_image\":\"\",\"menu_text\":1,\"page_title\":\"\",\"show_page_heading\":0,\"page_heading\":\"\",\"pageclass_sfx\":\"\",\"menu-meta_description\":\"\",\"menu-meta_keywords\":\"\",\"robots\":\"\",\"secure\":0,\"showmenutitle\":\"1\",\"desc\":\"\",\"cols\":\"1\",\"group\":\"0\",\"showgrouptitle\":\"1\",\"cwidth\":\"\",\"colxw\":\"\",\"class\":\"no-scroll\",\"subcontent\":\"normal\"}', 132, 133, 0, '*', 0),
(141, 'mainmenu', 'Registration', 'registration', '', 'joomla/registration', 'index.php?option=com_users&view=registration', 'component', 0, 123, 2, 25, 0, '0000-00-00 00:00:00', 0, 1, '', 0, '{\"menu-anchor_title\":\"\",\"menu-anchor_css\":\"\",\"menu_image\":\"\",\"menu_text\":1,\"page_title\":\"\",\"show_page_heading\":0,\"page_heading\":\"\",\"pageclass_sfx\":\"\",\"menu-meta_description\":\"\",\"menu-meta_keywords\":\"\",\"robots\":\"\",\"secure\":0,\"showmenutitle\":\"1\",\"desc\":\"\",\"cols\":\"1\",\"group\":\"0\",\"showgrouptitle\":\"1\",\"cwidth\":\"\",\"colxw\":\"\",\"class\":\"no-scroll\",\"subcontent\":\"normal\"}', 134, 135, 0, '*', 0),
(142, 'mainmenu', 'Accordion', 'accordion', '', 'item/2013-01-31-06-54-32/shortcode/accordion', 'index.php?option=com_content&view=article&id=3', 'component', 0, 107, 4, 22, 0, '0000-00-00 00:00:00', 0, 1, '', 0, '{\"show_title\":\"\",\"link_titles\":\"\",\"show_intro\":\"\",\"info_block_position\":\"\",\"show_category\":\"\",\"link_category\":\"\",\"show_parent_category\":\"\",\"link_parent_category\":\"\",\"show_author\":\"\",\"link_author\":\"\",\"show_create_date\":\"\",\"show_modify_date\":\"\",\"show_publish_date\":\"\",\"show_item_navigation\":\"\",\"show_vote\":\"\",\"show_tags\":\"\",\"show_icons\":\"\",\"show_print_icon\":\"\",\"show_email_icon\":\"\",\"show_hits\":\"\",\"show_noauth\":\"\",\"urls_position\":\"\",\"menu-anchor_title\":\"\",\"menu-anchor_css\":\"\",\"menu_image\":\"\",\"menu_text\":1,\"page_title\":\"\",\"show_page_heading\":0,\"page_heading\":\"\",\"pageclass_sfx\":\"\",\"menu-meta_description\":\"\",\"menu-meta_keywords\":\"\",\"robots\":\"\",\"secure\":0,\"showmenutitle\":\"1\",\"desc\":\"\",\"cols\":\"1\",\"group\":\"0\",\"showgrouptitle\":\"1\",\"cwidth\":\"\",\"colxw\":\"\",\"class\":\"no-scroll\",\"subcontent\":\"normal\"}', 58, 59, 0, '*', 0),
(143, 'mainmenu', 'Carousel', 'carousel', '', 'item/2013-01-31-06-54-32/shortcode/carousel', 'index.php?option=com_content&view=article&id=4', 'component', 0, 107, 4, 22, 0, '0000-00-00 00:00:00', 0, 1, '', 0, '{\"show_title\":\"\",\"link_titles\":\"\",\"show_intro\":\"\",\"info_block_position\":\"\",\"show_category\":\"\",\"link_category\":\"\",\"show_parent_category\":\"\",\"link_parent_category\":\"\",\"show_author\":\"\",\"link_author\":\"\",\"show_create_date\":\"\",\"show_modify_date\":\"\",\"show_publish_date\":\"\",\"show_item_navigation\":\"\",\"show_vote\":\"\",\"show_tags\":\"\",\"show_icons\":\"\",\"show_print_icon\":\"\",\"show_email_icon\":\"\",\"show_hits\":\"\",\"show_noauth\":\"\",\"urls_position\":\"\",\"menu-anchor_title\":\"\",\"menu-anchor_css\":\"\",\"menu_image\":\"\",\"menu_text\":1,\"page_title\":\"\",\"show_page_heading\":0,\"page_heading\":\"\",\"pageclass_sfx\":\"\",\"menu-meta_description\":\"\",\"menu-meta_keywords\":\"\",\"robots\":\"\",\"secure\":0,\"showmenutitle\":\"1\",\"desc\":\"\",\"cols\":\"1\",\"group\":\"0\",\"showgrouptitle\":\"1\",\"cwidth\":\"\",\"colxw\":\"\",\"class\":\"no-scroll\",\"subcontent\":\"normal\"}', 60, 61, 0, '*', 0),
(144, 'mainmenu', 'Tab', 'tab', '', 'item/2013-01-31-06-54-32/shortcode/tab', 'index.php?option=com_content&view=article&id=5', 'component', 0, 107, 4, 22, 0, '0000-00-00 00:00:00', 0, 1, '', 0, '{\"show_title\":\"\",\"link_titles\":\"\",\"show_intro\":\"\",\"info_block_position\":\"\",\"show_category\":\"\",\"link_category\":\"\",\"show_parent_category\":\"\",\"link_parent_category\":\"\",\"show_author\":\"\",\"link_author\":\"\",\"show_create_date\":\"\",\"show_modify_date\":\"\",\"show_publish_date\":\"\",\"show_item_navigation\":\"\",\"show_vote\":\"\",\"show_tags\":\"\",\"show_icons\":\"\",\"show_print_icon\":\"\",\"show_email_icon\":\"\",\"show_hits\":\"\",\"show_noauth\":\"\",\"urls_position\":\"\",\"menu-anchor_title\":\"\",\"menu-anchor_css\":\"\",\"menu_image\":\"\",\"menu_text\":1,\"page_title\":\"\",\"show_page_heading\":0,\"page_heading\":\"\",\"pageclass_sfx\":\"\",\"menu-meta_description\":\"\",\"menu-meta_keywords\":\"\",\"robots\":\"\",\"secure\":0,\"showmenutitle\":\"1\",\"desc\":\"\",\"cols\":\"1\",\"group\":\"0\",\"showgrouptitle\":\"1\",\"cwidth\":\"\",\"colxw\":\"\",\"class\":\"no-scroll\",\"subcontent\":\"normal\"}', 62, 63, 0, '*', 0),
(145, 'mainmenu', 'Map', 'map', '', 'item/2013-01-31-06-54-32/shortcode/map', 'index.php?option=com_content&view=article&id=6', 'component', 0, 107, 4, 22, 0, '0000-00-00 00:00:00', 0, 1, '', 0, '{\"show_title\":\"\",\"link_titles\":\"\",\"show_intro\":\"\",\"info_block_position\":\"\",\"show_category\":\"\",\"link_category\":\"\",\"show_parent_category\":\"\",\"link_parent_category\":\"\",\"show_author\":\"\",\"link_author\":\"\",\"show_create_date\":\"\",\"show_modify_date\":\"\",\"show_publish_date\":\"\",\"show_item_navigation\":\"\",\"show_vote\":\"\",\"show_tags\":\"\",\"show_icons\":\"\",\"show_print_icon\":\"\",\"show_email_icon\":\"\",\"show_hits\":\"\",\"show_noauth\":\"\",\"urls_position\":\"\",\"menu-anchor_title\":\"\",\"menu-anchor_css\":\"\",\"menu_image\":\"\",\"menu_text\":1,\"page_title\":\"\",\"show_page_heading\":0,\"page_heading\":\"\",\"pageclass_sfx\":\"\",\"menu-meta_description\":\"\",\"menu-meta_keywords\":\"\",\"robots\":\"\",\"secure\":0,\"showmenutitle\":\"1\",\"desc\":\"\",\"cols\":\"1\",\"group\":\"0\",\"showgrouptitle\":\"1\",\"cwidth\":\"\",\"colxw\":\"\",\"class\":\"no-scroll\",\"subcontent\":\"normal\"}', 78, 79, 0, '*', 0),
(146, 'mainmenu', 'Testimonial ', 'testimonial', '', 'item/2013-01-31-06-54-32/shortcode/testimonial', 'index.php?option=com_content&view=article&id=7', 'component', 0, 107, 4, 22, 0, '0000-00-00 00:00:00', 0, 1, '', 0, '{\"show_title\":\"\",\"link_titles\":\"\",\"show_intro\":\"\",\"info_block_position\":\"\",\"show_category\":\"\",\"link_category\":\"\",\"show_parent_category\":\"\",\"link_parent_category\":\"\",\"show_author\":\"\",\"link_author\":\"\",\"show_create_date\":\"\",\"show_modify_date\":\"\",\"show_publish_date\":\"\",\"show_item_navigation\":\"\",\"show_vote\":\"\",\"show_tags\":\"\",\"show_icons\":\"\",\"show_print_icon\":\"\",\"show_email_icon\":\"\",\"show_hits\":\"\",\"show_noauth\":\"\",\"urls_position\":\"\",\"menu-anchor_title\":\"\",\"menu-anchor_css\":\"\",\"menu_image\":\"\",\"menu_text\":1,\"page_title\":\"\",\"show_page_heading\":0,\"page_heading\":\"\",\"pageclass_sfx\":\"\",\"menu-meta_description\":\"\",\"menu-meta_keywords\":\"\",\"robots\":\"\",\"secure\":0,\"showmenutitle\":\"1\",\"desc\":\"\",\"cols\":\"1\",\"group\":\"0\",\"showgrouptitle\":\"1\",\"cwidth\":\"\",\"colxw\":\"\",\"class\":\"no-scroll\",\"subcontent\":\"normal\"}', 76, 77, 0, '*', 0),
(147, 'mainmenu', 'Alert', 'alert', '', 'item/2013-01-31-06-54-32/shortcode/alert', 'index.php?option=com_content&view=article&id=8', 'component', 0, 107, 4, 22, 0, '0000-00-00 00:00:00', 0, 1, '', 0, '{\"show_title\":\"\",\"link_titles\":\"\",\"show_intro\":\"\",\"info_block_position\":\"\",\"show_category\":\"\",\"link_category\":\"\",\"show_parent_category\":\"\",\"link_parent_category\":\"\",\"show_author\":\"\",\"link_author\":\"\",\"show_create_date\":\"\",\"show_modify_date\":\"\",\"show_publish_date\":\"\",\"show_item_navigation\":\"\",\"show_vote\":\"\",\"show_tags\":\"\",\"show_icons\":\"\",\"show_print_icon\":\"\",\"show_email_icon\":\"\",\"show_hits\":\"\",\"show_noauth\":\"\",\"urls_position\":\"\",\"menu-anchor_title\":\"\",\"menu-anchor_css\":\"\",\"menu_image\":\"\",\"menu_text\":1,\"page_title\":\"\",\"show_page_heading\":0,\"page_heading\":\"\",\"pageclass_sfx\":\"\",\"menu-meta_description\":\"\",\"menu-meta_keywords\":\"\",\"robots\":\"\",\"secure\":0,\"showmenutitle\":\"1\",\"desc\":\"\",\"cols\":\"1\",\"group\":\"0\",\"showgrouptitle\":\"1\",\"cwidth\":\"\",\"colxw\":\"\",\"class\":\"no-scroll\",\"subcontent\":\"normal\"}', 74, 75, 0, '*', 0),
(148, 'mainmenu', 'Button', 'button', '', 'item/2013-01-31-06-54-32/shortcode/button', 'index.php?option=com_content&view=article&id=9', 'component', 0, 107, 4, 22, 0, '0000-00-00 00:00:00', 0, 1, '', 0, '{\"show_title\":\"\",\"link_titles\":\"\",\"show_intro\":\"\",\"info_block_position\":\"\",\"show_category\":\"\",\"link_category\":\"\",\"show_parent_category\":\"\",\"link_parent_category\":\"\",\"show_author\":\"\",\"link_author\":\"\",\"show_create_date\":\"\",\"show_modify_date\":\"\",\"show_publish_date\":\"\",\"show_item_navigation\":\"\",\"show_vote\":\"\",\"show_tags\":\"\",\"show_icons\":\"\",\"show_print_icon\":\"\",\"show_email_icon\":\"\",\"show_hits\":\"\",\"show_noauth\":\"\",\"urls_position\":\"\",\"menu-anchor_title\":\"\",\"menu-anchor_css\":\"\",\"menu_image\":\"\",\"menu_text\":1,\"page_title\":\"\",\"show_page_heading\":0,\"page_heading\":\"\",\"pageclass_sfx\":\"\",\"menu-meta_description\":\"\",\"menu-meta_keywords\":\"\",\"robots\":\"\",\"secure\":0,\"showmenutitle\":\"1\",\"desc\":\"\",\"cols\":\"1\",\"group\":\"0\",\"showgrouptitle\":\"1\",\"cwidth\":\"\",\"colxw\":\"\",\"class\":\"no-scroll\",\"subcontent\":\"normal\"}', 72, 73, 0, '*', 0),
(149, 'mainmenu', 'Icon', 'icon', '', 'item/2013-01-31-06-54-32/shortcode/icon', 'index.php?option=com_content&view=article&id=10', 'component', 0, 107, 4, 22, 0, '0000-00-00 00:00:00', 0, 1, '', 0, '{\"show_title\":\"\",\"link_titles\":\"\",\"show_intro\":\"\",\"info_block_position\":\"\",\"show_category\":\"\",\"link_category\":\"\",\"show_parent_category\":\"\",\"link_parent_category\":\"\",\"show_author\":\"\",\"link_author\":\"\",\"show_create_date\":\"\",\"show_modify_date\":\"\",\"show_publish_date\":\"\",\"show_item_navigation\":\"\",\"show_vote\":\"\",\"show_tags\":\"\",\"show_icons\":\"\",\"show_print_icon\":\"\",\"show_email_icon\":\"\",\"show_hits\":\"\",\"show_noauth\":\"\",\"urls_position\":\"\",\"menu-anchor_title\":\"\",\"menu-anchor_css\":\"\",\"menu_image\":\"\",\"menu_text\":1,\"page_title\":\"\",\"show_page_heading\":0,\"page_heading\":\"\",\"pageclass_sfx\":\"\",\"menu-meta_description\":\"\",\"menu-meta_keywords\":\"\",\"robots\":\"\",\"secure\":0,\"showmenutitle\":\"1\",\"desc\":\"\",\"cols\":\"1\",\"group\":\"0\",\"showgrouptitle\":\"1\",\"cwidth\":\"\",\"colxw\":\"\",\"class\":\"no-scroll\",\"subcontent\":\"normal\"}', 64, 65, 0, '*', 0),
(150, 'mainmenu', 'Column', 'column', '', 'item/2013-01-31-06-54-32/shortcode/column', 'index.php?option=com_content&view=article&id=11', 'component', 0, 107, 4, 22, 0, '0000-00-00 00:00:00', 0, 1, '', 0, '{\"show_title\":\"\",\"link_titles\":\"\",\"show_intro\":\"\",\"info_block_position\":\"\",\"show_category\":\"\",\"link_category\":\"\",\"show_parent_category\":\"\",\"link_parent_category\":\"\",\"show_author\":\"\",\"link_author\":\"\",\"show_create_date\":\"\",\"show_modify_date\":\"\",\"show_publish_date\":\"\",\"show_item_navigation\":\"\",\"show_vote\":\"\",\"show_tags\":\"\",\"show_icons\":\"\",\"show_print_icon\":\"\",\"show_email_icon\":\"\",\"show_hits\":\"\",\"show_noauth\":\"\",\"urls_position\":\"\",\"menu-anchor_title\":\"\",\"menu-anchor_css\":\"\",\"menu_image\":\"\",\"menu_text\":1,\"page_title\":\"\",\"show_page_heading\":0,\"page_heading\":\"\",\"pageclass_sfx\":\"\",\"menu-meta_description\":\"\",\"menu-meta_keywords\":\"\",\"robots\":\"\",\"secure\":0,\"showmenutitle\":\"1\",\"desc\":\"\",\"cols\":\"1\",\"group\":\"0\",\"showgrouptitle\":\"1\",\"cwidth\":\"\",\"colxw\":\"\",\"class\":\"no-scroll\",\"subcontent\":\"normal\"}', 66, 67, 0, '*', 0),
(151, 'top-menu', 'About Us', 'about-us', '', 'about-us', 'index.php?option=com_content&view=article&id=12', 'component', 1, 1, 1, 22, 0, '0000-00-00 00:00:00', 0, 1, '', 0, '{\"show_title\":\"\",\"link_titles\":\"\",\"show_intro\":\"\",\"show_category\":\"\",\"link_category\":\"\",\"show_parent_category\":\"\",\"link_parent_category\":\"\",\"show_author\":\"\",\"link_author\":\"\",\"show_create_date\":\"\",\"show_modify_date\":\"\",\"show_publish_date\":\"\",\"show_item_navigation\":\"\",\"show_vote\":\"\",\"show_icons\":\"\",\"show_print_icon\":\"\",\"show_email_icon\":\"\",\"show_hits\":\"\",\"show_noauth\":\"\",\"urls_position\":\"\",\"menu-anchor_title\":\"\",\"menu-anchor_css\":\"\",\"menu_image\":\"\",\"menu_text\":1,\"page_title\":\"\",\"show_page_heading\":0,\"page_heading\":\"\",\"pageclass_sfx\":\"\",\"menu-meta_description\":\"\",\"menu-meta_keywords\":\"\",\"robots\":\"\",\"secure\":0,\"showmenutitle\":\"1\",\"desc\":\"\",\"cols\":\"1\",\"group\":\"0\",\"showgrouptitle\":\"1\",\"cwidth\":\"\",\"colxw\":\"\",\"class\":\"\",\"subcontent\":\"normal\"}', 163, 164, 0, '*', 0),
(152, 'top-menu', 'About Joomla', 'about-joomla', '', 'about-joomla', 'index.php?option=com_content&view=article&id=1', 'component', 1, 1, 1, 22, 0, '0000-00-00 00:00:00', 0, 1, '', 0, '{\"show_title\":\"\",\"link_titles\":\"\",\"show_intro\":\"\",\"show_category\":\"\",\"link_category\":\"\",\"show_parent_category\":\"\",\"link_parent_category\":\"\",\"show_author\":\"\",\"link_author\":\"\",\"show_create_date\":\"\",\"show_modify_date\":\"\",\"show_publish_date\":\"\",\"show_item_navigation\":\"\",\"show_vote\":\"\",\"show_icons\":\"\",\"show_print_icon\":\"\",\"show_email_icon\":\"\",\"show_hits\":\"\",\"show_noauth\":\"\",\"urls_position\":\"\",\"menu-anchor_title\":\"\",\"menu-anchor_css\":\"\",\"menu_image\":\"\",\"menu_text\":1,\"page_title\":\"\",\"show_page_heading\":0,\"page_heading\":\"\",\"pageclass_sfx\":\"\",\"menu-meta_description\":\"\",\"menu-meta_keywords\":\"\",\"robots\":\"\",\"secure\":0,\"showmenutitle\":\"1\",\"desc\":\"\",\"cols\":\"1\",\"group\":\"0\",\"showgrouptitle\":\"1\",\"cwidth\":\"\",\"colxw\":\"\",\"class\":\"\",\"subcontent\":\"normal\"}', 165, 166, 0, '*', 0),
(153, 'top-menu', 'Joomla Overview', 'joomla-overview', '', 'joomla-overview', 'index.php?option=com_content&view=article&id=1', 'component', 1, 1, 1, 22, 0, '0000-00-00 00:00:00', 0, 1, '', 0, '{\"show_title\":\"\",\"link_titles\":\"\",\"show_intro\":\"\",\"show_category\":\"\",\"link_category\":\"\",\"show_parent_category\":\"\",\"link_parent_category\":\"\",\"show_author\":\"\",\"link_author\":\"\",\"show_create_date\":\"\",\"show_modify_date\":\"\",\"show_publish_date\":\"\",\"show_item_navigation\":\"\",\"show_vote\":\"\",\"show_icons\":\"\",\"show_print_icon\":\"\",\"show_email_icon\":\"\",\"show_hits\":\"\",\"show_noauth\":\"\",\"urls_position\":\"\",\"menu-anchor_title\":\"\",\"menu-anchor_css\":\"\",\"menu_image\":\"\",\"menu_text\":1,\"page_title\":\"\",\"show_page_heading\":0,\"page_heading\":\"\",\"pageclass_sfx\":\"\",\"menu-meta_description\":\"\",\"menu-meta_keywords\":\"\",\"robots\":\"\",\"secure\":0,\"showmenutitle\":\"1\",\"desc\":\"\",\"cols\":\"1\",\"group\":\"0\",\"showgrouptitle\":\"1\",\"cwidth\":\"\",\"colxw\":\"\",\"class\":\"\",\"subcontent\":\"normal\"}', 167, 168, 0, '*', 0),
(154, 'mainmenu', 'Gallery', 'gallery', '', 'item/2013-01-31-06-54-32/shortcode/gallery', 'index.php?option=com_content&view=article&id=16', 'component', 0, 107, 4, 22, 0, '0000-00-00 00:00:00', 0, 1, '', 0, '{\"show_title\":\"\",\"link_titles\":\"\",\"show_intro\":\"\",\"info_block_position\":\"\",\"show_category\":\"\",\"link_category\":\"\",\"show_parent_category\":\"\",\"link_parent_category\":\"\",\"show_author\":\"\",\"link_author\":\"\",\"show_create_date\":\"\",\"show_modify_date\":\"\",\"show_publish_date\":\"\",\"show_item_navigation\":\"\",\"show_vote\":\"\",\"show_tags\":\"\",\"show_icons\":\"\",\"show_print_icon\":\"\",\"show_email_icon\":\"\",\"show_hits\":\"\",\"show_noauth\":\"\",\"urls_position\":\"\",\"menu-anchor_title\":\"\",\"menu-anchor_css\":\"\",\"menu_image\":\"\",\"menu_text\":1,\"page_title\":\"\",\"show_page_heading\":0,\"page_heading\":\"\",\"pageclass_sfx\":\"\",\"menu-meta_description\":\"\",\"menu-meta_keywords\":\"\",\"robots\":\"\",\"secure\":0,\"showmenutitle\":\"1\",\"desc\":\"\",\"cols\":\"1\",\"group\":\"0\",\"showgrouptitle\":\"1\",\"cwidth\":\"\",\"colxw\":\"\",\"class\":\"no-scroll\",\"subcontent\":\"normal\"}', 68, 69, 0, '*', 0),
(155, 'mainmenu', 'Video', 'video', '', 'item/2013-01-31-06-54-32/shortcode/video', 'index.php?option=com_content&view=article&id=17', 'component', 0, 107, 4, 22, 0, '0000-00-00 00:00:00', 0, 1, '', 0, '{\"show_title\":\"\",\"link_titles\":\"\",\"show_intro\":\"\",\"info_block_position\":\"\",\"show_category\":\"\",\"link_category\":\"\",\"show_parent_category\":\"\",\"link_parent_category\":\"\",\"show_author\":\"\",\"link_author\":\"\",\"show_create_date\":\"\",\"show_modify_date\":\"\",\"show_publish_date\":\"\",\"show_item_navigation\":\"\",\"show_vote\":\"\",\"show_tags\":\"\",\"show_icons\":\"\",\"show_print_icon\":\"\",\"show_email_icon\":\"\",\"show_hits\":\"\",\"show_noauth\":\"\",\"urls_position\":\"\",\"menu-anchor_title\":\"\",\"menu-anchor_css\":\"\",\"menu_image\":\"\",\"menu_text\":1,\"page_title\":\"\",\"show_page_heading\":0,\"page_heading\":\"\",\"pageclass_sfx\":\"\",\"menu-meta_description\":\"\",\"menu-meta_keywords\":\"\",\"robots\":\"\",\"secure\":0,\"showmenutitle\":\"1\",\"desc\":\"\",\"cols\":\"1\",\"group\":\"0\",\"showgrouptitle\":\"1\",\"cwidth\":\"\",\"colxw\":\"\",\"class\":\"no-scroll\",\"subcontent\":\"normal\"}', 70, 71, 0, '*', 0),
(156, 'mainmenu', '404', '2013-02-06-08-37-59', '', 'joomla/2013-02-06-08-37-59', 'index.php?option=404', 'url', 0, 123, 2, 0, 0, '0000-00-00 00:00:00', 0, 1, '', 0, '{\"menu-anchor_title\":\"\",\"menu-anchor_css\":\"\",\"menu_image\":\"\",\"menu_text\":1,\"showmenutitle\":\"1\",\"desc\":\"\",\"cols\":\"1\",\"group\":\"0\",\"showgrouptitle\":\"1\",\"cwidth\":\"\",\"colxw\":\"\",\"class\":\"no-scroll\",\"subcontent\":\"normal\"}', 136, 137, 0, '*', 0),
(157, 'mainmenu', 'Services', '2013-11-07-05-29-34', '', '2013-11-07-05-29-34', '#sp-service-wrapper', 'url', 1, 1, 1, 0, 0, '0000-00-00 00:00:00', 0, 1, ' ', 0, '{\"menu-anchor_title\":\"\",\"menu-anchor_css\":\"\",\"menu-anchor_rel\":\"\",\"menu_image\":\"\",\"menu_image_css\":\"\",\"menu_text\":1,\"menu_show\":1,\"showmenutitle\":\"1\",\"desc\":\"\",\"cols\":\"6\",\"group\":\"0\",\"showgrouptitle\":\"1\",\"cwidth\":\"\",\"colxw\":\"\",\"class\":\"\",\"subcontent\":\"normal\"}', 13, 26, 0, '*', 0),
(158, 'mainmenu', 'Portfolio', '2013-11-07-05-30-28', '', '2013-11-07-05-30-28', '#sp-portfolio-wrapper', 'url', 0, 1, 1, 0, 0, '0000-00-00 00:00:00', 0, 1, '', 0, '{\"menu-anchor_title\":\"\",\"menu-anchor_css\":\"\",\"menu_image\":\"\",\"menu_text\":1,\"showmenutitle\":\"1\",\"desc\":\"\",\"cols\":\"1\",\"group\":\"0\",\"showgrouptitle\":\"1\",\"cwidth\":\"\",\"colxw\":\"\",\"class\":\"\",\"subcontent\":\"normal\"}', 27, 28, 0, '*', 0),
(159, 'mainmenu', 'Pricing', '2013-11-07-05-30-59', '', '2013-11-07-05-30-59', '#sp-pricing-wrapper', 'url', 0, 1, 1, 0, 0, '0000-00-00 00:00:00', 0, 1, '', 0, '{\"menu-anchor_title\":\"\",\"menu-anchor_css\":\"\",\"menu_image\":\"\",\"menu_text\":1,\"showmenutitle\":\"1\",\"desc\":\"\",\"cols\":\"1\",\"group\":\"0\",\"showgrouptitle\":\"1\",\"cwidth\":\"\",\"colxw\":\"\",\"class\":\"\",\"subcontent\":\"normal\"}', 37, 38, 0, '*', 0),
(160, 'mainmenu', 'About Us', '2013-11-07-05-31-54', '', '2013-11-07-05-31-54', '#sp-team-wrapper', 'url', 0, 1, 1, 0, 0, '0000-00-00 00:00:00', 0, 1, ' ', 0, '{\"menu-anchor_title\":\"\",\"menu-anchor_css\":\"\",\"menu-anchor_rel\":\"\",\"menu_image\":\"\",\"menu_image_css\":\"\",\"menu_text\":1,\"menu_show\":1,\"showmenutitle\":\"1\",\"desc\":\"\",\"cols\":\"1\",\"group\":\"0\",\"showgrouptitle\":\"1\",\"cwidth\":\"\",\"colxw\":\"\",\"class\":\"\",\"subcontent\":\"normal\"}', 39, 40, 0, '*', 0),
(161, 'mainmenu', 'Contact', '2013-11-07-05-32-22', '', '2013-11-07-05-32-22', '#sp-bottom-wrapper', 'url', 1, 1, 1, 0, 0, '0000-00-00 00:00:00', 0, 1, '', 0, '{\"menu-anchor_title\":\"\",\"menu-anchor_css\":\"\",\"menu_image\":\"\",\"menu_text\":1,\"showmenutitle\":\"1\",\"desc\":\"\",\"cols\":\"1\",\"group\":\"0\",\"showgrouptitle\":\"1\",\"cwidth\":\"\",\"colxw\":\"\",\"class\":\"\",\"subcontent\":\"normal\"}', 41, 42, 0, '*', 0),
(162, 'main', 'com_tags', 'Tags', '', 'Tags', 'index.php?option=com_tags', 'component', 1, 1, 1, 29, 0, '0000-00-00 00:00:00', 0, 1, 'class:tags', 0, '', 123, 124, 0, '', 1),
(163, 'main', 'com_postinstall', 'Post-installation messages', '', 'Post-installation messages', 'index.php?option=com_postinstall', 'component', 1, 1, 1, 32, 0, '0000-00-00 00:00:00', 0, 1, 'class:postinstall', 0, '', 121, 122, 0, '*', 1),
(170, 'main', 'com_associations', 'multilingual-associations', '', 'multilingual-associations', 'index.php?option=com_associations', 'component', 1, 1, 1, 34, 0, '0000-00-00 00:00:00', 0, 1, 'class:associations', 0, '{}', 169, 170, 0, '*', 1),
(171, 'main', 'com_weblinks', 'com-weblinks', '', 'com-weblinks', 'index.php?option=com_weblinks', 'component', 1, 1, 1, 21, 0, '0000-00-00 00:00:00', 0, 1, 'class:weblinks', 0, '{}', 171, 176, 0, '', 1);
INSERT INTO `t41uf_menu` (`id`, `menutype`, `title`, `alias`, `note`, `path`, `link`, `type`, `published`, `parent_id`, `level`, `component_id`, `checked_out`, `checked_out_time`, `browserNav`, `access`, `img`, `template_style_id`, `params`, `lft`, `rgt`, `home`, `language`, `client_id`) VALUES
(172, 'main', 'com_weblinks_links', 'com-weblinks-links', '', 'com-weblinks/com-weblinks-links', 'index.php?option=com_weblinks', 'component', 1, 171, 2, 21, 0, '0000-00-00 00:00:00', 0, 1, 'class:weblinks', 0, '{}', 172, 173, 0, '', 1),
(173, 'main', 'com_weblinks_categories', 'com-weblinks-categories', '', 'com-weblinks/com-weblinks-categories', 'index.php?option=com_categories&extension=com_weblinks', 'component', 1, 171, 2, 21, 0, '0000-00-00 00:00:00', 0, 1, 'class:weblinks-cat', 0, '{}', 174, 175, 0, '', 1),
(174, 'mainmenu', 'Test', 'test', '', 'test', 'index.php?option=com_content&view=article&id=30', 'component', -2, 1, 1, 22, 0, '0000-00-00 00:00:00', 0, 1, ' ', 0, '{\"show_title\":\"\",\"link_titles\":\"\",\"show_intro\":\"\",\"info_block_position\":\"\",\"info_block_show_title\":\"\",\"show_category\":\"\",\"link_category\":\"\",\"show_parent_category\":\"\",\"link_parent_category\":\"\",\"show_associations\":\"\",\"show_author\":\"\",\"link_author\":\"\",\"show_create_date\":\"\",\"show_modify_date\":\"\",\"show_publish_date\":\"\",\"show_item_navigation\":\"\",\"show_vote\":\"\",\"show_icons\":\"\",\"show_print_icon\":\"\",\"show_email_icon\":\"\",\"show_hits\":\"\",\"show_tags\":\"\",\"show_noauth\":\"\",\"urls_position\":\"\",\"menu-anchor_title\":\"\",\"menu-anchor_css\":\"\",\"menu_image\":\"\",\"menu_image_css\":\"\",\"menu_text\":1,\"menu_show\":1,\"page_title\":\"\",\"show_page_heading\":\"\",\"page_heading\":\"\",\"pageclass_sfx\":\"\",\"menu-meta_description\":\"\",\"menu-meta_keywords\":\"\",\"robots\":\"\",\"secure\":0,\"showmenutitle\":\"1\",\"desc\":\"\",\"cols\":\"1\",\"group\":\"0\",\"showgrouptitle\":\"1\",\"cwidth\":\"\",\"colxw\":\"\",\"class\":\"\",\"subcontent\":\"normal\"}', 177, 178, 0, '*', 0),
(175, 'mainmenu', 'Architecture', 'architecture', '', '2013-11-07-05-29-34/architecture', 'index.php?option=com_content&view=article&id=31', 'component', 1, 157, 2, 22, 0, '0000-00-00 00:00:00', 0, 1, ' ', 0, '{\"show_title\":\"\",\"link_titles\":\"\",\"show_intro\":\"\",\"info_block_position\":\"\",\"info_block_show_title\":\"\",\"show_category\":\"\",\"link_category\":\"\",\"show_parent_category\":\"\",\"link_parent_category\":\"\",\"show_associations\":\"\",\"show_author\":\"\",\"link_author\":\"\",\"show_create_date\":\"\",\"show_modify_date\":\"\",\"show_publish_date\":\"\",\"show_item_navigation\":\"\",\"show_vote\":\"\",\"show_icons\":\"\",\"show_print_icon\":\"\",\"show_email_icon\":\"\",\"show_hits\":\"\",\"show_tags\":\"\",\"show_noauth\":\"\",\"urls_position\":\"\",\"menu-anchor_title\":\"\",\"menu-anchor_css\":\"\",\"menu_image\":\"\",\"menu_image_css\":\"\",\"menu_text\":1,\"menu_show\":1,\"page_title\":\"\",\"show_page_heading\":\"\",\"page_heading\":\"\",\"pageclass_sfx\":\"\",\"menu-meta_description\":\"\",\"menu-meta_keywords\":\"\",\"robots\":\"\",\"secure\":0,\"showmenutitle\":\"1\",\"desc\":\"\",\"cols\":\"1\",\"group\":\"0\",\"showgrouptitle\":\"1\",\"cwidth\":\"\",\"colxw\":\"\",\"class\":\"\",\"subcontent\":\"normal\"}', 14, 15, 0, '*', 0),
(176, 'mainmenu', '3D modeling', '3d', '', '2013-11-07-05-29-34/3d', 'index.php?option=com_content&view=article&id=32', 'component', 1, 157, 2, 22, 0, '0000-00-00 00:00:00', 0, 1, ' ', 0, '{\"show_title\":\"\",\"link_titles\":\"\",\"show_intro\":\"\",\"info_block_position\":\"\",\"info_block_show_title\":\"\",\"show_category\":\"\",\"link_category\":\"\",\"show_parent_category\":\"\",\"link_parent_category\":\"\",\"show_associations\":\"\",\"show_author\":\"\",\"link_author\":\"\",\"show_create_date\":\"\",\"show_modify_date\":\"\",\"show_publish_date\":\"\",\"show_item_navigation\":\"\",\"show_vote\":\"\",\"show_icons\":\"\",\"show_print_icon\":\"\",\"show_email_icon\":\"\",\"show_hits\":\"\",\"show_tags\":\"\",\"show_noauth\":\"\",\"urls_position\":\"\",\"menu-anchor_title\":\"\",\"menu-anchor_css\":\"\",\"menu_image\":\"\",\"menu_image_css\":\"\",\"menu_text\":1,\"menu_show\":1,\"page_title\":\"\",\"show_page_heading\":\"\",\"page_heading\":\"\",\"pageclass_sfx\":\"\",\"menu-meta_description\":\"\",\"menu-meta_keywords\":\"\",\"robots\":\"\",\"secure\":0,\"showmenutitle\":\"1\",\"desc\":\"\",\"cols\":\"1\",\"group\":\"0\",\"showgrouptitle\":\"1\",\"cwidth\":\"\",\"colxw\":\"\",\"class\":\"\",\"subcontent\":\"normal\"}', 16, 17, 0, '*', 0),
(177, 'mainmenu', 'Graphic Design', 'graphic', '', '2013-11-07-05-29-34/graphic', 'index.php?option=com_content&view=article&id=33', 'component', 1, 157, 2, 22, 0, '0000-00-00 00:00:00', 0, 1, ' ', 0, '{\"show_title\":\"\",\"link_titles\":\"\",\"show_intro\":\"\",\"info_block_position\":\"\",\"info_block_show_title\":\"\",\"show_category\":\"\",\"link_category\":\"\",\"show_parent_category\":\"\",\"link_parent_category\":\"\",\"show_associations\":\"\",\"show_author\":\"\",\"link_author\":\"\",\"show_create_date\":\"\",\"show_modify_date\":\"\",\"show_publish_date\":\"\",\"show_item_navigation\":\"\",\"show_vote\":\"\",\"show_icons\":\"\",\"show_print_icon\":\"\",\"show_email_icon\":\"\",\"show_hits\":\"\",\"show_tags\":\"\",\"show_noauth\":\"\",\"urls_position\":\"\",\"menu-anchor_title\":\"\",\"menu-anchor_css\":\"\",\"menu_image\":\"\",\"menu_image_css\":\"\",\"menu_text\":1,\"menu_show\":1,\"page_title\":\"\",\"show_page_heading\":\"\",\"page_heading\":\"\",\"pageclass_sfx\":\"\",\"menu-meta_description\":\"\",\"menu-meta_keywords\":\"\",\"robots\":\"\",\"secure\":0,\"showmenutitle\":\"1\",\"desc\":\"\",\"cols\":\"1\",\"group\":\"0\",\"showgrouptitle\":\"1\",\"cwidth\":\"\",\"colxw\":\"\",\"class\":\"\",\"subcontent\":\"normal\"}', 18, 19, 0, '*', 0),
(178, 'mainmenu', 'WEB Design', 'web', '', '2013-11-07-05-29-34/web', 'index.php?option=com_content&view=article&id=34', 'component', 1, 157, 2, 22, 0, '0000-00-00 00:00:00', 0, 1, ' ', 0, '{\"show_title\":\"\",\"link_titles\":\"\",\"show_intro\":\"\",\"info_block_position\":\"\",\"info_block_show_title\":\"\",\"show_category\":\"\",\"link_category\":\"\",\"show_parent_category\":\"\",\"link_parent_category\":\"\",\"show_associations\":\"\",\"show_author\":\"\",\"link_author\":\"\",\"show_create_date\":\"\",\"show_modify_date\":\"\",\"show_publish_date\":\"\",\"show_item_navigation\":\"\",\"show_vote\":\"\",\"show_icons\":\"\",\"show_print_icon\":\"\",\"show_email_icon\":\"\",\"show_hits\":\"\",\"show_tags\":\"\",\"show_noauth\":\"\",\"urls_position\":\"\",\"menu-anchor_title\":\"\",\"menu-anchor_css\":\"\",\"menu_image\":\"\",\"menu_image_css\":\"\",\"menu_text\":1,\"menu_show\":1,\"page_title\":\"\",\"show_page_heading\":\"\",\"page_heading\":\"\",\"pageclass_sfx\":\"\",\"menu-meta_description\":\"\",\"menu-meta_keywords\":\"\",\"robots\":\"\",\"secure\":0,\"showmenutitle\":\"1\",\"desc\":\"\",\"cols\":\"1\",\"group\":\"0\",\"showgrouptitle\":\"1\",\"cwidth\":\"\",\"colxw\":\"\",\"class\":\"\",\"subcontent\":\"normal\"}', 20, 21, 0, '*', 0),
(179, 'mainmenu', 'Interior', 'interior', '', '2013-11-07-05-29-34/interior', 'index.php?option=com_content&view=article&id=35', 'component', 1, 157, 2, 22, 0, '0000-00-00 00:00:00', 0, 1, ' ', 0, '{\"show_title\":\"\",\"link_titles\":\"\",\"show_intro\":\"\",\"info_block_position\":\"\",\"info_block_show_title\":\"\",\"show_category\":\"\",\"link_category\":\"\",\"show_parent_category\":\"\",\"link_parent_category\":\"\",\"show_associations\":\"\",\"show_author\":\"\",\"link_author\":\"\",\"show_create_date\":\"\",\"show_modify_date\":\"\",\"show_publish_date\":\"\",\"show_item_navigation\":\"\",\"show_vote\":\"\",\"show_icons\":\"\",\"show_print_icon\":\"\",\"show_email_icon\":\"\",\"show_hits\":\"\",\"show_tags\":\"\",\"show_noauth\":\"\",\"urls_position\":\"\",\"menu-anchor_title\":\"\",\"menu-anchor_css\":\"\",\"menu_image\":\"\",\"menu_image_css\":\"\",\"menu_text\":1,\"menu_show\":1,\"page_title\":\"\",\"show_page_heading\":\"\",\"page_heading\":\"\",\"pageclass_sfx\":\"\",\"menu-meta_description\":\"\",\"menu-meta_keywords\":\"\",\"robots\":\"\",\"secure\":0,\"showmenutitle\":\"1\",\"desc\":\"\",\"cols\":\"1\",\"group\":\"0\",\"showgrouptitle\":\"1\",\"cwidth\":\"\",\"colxw\":\"\",\"class\":\"\",\"subcontent\":\"normal\"}', 22, 23, 0, '*', 0),
(180, 'mainmenu', 'Animation', 'animation', '', '2013-11-07-05-29-34/animation', 'index.php?option=com_content&view=article&id=36', 'component', 1, 157, 2, 22, 0, '0000-00-00 00:00:00', 0, 1, ' ', 0, '{\"show_title\":\"\",\"link_titles\":\"\",\"show_intro\":\"\",\"info_block_position\":\"\",\"info_block_show_title\":\"\",\"show_category\":\"\",\"link_category\":\"\",\"show_parent_category\":\"\",\"link_parent_category\":\"\",\"show_associations\":\"\",\"show_author\":\"\",\"link_author\":\"\",\"show_create_date\":\"\",\"show_modify_date\":\"\",\"show_publish_date\":\"\",\"show_item_navigation\":\"\",\"show_vote\":\"\",\"show_icons\":\"\",\"show_print_icon\":\"\",\"show_email_icon\":\"\",\"show_hits\":\"\",\"show_tags\":\"\",\"show_noauth\":\"\",\"urls_position\":\"\",\"menu-anchor_title\":\"\",\"menu-anchor_css\":\"\",\"menu_image\":\"\",\"menu_image_css\":\"\",\"menu_text\":1,\"menu_show\":1,\"page_title\":\"\",\"show_page_heading\":\"\",\"page_heading\":\"\",\"pageclass_sfx\":\"\",\"menu-meta_description\":\"\",\"menu-meta_keywords\":\"\",\"robots\":\"\",\"secure\":0,\"showmenutitle\":\"1\",\"desc\":\"\",\"cols\":\"1\",\"group\":\"0\",\"showgrouptitle\":\"1\",\"cwidth\":\"\",\"colxw\":\"\",\"class\":\"\",\"subcontent\":\"normal\"}', 24, 25, 0, '*', 0),
(181, 'main', 'COM_OSGALLERY_MENU', 'com_osgallery_menu', '', 'com_osgallery_menu', 'index.php?option=com_osgallery', 'component', 1, 1, 1, 10018, 0, '0000-00-00 00:00:00', 0, 1, 'class:component', 0, '{}', 179, 180, 0, '', 1);

-- --------------------------------------------------------

--
-- Table structure for table `t41uf_menu_types`
--

CREATE TABLE `t41uf_menu_types` (
  `id` int(10) UNSIGNED NOT NULL,
  `asset_id` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `menutype` varchar(24) COLLATE utf8mb4_unicode_ci NOT NULL,
  `title` varchar(48) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `client_id` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `t41uf_menu_types`
--

INSERT INTO `t41uf_menu_types` (`id`, `asset_id`, `menutype`, `title`, `description`, `client_id`) VALUES
(1, 0, 'mainmenu', 'Main Menu', 'The main menu for the site', 0),
(2, 0, 'top-menu', 'Top Menu', '', 0);

-- --------------------------------------------------------

--
-- Table structure for table `t41uf_messages`
--

CREATE TABLE `t41uf_messages` (
  `message_id` int(10) UNSIGNED NOT NULL,
  `user_id_from` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `user_id_to` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `folder_id` tinyint(3) UNSIGNED NOT NULL DEFAULT '0',
  `date_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `state` tinyint(1) NOT NULL DEFAULT '0',
  `priority` tinyint(1) UNSIGNED NOT NULL DEFAULT '0',
  `subject` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `message` mediumtext COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `t41uf_messages_cfg`
--

CREATE TABLE `t41uf_messages_cfg` (
  `user_id` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `cfg_name` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `cfg_value` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT ''
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `t41uf_modules`
--

CREATE TABLE `t41uf_modules` (
  `id` int(11) NOT NULL,
  `asset_id` int(10) UNSIGNED NOT NULL DEFAULT '0' COMMENT 'FK to the #___assets table.',
  `title` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `note` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `content` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `ordering` int(11) NOT NULL DEFAULT '0',
  `position` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `checked_out` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `checked_out_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `publish_up` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `publish_down` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `published` tinyint(1) NOT NULL DEFAULT '0',
  `module` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `access` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `showtitle` tinyint(3) UNSIGNED NOT NULL DEFAULT '1',
  `params` mediumtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `client_id` tinyint(4) NOT NULL DEFAULT '0',
  `language` char(7) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `t41uf_modules`
--

INSERT INTO `t41uf_modules` (`id`, `asset_id`, `title`, `note`, `content`, `ordering`, `position`, `checked_out`, `checked_out_time`, `publish_up`, `publish_down`, `published`, `module`, `access`, `showtitle`, `params`, `client_id`, `language`) VALUES
(2, 0, 'Login', '', '', 1, 'login', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 'mod_login', 1, 1, '', 1, '*'),
(3, 0, 'Popular Articles', '', '', 3, 'cpanel', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 'mod_popular', 3, 1, '{\"count\":\"5\",\"catid\":\"\",\"user_id\":\"0\",\"layout\":\"_:default\",\"moduleclass_sfx\":\"\",\"cache\":\"0\",\"automatic_title\":\"1\"}', 1, '*'),
(4, 0, 'Recently Added Articles', '', '', 4, 'cpanel', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 'mod_latest', 3, 1, '{\"count\":\"5\",\"ordering\":\"c_dsc\",\"catid\":\"\",\"user_id\":\"0\",\"layout\":\"_:default\",\"moduleclass_sfx\":\"\",\"cache\":\"0\",\"automatic_title\":\"1\"}', 1, '*'),
(8, 0, 'Toolbar', '', '', 1, 'toolbar', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 'mod_toolbar', 3, 1, '', 1, '*'),
(9, 0, 'Quick Icons', '', '', 1, 'icon', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 'mod_quickicon', 3, 1, '', 1, '*'),
(10, 0, 'Logged-in Users', '', '', 2, 'cpanel', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 'mod_logged', 3, 1, '{\"count\":\"5\",\"name\":\"1\",\"layout\":\"_:default\",\"moduleclass_sfx\":\"\",\"cache\":\"0\",\"automatic_title\":\"1\"}', 1, '*'),
(12, 0, 'Admin Menu', '', '', 1, 'menu', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 'mod_menu', 3, 1, '{\"layout\":\"\",\"moduleclass_sfx\":\"\",\"shownew\":\"1\",\"showhelp\":\"1\",\"cache\":\"0\"}', 1, '*'),
(13, 0, 'Admin Submenu', '', '', 1, 'submenu', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 'mod_submenu', 3, 1, '', 1, '*'),
(14, 0, 'User Status', '', '', 2, 'status', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 'mod_status', 3, 1, '', 1, '*'),
(15, 0, 'Title', '', '', 1, 'title', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 'mod_title', 3, 1, '', 1, '*'),
(79, 0, 'Multilanguage status', '', '', 1, 'status', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0, 'mod_multilangstatus', 3, 1, '{\"layout\":\"_:default\",\"moduleclass_sfx\":\"\",\"cache\":\"0\"}', 1, '*'),
(86, 0, 'Joomla Version', '', '', 1, 'footer', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 'mod_version', 3, 1, '{\"format\":\"short\",\"product\":\"1\",\"layout\":\"_:default\",\"moduleclass_sfx\":\"\",\"cache\":\"0\"}', 1, '*'),
(87, 0, 'Menu Module', '', '<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Far far away, behind the word mountains, far from the countries.<br />Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore.</p>', 0, 'menumodule', 0, '0000-00-00 00:00:00', '2018-06-26 12:46:02', '0000-00-00 00:00:00', 1, 'mod_custom', 1, 1, '{\"prepare_content\":\"1\",\"backgroundimage\":\"\",\"layout\":\"_:default\",\"moduleclass_sfx\":\"\",\"cache\":\"1\",\"cache_time\":\"900\",\"cachemode\":\"static\"}', 0, '*'),
(100, 0, 'Front Page Feature', 'Frontpage Slideshow', '<p>[carousel]</p>\r\n<p>[carousel_item]</p>\r\n<h1>Helix Framework</h1>\r\n<p>Powerful templates framework to develop Joomla base website faster!</p>\r\n<p>[button type=\"primary\" size=\"large\" link=\"http://www.joomshaper.com/helix/\"]Download Now[/button] <span style=\"line-height: 1.3em;\">[button type=\"success\" size=\"large\" link=\"http://demo.joomshaper.com/?template=helix_ii\"]Live Demo[/button]</span><span style=\"line-height: 1.3em;\"> </span></p>\r\n<p>[/carousel_item]</p>\r\n<p>[carousel_item]</p>\r\n<h1>Layout Builder</h1>\r\n<p>JoomShaper brings Layout Builder first time in Joomla! It is one of the unique features introduced in Helix – II which allows anyone to customize the existing theme in any shape or dimensions without having any programming language!</p>\r\n<p>[/carousel_item]</p>\r\n<p>[carousel_item]</p>\r\n<h1>Useful Shortcodes</h1>\r\n<p>In Helix - II you can see many useful Shortcodes installed. Now you can add lots of extra features on your website, without having any coding knowledge.</p>\r\n<p>[/carousel_item]</p>\r\n<p><span style=\"line-height: 1.3em;\">[/carousel]</span></p>', 1, 'feature', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 'mod_custom', 1, 1, '{\"prepare_content\":\"1\",\"backgroundimage\":\"\",\"layout\":\"_:default\",\"moduleclass_sfx\":\"\",\"cache\":\"1\",\"cache_time\":\"900\",\"cachemode\":\"static\"}', 0, '*'),
(101, 0, 'Search', '', '', 1, 'search', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', -2, 'mod_search', 1, 0, '{\"label\":\"\",\"width\":\"20\",\"text\":\"\",\"button\":\"1\",\"button_pos\":\"right\",\"imagebutton\":\"\",\"button_text\":\"\",\"opensearch\":\"1\",\"opensearch_title\":\"\",\"set_itemid\":\"\",\"layout\":\"_:default\",\"moduleclass_sfx\":\"\",\"cache\":\"1\",\"cache_time\":\"900\",\"cachemode\":\"itemid\"}', 0, '*'),
(104, 0, 'Contact Form', '', '<p>Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas.</p>', 1, 'bottom1', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0, 'mod_custom', 1, 1, '{\"prepare_content\":\"1\",\"backgroundimage\":\"\",\"layout\":\"_:default\",\"moduleclass_sfx\":\"\",\"cache\":\"1\",\"cache_time\":\"900\",\"cachemode\":\"static\"}', 0, '*'),
(105, 106, 'Our Address', '', '<div class=\"row-fluid\">\r\n<div class=\"span6\"><address><strong>Twitter, Inc.</strong><br /> 795 Folsom Ave, Suite 600<br /> San Francisco, CA 94107<br /> P: (123) 456-7890</address></div>\r\n<div class=\"span6\"><address><strong>Twitter, Inc.</strong><br /> 795 Folsom Ave, Suite 600<br /> San Francisco, CA 94107<br /> P: (123) 456-7890</address></div>\r\n</div>\r\n<h3 class=\"header\">Connect With Us</h3>\r\n<div class=\"row-fluid social\">\r\n<div class=\"span6\">\r\n<p><a href=\"#\">[icon name=\"facebook\" /] Facebook</a></p>\r\n<p><a href=\"#\">[icon name=\"google-plus\" /] Google Plus</a></p>\r\n</div>\r\n<div class=\"span6\">\r\n<p><a href=\"#\">[icon name=\"twitter\" /] Twitter</a></p>\r\n<p><a href=\"#\">[icon name=\"youtube\" /] You Tube</a></p>\r\n</div>\r\n</div>', 1, 'bottom2', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 'mod_custom', 1, 1, '{\"prepare_content\":\"1\",\"backgroundimage\":\"\",\"layout\":\"_:default\",\"moduleclass_sfx\":\"\",\"cache\":\"1\",\"cache_time\":\"900\",\"cachemode\":\"static\",\"module_tag\":\"div\",\"bootstrap_size\":\"0\",\"header_tag\":\"h3\",\"header_class\":\"\",\"style\":\"0\"}', 0, '*'),
(116, 94, 'services Architectural Design', '', '<a href=\"/index.php/2013-11-07-05-29-34/architecture\">\r\n<div class=\"center\">\r\n<img src=\"images/services/archi.jpg\" alt=\"\"/>\r\n<h3 class=\"service-heading\">Architectural Design</h3>\r\n</div>\r\n</a>\r\n', 1, 'service1', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 'mod_custom', 1, 0, '{\"prepare_content\":\"1\",\"backgroundimage\":\"\",\"layout\":\"_:default\",\"moduleclass_sfx\":\"\",\"cache\":\"1\",\"cache_time\":\"900\",\"cachemode\":\"static\",\"module_tag\":\"div\",\"bootstrap_size\":\"0\",\"header_tag\":\"h3\",\"header_class\":\"\",\"style\":\"0\"}', 0, '*'),
(117, 96, 'service Modeling', '', '<a href=\"/index.php/2013-11-07-05-29-34/3d\">\r\n<div class=\"center\">\r\n<img src=\"images/services/3d.jpg\" alt=\"\"/>\r\n<h3 class=\"service-heading\">3D Modeling</h3>\r\n</div>\r\n</a>', 1, 'service2', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 'mod_custom', 1, 0, '{\"prepare_content\":\"1\",\"backgroundimage\":\"\",\"layout\":\"_:default\",\"moduleclass_sfx\":\"\",\"cache\":\"1\",\"cache_time\":\"900\",\"cachemode\":\"static\",\"module_tag\":\"div\",\"bootstrap_size\":\"0\",\"header_tag\":\"h3\",\"header_class\":\"\",\"style\":\"0\"}', 0, '*'),
(118, 98, 'service Interior', '', '<a href=\"/index.php/2013-11-07-05-29-34/interior\">\r\n<div class=\"center\">\r\n<img src=\"images/services/interior.jpg\" alt=\"\"/>\r\n<h3 class=\"service-heading\">Interior</h3>\r\n</div>\r\n</a>', 1, 'service3', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 'mod_custom', 1, 0, '{\"prepare_content\":\"1\",\"backgroundimage\":\"\",\"layout\":\"_:default\",\"moduleclass_sfx\":\"\",\"cache\":\"1\",\"cache_time\":\"900\",\"cachemode\":\"static\",\"module_tag\":\"div\",\"bootstrap_size\":\"0\",\"header_tag\":\"h3\",\"header_class\":\"\",\"style\":\"0\"}', 0, '*'),
(119, 95, 'service WEB', '', '<a href=\"/index.php/2013-11-07-05-29-34/web\">\r\n<div class=\"center\">\r\n<img src=\"images/services/webdesign.jpg\" alt=\"\"/>\r\n<h3 class=\"service-heading\">Web Design & Development</h3>\r\n</div>\r\n</a>\r\n', 1, 'service1', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 'mod_custom', 1, 0, '{\"prepare_content\":\"1\",\"backgroundimage\":\"\",\"layout\":\"_:default\",\"moduleclass_sfx\":\"\",\"cache\":\"1\",\"cache_time\":\"900\",\"cachemode\":\"static\",\"module_tag\":\"div\",\"bootstrap_size\":\"0\",\"header_tag\":\"h3\",\"header_class\":\"\",\"style\":\"0\"}', 0, '*'),
(120, 97, 'service Graphic', '', '<a href=\"/index.php/2013-11-07-05-29-34/graphic\">\r\n<div class=\"center\">\r\n<img src=\"images/services/graphic.jpg\" alt=\"\"/>\r\n<h3 class=\"service-heading\">Graphic Design</h3>\r\n</div>\r\n</a>', 1, 'service2', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 'mod_custom', 1, 0, '{\"prepare_content\":\"1\",\"backgroundimage\":\"\",\"layout\":\"_:default\",\"moduleclass_sfx\":\"\",\"cache\":\"1\",\"cache_time\":\"900\",\"cachemode\":\"static\",\"module_tag\":\"div\",\"bootstrap_size\":\"0\",\"header_tag\":\"h3\",\"header_class\":\"\",\"style\":\"0\"}', 0, '*'),
(121, 99, 'service Animation', '', '<a href=\"/index.php/2013-11-07-05-29-34/animation\">\r\n<div class=\"center\">\r\n<img src=\"images/services/animation.jpg\" alt=\"\"/>\r\n<h3 class=\"service-heading\">Animation</h3>\r\n</div>\r\n</a>', 1, 'service3', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 'mod_custom', 1, 0, '{\"prepare_content\":\"1\",\"backgroundimage\":\"\",\"layout\":\"_:default\",\"moduleclass_sfx\":\"\",\"cache\":\"1\",\"cache_time\":\"900\",\"cachemode\":\"static\",\"module_tag\":\"div\",\"bootstrap_size\":\"0\",\"header_tag\":\"h3\",\"header_class\":\"\",\"style\":\"0\"}', 0, '*'),
(122, 102, 'Xeon Slider', '', '', 1, 'slide', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 'mod_xeon_slider', 1, 0, '{\"slide1_title\":\"Architectural & Graphic Design Studio\",\"slide1_desc\":\"some awsome text here\",\"slide2_title\":\"3D modeling\",\"slide2_desc\":\"some awsome text here\",\"slide3_title\":\"School & Art Studio\",\"slide3_desc\":\"Take your career to a new level with our specialized courses\",\"layout\":\"_:default\",\"moduleclass_sfx\":\"\",\"cache\":\"1\",\"cache_time\":\"900\",\"cachemode\":\"static\",\"module_tag\":\"div\",\"bootstrap_size\":\"0\",\"header_tag\":\"h3\",\"header_class\":\"\",\"style\":\"0\"}', 0, '*'),
(123, 92, 'SP Portfolio', '', '', 2, 'portfolio', 0, '0000-00-00 00:00:00', '2018-07-04 18:59:43', '0000-00-00 00:00:00', 0, 'mod_sp_portfolio', 1, 1, '{\"catid\":\"12\",\"limit\":\"30\",\"column\":\"4\",\"ajaxlimit\":\"2\",\"orderby\":\"a.ordering\",\"ordering\":\"ASC\",\"show_featured\":\"\",\"module_layout\":\"appico\",\"show_title\":\"1\",\"linked_title\":\"1\",\"show_category\":\"0\",\"show_url\":\"1\",\"show_introtext\":\"0\",\"show_readmore\":\"0\",\"ajax_loader\":\"0\",\"show_filter\":\"1\",\"load_jquery\":\"1\",\"moduleclass_sfx\":\"\",\"cache\":\"1\",\"cache_time\":\"900\",\"cachemode\":\"itemid\",\"module_tag\":\"div\",\"bootstrap_size\":\"0\",\"header_tag\":\"h3\",\"header_class\":\"\",\"style\":\"0\"}', 0, '*'),
(124, 0, 'portfolio-heading', '', '<div class=\"center\">\r\n<h2>Portfolio</h2>\r\n<p class=\"lead\">Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac<br />turpis egestas. Vestibulum tortor quam, feugiat vitae.</p>\r\n</div>', 1, 'portfolio', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0, 'mod_custom', 1, 0, '{\"prepare_content\":\"0\",\"backgroundimage\":\"\",\"layout\":\"_:default\",\"moduleclass_sfx\":\"\",\"cache\":\"1\",\"cache_time\":\"900\",\"cachemode\":\"static\"}', 0, '*'),
(125, 0, 'SP Quick Contact', '', '', 1, 'bottom1', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 'mod_sp_quickcontact', 1, 0, '{\"email\":\"email@email.com\",\"success\":\"Email was sent successfully.\",\"failed\":\"Email could not be sent.\",\"moduleclass_sfx\":\"\",\"cache\":\"1\",\"cache_time\":\"900\",\"cachemode\":\"itemid\"}', 0, '*'),
(126, 0, 'Design By', '', '<p><a href=\"http://shapebootstrap.net/\" target=\"_blank\"><img src=\"images/xeon/footer2.png\" border=\"0\" alt=\"\" /></a></p>', 1, 'footer3', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0, 'mod_custom', 1, 0, '{\"prepare_content\":\"0\",\"backgroundimage\":\"\",\"layout\":\"_:default\",\"moduleclass_sfx\":\"\",\"cache\":\"1\",\"cache_time\":\"900\",\"cachemode\":\"static\"}', 0, '*'),
(127, 0, 'Pricing Table', 'Pricing Title and Subtitle', '<div class=\"center\">\r\n<h2>See our Pricings</h2>\r\n<p class=\"lead\">Pellentesque habitant morbi tristique senectus et netus et <br />malesuada fames ac turpis egestas.</p>\r\n</div>', 1, 'pricing', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0, 'mod_custom', 1, 0, '{\"prepare_content\":\"0\",\"backgroundimage\":\"\",\"layout\":\"_:default\",\"moduleclass_sfx\":\"\",\"cache\":\"1\",\"cache_time\":\"900\",\"cachemode\":\"static\"}', 0, '*'),
(128, 93, 'Xeon Pricing Table', '', '', 1, 'pricing', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0, 'mod_xeon_pricing_table', 1, 0, '{\"plan1_name\":\"Basic\",\"plan1_price\":\"$29\",\"plan1_details\":\"<ul>\\r\\n<li>5GB Storage<\\/li>\\r\\n<li>1GB RAM<\\/li>\\r\\n<li>400GB Bandwidth<\\/li>\\r\\n<li>10 Email Address<\\/li>\\r\\n<li>Forum Support<\\/li>\\r\\n<\\/ul>\",\"plan1_signup\":\"Signup\",\"plan1_signuplink\":\"#\",\"plan1_is_featured\":\"0\",\"plan2_name\":\"Standard \",\"plan2_price\":\"$49\",\"plan2_details\":\"<ul>\\r\\n<li>10GB Storage<\\/li>\\r\\n<li>2GB RAM<\\/li>\\r\\n<li>1TB Bandwidth<\\/li>\\r\\n<li>100 Email Address<\\/li>\\r\\n<li>Forum Support<\\/li>\\r\\n<\\/ul>\",\"plan2_signup\":\"SignUp\",\"plan2_signuplink\":\"http:\\/\\/joomshaper.com\\/\",\"plan2_is_featured\":\"1\",\"plan3_name\":\"Advanced\",\"plan3_price\":\"$199\",\"plan3_details\":\"<ul>\\r\\n<li>10GB Storage<\\/li>\\r\\n<li>2GB RAM<\\/li>\\r\\n<li>1TB Bandwidth<\\/li>\\r\\n<li>100 Email Address<\\/li>\\r\\n<li>Forum Support<\\/li>\\r\\n<\\/ul>\",\"plan3_signup\":\"Signup\",\"plan3_signuplink\":\"#\",\"plan3_is_featured\":\"0\",\"plan4_name\":\"\",\"plan4_price\":\"\",\"plan4_details\":\"\",\"plan4_signup\":\"\",\"plan4_signuplink\":\"\",\"plan4_is_featured\":\"0\",\"layout\":\"_:default\",\"moduleclass_sfx\":\"\",\"cache\":\"1\",\"cache_time\":\"900\",\"cachemode\":\"static\",\"module_tag\":\"div\",\"bootstrap_size\":\"0\",\"header_tag\":\"h3\",\"header_class\":\"\",\"style\":\"0\"}', 0, '*'),
(129, 104, 'AboutUs', '', '<div class=\"center\">\r\n<h2>About Us</h2>\r\n<p class=\"lead\">Pellentesque habitant morbi tristique senectus et netus et <br />malesuada fames ac turpis egestas.</p>\r\n</div>', 1, 'team', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0, 'mod_custom', 1, 0, '{\"prepare_content\":\"0\",\"backgroundimage\":\"\",\"layout\":\"_:default\",\"moduleclass_sfx\":\"\",\"cache\":\"1\",\"cache_time\":\"900\",\"cachemode\":\"static\",\"module_tag\":\"div\",\"bootstrap_size\":\"0\",\"header_tag\":\"h3\",\"header_class\":\"\",\"style\":\"0\"}', 0, '*'),
(130, 108, 'Xeon Team', '', '', 1, 'team', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0, 'mod_xeon_team', 1, 0, '{\"team1_img\":\"images\\/xeon\\/team\\/team1.jpg\",\"team1_name\":\"Agnes Smith\",\"team1_desg\":\"CEO & Founder\",\"team2_img\":\"images\\/xeon\\/team\\/team3.jpg\",\"team2_name\":\"Donald Ford\",\"team2_desg\":\"Senior Vice President\",\"team3_img\":\"images\\/xeon\\/team\\/ners.jpg\",\"team3_name\":\"Karen Richardson\",\"team3_desg\":\"Assitant Vice President\",\"team4_img\":\"images\\/xeon\\/team\\/ners.jpg\",\"team4_name\":\"David Robbins\",\"team4_desg\":\"Co-Founder\",\"team5_img\":\"images\\/xeon\\/team\\/team1.jpg\",\"team5_name\":\"Philip Mejia\",\"team5_desg\":\"Marketing Manager\",\"team6_img\":\"images\\/xeon\\/team\\/ners.jpg\",\"team6_name\":\"Karen Richardson\",\"team6_desg\":\"Assitant Vice President\",\"layout\":\"_:default\",\"moduleclass_sfx\":\"\",\"cache\":\"1\",\"cache_time\":\"900\",\"cachemode\":\"static\",\"module_tag\":\"div\",\"bootstrap_size\":\"0\",\"header_tag\":\"h3\",\"header_class\":\"\",\"style\":\"0\"}', 0, '*'),
(131, 105, 'Presets', '', '<p><a class=\"btn-preset preset1\" href=\"?preset=preset1\">Preset1</a> <a class=\"btn-preset preset2\" href=\"?preset=preset2\">Preset2</a> <a class=\"btn-preset preset3\" href=\"?preset=preset3\">Preset3</a></p>', 1, 'footer2', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0, 'mod_custom', 1, 0, '{\"prepare_content\":\"0\",\"backgroundimage\":\"\",\"layout\":\"_:default\",\"moduleclass_sfx\":\"\",\"cache\":\"1\",\"cache_time\":\"900\",\"cachemode\":\"static\",\"module_tag\":\"div\",\"bootstrap_size\":\"0\",\"header_tag\":\"h3\",\"header_class\":\"\",\"style\":\"0\"}', 0, '*'),
(132, 103, 'test ', '', '<p style=\"text-align: center;\"><span style=\"font-size: 36pt;\"><strong>TEST</strong></span></p>\r\n<p style=\"text-align: center;\"> </p>\r\n<p style=\"text-align: center;\"><img src=\"images/headers/raindrops.jpg\" alt=\"\" /></p>', 1, 'slide', 0, '0000-00-00 00:00:00', '2018-06-27 06:44:59', '0000-00-00 00:00:00', -2, 'mod_custom', 1, 1, '{\"prepare_content\":\"0\",\"backgroundimage\":\"\",\"layout\":\"_:default\",\"moduleclass_sfx\":\"\",\"cache\":\"1\",\"cache_time\":\"900\",\"cachemode\":\"static\",\"module_tag\":\"div\",\"bootstrap_size\":\"0\",\"header_tag\":\"h3\",\"header_class\":\"\",\"style\":\"0\"}', 0, '*'),
(133, 107, 'video', '', '<video id = \"vid\" width=\"auto\" height=\"auto\" autoplay loop muted preload=\"none\">\r\n    \r\n <source src=\"images/video/coord.mp4\" type=\"video/mp4\">\r\n    \r\n</video>\r\n\r\n<div class=\"video-overlay\"></div>  \r\n  \r\n', 1, 'slider', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 'mod_custom', 1, 1, '{\"prepare_content\":\"0\",\"backgroundimage\":\"\",\"layout\":\"_:default\",\"moduleclass_sfx\":\" video\",\"cache\":\"1\",\"cache_time\":\"900\",\"cachemode\":\"static\",\"module_tag\":\"div\",\"bootstrap_size\":\"0\",\"header_tag\":\"h3\",\"header_class\":\"\",\"style\":\"0\"}', 0, '*'),
(134, 115, 'services', '', '', 1, 'portfolio', 0, '0000-00-00 00:00:00', '2018-07-05 13:18:15', '0000-00-00 00:00:00', -2, 'mod_articles_category', 1, 1, '{\"mode\":\"normal\",\"show_on_article_page\":\"1\",\"count\":\"6\",\"show_front\":\"show\",\"category_filtering_type\":\"1\",\"catid\":[\"16\"],\"show_child_category_articles\":\"0\",\"levels\":\"1\",\"author_filtering_type\":\"1\",\"created_by\":[\"\"],\"author_alias_filtering_type\":\"1\",\"created_by_alias\":[\"\"],\"excluded_articles\":\"\",\"date_filtering\":\"off\",\"date_field\":\"a.created\",\"start_date_range\":\"\",\"end_date_range\":\"\",\"relative_date\":\"30\",\"article_ordering\":\"a.title\",\"article_ordering_direction\":\"ASC\",\"article_grouping\":\"none\",\"article_grouping_direction\":\"ksort\",\"month_year_format\":\"F Y\",\"link_titles\":\"1\",\"show_date\":\"0\",\"show_date_field\":\"created\",\"show_date_format\":\"Y-m-d H:i:s\",\"show_category\":\"0\",\"show_hits\":\"0\",\"show_author\":\"0\",\"show_introtext\":\"1\",\"introtext_limit\":\"100\",\"show_readmore\":\"0\",\"show_readmore_title\":\"1\",\"readmore_limit\":\"15\",\"layout\":\"_:default\",\"moduleclass_sfx\":\"\",\"owncache\":\"1\",\"cache_time\":\"900\",\"module_tag\":\"div\",\"bootstrap_size\":\"0\",\"header_tag\":\"h3\",\"header_class\":\"\",\"style\":\"0\"}', 0, '*'),
(135, 118, 'Xeon Slider for Architect', '', '', 1, 'slide', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 'mod_xeon_slider', 1, 0, '{\"slide1_title\":\"Architectural & Graphic Design Studio\",\"slide1_desc\":\"some awsome text here\",\"slide2_title\":\"3D modeling\",\"slide2_desc\":\"some awsome text here\",\"slide3_title\":\"School & Art Studio\",\"slide3_desc\":\"Take your career to a new level with our specialized courses\",\"layout\":\"_:default\",\"moduleclass_sfx\":\"\",\"cache\":\"1\",\"cache_time\":\"900\",\"cachemode\":\"static\",\"module_tag\":\"div\",\"bootstrap_size\":\"0\",\"header_tag\":\"h3\",\"header_class\":\"\",\"style\":\"0\"}', 0, '*');

-- --------------------------------------------------------

--
-- Table structure for table `t41uf_modules_menu`
--

CREATE TABLE `t41uf_modules_menu` (
  `moduleid` int(11) NOT NULL DEFAULT '0',
  `menuid` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `t41uf_modules_menu`
--

INSERT INTO `t41uf_modules_menu` (`moduleid`, `menuid`) VALUES
(2, 0),
(3, 0),
(4, 0),
(6, 0),
(7, 0),
(8, 0),
(9, 0),
(10, 0),
(12, 0),
(13, 0),
(14, 0),
(15, 0),
(79, 0),
(86, 0),
(87, 0),
(100, 101),
(101, 0),
(104, 0),
(105, 0),
(116, 101),
(117, 101),
(118, 101),
(119, 101),
(120, 101),
(121, 101),
(122, 101),
(123, 101),
(124, 101),
(125, 0),
(126, 0),
(127, 101),
(128, 101),
(129, 101),
(130, 101),
(131, 0),
(132, 101),
(133, 101),
(134, 101),
(135, 175);

-- --------------------------------------------------------

--
-- Table structure for table `t41uf_newsfeeds`
--

CREATE TABLE `t41uf_newsfeeds` (
  `catid` int(11) NOT NULL DEFAULT '0',
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `alias` varchar(400) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL DEFAULT '',
  `link` varchar(2048) COLLATE utf8mb4_unicode_ci NOT NULL,
  `published` tinyint(1) NOT NULL DEFAULT '0',
  `numarticles` int(10) UNSIGNED NOT NULL DEFAULT '1',
  `cache_time` int(10) UNSIGNED NOT NULL DEFAULT '3600',
  `checked_out` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `checked_out_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `ordering` int(11) NOT NULL DEFAULT '0',
  `rtl` tinyint(4) NOT NULL DEFAULT '0',
  `access` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `language` char(7) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `params` mediumtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `created` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `created_by` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `created_by_alias` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `modified` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `modified_by` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `metakey` mediumtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `metadesc` mediumtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `metadata` mediumtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `xreference` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `publish_up` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `publish_down` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `description` mediumtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `version` int(10) UNSIGNED NOT NULL DEFAULT '1',
  `hits` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `images` mediumtext COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `t41uf_os_gallery`
--

CREATE TABLE `t41uf_os_gallery` (
  `id` int(11) UNSIGNED NOT NULL,
  `title` varchar(255) DEFAULT NULL,
  `published` int(1) DEFAULT '1',
  `params` text NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `t41uf_os_gallery`
--

INSERT INTO `t41uf_os_gallery` (`id`, `title`, `published`, `params`) VALUES
(1, 'architect', 1, '{\"fancy_box_background\":\"gray\",\"open_close_effect\":\"none\",\"click_close\":1,\"helper_buttons\":0,\"helper_thumbnail\":0,\"loop\":0,\"open_close_speed\":500,\"prev_next_effect\":\"none\",\"prev_next_speed\":500,\"img_title\":\"inside\",\"thumbnail_width\":50,\"thumbnail_height\":50,\"os_fancybox_arrows\":1,\"os_fancybox_arrows_pos\":1,\"close_button\":1,\"next_click\":0,\"mouse_wheel\":0,\"os_fancybox_autoplay\":0,\"autoplay_speed\":3000,\"os_fancybox_thumbnail_position\":\"thumb_right\",\"infobar\":1,\"start_slideshow_button\":0,\"full_screen_button\":0,\"thumbnails_button\":0,\"share_button\":0,\"download_button\":0,\"zoom_button\":0,\"left_arrow\":1,\"right_arrow\":1,\"imageHover\":\"none\",\"watermark_position\":\"center\",\"watermark_opacity\":30,\"watermark_size\":20,\"watermark_enable\":0,\"watermark_type\":1,\"watermark_text\":\"\",\"watermark_text_color\":\"rgb(0, 0, 0)\",\"watermark_text_size\":17,\"watermark_font\":\"default\",\"watermark_text_angle\":0,\"exist_watermark_text\":\"\",\"watermark_file\":\"\",\"backButtonText\":\"back\",\"image_margin\":5,\"num_column\":3,\"galleryLayout\":\"defaultTabs\",\"masonryLayout\":\"default\",\"minImgEnable\":1,\"minImgSize\":200,\"imgWidth\":600,\"imgHeight\":400,\"showLoadMore\":\"0\",\"showDownload\":0,\"showImgTitle\":0,\"showImgDescription\":0,\"number_images\":5,\"number_images_effect\":\"zoomIn\",\"rotateImage\":0,\"number_images_at_once\":5,\"number_images_at_once_effect\":\"zoomIn\",\"loadMoreButtonText\":\"Load More\",\"load_more_background\":\"#12bbc5\",\"facebook_enable\":0,\"googleplus_enable\":0,\"vkontacte_enable\":0,\"odnoklassniki_enable\":0,\"twitter_enable\":0,\"pinterest_enable\":0,\"linkedin_enable\":0}');

-- --------------------------------------------------------

--
-- Table structure for table `t41uf_os_gallery_categories`
--

CREATE TABLE `t41uf_os_gallery_categories` (
  `id` int(11) UNSIGNED NOT NULL,
  `fk_gal_id` int(11) UNSIGNED DEFAULT NULL COMMENT 'The gallery id from table __os_gallery',
  `name` varchar(255) DEFAULT NULL,
  `ordering` int(11) NOT NULL,
  `params` text NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `t41uf_os_gallery_categories`
--

INSERT INTO `t41uf_os_gallery_categories` (`id`, `fk_gal_id`, `name`, `ordering`, `params`) VALUES
(1, 1, 'architect', 0, '%7B%22categoryAlias%22:%22%22,%22categoryUnpublish%22:false,%22categoryShowTitle%22:false,%22categoryShowTitleCaption%22:false%7D');

-- --------------------------------------------------------

--
-- Table structure for table `t41uf_os_gallery_connect`
--

CREATE TABLE `t41uf_os_gallery_connect` (
  `id` int(11) UNSIGNED NOT NULL,
  `fk_cat_id` int(11) UNSIGNED DEFAULT NULL COMMENT 'The id from table __os_gallery_categories',
  `fk_gal_img_id` int(11) UNSIGNED DEFAULT NULL COMMENT 'The img id from table __os_gallery_img'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `t41uf_os_gallery_connect`
--

INSERT INTO `t41uf_os_gallery_connect` (`id`, `fk_cat_id`, `fk_gal_img_id`) VALUES
(1, 1, 1),
(2, 1, 2),
(3, 1, 3);

-- --------------------------------------------------------

--
-- Table structure for table `t41uf_os_gallery_img`
--

CREATE TABLE `t41uf_os_gallery_img` (
  `id` int(11) UNSIGNED NOT NULL,
  `file_name` varchar(255) DEFAULT NULL,
  `src` varchar(255) DEFAULT NULL,
  `ordering` int(11) NOT NULL,
  `params` text NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `t41uf_os_gallery_img`
--

INSERT INTO `t41uf_os_gallery_img` (`id`, `file_name`, `src`, `ordering`, `params`) VALUES
(1, 'project-2D636F1F1-4139-96C0-DE23-67F0119EFA5D.jpg', NULL, 0, '{}'),
(2, 'project-319354E01-4B93-BDD7-7876-87BC29968906.jpg', NULL, 1, '%7B%22imgTitle%22:%22%22,%22videoLink%22:%22%22,%22imgShortDescription%22:%22descr%202%22,%22imgAlt%22:%22%22,%22imgLink%22:%22%22,%22imgLinkOpen%22:%22_blank%22,%22imgHtml%22:%7B%22html%22:%22%22%7D,%22htmlPosition%22:null%7D'),
(3, 'project-46D355EAD-E357-55F5-D7AF-4020E8ED070B.jpg', NULL, 2, '%7B%22imgTitle%22:%22%22,%22videoLink%22:%22%22,%22imgShortDescription%22:%22nkaragrutyun%20nkari%22,%22imgAlt%22:%22%22,%22imgLink%22:%22%22,%22imgLinkOpen%22:%22_blank%22,%22imgHtml%22:%7B%22html%22:%22%22%7D,%22htmlPosition%22:null%7D');

-- --------------------------------------------------------

--
-- Table structure for table `t41uf_overrider`
--

CREATE TABLE `t41uf_overrider` (
  `id` int(10) NOT NULL COMMENT 'Primary Key',
  `constant` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `string` mediumtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `file` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `t41uf_postinstall_messages`
--

CREATE TABLE `t41uf_postinstall_messages` (
  `postinstall_message_id` bigint(20) UNSIGNED NOT NULL,
  `extension_id` bigint(20) NOT NULL DEFAULT '700' COMMENT 'FK to #___extensions',
  `title_key` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '' COMMENT 'Lang key for the title',
  `description_key` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '' COMMENT 'Lang key for description',
  `action_key` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `language_extension` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'com_postinstall' COMMENT 'Extension holding lang keys',
  `language_client_id` tinyint(3) NOT NULL DEFAULT '1',
  `type` varchar(10) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'link' COMMENT 'Message type - message, link, action',
  `action_file` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT '' COMMENT 'RAD URI to the PHP file containing action method',
  `action` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT '' COMMENT 'Action method name or URL',
  `condition_file` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'RAD URI to file holding display condition method',
  `condition_method` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'Display condition method, must return boolean',
  `version_introduced` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '3.2.0' COMMENT 'Version when this message was introduced',
  `enabled` tinyint(3) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `t41uf_postinstall_messages`
--

INSERT INTO `t41uf_postinstall_messages` (`postinstall_message_id`, `extension_id`, `title_key`, `description_key`, `action_key`, `language_extension`, `language_client_id`, `type`, `action_file`, `action`, `condition_file`, `condition_method`, `version_introduced`, `enabled`) VALUES
(1, 700, 'PLG_TWOFACTORAUTH_TOTP_POSTINSTALL_TITLE', 'PLG_TWOFACTORAUTH_TOTP_POSTINSTALL_BODY', 'PLG_TWOFACTORAUTH_TOTP_POSTINSTALL_ACTION', 'plg_twofactorauth_totp', 1, 'action', 'site://plugins/twofactorauth/totp/postinstall/actions.php', 'twofactorauth_postinstall_action', 'site://plugins/twofactorauth/totp/postinstall/actions.php', 'twofactorauth_postinstall_condition', '3.2.0', 1),
(2, 700, 'COM_CPANEL_MSG_EACCELERATOR_TITLE', 'COM_CPANEL_MSG_EACCELERATOR_BODY', 'COM_CPANEL_MSG_EACCELERATOR_BUTTON', 'com_cpanel', 1, 'action', 'admin://components/com_admin/postinstall/eaccelerator.php', 'admin_postinstall_eaccelerator_action', 'admin://components/com_admin/postinstall/eaccelerator.php', 'admin_postinstall_eaccelerator_condition', '3.2.0', 1),
(4, 700, 'COM_CPANEL_MSG_HTACCESS_TITLE', 'COM_CPANEL_MSG_HTACCESS_BODY', '', 'com_cpanel', 1, 'message', '', '', 'admin://components/com_admin/postinstall/htaccess.php', 'admin_postinstall_htaccess_condition', '3.4.0', 1),
(5, 700, 'COM_CPANEL_MSG_ROBOTS_TITLE', 'COM_CPANEL_MSG_ROBOTS_BODY', '', 'com_cpanel', 1, 'message', '', '', '', '', '3.3.0', 1),
(6, 700, 'COM_CPANEL_MSG_LANGUAGEACCESS340_TITLE', 'COM_CPANEL_MSG_LANGUAGEACCESS340_BODY', '', 'com_cpanel', 1, 'message', '', '', 'admin://components/com_admin/postinstall/languageaccess340.php', 'admin_postinstall_languageaccess340_condition', '3.4.1', 1),
(7, 700, 'COM_CPANEL_MSG_STATS_COLLECTION_TITLE', 'COM_CPANEL_MSG_STATS_COLLECTION_BODY', '', 'com_cpanel', 1, 'message', '', '', 'admin://components/com_admin/postinstall/statscollection.php', 'admin_postinstall_statscollection_condition', '3.5.0', 1),
(8, 700, 'PLG_SYSTEM_UPDATENOTIFICATION_POSTINSTALL_UPDATECACHETIME', 'PLG_SYSTEM_UPDATENOTIFICATION_POSTINSTALL_UPDATECACHETIME_BODY', 'PLG_SYSTEM_UPDATENOTIFICATION_POSTINSTALL_UPDATECACHETIME_ACTION', 'plg_system_updatenotification', 1, 'action', 'site://plugins/system/updatenotification/postinstall/updatecachetime.php', 'updatecachetime_postinstall_action', 'site://plugins/system/updatenotification/postinstall/updatecachetime.php', 'updatecachetime_postinstall_condition', '3.6.3', 1),
(9, 700, 'COM_CPANEL_MSG_JOOMLA40_PRE_CHECKS_TITLE', 'COM_CPANEL_MSG_JOOMLA40_PRE_CHECKS_BODY', '', 'com_cpanel', 1, 'message', '', '', 'admin://components/com_admin/postinstall/joomla40checks.php', 'admin_postinstall_joomla40checks_condition', '3.7.0', 1),
(10, 700, 'TPL_HATHOR_MESSAGE_POSTINSTALL_TITLE', 'TPL_HATHOR_MESSAGE_POSTINSTALL_BODY', 'TPL_HATHOR_MESSAGE_POSTINSTALL_ACTION', 'tpl_hathor', 1, 'action', 'admin://templates/hathor/postinstall/hathormessage.php', 'hathormessage_postinstall_action', 'admin://templates/hathor/postinstall/hathormessage.php', 'hathormessage_postinstall_condition', '3.7.0', 1);

-- --------------------------------------------------------

--
-- Table structure for table `t41uf_redirect_links`
--

CREATE TABLE `t41uf_redirect_links` (
  `id` int(10) UNSIGNED NOT NULL,
  `old_url` varchar(2048) COLLATE utf8mb4_unicode_ci NOT NULL,
  `new_url` varchar(2048) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `referer` varchar(2048) COLLATE utf8mb4_unicode_ci NOT NULL,
  `comment` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `hits` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `published` tinyint(4) NOT NULL,
  `created_date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `modified_date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `header` smallint(3) NOT NULL DEFAULT '301'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `t41uf_redirect_links`
--

INSERT INTO `t41uf_redirect_links` (`id`, `old_url`, `new_url`, `referer`, `comment`, `hits`, `published`, `created_date`, `modified_date`, `header`) VALUES
(1, 'http://coordinate.design/index.php/component/?itemid=156', NULL, 'http://coordinate.design/index.php/item/2013-01-31-06-54-32/shortcode/gallery', '', 1, 0, '2018-07-04 19:04:48', '0000-00-00 00:00:00', 301);

-- --------------------------------------------------------

--
-- Table structure for table `t41uf_schemas`
--

CREATE TABLE `t41uf_schemas` (
  `extension_id` int(11) NOT NULL,
  `version_id` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `t41uf_schemas`
--

INSERT INTO `t41uf_schemas` (`extension_id`, `version_id`) VALUES
(21, '3.5.1'),
(700, '3.8.2-2017-10-14'),
(10018, '3.0.0');

-- --------------------------------------------------------

--
-- Table structure for table `t41uf_session`
--

CREATE TABLE `t41uf_session` (
  `session_id` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `client_id` tinyint(3) UNSIGNED DEFAULT NULL,
  `guest` tinyint(4) UNSIGNED DEFAULT '1',
  `time` varchar(14) COLLATE utf8mb4_unicode_ci DEFAULT '',
  `data` mediumtext COLLATE utf8mb4_unicode_ci,
  `userid` int(11) DEFAULT '0',
  `username` varchar(150) COLLATE utf8mb4_unicode_ci DEFAULT ''
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `t41uf_session`
--

INSERT INTO `t41uf_session` (`session_id`, `client_id`, `guest`, `time`, `data`, `userid`, `username`) VALUES
('3c5hcaki4gmid9hj3a8ipets24', 0, 1, '1530658306', 'RX4RPcOxoOy0ryfXBscLSrtra9l1h5V6uSfdG8-wiA5PZNNvJiKASBzLvtAkJecy9MB-TuxGNAKAiAyJUD_aG1yzDqp0ML6rLEvBgjO6eNoau1Vu-w4JLp7aq4q9GmkzzWzM_cQzJ8Ptc4OqaFY0EA-ho-DuXu6_RpFfGqVfKUPy_JP5XfMOUyi4iMyhmFi4qIJN0obyxPws-F03tgn3ztX5D6NMsBV-k8Q8IZsoCkMaZ9IhDw-qYYaxwXc8PJpk5otTnTgZjvCYp7XvV73AXCMZKAZSxspWZ384XktQYeOlAWgh3Ge_2nXPI_PYVIAyOEvuCzzUeYj-SYH1t57dEK-Cvnwy-q5eYXu9pX2pR2tqO4Mf9fWhzVdGF3gTNANOStoPELVxlfcs4MdJAedEdH1JRN4V73L0HVeemjoR5lfSjPFvep-jLEV9OAfptv7e3sAaeTRQbZyyx1Y3kCsiAY6R_B8XOQM3De6q5U-8TJENMPdF1HOMP0Ya7xTwlJRH1pMC3ii-tGsEMy9jJ9wz_2MFl0v7CCo5qKfaK49rDMXi8R76f_q4ZnS7UAPK5qh-vrb1qObmvU2VVM3jcHJzDlC3X6KO7BUJutOmSIaPa6RF_bFOr-2Sh_XwQuoHKK0M0rmZPpcDFOeFNFC2SvXI354hG3TW_rc8zZm5fO7uuouP2t53qDMZRy08Mx0111HtHSrtHtqHCRPfamkVIPqUdIV2j_sSh0VjuwwIprklLroNYuTjy6iOuI30S2dlWLNHlXh5EQ_BvtIMFqornn7oI57LK8VXFrt8e0g_orBzWkuqSrpHBClQTc-6QDMWCCvTaebvs7VTkmFq8zjemjP_mLKXFxCsylWvlpwwTPjfFWcITTcibWeTZD3ohGMIE9xpexWPGgtfF1thCD-lNxc7GMQSKvkI_MY1SqWgF0fkk90.', 0, ''),
('3qsevar7pmhpqnph9pq8mv1531', 1, 0, '1530862329', 'KRYCJ3vzvIKf-f5OS3zXICLCV7lsJBW4N5-NzOPfPlFW357HFKjPSC5SmQ9OLc_g1xyei557_k0fQNVa--bVpi2fu5WK_JCJ5YEk3n-IOJQ9KlEc_sB-BD5cnepxNpnkDqOkHEUwY-XnnHut29Yj6hCLDf-J0F8LNJbY4u2zQ5s17EOPDFh4Ha0cKofXWzTRyWvBbXaTziM5_PusE7ZrJ_dw7L19cKvVhEvEYn0trY9I2ICcr2YrD33VLXtyweuiNq_ViRGUmH9Wh-M6V_6jNIDDwU0EAPL26Kq6bqSNzseCID-rDftMOD7MMULInDafHE_egN8y6Vb5yAq3QRUUHXviT2VfUKmWG0xUf102w9KsBoswyScVIL0r-tmYsPGl_jjpnEUDwCahLvBDI8NbffjYOX-JS20fTCuFNA75Ch3hV6ndPgmCxQ3aAEwqnMT6Pewlb1DntB7JwjRYYprjBeI_ewVrj_WOFRo0kN7WVxNHjRj9L4zOMVBqk4_QoFrPM7Y-xUF9UxGjpzkjFzTuwdQa4H2Q0ktRivSJmU_ZsO_JdlVlKXXTe02apWiEzEi_swFlwd90rR7dfSGilaDGJnramFGstazG4p3rQrCzwp6hsTzFamgNmIBAAkQ2CilHEvJ7Ish_LSQg1Vo5SYoFaHVMo0Jfs-UwjBM6ezrhMUDKtbMYae3dgV3V_aKbnCUbG8dTA4E6H6DbjvdnlgFk5wVAm4hO5lECgNPwsW9yzRcoMO4UYK_n0NAhwWkvow2GJQXK5WQ4oyXaZFS5ux7ewea7paHbtCVmYClF8fuA2cVjeyO_KOzaTgChz-0vmRfxNRuO0q8IZ-Rp1Op3LFmHQK5SSCntG7zv_RAHj7lfl9QkR3ZrC5wmCUBzzvY-pSgZi7xvuOyyo2ypvht2JjWiA-cXkMvI7nj6GpDrBXcaqTWrtZgbCa9AlquvZtlWT8pUHfDGzjxSntPMA6eO4Xts6K4D0XSuOi-l69s4uBBQnAdjOuo0gnmicb-aE-QtSFSg9dC4ynba3F5Fn_q_qosfrlasDhq-f1xrIVhDxCCavAXOQWEQEXJ9wZGehpjZWStfWi6OBPGxKfdfwI1uP-fzn0JQzjOtlWm4d4_mV66M6wGMbweTaVFd3E7QUfRWtigg59ynOom8asA6wTbsllNIOXAkIl79ANArJnnLPXFNj956w8fulQzdih29mXxljdXP', 239, 'admin'),
('3rriqre0p07s41pvtrh9gflfg6', 0, 1, '1530685001', 'Rd6E7bfaYrmRIlt3PLcfwowyo5BylLBIVmjOrxx1x2hq5hP69VYRwCtzQTWqqYIEB_yCJuH-7GZQEvycN6NHOBaomjO_QQTF0QjwPa4RlBbfvbZug4SYIh1CgXGS2f-RTVqJDJksg3VXdWNvsu6O7-FFJqv64Pq85dj1DXCLZg882n3e5JYnft_Uf8lW2NXnzQPRe-_SI3LlWhArxOfv_udMhQ4oujxwEIjHmTZjYFh0WomcFOxdM-g9xnpCC_1FxPN7uao4mKHJjZvzadUfxp3vuKhyYOJOfgrQdBpVEmLg-eaoT6nD8fuZgkcG-80pXF3FUyA7mXFGymiCTc_WUyhSHhGuChjpF1YhPnI-2I-dvfIzv0Q7y2WfPAvhI9Ea-9J75XBwpqtN5RT1uKqDKC_VA9jKSKf2OQIxsV0Vt_bKO4Fybt9JWc0fmy882O1fs9Z2acWezGSvgaFVisEQ4QeD4b9O8qPnnLau1uOYW_wVA0GvbtQYSSzG69ymuCHBpPxsP2Tx-07ySy8h2AL_K-T8MWGZfNgnoHZPG63O6fVqw_uy87lfDLrQKI-s-9s2ElcvBK8rvMbnkVcduZunhBo7adUifqc1-3O2WA_wD6G_pgIJXHFrIRNCmdOksKaLLo6pNpxVPQKCMYAI7Ef5i2XAYqc9IjNWnnK0Wgwnng9Id_Tj2fgI0NKeOB1udkZ4mU8eHbvFS4ryieSImFFQNjWOjT2QzQW1vQMmiaZZo6z3Aix0l-LFpGKr2bwjAXAjgqsZWFl6sqxK25gOZ7bMJSWzSZlZ_lQoef9v6N-m-h6ymM4vu1U9ZSpCwqK8_b7ySyi5ihDdPXS5gKjAfuUl0QH5bhYb5UAH9ejkhCLPWDUEzBGHDZR0A--dUjlEgMjywjwKLeHTZrP6rY8otaRMvWb5An0AATjsPD65IQ8EDZ4.', 0, ''),
('4eed42lpv2v4kcdvgjs1aogd71', 0, 1, '1530851036', '2uAiQ9ziDC28FbVUCJYkS4CqV0oIYwe6WF3XO73KK1nYMvFPtopGWEhe7FmtmVaqzMI84Wa9YLW7M0RXR2FaPjK7WaFNi5xhjd3XuKkyC7PGdugKFNkZogZ0DFzL_utOfV8cLLdoHwzozDRA9ZutBWWEKkv9vvTa6PyzQotK_a9Twy2OtW_XB53kVlMj1fbf-U9GrK2OF5EgQLJBTDPkXVfxHJ76qJIqSMwPlDxw7zDzgfBXq_AU3dGaOzEzOn4xxKRqCFW1IGMQZsFhjanjdpeLSvczcR2BFf1BIkom-I_63pgJnaA3VSsq8fhFHCXxmmElX6sGZykvAqoCUU5qNC8RueGQAhay1KW4clMYyARwCZ33thNExoQWerEmpgqnZiWHisLZjQ3UUOl2D86olyhHOy21m2H1FZ0H3eoU2gOS7V5rPfgRWAXsjvPDWFYSYSLJAO-Gk-5_ScfnvnApERuefiplN0V8qyxz1_8bkxLcErc5_DLR3PlIdfezJxiv4lHPAROCEjVDtVN4JhCQLno5qle1mfaaZ9t4mszpXeA84XT2buk2fYAgnEYZDRxkHEadD4OS1KtICY-klkXPp2lLEu4ZOJPWr08aZj1-9QqgwzJObCt0xNtsFPZyQ05OgNFDU9F9wks45npX4bYBJFd9wky0ufpIUbJDPgTs6m7OSDPaxmz0v_Qe7TOaO69qWc6fSaz4t3NTVqJwjeVfqgma0tkGvEsn065_xOGb_PU9G_MY17r_t1uZqJlnMM1dHEinMVZQ_tL1yrSO7NKnu8mQL9iDWBXKZWQwwrM8W47hTlFmd6WAo9kAWLfA80rU8WcOI94ar6ZPvlDQsG-olxQKoevTMiDve1lrm-CsC4KJQjmrlZ2bua2-f_2mY1QyBwG_28EKwqQqMrrqkkSIRRhymZzjq0C-4EwxwApXsvI.', 0, ''),
('502ber94hsu2ra18g96f7k7gq5', 0, 1, '1530633762', 'SBkPKOup_yhDp02GqaBMxJimW6FFuwgtOCFHaPT51adJ1Wk8z7ZCRBHhjAE26sM0JBOi6PIPbcBBXfZb5byTWlxaloB5cJ-4BaFxJODA5RAnHeSC1AHZI_g84q2s2t8gfQwCkGJuYvri129tqYUnfa2G8I6nzjB26kg5ztEMAyms1Z-DzCFfhmKBfm7YGglLNta8UWVJflmpe6Bb5hMVysC6MyoI6VF4g6_jqNl3B3su3W-cqKHcHu9AXGGr861guyMnL32i7HT1aES4DWZNjKY9-BB8QwDw0Hc2fzEbxM5tUOl0hlGmHPeqavIQasGsi7IFG-Om8nUopEIEqWvN0eEP0j448I7TW3fK7qXiZ7H5TrjyrGO-mftEd2lzOO30oxdMYgDeXUzWxRrrV6VAlkyjOMfUhSWIQ41xwfX2Bqz44-7nEsmAanwdOOmFfj9f2qwsT4IZhrdebXh6YfRFuu-jctT4g8fGUQ7bTUWz4k0U5448Y19tfGIRYpAY6ljV42HyvmEnqD1m4h4jXcHH89ZUwFpbDsNHup-K4dmYfy4U4Thdn8UbIFAmp7_OrNFG6617YjG_lmDLfnN6-JKvq1JR8YoTxyEfva4zi5MXYP6rnpslL67w-ad36UW1Ksf-HZaGstM0fDFbNXX_WzE95iQjXphy71AmyFiwie2-kkJfic9gWQHBU4vMsWLF7CAZXhnFLwYxvCCUH_2_U1Y4grmAD_ZIo2iNUIYB_QRQUwZho0QWofL7YLUZ2Saam3g41fFoLpBF_NCmrdBwyc2oerzYH4B_67L_jvzFlS5YnzJ6n_mnMaK0onppCjqzsjS9jgHj3UE_na6RvY09X9-5EtG0kgDrm5zfPC5Btz8SzgX5OgyzAwwkW-lgshCC7w2DQHw-0ew5J0t7y8NY9rG5Dmsy24gbhCcYHUF7MO9LZxwMM-P5fZ4f06m3uSV4KZS4WxDMs6IH6waorRIWrTFiD08WuN8E3qo_ky9d6hwpXshbDnXAZDFAumWttK52ZTXj9oOzez2VqgcBXz6iA4WgHbMEQmSOEePr0LB74wW3yf8.', 0, ''),
('5bm5kua9lstuiguhf882967512', 0, 1, '1530859631', 'dZjvjoe65Y0zfMKzuTqaveySSiNZJAmFS_rqg8CIDVQ7HpecwvrUd5m7L4TQtMWgjM5Try8dSGUTU3dtnWiOTZxOOhXg4bInCBlSE8223lDTUqQgpHyu8yOW0StfVbs7QLQwrM02xNSGFpq1ZeiksHDL980thqEDvnddk8lVcJkMG8NYzyO8R0cZMGaRYOAfv9xefhuVKNrHEP5wV4r2Uo56eDyb1-iN98FosGJv2w4uueYuGNrJV3csIjeoMgQMS1hTxBUjpaUhj_d3wJm8MHn7eAnL86vdatJUl1KdqZ91TInN_eyXUku2rZI_M28BtccQrguhbO1JhACM6xiSnHNGaMKCde8U6hViBVJl_yyaclyix-WjteETIY996nny2TUnBRk1R23CQ23DkhsvJ-1SSvAag_-lYBX4HQKLVZhG-9NTL8Q0y7vAQe7piGI0KqaZHYxuPXXxs8ZS1TqhZvcY0B-eKoAyYsgzyMbrCLHaTD1Fym6A7A686rmz1a4KVL90nD3UlWyafqSJHPTRGPVYHqELgE39vBgpXcw2GHO05od8wfoZZyg9CS11b5B_dr8TJaBCZ6S5844bV38AptH9L53kdikYXrDarIKf7V5PtW6FWutG3MmzEBK7gsD6aSDFBuFWBKDz25JfEbiIo8stD1-jMIlSvOBMll_31i7a3PesxVD8NFbunAGnCVgzRvF9FL6sMUnaziyRRjMre3fToISSbTigqZw1lVteC267O2-f9w0ukd5ugY2mxOCrrKvDm-2kHwgNC9zJZ0Dp-g-STN5i0cBUJXGjTwjrkUjYLh1sah0SinBkIbgyoizt2xwYIdJgAXl-mZJG962anMjNei9aXZhFbImxyaUeRzKVqBVL-mA-QVlpyBgsO2mcRfQP4lpKgSqJTb5CQVBB2aE1Vy2l5ssQH13Z_c7YN81NCO9-ACACxmPUdSbfTk08Xch7MI0bytkPAsV84NyX3-d1Fa9Yw1I8E1v9dvKr8hR4HtVpmBES0Za6bnmtNxxdN68KB-fDGh1B3LMFm5DChpMimPZVb6DiwvLv_5TpxfT8mcUnRASL_60ReVJwhY83q46kw8TzZK8y9kuJcKlEdQocidRBBdOb7cr_2GAteV6gJW83BjzprEX5YqqquQJFDx1bY2Dz9Gr8o2vBkP_I0yTIGHh-TUslFShVwytkvu7WfGmX2D8QxLd4zxA1ZpcHrwW8-VNYiYgJf7bIVrTA2s7SuJqktc-DpIwZ2jC2caN4jJDIofL6w8xUfjmw7QJp-2pilmBSuuOthVmIZeZN7xYMFtyEi02A30Y0GbyBwhFK_dZ-EpK-qek4JAqcXV3oHATg3Q0nhCiYz816PKgwg3wlKzfyM7-yiNbG8BrootxOhDljXVFwksQ9Lj-1Vly2A2oGRhVnhshdgYTKVULpFGAzx81Yf8Jtnkw3qCDhVFVpsoRIBgOesyNLWfVIl1Z81ICSXfVY8MS4gpcZiHYgJ1JMVoqt9C_sn1QDIqoLlRMqigaZc4wmzeUA_rJ0DiNdMCKnRDybGS8x_t0cieHE2w4k7CdqX5dfpJqEYHTJwzezgUOViEamyGLfWJM7QnPlrs55slpqeme2Y0AY9pulXiQhlk0J-ZgP6CUBn3nWvgQUnqxI4S0Z5-Wkb8gGBelRKxOi03vLgYStMS-zDvn9Oe7kBF0KX1jNl55jWrvP_8ih1A2B8z9luz-dRu8RfQvg3uDLec2Xz6eHm2mQJW6YdrfQGweoUIHZxPM750WdNZFy6OOlhtsVDbNqmSS8QzbiRgVlU8hYOso9kAewIoVpIke0CCY9khyvXiAAN3XqCVY2uE4cWPIPIHZj_OHGDJT77wgCSNMTvGSRhRZQhhjiJEBhYEML9CpYKX52G86KdJpABAISR_hETugA_V5YtNebgjv7a6vbYUQ10iO9hInicpu4L3c7lxJPB6LhOCY7RSMA7V6fvf5A3O75XKeeMM7z0LrY3ncFRpFg63SdkvKpGgvxjZqVflhZkAQe6CNm6YDUrjVvzghcvwitVGpW1pRIma8cGt8HOCKaVkSpIjZE-0myHO4QVn1_tM82GReb265u3R26uCH9o19g4qCSzVX1Eo5upfUC7Cxe8263BCoSSVcgGBm3JCCKu2oGUr_28iCLIkRxFBuTBYV4EY0flKe0H68vYQl1BhLpKUYQQ-bkwdkYQQ2DxHQ4NjrLVKAiQJ6xeNRSwgMaiPx1mFiMGuzHyScjKXwO69x1hyxlE_JbUiMy9TcDK6vAokKThI5TP_cLAA47aYEIQaT1mXtX1U3E5h08nC_6zNVrD-7J5mCLGgB2X3a8p98rV_k28Fhe9SPUBYKL6KvR28ZEemXhWJgAdhkTRsvuayi62_JaaTWWcVIGWpsntAH9ohTipwkEiHEfO5XpyQ_pqGh_E3nav2pcho9IXhh-wDZCs3DCM3T9IBJgzSh1I-YYHR0IEJE12FX0QDYJUqoF4dYCqNin50mcGdUSaabnv3WY4s3_Hxz4tZYdl3mmJEXHd3lvQh02awyyWr303iMDsMBqQ_J_CHEgkZIAGA_dXmHbbeyHUGkXfQ..', 0, ''),
('5c5nbu5hqn09jguuu3nulqedu6', 0, 1, '1530811845', 'zvhJGVk253GQhBvVViMidzE96gO2L3BIwElD5e32uw3FBTNVCv1PQ8SVsNbi8UH7yA9Kr3QgQ5qq0fqfoC8KkqPlt3et1FmDqP_CrxLBpxIxRKIGNPjGyqeGPSur0ud75gzunlJCDXOy9QQsKPDFC26cNvLswIKY1VjakdOOrWTqpT6Z571CjJVvkRj1w9-fyR55Uq_UE5T0KHKUB4s6kr-vFdTSg1HKDDcS5dVWYeN2WXd5lnTs-aPo7kLsVhlMmPesXjagdoKMCCKPidpKS2RciTU8z9lgo6tivz15hel3hQqJ52dSPXAfXfx_CyZPPB-yMx3XUfchTlnezNVfJdpDpCzPl_PDUnWgrnRkkWa3DMDT2nqdZwugd6YY8Etb_MNQ9q6x3tN3mCAs4yCqJcDK-0wZino9QB1QiFqbrZi99RinlP9rffV19HHlIQXBOYwAK6riqREz2VFdhf7Mvex2N6lji2NPcIVNBS04kZKYQC6kfuYP1G6te1MYKiLAcYB8yP4jXsbeaCZ4J4mVwxtimIDvx73ZNUui892UWzz9BRz6CVjZ-PvGhR-blU730IbOAu1Xsv3Rranm9Ei_Q5mBrYP4FoypHWsUCGiF8pmVsEqenroxP3yOE0yOEUqDzgQtoDUWdN9cKRj-aWb9LDNavHNZ0ouYqyldVRfCl8kN3SwEH5ghQ-wvnzrzeTHgXZaB1JoEthZ0IXXZ33UTKBpSezVcd72e8g1VwH6OUc48VyXWzl_XDN8llv97ugU5Fnz91MKk4l97rh5zR1WuI-AqG4WXfe2VzCBoLlHPyM-4FEpXgHnTM0ZBMo9_ukWPqc15slkR3UUJlIIyz-SjqO8WMDQSbpVLsyl7wgcJtZBjFNe_tk2gJIItokGOwAcSxw2Y4aOisOGZ7BjI8HUr-s-KyE2pYhar3MMnrTKiZh8.', 0, ''),
('5h31dv4mvc8jv6eirqeodl2ma3', 0, 1, '1530658306', 'RX4RPcOxoOy0ryfXBscLSrtra9l1h5V6uSfdG8-wiA5PZNNvJiKASBzLvtAkJecy9MB-TuxGNAKAiAyJUD_aG1yzDqp0ML6rLEvBgjO6eNoau1Vu-w4JLp7aq4q9GmkzzWzM_cQzJ8Ptc4OqaFY0EA-ho-DuXu6_RpFfGqVfKUPy_JP5XfMOUyi4iMyhmFi4qIJN0obyxPws-F03tgn3ztX5D6NMsBV-k8Q8IZsoCkMaZ9IhDw-qYYaxwXc8PJpk5otTnTgZjvCYp7XvV73AXCMZKAZSxspWZ384XktQYeOlAWgh3Ge_2nXPI_PYVIAyOEvuCzzUeYj-SYH1t57dEK-Cvnwy-q5eYXu9pX2pR2tqO4Mf9fWhzVdGF3gTNANOStoPELVxlfcs4MdJAedEdH1JRN4V73L0HVeemjoR5lfSjPFvep-jLEV9OAfptv7e3sAaeTRQbZyyx1Y3kCsiAY6R_B8XOQM3De6q5U-8TJENMPdF1HOMP0Ya7xTwlJRH1pMC3ii-tGsEMy9jJ9wz_2MFl0v7CCo5qKfaK49rDMXi8R76f_q4ZnS7UAPK5qh-vrb1qObmvU2VVM3jcHJzDlC3X6KO7BUJutOmSIaPa6RF_bFOr-2Sh_XwQuoHKK0M0rmZPpcDFOeFNFC2SvXI354hG3TW_rc8zZm5fO7uuouP2t53qDMZRy08Mx0111HtHSrtHtqHCRPfamkVIPqUdIV2j_sSh0VjuwwIprklLroNYuTjy6iOuI30S2dlWLNHlXh5EQ_BvtIMFqornn7oI57LK8VXFrt8e0g_orBzWkuqSrpHBClQTc-6QDMWCCvTaebvs7VTkmFq8zjemjP_mLKXFxCsylWvlpwwTPjfFWcITTcibWeTZD3ohGMIE9xpexWPGgtfF1thCD-lNxc7GMQSKvkI_MY1SqWgF0fkk90.', 0, ''),
('5ms0apd3o9g3j76ovmt1evig05', 1, 0, '1530803343', 'dPd2q6fWD99mug8yeG7Ml3OfOoTyr7CSi1uMDkbKjsx4cfr6psAfoRybi1_1E1x8Yr4ar635usrNXGYq35p8LyKuHGQRmGzntHw3S4qUJ_9Y-MEKWU95wikPtODQpZior-vVeCfkjf-Zag__wTGj7gSp4dpWswB5XoEYYVaEg5rRWsGiuOE8JwpyABD8qUYw_miul_-ncENH8OKP4wrogGRRqc5tLib1d057JKe8m6dZkUr5-ttmRzj6Tr7X6lIpYNV36QewX2kXt0kdXuHK8zctZsQcvkQoX4PFCUGW91VOqZ2pq0VKY3qYZj3AFe5tIgfHzxbDQOxbGp1HFplrQbhIf6EVIQwpt7ftX2yYxVHowxl-Qz3cPH2Cx1yKiNw_S-ujT0EpYS542qRfg5QNUl88Nt0NpbFXHF1D4jR7_4naPbAO7S1o0CZq5tFOb1THfZCoRi-Xs1_arYGlqd1tbxY7CwSis3S9yQq1u9yx0YAUYEnQgtW9XqDePJpozZNjt-CUqpfbndFbOu6RsBoLU0RSaisjQpH3-N3HCMRmLR7Q_-liLf5aw4ZmhZKJvLVVDbRQy4LqEP_86XGmhtX4BCjCDa20vIOfXTxnNIhEPNirax8_qCdSsM5ZwKXOrmWRZMexIvzIvBRqp4xjEl3DjcSSHF68lYmNIRJssECvQyWYMg2eYBGHYLsIDC_HO25Ig5fqRObNOGQMy62nQ1C6SukUiCD2saDu9MzZFTD47F38ZRQX-4FKNQICy7qQiS8a256_c9UY9_bdHwAL7s1czvBPOKZ2HpZJ2DHSkQnRJ6h8j89DIGV3mxBbampXG1ajcPCKY0OG14DzPjM3JEeaouvV571ldU4q_3XnX-jqX0YJcv9WiAkZpsRrsoGqfRnAT_Bpi3IrTwikEBy2YPlUI7FcuPOQPND15fA7R9nCPYRsXa5WNy4AYtSCVZrtKFuq-D5b-9taQHF14P-iobScSMkZFDhtFS2gNsgFw7-HdeXu2WZ6iy3UxLfrF0lN1PbH4UJXyUzOgkS5fu4lcf8uCYXSNZbUN8quztAh0TLwFTa_7LuGXj-EVRxTHmHhSgMCDUQc18A0YsX3htgYgVQqVrHtGT2MBFk2iDiMiZ-2ylYagO4oarFtHmW6qlqPmJiVPcvYPJsvECCiljt5eZfiCPMbSLY00vy_8RPCZk4s4AX7DVhZq2FkjEFbpBsqu1Tp3pTu_-3Ibv6b6hS47wFW4bsjwTFACQGfEOerLNE4BA9DzLAnsOTC9_TLjyGItdhSDZW82NIJUAj9Cw4bLoq5qDwNdaFpwQagMf6Uhai_T9PjYEHsLjjuxf5OQeuw_KI8ZIW4gGArNnHd9RC-xhC5hfy0KGPBmXhAqd-kzgFi7MANWQkNN1WqnepVN5-NpQ9JhoTLT9k06obAiq-Q9KmnkQ-j5gPAJOZjRCgJq_2IM_mEC_Yc4WwdV6x6UAuRNZjL_DZXTaNSoh0JDeCxerCT9EvxfQe3FEJhJx9ELuWrL9IYxp9ZU89pcjL6xahKa6UnE5VpiunxbRbhN6DxsWZKIzBGKJVdJct2WiJ9MwJ1yxZa16taI4wDzpYFGdmiZKxzxZKhrg3acBoTvLvbpNid_WHPcAd2w4EcslCb2TLHQfwMt4OyUuGC78Md1ZOxiHNzL7MwvGMRRQrpybjej6jGZAcSvd7DH4tb13blHIkYWTIPBY0KmEb272-_REevCtFZebfvNEH22zYyAq-z5CeS6J1Qdofpmx77k2wxotB34WxClnT86kR1vL8CXjcp2-ZEZayTm-WYFv7j00ZOPv9eZgWOxpvxO2nssn7f6kU2tkcOlRbCzrStox9zWTjnfv2wb9LSGg-20YgG7IIjzfM-3sZZOjqZh2aJt8rbttzvKbLcr7YjaDCJYMEee7O4lv8g_1t5vbz3DfBI8CmCIOxysO4N2hSzwKhVsEZQcNzC76ZFxlOh9JKw9olr7zHSMjIRUI6ZbFLzuTvms9C_tVMCwRP_ChioMZmhjl7O1oD-U1xNH_OGudyAxeH81N2NELVQNKHOXuYdfZ1GuR_kKaSS6LWmFIs33ByDaI1VEh1_oPmbmh3zx9dLSCIn2lt0TS5xjJCbTZs1PmdNk2B0xePnecF0o44qtEMtBiMbuL-iR2_mSdFlyNHaPfhpvlM4fBhqpUuNwf7CJYqLHBBwT1Qtc_amCTNm0RPnLRvOlLFiJLp3E1UUzHzFNButmirvmHbVL0uRyk-ahLtY7Ecuc8xLQ0gx2waJV-MJUxUaG0yz76fYtlDhgjDYpmRTWetUOudWLi6rhYVhGBvyymAjngNfP5m6ii7NF07ssl_R-7_4vnMcbODE2mbpyM0Gbx97YK39Iquxz5fu1fEEjzegSX52g3fCuYOF6FpMuCcEG2mYpiDin2GqyNdKs3oYDbenHkjdQBc9LK8ar3GNYa_Nrptt7hM9HLA4SLVxLZ6MUoQIM3bhLN0BwcSUiehXByf1eSOC8Jbds6PkoFKSHdyOFBT_HsobYdJVkwFJLuxAd4HBMsnUQwKoeomAgv6BymlvJ5mzBjFjeZC9jSSESbRVey5gufOvpGw9P3mcmKUr-6ZD95_ywqTZxlfv7LeuhjBzyMObxI2fOk-TZdBpKMVQiltcdqIrcmfl4VEAgh3NEVAl5rlwyYWOZ7S8xDY_oiaEQVGko22QnyvQe8klicRlwUHi1AgYWQbREG03lx1-Ps9Zq2c4jx7Mzf4TsI2qSKUtNNCMFhmBsgIJNJY9OYW9TeFnEnJxEHfe92u4NlBP9iKguk6NLfoipBCSoAPhuL53h3z-iM8CnVea2LpspYACsXLvKpbXfFwy202O11hkUB0yZp9YlHqEiuIPCE19BQhlB4vTEkG8pXYhs074DzbTUKx9MhjSEl05Udu2-7mbu5o9kALYoilpDIwzx4VOkehdRZph-qpmmROQYSMMkx6eCv52IoDZosoV0UJ5Sg8YxppOiuBYK81jBCLn5XyKDmIcv8AhznfQGMrWce3mECPtbGo4w6HUXefTbzAUp7CLOxzX9Dz_8_Xw2zbhZng4WMWf55DalBYACUFx_6csBnTKhK2qnkT4ACwwizjKajRyIzPGHYC-KLzS2NCwGRUlilm9lgM7roLA4uX0frpoFalCOPjhcUpFVpghQzZUCcZg8oTobeAnjNSsGTTAVBsNI-xX9dLVcW_gmK0TD3YOW8r66B4euMDWrgnCWEf6ebu52U-EiWnCz0o1ueV9SCNv6khqruoKmWbiKSBuim_AhEYiFRX4h5mzyM1e1x1aKNQk25YQACHIhhLYPcUoyPqYQICwAainQodP7w3GJYLtIUvyShTS6uKCro4ETBKhCwWYXhqRErWUGxRzdeocYmxv2IMd3ZWFhPT_ghAeNSfJpqiRvOZQtHnSKjH6QncZr2B9rO0Ml9AokLWUVCDgSuA_wWw_bM-BSIJxIAJRUjhaL-GTQYeR3W04zu0jBSeVHpKXl7s5ce1giVa19vJF0Ctba5NjVwuhxh3-b8CAFTX8qf6XUx5V9kdIG_kdhS4QH--jfXzdkkxvy7aV5hysQKNiS1p_dxscHGAW6KONW_EMyu2_7IPrxXbqs3CFea6ReOaFgc2GUEV7m9hIwv0jEsnRso5ypkjxBYJAeP7vLaWhZEkTa_4OhMhiW9X4tsxlcI6Bfl9ASYgxAWg1PXrauOvROYv4LLfynOx5d1AEzUSTXfmGRiNSX5L7eOE8xohm3YCwup1NoE38IdTNMJ3JafEaTZst_neY7AoXQpJPMyToftNn9GdKi1Gfwb6aMpEj4dEgCEQ1-8JtkgoSMEE_Wb5Zfm6QFWIqSRPpjvOd7bFcX3Fl9GTMQJUjJ1SYM_sWu4roVYn2uJpJe1BQcfX75ls539Jwwh7gLrgAj1EMRwtsRsUJn6hia62WVTkSmAxNUwDKQDgllUNdWtBnt7N3JVY9F58jUgwRBaf8LGlyKPbHDJwCAbi9vzr6puNlSMcMItS9lcjyum-C36ZpXGih7DrZXoU6S3doosZgNtrRVk5sXfY9aT4ppIa5mCHxAZXDWQ87sXhq3ABfXI2INFKEIej-oW_ZtfiP_-0dG4DvEtZsqxXywM3vJqyyND3M2jtoy9Rmj4ITt-IVriv5TqoiNsQVhgWyk7Rh9g_k6z0Bgy-_fG42E5FgrNIQsmGHIevta5F0sk9WHZWzlv-j9c3OUbSd5oI7fcIWbIq1KmQdW7YEUrclH3i9qbjO6J8cxdmLAjOPD3nxTVg0Ct6imZIFBT7rM4HV99B6QW_7aNeEiVIjqQMJ9WOpu9DxT-q0NgXyuJJEz3in5ciNWSR8Jw030_DshS8WQSJnmkmlv0r_BnB99elhxAf-KbcLylgbd19_CFsuh5cGTaxpPGanujrF8IXFPeql66-XY9_poqz3klDY_j8N2Pp3u2wPpuYn6WeS0BeMu2thG8_gSxLmCuKU7P3D4BQ2DWQghVeZBF-aSew9JWnLR5zC5DVzGtCp9a3LI7ofR-45oSeL5lcje0Olup1sc8tiScbRxw0-KShNr1Q7TqFB4_IOW3FsC2nObhGG_7obOsFZK22wLWT9YYhAjFWLXSS3oKUvd-U4MqE-ApSLXCxcNFK93WQEVX0kNY20NG_fWPvIpBiZCNlLL3sXH8rKOZWjB--UT9wpj-I_rTVHn5riWMfjh2PCCae-ERJ3aWBei5nyYy-3Z0jAcg-0YT7YPsyUorLeVzJqVfGhFCEBhZJUCYHR5vEJ8pzotThvJb7Drozm9OMrIo7NcTfYEmi3MZCQgwqam_puPRJ5ZWCfTqZfQJOQA6Qxz-mnN3vwHXq--sACnIOyxxQeSGTEqXVqIfS6St8Y9pKF0NbakcctVG56RME2IhzR-NOwh8SSiwiRxqIKt7Ax-EZuS3udfCV0ZdTVt9R65XdJ_kFx7PfrBp3utmKqxMiqZq8pJikV3K4gkSX0f-UFRILvjaxQj7aTMrBhKXH85KkC8_-as71cDjfzO2BHRp3ZwZl2O73l267_w6hi6dkU7U8BciZ4e05uRVDv_pu_XtNhWU2oonpeEfUAXXjed1W7GXrcz-0OPwhUmm9B60h44r07bBXH-oCIjkNR8PT1', 239, 'admin'),
('63pi5to6f76r463cm1hmto3e84', 0, 1, '1530562005', 'KBCsm8CtUagLa56Hz9f5_V0AfnlCViztOfpAvvzoDpQtXqurGXSFsYMjTQ7S8SG9Enwq_-3feD1sybbYRw20ajcpf6K2rpfU4uDHgYZowKYaFjI7uQHIQW269UihbvBVoHzc4W-vvMusnPYs2qgNXxIfDdwlZpe3jaW3M2jAvA6cCDFxQBkoMIRdf7vNN7xCwlkMj5-4EooKXQk2CCL2P58ZZU2DHYR7fnLawU622vMdhKlNIQIiizNPjFX6Kmym4VMrKdehrVt6IA2EjUFKXxL2yYdR-psdrjJO5OfvEhaX6_Ji_ps3oVHkgGvVccSzjHG0cqfctNmEIUR_sozyTvn6hqu4HGvCR2_Yk65H-jRRZYHI6FDGp8j5ywqPrr87Vpg0b61jPlbti8l5TeAkkvKCphYSN_5asQyuhmjcI3MBMB3Uzl3hyGqoreCKWQB85CYD3pdOJEYDhCX4RNiM5pZnqsfj4ze-_5HHjSk3yFed-TjjnQxsr5Xa3lFyjPJalR87s4xSKbedShMr7rfvoBxGqMxY2iPqeCF_2BbStGkgSANj57_QNsaYJRXXRkoLYlYmXEk_H6s42OrFSv5ik6wghb9lQ4ZcH16ErO0NHa5FKDf0rTpekpIwlK2nHWdA1Rd6suZRmeZaq0ABion6eDM_tbSLf2TXDQ0iB8vMiTGSBVupc0cKvwQWjtBocjKbYLpuqcf_XMNfqEP_IjFKBh04QrTctLq_qe25en7QF6kIl45Pv-tq64BMhZG02PyIq4hmYOZREGZTRu9BhC7d9cfTE_0rA7hV-ytRkbBUYAeNpERHM81G_p-SjpAoZVFjxfn3-jFkyxQmka2Lt9JRIMr-hT05JCnVc1fXmvQYy17IDgwRentGrcS8p8clrjXiuSwkP-Dpp4EXDm5mM6nAH_ZbMm3B2nmUONlZTHGS5Qo.', 0, ''),
('7qmvcrhkosoq4vc52to13ccgo5', 1, 0, '1530683681', '6_97EirsNCPcqSW7QzLZ9kO-VoNauySiEZ5NynamvWuVhbIfIe1QVEoXA8qCNm5Gq0O3AaTdPn7PquFQlrC92-leMZfZA2IOoN327X8mdO8naJqM_TBrQBuYZhSibMY3a-ZTQn7FIcbpyj82iikhcqMcbGzpbjkMDLHs-nxrv2akw-BV8ZOvWlLNHyEuk2NLvKpkfbLo9LYC-jQijvdNXQiOx5hRh3GRvMFr5mSMmZxPZRp3gY7qEdFYkdmvzV8d8y94YbrzKajPIeWyeEhwapTLN_5zpr20wLGrqdhh1FUbE1jkVeOJc8OZGJ9IuMExBEh5TquMuy7EDytOpMPMA6PAPwQ2k6Kn8kHNpI4j9mTWkgdrvxGMtSPfhmMRq55MInnzmN2J04K0u2NLAmnAvW1oYpXBF4ypIyKqlQ9_k5fc4CU9isPllUTk1y66Zv1Q61Npp6b3PFKtmHaO2qbtnKftvOEPDX4K_tFLMEDQtkcrjXkKDo7DWZAdqdzOPJXWMAu2n2EovsGqFHS6d9AZUYK_2on7Jkg_sCfb95PScp8plfKPQvwS8grE06PqYDbUGPF-WRprvzJN-T2Lb6eHPcRYyuXJaLZoQhQEqHi4wPEw5R4Uw1i4G_HdvFJR9GUlxKd2V5Ne6pNnAYJd5xuOVGyoZwbGmOibb3hJQ9oM9srfvMiLS9aVgRilupCOsoFkzfWUUvgC-Ho9eC_aV6IFPtf84w6_rfNJ_orIOi69GyGq5LhgsF091oh-L7puxAvk3FctsT-oZw0kQe3vrM8bkrDCPtJfjOBpjt_Xw8gk_4__jPwxPFOUIq0KBVqtrMUPDyRsbur9MUOaHB5fj7K5kY24Oi3Q1nYo-lRi-fYwzqAMBBfhRAmLONeKZEPW8VHcHbtFPUfRKQgYL5cM8w0ssF11J2F_t6Mi_0c1m8AvWVbQ7IgWu69u37Mn4gGBTb2xWviSIt0Uni7mxtkOFatjlV1slNKaWIYL27rUREcl975IzrybRf0Z8_Bl39c7OEDxRZYBbQPzdxc6pXCHAB5WLQ..', 239, 'admin'),
('cbtuodv87m4d7qtv25tf8d3tg2', 0, 1, '1530562007', 'n4NYsISaEJc3I_4OoH7N898DUDJR1VLoH1qTxgfHL0W9EPcoa3pjXoWbEvY3HcvqV1zcOi-XPt8t8EkesdkW7XJgWlE68Dxc6-0bHg_WbXz35fFG7ypKNlsGhCsqOEpp3V9DToYnJcQXUQxrc_QhThkFddv1jme7yeXe6kh86aIKfE5JF_c9CJruwz9irRTQZnPzR_57H-hZ0nCZ09neO5Fy0J_LuFyqFjIhb9xjDWAGYAe-eDVhS51YojdZY-Z0SMHGLegih_tyukPMdWK9JtPKVqK-wmYqHfpti-qxrVSUhs9IJD5BhQVRXCcuAaMYT0S4JEErS6Fj0HU_ihomsqTO6qbTZx5rwJx24Wr00yCtEIeNMeuSh6wXaoEghsuLmaWqJ4CYJXSK0byilUCWQILm7GBDyrCzYRYP-nJcUM1ePAkm8bJUCtyqhXmTrL2KjNBvcnv71JZIl3NCAyhKdk24ov-pGDZjkthXU-UoqSkiBSEGZKGpZnWctgtgB4ftIWVDbTOZVOx-pRWbENkBByI34PoJMxGM_m-OYddm5nIPgsEK3OuPuG-fDtHWUsCV69fFUCCj1qeuyGMYEwrC0wPMG5swIpvj0atjFBUMi7q15TxY36ZALemZFjQMm-O9KECJdqXhBPAurGkoFjOVSZxAdVzFSKLb0Ibu9qm00WzXlTEpBvyphEJqEl9Uqo37_3q7FsDNrOYQ5jnBrDzxVPdXtEPAaLU_UcuP4atec8lgU7vabTsEZ94teEDTZtQzYg40KZdP5NBQ0c-apXislIEooX6rEIp9M3LvCYpFgxzWhyWGp7Iu-Z7fQIXjJUom7eG1ERYb4t_D9S1Ka1N1eXJh5NWBrj2TeNEP-osvYiWwu7J5UamAbFXCkg7Cx__GylpwfTLJXISwZ3-UXvetShUGk5Lq9isTJVLkluHcH0w.', 0, ''),
('cp9tsoa57tvi4vb2urgbc9i084', 0, 1, '1530709154', 'cgnUJRwWzm1RnZnUanJkVtIfemlMA4ZNwaccE4qK-kPA0oxPLWMdIYnJ4OdgQt3yGCHVMlKrIRPPQ8SfcZQ_1yUDkFp5b5BWvhN10uTLqDkprTl3y2wSaFH-JgLDsMZIWI7nINq0EMckgXZB-4yDs2yjyqZOZgN72JFzfOkIBkDmnWqhjVi8SfByVg1F-T-STC7t3z1oS9NIXEw6nuTJ-9q16RsbUKGE2aqMJ3MVx-adaWzl7niHMAm5skUF0Skm7I1T5yxLP8FUF2Y-CrlgYN6dSy2heQDFO0ISRpxtABNfd7M-Qo4bFM0UnQUC5yedfFTaDm8zdb2lpMNBwW--boPFAZBNTce28Sh3b_t4v1coLtV3fz8ek1lI1IQui08w9OetTwszeRHilsX-02vOmxJTpI7315MyEcRXlZMzWFql71IIUsO_tALcnfmTIrA1p017SbAWTUy-JpHcfDLhtVZQtbLatj3x9y1NR8QunfRpHBPy5W-6kqEtK8Ct498AKECLIw5azgj1qmZGN-AnoydtsULV_1cnrCo-FZpdDiV49ajAywhPZX1sy9NNpl2TIZRKGTXJr7HUet7vMtGM_snvHNMfz8w8UASMqAekp02peEVutogveNx6r2BQVydAZS8q2_NrCev_nso85CuLfscfCZBsD7dl-MsMPJDSp80i_5ufInwBi70wNhJBaQ16gnswTm_MbpReCHWBp-SQTUldiHcWfITBn4I2_n68W460EXOpcD-CNb0gZ2qkfz8dCI8GzJqGLeKTwqGt9AId_D-2lsQIfNa4nzrrAiHY-YikHUMXKehjA4IxRehGZvQ-weUq8Fw2tVUtmx6NfPhNmamk4IsYmEZS5OZ4TJvKt4GjgtAsX_YdowiFEMDoyVJVWwlXD3R2O4O5RAlK_nLocEqLm98-bahN0csxgiCqwHk.', 0, ''),
('d0c6chc9bre7ubcinvbjevvt30', 0, 1, '1530625369', 'SwtDPBQ6tbxscLpxjmOMlDhS84MofPGfYdZLqVDFyY9yg2FgZ66-uWQxJ-bfNV_E8OP_mHaX_Rv9ehK6Ps0VXVdb6nO1eK2A4gR3REqrMKX_9VZv3uowd3LJuxk2mtawHw2c1t6HACuArqruTS-ex74o1f2Ks-DJwen9oWTlva6IbHUDfrufBY75nPFWJx3rjVqPqlsYAhgTd5Bpr5BiccYvW83uJ_2gWw7KCfDQhCOkYQHqIYo358ROqgle-aY_jF89XTChxSS0dMFWDurqJCPkqtd33nFpVDc8Adz0FJqj1EoTJP2RxI8daFTKcobl-CHJ1b17uqxjoVVmfn5gSKXiH7upJytPjwuTtW58mWKAnSCaaFubUQ22MBw171RJy8uEnNoaF0f4S7PVvfG3IaiH1Kl82KwEnBYsKDh0yF8e4GTSC18x3KGIszOiY9u0fftQWe6Zrd6Hr9ifqRzb_YRV3fk3_1wSRJpm0Xrnd1PSgNFdwIl0EjdgdXxMNy4bloP8e7enUBdpWuO1Cxoji2ucxJDvD0a3p8emEA0gUMEXpGj6UdrJzotPgvJMFGlEchTN-q92KyJcIWRVpiaw9thqqzysK4IwooLJrEk7DPrh_JFEonAUtmKLCElX9dF7G8kX3u1QoZsiQ9plQMHFrVOl8W-C5nOV1zsHHmhBrUkuhZ0xacd_v_eIVN_cmyECKWyHcNHffYshTbRD8KA0FqSl1WSxe3NVM9oIQXh1vlw.', 0, ''),
('dbg3246cbf0sudmbr7mrou6er2', 0, 1, '1530597238', 'gzdURlJqdItFuXuRcxbC6MK6OCnyh018LbIfS5j0G9URrtvODsD1S_VA8baJOSLxDX_rmCcWoEqZUGThIpm-liP3ClPD97958OKAbBgawSWd1ofJlRe0G9x3CiPBx2UR5U2AcV-8DWkZ17HzaOH-JO2Qq7yCARPOJDxXPXXBeTDnM6g5R7hqFdZedNIp0-DzKGc4mdpHzw9ynl9pmQRmfl5HCXXZGUa8kY2zki1AEX5JSNLOrv9WQXjoBsaPYX77yR3onICnhJ5neJSKPt8eqROSlJGX2oU1U4j2ZQig51qqC-hGZqwDESj7KmiU50Wclt9DA2yld9o2baNiZtuEZTCljyTlwABDTLJ5zGqTSkFi7tGLg2zz7grAa5ulAa2RWBcUzh5lLD--CbBEXc3aeGZdgiJi1kDDOTAg007usOzIwwkTN7YCOggRIBvk7lJPsxJ6A_K6keoHLpuMDVVT0hO-H0mfaar3R_LJ1JUISG7JDvRty24-4laD47OIFMr1mNYUPiWK0vi9oG6pmHYRRm0u5zXBp1d51xzHBB-39Xy0xgS9Wj5iloY43kdnNujvhwyt2jhYPDVcOorv5TmaxuM_fdR12P_8z3nYKWPr6vtIu3niIuyjZwxWG6F47xRKrrOWZD196fGWXSMCTJi--BUBDWmR9GQscY4j-ynXu1p89uTianGKur3J_IXCkF-fz8tB0gXlb0ZBOpKTsHbKR_67EITODUQ0wcthuHUwXzv1pPeVm97hJ_MobFYsU0FnoUMR5JrgwFuQUjM0MExnZmxIzGkHUV6Dxsq6zczKPllyUXqUQUylfvSiDQC1d2YdIIyRqoY4_AHBysIXIXdEPrc1lZ-kdOPq06UGFnZTXW7nWN6q5ViaDoEQimh8hv8IOk5zWmZDfsW4n8BKxAghR1b-XmDv_jbCFATvftJnTkI.', 0, ''),
('e0oek4p8qojep4nthb5b9loaj0', 0, 1, '1530855087', 'QavWDuj-vq5A9oKbstfp8FGFxrIB9vghKWRHyzTF4CqVuMijbAmnV4sYjkqSAU7o4WTxLU5uWQJd89RYynEzW4pSP7Rn7uVNuLr-H_sKlImfefZb1legushlZnZub-EqOlZtBOXvTy6QqkF2mqZoDsCKFSuNIeyIkt1UXnIm39t5JruFTKdITmF177QA7Xq4azMDu-PUjiXJ_gF55xsD2u93n6iGCGD2weMvEQyJP3vosXsPVAxPxBA-k1tkrqc15BPDbtoMnjf5tK2HPNCvmgUmCu4lUsh0nxPRL5FblDu9hzzK77xJgX9HiddsgYz-JFf9tG7gQLN0sqQqYEnfLCqABvs5N3s619mPRLSBtRy0SoQUZfHyLpMvJnTtC5ZYhU-CFAiejOvJPtXSebMLheXS287N6UdP-JxOJZqrISmsDR_CzBMC7htmyF1a2-zQ7-YdyhcWCoCsiRJ90ulTQJFNoY0kgt15JoY371wLsU1CCsS_Rln1OAlRxFVYuOS7H4iNi0bSIiOviDlBAzjKuepd6Mdbndm1nwIClF8fq35wYfNbUGruVY2qxdA4nESpV47bUIE9xLjcxfCbmBAOjrdMJeco1wQ2wkeV3RL2Y5ZLM1WKrkwfhadTIB81vtyD2LnLQqiah0e1_8V18asHMjmGFRwLvqkA39tI9z2E242z8EeuvZUnwd4TUZzzx2l8H866tAJVqz9oLYo0Z0Xe79naRqAXFg6iY0PbBqUzZKet01bOvjCl_Xhp45tFCZ8_q1oIHct4YQCNmWTba91qmB-2xuSWaGjOqhpOglPyX4p-MjTgdhwJ4pF6hqY7jUUW3YSNqTkpkvkndJKczd_opYztZ-6SUGZFi_c-niPXOWVgBfGzrK2HNqB_DH773VoO-4Nis_6pf6NEr0tQbtHr4PiVg5a_VISw26tJfjiSsKvd6fI_PpPBj2RENIaj1vUYsEBQ-GwpRbdNthnfgGyFi8fAEttntVffcXnCRp4yan3UxoQ6Z9apf59PVVqWUsxWNpz5bdvMON1F8dDbz9-VdksmLQfx457Z8fq6Mc0TQtM.', 0, ''),
('ecph4d0ocbu2i17tha148mqpf5', 0, 1, '1530810853', 'uuKY1-V0p461v20iZRg1WQYOqFdHlpmrN3pUl3_s6lmPYw1grthfsuyqT0ZJ3HiVfRmMYcwVtWHr7RsUSmsYZ30yUPvAeHEualYaMCLn43Jx4gwP4sh2QhDu8cZz8Bf4jgYVZ-pqs2rjhNF8OTkprxOHLGuwAafHfjNviBOVMzrv6CNhvyH-WTuLcntCtNE0-6Q-brhhpC5hZadFX3m3RebycYlhTf1b76fbgBoRGvg2KksKW7QMWHJARzqjwjtYr_Rt1DQvwt4Fat9YExhvUGxP_fcq75w_JXvqEw4PncaOlHjQdHiz7hijrh-4vXc6g_Vhv2kHA4kmY4vqTmOy9E20X2k8_auNpWgN8k-gZXlhW8cmVmCXpaw4qG5Zp-p5z0iYn4j2pj2flkTZvPf36JQwitxV16MYaLGU2jw6CoXeRoQatINXTAlw-PwLiIHxHWQ1L8oMgXte9yksZ39V4ngganHLLCeUC83Tadla5ZOZzjyBoaPIhUT2EcKj4xZG_qZaVOJ1XtEBxfTzaInU0O4actCTHWlLD3ZOoAy_ToNLfNtrI-uBQs9zg7uuLg9zBucLMZrSekbgnW2P8QBPLEDEqegFnUrv2nLPFX5KrwIe_5Bc_QiaH1EqNK4WzKcRIv-6UdPC3xcQLWFRLgaRmmamQ9oVivJer32jzgUJOmN0hdpfvsk5qyV_ZdQdwV09zyHSOvWilq7FRn0CeAe2o5g0Y-ySvCxd4bTc3zd24WhG232KaDSlc7AKXSX5JPRlC6yea7_jkTK8mTBUTtN52g_3WAVSKh7MZ4vXJx4Y7fbhFiH9EgFeUUh1shoyyDuYTiLK-GdZ2IY2thibUOnpZj3-OCmJsQ4lRaK1-pkW1mw44TsQYeoC39eRQ4lwWD3EyC3Gex9WjL_5mKK5TrVL_kBuToUMd7WKJGWUodma_F4.', 0, ''),
('fpcej0ehtbrk45lol6rbfk3j60', 0, 1, '1530855604', 'AI2bJY-Dcnmj06Ed5318DxS1iCz-dC2hP7sNAZ0yUMwRf1lBb-vrClbSwSmI0V9v7zPnPFg_hcWNH8egroZzuLtBafmRxXF7HLXDgtm2xvz0T5mCfnafI0sUAOyOBQVwxJw10IJ0auiCkybLKgFNyJy-EyW1ThoQdOOWI-p5Cv6mt1pBEOreZdnkAoyUTAYJf7kumdoiZS24LKuk56p921FqBtrhl21gYpr0vnwxeEIymw0NtaPECN5aKaOwnYjrF_-3SnQ9R1ik0uuFzQyvg6hyFmx5Hvp2TIF_xayaXwqDPe4p8X11C_0zGZg0dv9DijClNPc-_yNDDQ4Jql1vqrTE5jlfK8bcaw401YSzPCm6wIYwytWidDB_FNjg3gUjrIJc3je1DDGVX62rFyjBnkd0XmpKPV3ZUr-OIZYZc_KFS54UZlciIiUcB-Trns9FpaFVcjWdY6uTM9coTUL_Ga8tCSdm8upML2DUBDzfXgzt8OXrk79XB8aXoO3404CXImAEFaL3c5ORKcuQnEePM7dqdg8c83YwG6yI3Kf8Y4-qPiazy6MRORqEAI0a6gzLefBLkl_Pu288DvxrwSHxg-yX4yxl9KbKo4eMCZDgZKmadWGF1EaMl90wvMeJtxrpo42PKHFqwOjxFTuGQrF7TAZSae8wmze1ShwAsLmhdhS0uZ9i9dLJmjr4ifgR38rxIgOuXrklsoFiZzz2wM9ELCCr0iDAraeGeqSqFJwm2OsmQy1On9t9VAYjsQe4bdZHSJvzuYiEXaB9NnXjBLYjrem6G20yY6vaNRdF15dXvTUg8oTmBeB2k7LFtCK7bgz0ys5d8mciadMNzgN7eLkjCJp2_TngIbd6CoR6Sr25Q0qItdkxq2LBPt2RX1BrY88HJa0cFDWZax5MraXmAqrZwbxDQEEHU-01mRTadm5PayI.', 0, ''),
('go1ap49v9p4i4ubvm86qsn55m5', 0, 1, '1530834710', 'U9r_q8EoNoZrt-OLnfg1Wkco0Cnia-oDbN959cIVgW8_Vp-8t0L0-hHoaqZLDzK7c3VtWTnbr664JJT67Bv5q7BE7pnmG7k952tkJYkO2COTiSq9fHGxuKXuEOQvBwTRNjZyU0AGzn2pdEgYf-J_yE94nhKxJnQUOpQDAk1l2aKVSJQlxcNJSxe4wF4xbC4Ay0-PguvGWzeTXPobO9vWeEEncb1PwVUtT_2IaiLMpFV41YdhN8Nfi_Y9akEJL0pnHY51rlqf1T8QsLPP-dODFIgqBPIWbnl7f95b9bd7TRrtCJKvY8cGDpUwfpyGTEws3crgdnu8uJUk0qe_NBqKQjzlVK-VIvkYal68R0mb0FDDYrOB8iBWwqwoETs2xCUAXtG_xZCMahfNUoGy_kesUlP3P_dWKc8j6OmpViTuCFjfW77-UVWFrvIp6O67VlJlakqsOmYWUrRuzz2IvlM3Js9vFBHv9aHWgXttb_LSRHlO1Tv25HdiC7Z6plRfovoDir0aKSLxL7czi7kb8b4lErR2gvhwxSQGX2EOCHhBuR6Nr3r3IOC07cs__J87KyRSCORzNA9yjqwqnZ4P3ILxh0131rd75YOe1sL7W3zxzS2yydpCr3HvhpoVZC53RikAQ5v2J5_pbVwzmJdN00SXEDEnRGdNkRpBkE8Hrg6a_0JyJOZ0QGptO2I0uvPg_t_HdT2yU2-5JLoZsbZBDOI4-LinffMBGLBBaMq2CNRNramffgV3simD5HCSojRc6a-1v-iwbgIMo1HGtfmmIk4snp9u7lO46VQIXNcbIJu4pe49-JWfAQZKP-3v79Yw68RayELA7aCbfWHKGfl1Vz574zSZysNPHJatdTrnf28CVihiG7jADKaxXTU9mYdJx_42y8igIuibUW_dwNkZuc_3kDcMqkBhzM2mREIPH4fiN7g.', 0, ''),
('ijig2didff5nnpsk8591gqml15', 0, 1, '1530637850', 'Yq6IUwVtEEdL4DDmpNTAOrt23FVPFUa7S3bU6gw_5OdfaBZzICoiIE342og0ZGSwoIpizs5yzfSLqQLM1LbX9j1ySZs1zQPuA1Y3mqnEALRxdBEYukOE95fVtHe9ozOERzvIDPbG8xSndutONZykcf7ddlsrjqGDk95DclO0hj5--UEjYgwlGa6Uh9UwQJ1aRw7sFSoh0JkRjrJT7ZB2d0O7mS1m5Sx3uCYv4uYvaQsO19WLZJF67OO_a6Gbp3BWLAKVdNyblz_yqoqgpri8mOQjQVT1cZ_OlG7vzz-OrIqqUGUFjB6GNkTAZ-LyGDjz9oByTfk6PMxNgugSsGl8wSemPaNmfsddgkUZvoOg-DQO6fpeBzqwtnrfAwRTWSinjFC2RaBflb2OUWiZvcALc2DEn8MXCK0oqdYATGuJ2RGrdpbnFWJlKmR6xFHgYfsHmpaTEJTd2h1nPaCU5cZLpgqHHWTKjnwjkEiEPUgCqXD_NvegITULY2PqrG7uQ778FWcpVc5aSTwGEwMIqZyhuBptqkIWN8EoWGo2NT0K_Hq2vv5c3ETZka6drELyBBE0tTzwti95laSDDn6b-8zk0iO17dPl5RK83TYxY3phGIwyTfymZnLj8zIErjYim2K8V8z8O8rfjcmzXIHp2vUXNjC8W2Jq0irYV4GHJVnGnzoztdABirKGnQQFPjXuHW33GE8yqlsLI_DP-J_aNTrlF7R6Neq5-w_KbgJ4FPNXsam65VEXinHK8pncVMnHLNWWYhfaNwKic04Qf3UxBm05lPcLfsnnDd1YVoDnSPQqwLfsOyR53q2911GCBjTEtOwmxJVRbSfHJXf558dGXNrIpRhTL4gIwJSuxDvUTPhux0Q.', 0, ''),
('j5d9gsk42cij8sd8n520g8nrn0', 0, 1, '1530776413', '2YHaPd7en9WRFax-61KD5nMxXMuUDGx22zEbtw6XK3Sobofm1cnyVi8a-uJVfDJKkPx7dv3v7cK8rl0qVUUcMdnY_cxr-xAakJEGbTeNg7hjcpKi4mg19LVoFFUCZBJVgBquTRQkp0qXoDmEa7p_zJwwNgu3G-KhActBJUnIWYZJNg4HDeFRFNRUuPVoFzDvI9-4BYnTCrvJGim3ZlmmGL1zegFZAdwT-T0hDPLQknemUlTwBHBAX_0aOo11Qd_u0Q5VrK0V_6qIDYInBAUtuQp8smybXmnawIsJpV3d9DCjxTgMjuRH_A7Lil_RucDORi2V6DRWSsmQo7stfTcGwK60IcjKDSPnco2Yr4Gvi_MHQe-ujzI0XyollHm3ipRaxa3MN2EMLIc-aXqEZlfm0B85jewmceX513rOcL6bx_tADePIERfW39988gHdb23DJSA_G9InKxveBEy5fMOUQX_kKpsLSlJfO7uUfWsDwXVn-vxKyCYgK05mxhJHmfA3PiSG3QCpfTjiwv04sCIDhLN2Kp4xY8cDnte1orsU9sMJud633J0hPLydMMxUFdyJTzKQpqPzGruzKhEjsB5ymp9bnUh5-hhw7LdQLyvmx-ihA5XjDQG2DAHW2rmg_poohzPrzRE9emxcW41-mj5Cksl1g8flzKeuUxo15wLSfSEGCuAGhnJ9gZbRao4G74HSnl9tiTzwLOWUW6XPxMlvZ5qc-We3ipdJ2sxspEXgFB7hWQkbZ3yC-Ef-eDm97Fh0XdTuONCjFYD3snj0Tb67O1QUpTGXOJyJ4BpWfrzlLbOD5Ut0JWzVOBY7qgX99OTNH9W2JN3bBygFRX6B0QO4uGZ9EJDHFpIuzmnoWOJ1I5_uJSGexCJOvLAHfgrpsxo69r7MW9EQ5qIUcyhkBijitwm8zGHg-tW__aGS9fTKOBs.', 0, ''),
('jmre2tur65m7kuaemktqcqktv1', 0, 1, '1530563195', '3qpndSNO3QjIS5ADXrm6iHhoZbIKoyoI15QsIqEJ8h5_o4ToYkok9ZHe8Iq8oM8bOmJi1dKBiPLJO8uCki2kgDd4LSi7QgXrP0TAL7UgQ9OPFBziXsrIclydUTNblRGxqXeh1Cn5B737SQ6KgTjNG66Vwx410QZlKb7qvV1JIkarOyU370cp2JN2D5A8xaJQfTHyghTbC5REPx7xAvFW-tE27JdPgeV8Nrlsu-dKgIOJhvzvu_FL-OOKPEH2N-sTGmVqCjubMtMClQp6mejGOoIOc72iGpEzlAuCEPa-xxmrqqD0BW3EAkKjlKzGOkymv6NfTs_IhFU5YZ1MvMS-2NLA5BSIj5SPky8eKle4eLHwBtmAmCoB6zBaFp9C8TKlW7tpAC6ARlwzg71fqIijenB5rUG8dNv1Ug4fEb3KJpvVXbmlag3ZivOO39FEW9HOZXu9ga_2hZJsTR_VoEKSaN5lPfQFKGrCX3s5_6Y0gbUxSO1fElyWx_jCgP0O3l9zg3sgaQi6b9iJDPeOVfkFCAIkg8MOkm76KoC5JA-BmPWE2TP-fpK6tKscXW48cFsu7nApawLYeb6WRTmAPAPm1C4UjptinDV5_lBs4TjIS3qmT74jQXCzBzgJtOUAtPiPsQJot4AF7jwYRFeOnOMol7sChgS5Pa_DL1gmlf71wrQnrTmY3oHqQ52XKsXxQI1F3tFD1Kx_QEJDhvJad2LvRJauQIcrlYxFtmW1wQAhvL5ranvMkMNogsTGHgLJjo5umLB0KigWW5LMz88zX95HWOzVBLDJvCDUTNRQpUAs9_PE3wsG1nX8yM-k_sAIwt30Tun_yE0dDJ5pZU0a-MjK-Ef646b1xPQyBtVu6RXzZ8C5A_c83tDYy3B1R9Q99J5rRMV5hxEsvJoYNTrMQVo5zUjEOIhKTs3LZ2Gz8sjJ0OI.', 0, ''),
('kpja42nebj05u0ue3f2smjfee5', 0, 1, '1530631912', 'MsZ9eRRwzx8VpK2mMp6C9Cmv2PuYjthlw1olLb8V5IFJ5uGHenCeaDU6_CeraXNnxy8Xax0yVkAflOKdPGWTbJ0YcJQVpXxh2EZmFX1_kEABe2c3ks1ra_RuO9_JewkVNkunEPMhKVgTv9UWdXScBoIq7TSgnUxiIPPJpJqxbOqPQlR-2irqtpNN6wtlbMxaSkJ_vsDTOnt5xN7sBiOCoMqBzoORcPsZUazc2VpuM6uftzEDzSKZCwv1MBXIijCIW6Esa8EvYTuEGjIziDzfCEBtst0aWTH9h0iTV5IBUhRY_KJU6mH0_XlpNeQRfh2HQ8uD1UVCQyQTXA8n5W8g3z7B3zyC12ROOktiXZ7usR23gyj7t6cuO7_Axe3nSTfsyzeMo578wSXR-ixCEudR0Yd5aY09tami4YMKHmulJ0am_Vi-zW0jFgPzELMUWraRpWgS22OneCtC9UWfL-ldWEGJgHLaBY32QSMZAuueNK9YssCwOrmI5oP3inSaJHZXR-N4BYn_babYyrNmvyp0irhmGrEkW-FqwQW0cJneNIfZ05i-juGVldXwoQDNEiMbSmLlmU27fxHOEOoGuGeXICJxIivKKfTz51ybnBZ-14aNgOG6Duija65AepsvxjuKrhZYBwchevUVLUI4obGIuecNni3judIXrqsYE8t2rFJmRaQEIvxldxp9XqfkiIhCiQFvLbmcK8fe_CeakiPy-aj5sytKgtyHPYM7TZ__zFroNKWKphzz0K2-AnWZ8Zkb3h3PR6J8Dh8R70Cb5ZRQDn5gK5QA1QH2f_UuBgHzssZtnAaFzNHFOXtPVPmWtvtQe_seUJ3-ExwwM_Oa46L5-N0OjuoQl_QteN2-AT5EVdVgFq1CPCF3GL_sFawPVGDf6ize5yPigF1KyyVFGg8yZlg-QKBoSyJTQlnbW3_gonw.', 0, ''),
('ksec79gvrj61s2g6n1muo5o3c1', 0, 1, '1530801497', 'k8brGkgeKhVG3vO60Dl_0eHafSOjU-3Ulg3_hiCDMb2Gk_3D2FDiX9kB6fF7vPJ5K6g7iu9C_bBwaKTCamTmDON_-mZFw0pFC0XqbXg8sATwUagtKGC7SAAaGYO9mMpEZEZFreDpEQsIRk02IB9ZoeTV1jit_ztziiksLpAFdCUDFzjryBAjaIcF4BYLLopv1pA7xejVEGlH94PQNaXlgc2LYOypEMLc7TMdayyjV_IvYBxreUmh1cd2jBHX83O8H-3TWFm44hI3POKA7sS4UlaQEYNCOOc2R4OS9W9ni0Ld44bCdnA4Zgk2AzWeBkgMOM_6UN57oZM7cGhn_MW7CqTaOPujkeRph6VysgoWPj7-ZaRvzgjm6WE5IQ10YBB1LVJ4ERLWCdES9ISwjFes1DYBbHHE1cNYEt9kl86vcydMFSneGbcQoDk1kORLd20p0LvbfIEchAgwnAQ-7xOwBkqVGviX3dVuUywUeFZarB5LWc3L1X19SwRZLARq91rMuZf0qny-_qJEXlmktKonYLpOqHJ_SU7XniaJPIUc-dImLUNKYI1VkuJlVWqClN33goCqpZ4NTdG_3AkNWCtflss3XNl5TuLa0pXS7DwL77vHGFPn0I34yUwKoUmaH_yIIobx2cSrT2IQiqSi3na-WDVMVR1u-BfNpGKSTCsSJf6f9W4WCU8KLN2oen293jYbTM733SvWM91YoS6gFmbp7sxxPsliQD7g187d3a_gZYSMze3GPEeHRgbZZd5eK1lbHx-2TvbF3at7a1bEL9bnYMv8qLTyDovmvu6EGLApyU-gogcCMS5tn88djfN1jxt8F_rTSpfLywqpx5nXl_Sd6oZu-wAY7-QLL2rinJ007EXlYLiTYRlOKBDY1__oUV0ez7A30RIdpdk4wfcJGrwHnWzAlkw91jv_DlRruTW2qlk.', 0, ''),
('lb5phic028n1716bl0s56530m2', 0, 1, '1530863153', 'n_APgWFSC-4-99-0d5TxPxa5ZEVK3vURvpe0LKTKDMhvxSKkJItCMl--ui3smhJ5MIS7HvqoLBcF3gN8fkQulCw4VRKIvY-A__Xj7LDiiQuyMnTRqoJqeOzIEDr4Ek0rsUqItFZXWO3gcEEI6zC6s0MN7UsnXpBTqbKROqVcQJiqpUdgVhezqqv_7kdkV5hoQir-UG3TJ3pvCDyATw6-vXDNZlNpBJtz_cKmG6eenFrYWboeW_1gKP1joHgNZVXxEruCCInq6X9gwvAluyg5E2NTJnOpp-pbdKUMgs4TNE4x010m-xBU1pSIWN4dDxO5BILy-2ldnbgoJLD8Mg5VVBuuW9WKUIZbMn4AeV0phnpBwUF1oy-57biEw32xnBqORPw1iaQ1IW5bUpFXbePv_WJcWSay6fV6sqmtRkGbdxp4WuX619qQ6wTjFUBFaHn8IuIMNxuJaHaypusZy2ZO5QLGXNcGeWY76IlsH-9AQYAAvmyRISShrHr3jOHJQ0iFETSt3yC1T7K3373FO0QFvgarpf7rN0hUzsZH1Xuif4Mgh8tCWcOaXCn1LZhl8Vzo7FQ9Kpdg8_choeKf2QenB08Td8G_bYhuhOpCdu7u28ziTH355hEt6z60uAubQmHnemgj67Ut5eU6ACwSKRvfO02HpaEAs6wxK3gtQA9t8qJkUj5FWQh9VsRq9LjHTNbGnwSpWx63SkYyxHuLiTPmRtYFvjky8Wdr-zZ2NL5AUq6IKKxg8Vk8IuoXP5_HmzSc5fcMRZmtgvi2xQz-H7RwYsW52d6gDG_2FSDDZxDDN7oL4apu5snYkqRuweEX9YClh-_QzG8y2OYZVYUpjWvkk82JkU06U5F_KBpRBgFWmmysSorBn2b8c9WNie9PZI6nbGFAqKEy6fUarhyDG3T7cx-yyvkcCnytxNPzM0pphOeHJjiuhwCwcHcmXZs6d0qWFW0b8B2FqzYz4JqTka6SNbj73WuPJpjJd3FA7DXx6WeoFsKkuF9Kqsr_IJ6YekjxYqDPg3JCs6egoLswrfn9NAwFsVUplQaWXQvg6AUlPSuLeXF92i_FA_DfolTlJ5JviATDGo0sMICJ3ygVpC9dB0W-tKFujrfunNZ1zagmg88kw3AXB3y5P172qfgSSz34x4aRjuTvqKKhWAxxFK6XSAJU3g5X9U9nHqzFL1EeK4m9qBHsqQbgrJEmtYA-YLSiS-V4kBMlQRA1z0B04ecquNSl0D-j73fw9i3i-V0k7A6ll5rCOT1ArwFvVzHOOLFp8WqQdLXotoZ-4mAF8jCZ2nTilaAiQUPOd8z9ndPR4g3POI4ZXqd11IfuLdxueQYXISI0JSNJFpKkjI6j69KNKg..', 0, ''),
('lkebg7lg5il0ut9tujpo0an175', 0, 1, '1530562006', '5jD5v3YV6Ar_RTlhv56RAmdMZhdLadp9ypNbQkM_tZaitvhY9IBqkYz6yCLDSw48qPGlAJsTdnDmG4Bpeulq6H5hCS_g3iklqAvm5jyOrOPFoeXarXDI8lZq1qvoYJHiWc5o8-R6cV0FxyOuE9wi3DMcLtvoecluPP8l_7P_j0RatPj0U21CRq7VQDTqXDMlMuI39u9eLMOL3TFDy_LRDwIGBBqDLIfIubrCEfG343HBYbl9alhGSJj8BZl1fkaUBN6AlLsEHUOoJfirmcqgCMUAgznS3tvY-OAo1aoXDju5qSph83PccCqW7TrLquw7gEW5-yCEZNJwnfNs5rkeJJr_n-aIAQGCpGCOhJ_CGW8HOCph2QF3s0f13xbd0QkGAFPA-Zjl2sny7jirU36kGWE3FHdnzjghTYq3t6nmKRHkZk9iEkSCFjtTcScvgcNZSRPLsZXKl3wF6YcUgrHwLI3BrPKLIunYJNrOm_IrbQFT6hnOrjDHVMB5WNrorTd4cQ28_17yinzL4Mx-IjuLOioLRY-ss-ZxoJficVYTHwsEF_RNIkVDfEoGbuKOWcwGlIU1sucNq4xm9FYtR5rkNr4ayiNdGOXQn1mCIL4uj-hg24NVRp1MrCbkcOJDsm3GMiydUMu7WvLj_YobqB3LAnmgU57JSRV1tjfMv7hZ-g7k8zw-rrKwNrPWAV-jwxxH5ZaEBAsU91_fggA2BTRCVUXtHXRT6_fQ2bP40qF5l40TUR8dzj7U1JjOHlfCa-EdoIEBKUHSdR0R8ZitC_pz8Rpip7ryQWMhj1Vp0BjsKxpLsrzzdZV_jfogu2ZD0X4DiQzRDD0p-GzADPBVdFEx2Ze4c4xLTFbDcfvAV0Sxhd6oR8oHKq52Wcqpofvz6_4ncz-Kx1KKV4rijg4b9zeFJpuTTFhp8cVHK2n0GnWYVTg.', 0, ''),
('lotdeop80e9mvld3088inkth11', 0, 1, '1530563050', 'NI7LhDBCGq2wHlJbIHv_vQHanxroAher-YrCGuPSl90M_kOaNmvhlGIfJLTAAPbfnbAsjAR9rQV_N_r0lWOpdI3t63ezKGB0ff4wqUcCm1rdIq4B9tdbsvt4xvFzIZl0z6qono9Md3-kRSHAPR-ldk-iHrvz8Q_yWXfLaOzN7r2RFuJi6ZIDTBT2dydRnqDMiddk5EkLa1Vet2ZslCWCVX3-ICSb5pjLyzc4tfdw-Yv5SgP_L50_MAk9wEsAplULY-kShgRjk00oxxw2-6KjorkI0dyP-AOwhWpTk9wA9dR0F_K3445siczHfklz_3MWx-ienl8qjSomwodzS6ILswbLP1oLTrhYltHqjXU8Deq7X_M35NqJ6b1OM1wHffDzxzbZZGTdpKPje57aLKumVBtbU1_pwVBSvD4wUcOz7iLYHurPz2oS9uarluNRFsnBmCFWEEFqqMDlrrWbqgTP1IPxHJHI28AH_Ut56mKukHsMOrNiS7cldvTpWZnuWzgVgENLIgvQ8YQ2qPRW-_AvXCUtE9pYQ2dsj-GcIOalGD9d3kWNixHtcRIbEpNQ6I55E2oXFVuh_icFaaWD8rC6OkQXOR24_yFjm1HRrEwd8d9BLabG4TevTP5fzmclkitjtFI6aXwDZlnAL8XGZDB-v5iluFxXqZI4nHbkhwKrHf_Uh79nbo0eVES30r1Wbzsp9-R56NHiIV3FKpqYs6o8Yq2VTZNBywtsNZZVuJ8KP91Gvb32zKScSxtsstALKKem8BMh6bPFS3bjpr8vtZp4jl8nf9BpA9Uv2AG1LvoouuLonU7Kka2RhVJq2qqF2bjDfg86QxNmQsOkUrKYIfWlzn5wXtPSsIizvbdPn3wdhBh9pfOtV5vI0vuD2RYb8lmW9E5lRxMfNMvDo8ieFzIm30MudcqCQqbhj-KxJdz2ui8.', 0, ''),
('o431f4i3atr4nekglhums7g6m3', 0, 1, '1530562007', 'n4NYsISaEJc3I_4OoH7N898DUDJR1VLoH1qTxgfHL0W9EPcoa3pjXoWbEvY3HcvqV1zcOi-XPt8t8EkesdkW7XJgWlE68Dxc6-0bHg_WbXz35fFG7ypKNlsGhCsqOEpp3V9DToYnJcQXUQxrc_QhThkFddv1jme7yeXe6kh86aIKfE5JF_c9CJruwz9irRTQZnPzR_57H-hZ0nCZ09neO5Fy0J_LuFyqFjIhb9xjDWAGYAe-eDVhS51YojdZY-Z0SMHGLegih_tyukPMdWK9JtPKVqK-wmYqHfpti-qxrVSUhs9IJD5BhQVRXCcuAaMYT0S4JEErS6Fj0HU_ihomsqTO6qbTZx5rwJx24Wr00yCtEIeNMeuSh6wXaoEghsuLmaWqJ4CYJXSK0byilUCWQILm7GBDyrCzYRYP-nJcUM1ePAkm8bJUCtyqhXmTrL2KjNBvcnv71JZIl3NCAyhKdk24ov-pGDZjkthXU-UoqSkiBSEGZKGpZnWctgtgB4ftIWVDbTOZVOx-pRWbENkBByI34PoJMxGM_m-OYddm5nIPgsEK3OuPuG-fDtHWUsCV69fFUCCj1qeuyGMYEwrC0wPMG5swIpvj0atjFBUMi7q15TxY36ZALemZFjQMm-O9KECJdqXhBPAurGkoFjOVSZxAdVzFSKLb0Ibu9qm00WzXlTEpBvyphEJqEl9Uqo37_3q7FsDNrOYQ5jnBrDzxVPdXtEPAaLU_UcuP4atec8lgU7vabTsEZ94teEDTZtQzYg40KZdP5NBQ0c-apXislIEooX6rEIp9M3LvCYpFgxzWhyWGp7Iu-Z7fQIXjJUom7eG1ERYb4t_D9S1Ka1N1eXJh5NWBrj2TeNEP-osvYiWwu7J5UamAbFXCkg7Cx__GylpwfTLJXISwZ3-UXvetShUGk5Lq9isTJVLkluHcH0w.', 0, ''),
('ok3u2efn04p0g9eenmlotusnr7', 0, 1, '1530803394', 'cUeHcsqSOGxmrM8OpiuSve24oof4HUq9M8pfHE1pVBYkFO1kTMxRjV9xpYZwOsBPBFUHkeutsZ1SohXIYoAE7JfQPksdu9TB6yFFPVJ1rJLRH9tYr08xigldR7HMjRt_jQoVnUI7tYWamI3LX4EbcrbCEDF1PXs2lkG5DqNmDNzCskTj0jMqJ5EeFQoHD7GpB2KE2vjfyebTADwT9nNe1kmsibBuWti_GLA_8noIMWUUTZ9gmOHnWDHQwg391bOSoxPW8nHoKeedeJ0O6fmKr7paRI6MWc5A196wGoBkuIMd78tpnRvvJGO-IIzkraC0izoJeKYzPxmRC5D48iwln_SzvWnB_gH-cZxerlRuQ7NnfkURuYwIkq6LOpbk7WbL_tRosFQkTHQ5Yrgm2Q4xVOxJXpV9m4bgCBm-Gct-XSlZc17vc_0O4MzT2y-uovY38B34LYg0KpAHHPsVpPkNQnwpBjV3cf5-eABFyW8XT9GAfJF0VByfwnrf3u-QHpgYSlkw8RE2H4qhVNB0zwgkx_-gyaR8LXyXni5J30zV0yNyGpYiuUHS4wjS4Hks4_cjOCRN7MVX0YhcwrliBbKV6ydBax0fN7eGn-sz3vWWR2kxypXS_m45G6M-CSycjsxAev0fIrbFg0IKw_3gHDxF6qHE4pafkaWtzuCifNDGi4LBOlFdaJt-12kpX585J45iPSOdLAcpytO8DqKWB8NTpA0zHd2il-dAjqROxOA9kID4ltmcq77Fig2Eg6iS01xPnT0i4WSPitN2-cCXARYRki946zrMNm4RSFZnvR9WUYywhVpty1MktegIfufbKUgxiZTM-sTTE5cOawB8WSqoOpY4gwp-d24MBNGRCgTETLpY9JsJ3A4SlfwTABaCeSv66vPpO_qhmtiyDjxAOgFnI_3Inv-qfuGJo5TyUEEPMmbYINljAk49Gm411BUi8-QHCyr0aBli2TSDzW2yjhqV4wKvQ3rEC51gZbPPP3yVLB7Qf14ILPKPwRTl3E5vG8Ip2Gz5YY9TfpiyXtln4DdaCXQrDy-eufXEAYNhjcHTjAIViysrmAoR6ruQVaxk8gAMc3ftnrTyU8d3V_qteCdki2Dpv4NbvbUiqA1e01VFmVN5i2YtsmEkXXEndT1p29r-jjYbcaJTIyYIvSiMkjhrYzdk0xjzjjZpzrnfSFksYnNNZhZIcK63SymJQjvh8sLKde5H4xKHuFHwiopotpxHO4Doc9IqKchl0uS9wNQz8y056cmgQHxDp8LW3hMN1a8rLje6t5Jj1IQbqU8dzzRQolwPdWNLLLE3N8AZ5GfLhk-RMq7rme-RJEyfY9J9sAkOrrZiL0RIJbbxybphhWwOpgPjwe8LY_evpeRD1f-ySIS50hayCtk_BObEs-yCO7qi125LvounOrXRlsbw1DYWyKb83VnDtxlYZfhV7rxB3aPogulAm99YSCVlVXCkAE0suN_TAIaNkue9HyJCXf_vN--FLlLLdVIoHF1YLpzBqwnADaJ79fkh4l3_up2fx005oafXf_5fvpY4K5DlUVPB3EjLJtseMzgPJt_FIxY1bbllqhjWIFwIQxHpjTMhfBu5JXMRRVRAfFem8EN3MLttiE057DN-1aNHVOZaCXWURH1fmYkMkTuLMzzKYlJAx38EjcwINpquY6berVQmALNbrLzKof0gT5FfkCUPfW67OrFlQrV4grkSarQ7HpnXg3-THdkMzXAdgdHaQck_2aaq3mXrWcm9TmKBJRLbf6B_XPye-ba_mn0xPELcg5ePygKGElRpIumcdXkLLNO6QNA46WF2RZzgUufDszIBCTdyK6hPsSnvwsuhP4TJrBN40MqYXsJRG9nfYcW0iD-cfyhr1A55fyZ_D1-9MuXEmP_kifFq4fPapw9bi9K0MqooCO7gxv-EylFwLC6SOi1k6mLyWbD3rPYI1mH-e-hJPrmRl7bFWfQuZybRjItijF5lN0tFjkSafYEEP2C9lC4fy5LpIuvdsDlU7k-onShZMcQEOUEQ7I1xaBffHGh7Z-on58y514Us6uS12ATvPK4GlqudfXlstUWsNSz7XGPI6W0WoSWBl0fMrddnZ75xZUsPBAa_TWXj3wrMZ3W0j6pz6Wpbz1_41jxU1jJq7kFUCzHhzxLPoc7RBDpnKwY5ZJc2s-PD0oHuaxPD-IOjPfHLo2CPNjCqiWqNtiMqfekhHxOFQBbTZ_cCGpTP71CV_rv-AzRYkBO3H0AfAaNYqtrZyjuGdZffOx4HqyAe1mo0cO-9NqlRQBIJyy1QGtrmJG24VCcCnEALj19-zH_aW22lG0jSp3fYi1ujTPDbMfLctsw4ogal2KfNaFGNkgDzzDkp6PF8e2VD-er7CjLlahDxOeLedw..', 0, ''),
('ooqp8g83h0us9phvfqa87r30n2', 0, 1, '1530718085', 'LMMiPMswUc1QsnoRe_hKVX_y8l8Al0lWqcAsAtmATGnchbNkBoeWJnjO9YrlN06lYYQ5bl8hHTctS1yZKZzKTosep1mhSyavRTdbFIjCxxKFZlxFO4BJboKJEcD6OQAdbMTR__Jxk64PAkKMZi1vc7BUJkRuSr-WQUQj9EiCxrCPQoa2l5sRpwmGacZSbqiiVTozXLBXWUWif7UyhrWJfr7hQEFyT5E92CYL4v98QBI4YAbdYN7EsbYfV9FK_ZzyyoFTsMrKfQpuQvhK9KOWQumyvmy9bmoMaiUkiP7cLnuEE4T4UdG-hwZnFm0ScyDqS9Ar3nd6tSoqJIhSu8wZtsM5hka9yPIb2TKsXP65ouNe0CVwlldSnyKDJqb928lDR7wcgQN27z1l31sqNkgCy8yOquLlseDWJ_KyP5wvLAhU3QuE8egJLbWAo9dLPKpt4cf7Jks6fyC7nz6l2oy07b6Pw90SYi05ZCLa7VVKas77EK6UHAxlA3WaOA5qlQTbbiWd3mMMvGBbQffy5zCHWgaESn1t027qj2VYahiaI_UTDsels1zI3SirTksO2cXMvIwhmE7P5f7jtYsW7aH3JpJiB9RSPZdP-uDzk_vwbXIqJKX0ipfnafONGDNi1zFFS-wG2BotokMLhMfeXxZB8aamFU1wPa20OtK31XssP3aqq6FtrHnLKe2A_L17tXLQ8y5jzwKCgy-RRJQjaX1KOTRtywdaLh5tqqHNthRdB5irVjH1Ynw7_ugLF_hojsYVe1Gfzkmnc8xjj441U__mJBsLR4FiVKNUTR7mXG3AO6V6q5Gj0PpoZsBJheoLXNvRxDfPWpX8VCPTs6RpePc88y8PY2mnKsLxIcYOgC0UUd4_qhyEoe0bNduZvW7kbWzv4FegPjB6MRHEe11nEGwEgppU3oh26u1sRCmv_SNI92k.', 0, ''),
('ron535tfbvju7mes0dcc8k8v51', 0, 1, '1530783377', '9T7yIHgCiUgDWhSb77kFP0nEYvXWel3BXAJl6spSN10WXRW7d0fyvPibXYh26Np3UmUYHFIzV7QX0BvcRsjsgn8l2jlyUH1NURfbLZGdF57lBPBShJOoucmwei_-0eHDGLBF3BBw9K1l4feKJH6-SgZhj4pOqODWDEmwwgnUY8GViim29qt6w80WKyUlPa2B0Trnp0KD6X7XU4riZ4zfvtuu-ORvr5t9VZLcDMgLtoulWCf5ovP_Zn3Ktw5f_U2OBabSgIiOogL8LnlbnsHfPQV37edBA7B0krdyI4EVPwCwwtSBKujjdTOl4BWtiLU3ARcmOD7vBj0JUSLxl88Na40hAT1zjc2Y_NQ3bIRO-C6nvOJfBu1zN8OrZcjm8CjndfHr3MEGKnssT_Up7OKrDpct0wY1Ff4_Xt9WVk0PVT7FRwLpou2shgNe7ePu6YUaVhO6qRO8-HeStAMoQMD_7375jHOaqf2-oHMMjHpEDC5H92fIWxBInWaUoc0AVxYnnv01S4CojD3Ff4uimN0gaonJRk-kptHRGIZl0UT7sW2JIJBad22x9dM1Yx8gQmbsdlcCIp11rMLbitAcv0bCEvcgrzS-309XiNddmPaaDZNDBcP7TTFPb6Lwa3rtLoWpFk3FVa-QZl4PX3gNaIDPaKvYIt8BnMXW-xgCcnbKrX_Zl6fE_dj1rvI_1Zqysb5P0o4tx7shq93E-wMqG8degH4QvtAZDlOdbQaWW-5NQ1_G2bWWhg_LC41ztI4X3rwKpwHwJg4ZE0qYOuKXKUKIBneJpT15RXPculABx5lgL1EFW6w5YAMfxBHwH2UR3S0_QOk8FjVWkBMr3XZnQieX5oP69WFvws3i89IR6-cUMn867nJxFzTpz2ANak5LY7bjNcHZpwxO2PkPXVxRNb59vSw5QtSvVCCXNOYn6OFuaT4.', 0, ''),
('rva61eljke4oprmome04klcd01', 0, 1, '1530855087', 'RQYLM6yc3EDIKbdAC20D9CvLSMQ980reVemD6rAcmUNNcCc0o72OMqbBsulqfHbLasRyAKJadXAnntVXbLWerDZgI_dK56nX-NwPOSQ0UGtFN-v6L8VswmA97UPRpf2h3zFd0sZAYgB8yTvplVGysFF_rVIWipZzl35pSLItXnpAS1cJu4kqs6yGhbFvxSNQ_01TvUtcZYS6aATJegh7vlxQwCh1ObTsTGGNy_ECUK2U0g9OQAiLGgrNQ8OjVV7HEzg1VDpqeovDC5J2VqEvDOVxIx5qAzMJPQ3yJlOSijSSPaKmQ7XkLOUp08Z1NtPAMcxFCLxuWkmpRDHfabxenNxmV6GQzgh_wZuadE7SUpvfskcgAKrLzL-SgoYNAdU4BohPCai5wdStT7DHriq5ZH-6sd0686sO4UC4ZXJ084d4_ZOBI1Qfy6Vm7T3UbH1dtsctx8gkM280VD2a8HFq1M305BDwrBkehLkK4R8TQrQUp2QDoHh8bEj0ah_OOCwajitAx_WatJO536GMdMi1W03iILUMZGS1s6EF1EeQYk5BMqAKhuMzikC0G5H9OFPXKuXQ_ALycKwMgJQ07lt0FBcGon7FsAXjEyx8yOwvryq8y7DWp-_BUHK_ihEF7c3IkfiaQZWtdQY3qhkMvG_2EK9p52fQFYqoFHMdGQQLNRgyBhtOGYPugAE_2vff6g29Hcirap9SIjJJEV73981HKYcJV8wpEdMQPfn_zjVhEP71UUSTYwgtm_eyYjoqiErkUFLOt8FoiJB3RrspKBxycIzoINVSF5IS5dLTt-iSwafLfMfCEV3zYnkB2Rz58slDKgvf1_Zp3qCwqKxhvaWjCH7VTkA3o1GgcKAmOM9lR7LZ_p5etPIGm9BSd3H1FM4CzkqwLleZb-X82Z2eACyUDBshALHcYXoIgrdJH9fpAvgAakBKqc2l2BfLOQ2Avg1y9ZymB3D8NelpXCHIb6Ze7PSY6aiwoB-TWJxEMLkJs6rMfFPYFl92aBqgl-8KqILyBFh7fea27PpNObojeUH3hW-aOr10-sjQ-CZO6TrrTWs.', 0, ''),
('vm1ee7fre0jsdqq72dt7asmq47', 0, 1, '1530765354', 'wjOfRXhPQFWoQ4XMmbFtAMMf9DHRngmwcfaA7j4ZMUccPctN15RyIuBkIgXuVFbOvSGFB88XiBdeLZlnBSDa7WcvNF9m3rmtljeZR_85XdKKCX4FM6I8eK_daHrF2ZkJc12Py3kbzmEZmseV10fpJhN67Cq_0XK4Vz-zCma0luprsYn2pTinoD9dv0s1No_6zjvLSAQ5eXvzsGUY75wtIpAr9WrCBtKES00xatr2A-32aLvqxy1fB9YUmPdKoH4d5j4cXvlhfSq1I7HiVsMz1PZSn8Vhfxssmz8iirGY5pCwQjy0VTNGq6XTrtrAUdb_h4DKgDHNmITcu4LY2_U-AMmSf_lfFp3r5HCy6PWdCB7PkhmDKFobKjXvm2UxBplNmcQdhzPE_lkUIEJeZT6PMxaG4JFoi4SfrzSpQ171AdSBMJl2NHIlx4fJbhAruIWhEYV3bfzs-gNMZp4eL5l75m9VS0yyj9imlurRf04NGrW3XgGP0d8YZJXTcQk507tDwJeA37OFpkXpygyoXqnaz48N_Ce1P7H1oCjwHUZV9q_9MKbd0yKOjZ2ngn-SYTVt0cIx30aGVsQnVPxGAU8elvnLDp2WVu9BxhrlLozi6R6qMLobLIzwFvrPmEeX-4ob-eqHImeMheLSJfe24thimD90c2Naah7vVteK906gNOTTZ0NZNIRpagjAGPVW5dDXsCddiQXSAsEHCNsjllPOaB3NO3dDumeK-fRyPxV0bm4vGWCrW4keqkQksgiMc3gICqy_FFYgfziSy36aAMcN5LUVyeXPMDlYyUPX6igyrczvKOM_TwFvTH4OMiaXBDmG3QeG1qEkL0OQTuEjKfZniJquzNisHlcgn3EkPK1WarOhF-WgoElhPbLtmSXKZxhv9RMbXv8P5bizRFVjwfwrFAdKzaz8m3rrgnsFwA2wb_0.', 0, '');

-- --------------------------------------------------------

--
-- Table structure for table `t41uf_tags`
--

CREATE TABLE `t41uf_tags` (
  `id` int(10) UNSIGNED NOT NULL,
  `parent_id` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `lft` int(11) NOT NULL DEFAULT '0',
  `rgt` int(11) NOT NULL DEFAULT '0',
  `level` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `path` varchar(400) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `alias` varchar(400) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL DEFAULT '',
  `note` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `description` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `published` tinyint(1) NOT NULL DEFAULT '0',
  `checked_out` int(11) UNSIGNED NOT NULL DEFAULT '0',
  `checked_out_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `access` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `params` mediumtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `metadesc` varchar(1024) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT 'The meta description for the page.',
  `metakey` varchar(1024) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT 'The meta keywords for the page.',
  `metadata` varchar(2048) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT 'JSON encoded metadata properties.',
  `created_user_id` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `created_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `created_by_alias` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `modified_user_id` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `modified_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `images` mediumtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `urls` mediumtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `hits` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `language` char(7) COLLATE utf8mb4_unicode_ci NOT NULL,
  `version` int(10) UNSIGNED NOT NULL DEFAULT '1',
  `publish_up` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `publish_down` datetime NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `t41uf_tags`
--

INSERT INTO `t41uf_tags` (`id`, `parent_id`, `lft`, `rgt`, `level`, `path`, `title`, `alias`, `note`, `description`, `published`, `checked_out`, `checked_out_time`, `access`, `params`, `metadesc`, `metakey`, `metadata`, `created_user_id`, `created_time`, `created_by_alias`, `modified_user_id`, `modified_time`, `images`, `urls`, `hits`, `language`, `version`, `publish_up`, `publish_down`) VALUES
(1, 0, 0, 1, 0, '', 'ROOT', 'root', '', '', 1, 0, '0000-00-00 00:00:00', 1, '{}', '', '', '', 239, '2018-06-26 11:45:53', '', 0, '0000-00-00 00:00:00', '', '', 0, '*', 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `t41uf_template_styles`
--

CREATE TABLE `t41uf_template_styles` (
  `id` int(10) UNSIGNED NOT NULL,
  `template` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `client_id` tinyint(1) UNSIGNED NOT NULL DEFAULT '0',
  `home` char(7) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '0',
  `title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `params` mediumtext COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `t41uf_template_styles`
--

INSERT INTO `t41uf_template_styles` (`id`, `template`, `client_id`, `home`, `title`, `params`) VALUES
(12, 'shaper_xeon', 0, '1', 'shaper_xeon - Default', '{\"layout_width\":\"1170\",\"layout_type\":\"responsive\",\"logo_type\":\"image\",\"logo_position\":\"logo\",\"logo_type_image\":\"\",\"logo_type_text\":\"Helix\",\"logo_type_slogan\":\"Joomla! Templates Framework\",\"logo_width\":\"115\",\"logo_height\":\"46\",\"footer_position\":\"footer1\",\"showcp\":\"1\",\"copyright\":\"\\u00a9  {year} CoordinateDesign. All Rights Reserved.\",\"show_helix_logo\":\"0\",\"jcredit\":\"0\",\"credit_link\":\"0\",\"credit_text\":\"Joomla Template by {Joomshaper}\",\"validator\":\"0\",\"showtop\":\"0\",\"totop_position\":\"footer2\",\"preset\":\"preset3\",\"preset1_header\":\"#fff\",\"preset1_bg\":\"#e5e5e5\",\"preset1_text\":\"#999\",\"preset1_link\":\"#52b6ec\",\"preset2_header\":\"#fff\",\"preset2_bg\":\"#e5e5e5\",\"preset2_text\":\"#999\",\"preset2_link\":\"#1abc9c\",\"preset3_header\":\"#fff\",\"preset3_bg\":\"#e5e5e5\",\"preset3_text\":\"#999\",\"preset3_link\":\"#e6632d\",\"layout\":[{\"name\":\"Header\",\"class\":\"container\",\"responsive\":\"\",\"backgroundcolor\":\"rgba(255, 255, 255, 0)\",\"textcolor\":\"rgba(255, 255, 255, 0)\",\"linkcolor\":\"rgba(255, 255, 255, 0)\",\"linkhovercolor\":\"rgba(255, 255, 255, 0)\",\"margin\":\"\",\"padding\":\"\",\"children\":[{\"span\":\"3\",\"offset\":\"\",\"type\":\"modules\",\"position\":\"logo\",\"style\":\"\",\"customclass\":\"\",\"responsiveclass\":\"\"},{\"span\":\"9\",\"offset\":\"\",\"type\":\"modules\",\"position\":\"menu\",\"style\":\"none\",\"customclass\":\"\",\"responsiveclass\":\"\"}]},{\"name\":\"Video\",\"class\":\"\",\"responsive\":\"\",\"backgroundcolor\":\"rgba(255, 255, 255, 0)\",\"textcolor\":\"rgba(255, 255, 255, 0)\",\"linkcolor\":\"rgba(255, 255, 255, 0)\",\"linkhovercolor\":\"rgba(255, 255, 255, 0)\",\"margin\":\"\",\"padding\":\"\",\"children\":[{\"span\":\"12\",\"offset\":\"\",\"type\":\"modules\",\"position\":\"slider\",\"style\":\"sp-xhtml\",\"customclass\":\"\",\"responsiveclass\":\"\"}]},{\"name\":\"Slider\",\"class\":\"\",\"responsive\":\"\",\"backgroundcolor\":\"rgba(255, 255, 255, 0)\",\"textcolor\":\"rgba(255, 255, 255, 0)\",\"linkcolor\":\"rgba(255, 255, 255, 0)\",\"linkhovercolor\":\"rgba(255, 255, 255, 0)\",\"margin\":\"\",\"padding\":\"\",\"children\":[{\"span\":\"12\",\"offset\":\"\",\"type\":\"modules\",\"position\":\"slide\",\"style\":\"none\",\"customclass\":\"\",\"responsiveclass\":\"\"}]},{\"name\":\"Service\",\"class\":\"container\",\"responsive\":\"\",\"backgroundcolor\":\"rgba(255, 255, 255, 0)\",\"textcolor\":\"rgba(255, 255, 255, 0)\",\"linkcolor\":\"rgba(255, 255, 255, 0)\",\"linkhovercolor\":\"rgba(255, 255, 255, 0)\",\"margin\":\"\",\"padding\":\"\",\"children\":[{\"span\":\"4\",\"offset\":\"\",\"type\":\"modules\",\"position\":\"service1\",\"style\":\"sp_xhtml\",\"customclass\":\"\",\"responsiveclass\":\"\"},{\"span\":\"4\",\"offset\":\"\",\"type\":\"modules\",\"position\":\"service2\",\"style\":\"sp_xhtml\",\"customclass\":\"\",\"responsiveclass\":\"\"},{\"span\":\"4\",\"offset\":\"\",\"type\":\"modules\",\"position\":\"service3\",\"style\":\"sp_xhtml\",\"customclass\":\"\",\"responsiveclass\":\"\"}]},{\"name\":\"Portfolio\",\"class\":\"container\",\"responsive\":\"\",\"backgroundcolor\":\"rgba(255, 255, 255, 0)\",\"textcolor\":\"rgba(255, 255, 255, 0)\",\"linkcolor\":\"rgba(255, 255, 255, 0)\",\"linkhovercolor\":\"rgba(255, 255, 255, 0)\",\"margin\":\"\",\"padding\":\"\",\"children\":[{\"span\":\"12\",\"offset\":\"\",\"type\":\"modules\",\"position\":\"portfolio\",\"style\":\"sp-xhtml\",\"customclass\":\"\",\"responsiveclass\":\"\"}]},{\"name\":\"Pricing\",\"class\":\"container\",\"responsive\":\"\",\"backgroundcolor\":\"rgba(255, 255, 255, 0)\",\"textcolor\":\"rgba(255, 255, 255, 0)\",\"linkcolor\":\"rgba(255, 255, 255, 0)\",\"linkhovercolor\":\"rgba(255, 255, 255, 0)\",\"margin\":\"\",\"padding\":\"\",\"children\":[{\"span\":\"12\",\"offset\":\"\",\"type\":\"modules\",\"position\":\"Pricing\",\"style\":\"sp-xhtml\",\"customclass\":\"\",\"responsiveclass\":\"\"}]},{\"name\":\"Team\",\"class\":\"container\",\"responsive\":\"\",\"backgroundcolor\":\"rgba(255, 255, 255, 0)\",\"textcolor\":\"rgba(255, 255, 255, 0)\",\"linkcolor\":\"rgba(255, 255, 255, 0)\",\"linkhovercolor\":\"rgba(255, 255, 255, 0)\",\"margin\":\"\",\"padding\":\"\",\"children\":[{\"span\":\"12\",\"offset\":\"\",\"type\":\"modules\",\"position\":\"team\",\"style\":\"sp-xhtml\",\"customclass\":\"\",\"responsiveclass\":\"\"}]},{\"name\":\"Users\",\"class\":\"container\",\"responsive\":\"\",\"backgroundcolor\":\"rgba(255, 255, 255, 0)\",\"textcolor\":\"rgba(255, 255, 255, 0)\",\"linkcolor\":\"rgba(255, 255, 255, 0)\",\"linkhovercolor\":\"rgba(255, 255, 255, 0)\",\"margin\":\"\",\"padding\":\"30px 0\",\"children\":[{\"span\":\"3\",\"offset\":\"\",\"type\":\"modules\",\"position\":\"user1\",\"style\":\"sp_xhtml\",\"customclass\":\"\",\"responsiveclass\":\"\"},{\"span\":\"3\",\"offset\":\"\",\"type\":\"modules\",\"position\":\"user2\",\"style\":\"sp_xhtml\",\"customclass\":\"\",\"responsiveclass\":\"\"},{\"span\":\"3\",\"offset\":\"\",\"type\":\"modules\",\"position\":\"user3\",\"style\":\"sp_xhtml\",\"customclass\":\"\",\"responsiveclass\":\"\"},{\"span\":\"3\",\"offset\":\"\",\"type\":\"modules\",\"position\":\"user4\",\"style\":\"sp_xhtml\",\"customclass\":\"\",\"responsiveclass\":\"\"}]},{\"name\":\"Main Body\",\"class\":\"container\",\"responsive\":\"\",\"backgroundcolor\":\"rgba(246, 180, 74, 0)\",\"textcolor\":\"rgba(255, 255, 255, 0)\",\"linkcolor\":\"rgba(255, 255, 255, 0)\",\"linkhovercolor\":\"rgba(255, 255, 255, 0)\",\"margin\":\"\",\"padding\":\"\",\"children\":[{\"span\":\"3\",\"offset\":\"\",\"type\":\"modules\",\"position\":\"left\",\"style\":\"xhtml\",\"customclass\":\"\",\"responsiveclass\":\"\"},{\"span\":\"6\",\"offset\":\"\",\"type\":\"component\",\"position\":\"\",\"style\":\"xhtml\",\"customclass\":\"\",\"responsiveclass\":\"\"},{\"span\":\"3\",\"offset\":\"\",\"type\":\"modules\",\"position\":\"right\",\"style\":\"sp_xhtml\",\"customclass\":\"\",\"responsiveclass\":\"\"}]},{\"name\":\"Breadcrumb\",\"class\":\"container\",\"responsive\":\"\",\"backgroundcolor\":\"rgba(255, 255, 255, 0)\",\"textcolor\":\"rgba(255, 255, 255, 0)\",\"linkcolor\":\"rgba(255, 255, 255, 0)\",\"linkhovercolor\":\"rgba(255, 255, 255, 0)\",\"margin\":\"\",\"padding\":\"\",\"children\":[{\"span\":\"12\",\"offset\":\"\",\"type\":\"modules\",\"position\":\"breadcrumb\",\"style\":\"none\",\"customclass\":\"\",\"responsiveclass\":\"\"}]},{\"name\":\"Bottom\",\"class\":\"container\",\"responsive\":\"\",\"backgroundcolor\":\"rgba(255, 255, 255, 0)\",\"textcolor\":\"rgba(255, 255, 255, 0)\",\"linkcolor\":\"rgba(255, 255, 255, 0)\",\"linkhovercolor\":\"rgba(255, 255, 255, 0)\",\"margin\":\"\",\"padding\":\"\",\"children\":[{\"span\":\"6\",\"offset\":\"\",\"type\":\"modules\",\"position\":\"bottom1\",\"style\":\"sp_flat\",\"customclass\":\"\",\"responsiveclass\":\"\"},{\"span\":\"6\",\"offset\":\"\",\"type\":\"modules\",\"position\":\"bottom2\",\"style\":\"sp_flat\",\"customclass\":\"\",\"responsiveclass\":\"\"}]},{\"name\":\"Footer\",\"class\":\"container\",\"responsive\":\"\",\"backgroundcolor\":\"rgba(255, 255, 255, 0)\",\"textcolor\":\"rgba(255, 255, 255, 0)\",\"linkcolor\":\"rgba(255, 255, 255, 0)\",\"linkhovercolor\":\"rgba(255, 255, 255, 0)\",\"margin\":\"\",\"padding\":\"30px 0\",\"children\":[{\"span\":\"5\",\"offset\":\"\",\"type\":\"modules\",\"position\":\"footer1\",\"style\":\"none\",\"customclass\":\"\",\"responsiveclass\":\"\"},{\"span\":\"2\",\"offset\":\"\",\"type\":\"modules\",\"position\":\"footer2\",\"style\":\"none\",\"customclass\":\"\",\"responsiveclass\":\"\"},{\"span\":\"5\",\"offset\":\"\",\"type\":\"modules\",\"position\":\"footer3\",\"style\":\"none\",\"customclass\":\"\",\"responsiveclass\":\"\"}]}],\"menu\":\"mainmenu\",\"menutype\":\"mega\",\"menu_col_width\":\"200\",\"show_menu_image\":\"1\",\"menu_image_position\":\"1\",\"submenu_position\":\"0\",\"init_x\":\"0\",\"init_y\":\"0\",\"sub_x\":\"0\",\"sub_y\":\"0\",\"body_font\":\"\",\"body_selectors\":\"\",\"header_font\":\"\",\"header_selectors\":\"\",\"other_font\":\"\",\"other_selectors\":\"\",\"cache_time\":\"60\",\"compress_css\":\"0\",\"compress_js\":\"0\",\"enable_ga\":\"0\",\"ga_code\":\"\",\"loadjquery\":\"1\",\"loadfromcdn\":\"1\",\"lessoption\":\"0\",\"hide_component_area\":\"0\"}'),
(14, 'isis', 1, '1', 'isis - Default', '{\"templateColor\":\"\",\"logoFile\":\"\"}'),
(15, 'beez3', 0, '0', 'beez3 - Default', '{\"wrapperSmall\":\"53\",\"wrapperLarge\":\"72\",\"sitetitle\":\"\",\"sitedescription\":\"\",\"navposition\":\"center\",\"templatecolor\":\"nature\",\"backgroundcolor\":\"#eee\"}'),
(16, 'hathor', 1, '0', 'Hathor Administrator template - Default', '{\"showSiteName\":\"0\",\"logoFile\":\"\",\"colourChoice\":\"0\",\"boldText\":\"0\"}');

-- --------------------------------------------------------

--
-- Table structure for table `t41uf_ucm_base`
--

CREATE TABLE `t41uf_ucm_base` (
  `ucm_id` int(10) UNSIGNED NOT NULL,
  `ucm_item_id` int(10) NOT NULL,
  `ucm_type_id` int(11) NOT NULL,
  `ucm_language_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `t41uf_ucm_content`
--

CREATE TABLE `t41uf_ucm_content` (
  `core_content_id` int(10) UNSIGNED NOT NULL,
  `core_type_alias` varchar(400) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '' COMMENT 'FK to the content types table',
  `core_title` varchar(400) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `core_alias` varchar(400) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL DEFAULT '',
  `core_body` mediumtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `core_state` tinyint(1) NOT NULL DEFAULT '0',
  `core_checked_out_time` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '0000-00-00 00:00:00',
  `core_checked_out_user_id` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `core_access` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `core_params` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `core_featured` tinyint(4) UNSIGNED NOT NULL DEFAULT '0',
  `core_metadata` varchar(2048) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '' COMMENT 'JSON encoded metadata properties.',
  `core_created_user_id` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `core_created_by_alias` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `core_created_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `core_modified_user_id` int(10) UNSIGNED NOT NULL DEFAULT '0' COMMENT 'Most recent user that modified',
  `core_modified_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `core_language` char(7) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `core_publish_up` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `core_publish_down` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `core_content_item_id` int(10) UNSIGNED NOT NULL DEFAULT '0' COMMENT 'ID from the individual type table',
  `asset_id` int(10) UNSIGNED NOT NULL DEFAULT '0' COMMENT 'FK to the #__assets table.',
  `core_images` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `core_urls` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `core_hits` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `core_version` int(10) UNSIGNED NOT NULL DEFAULT '1',
  `core_ordering` int(11) NOT NULL DEFAULT '0',
  `core_metakey` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `core_metadesc` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `core_catid` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `core_xreference` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '' COMMENT 'A reference to enable linkages to external data sets.',
  `core_type_id` int(10) UNSIGNED NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci COMMENT='Contains core content data in name spaced fields';

-- --------------------------------------------------------

--
-- Table structure for table `t41uf_ucm_history`
--

CREATE TABLE `t41uf_ucm_history` (
  `version_id` int(10) UNSIGNED NOT NULL,
  `ucm_item_id` int(10) UNSIGNED NOT NULL,
  `ucm_type_id` int(10) UNSIGNED NOT NULL,
  `version_note` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '' COMMENT 'Optional version name',
  `save_date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `editor_user_id` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `character_count` int(10) UNSIGNED NOT NULL DEFAULT '0' COMMENT 'Number of characters in this version.',
  `sha1_hash` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '' COMMENT 'SHA1 hash of the version_data column.',
  `version_data` longtext COLLATE utf8mb4_unicode_ci NOT NULL COMMENT 'json-encoded string of version data',
  `keep_forever` tinyint(4) NOT NULL DEFAULT '0' COMMENT '0=auto delete; 1=keep'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `t41uf_updates`
--

CREATE TABLE `t41uf_updates` (
  `update_id` int(11) NOT NULL,
  `update_site_id` int(11) DEFAULT '0',
  `extension_id` int(11) DEFAULT '0',
  `name` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT '',
  `description` mediumtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `element` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT '',
  `type` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT '',
  `folder` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT '',
  `client_id` tinyint(3) DEFAULT '0',
  `version` varchar(32) COLLATE utf8mb4_unicode_ci DEFAULT '',
  `data` mediumtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `detailsurl` mediumtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `infourl` mediumtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `extra_query` varchar(1000) COLLATE utf8mb4_unicode_ci DEFAULT ''
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci COMMENT='Available Updates';

-- --------------------------------------------------------

--
-- Table structure for table `t41uf_update_sites`
--

CREATE TABLE `t41uf_update_sites` (
  `update_site_id` int(11) NOT NULL,
  `name` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT '',
  `type` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT '',
  `location` mediumtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `enabled` int(11) DEFAULT '0',
  `last_check_timestamp` bigint(20) DEFAULT '0',
  `extra_query` varchar(1000) COLLATE utf8mb4_unicode_ci DEFAULT ''
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci COMMENT='Update Sites';

--
-- Dumping data for table `t41uf_update_sites`
--

INSERT INTO `t41uf_update_sites` (`update_site_id`, `name`, `type`, `location`, `enabled`, `last_check_timestamp`, `extra_query`) VALUES
(1, 'Joomla! Core', 'collection', 'https://update.joomla.org/core/list.xml', 1, 1530862329, ''),
(3, 'Accredited Joomla! Translations', 'collection', 'https://update.joomla.org/language/translationlist_3.xml', 1, 0, ''),
(4, 'System - Helix Framework', 'plugin', 'http://joomshaper.com/updates/plg_system_helix.xml', 1, 0, ''),
(5, 'System - Helix Framework', 'extension', 'http://www.joomshaper.com/updates/plg_system_helix.xml', 1, 0, ''),
(6, 'WebInstaller Update Site', 'extension', 'http://appscdn.joomla.org/webapps/jedapps/webinstaller.xml', 1, 0, ''),
(7, 'Joomla! Update Component Update Site', 'extension', 'https://update.joomla.org/core/extensions/com_joomlaupdate.xml', 1, 0, ''),
(8, 'Weblinks Update Site', 'extension', 'https://raw.githubusercontent.com/joomla-extensions/weblinks/master/manifest.xml', 1, 0, ''),
(9, 'WebInstaller Update Site', 'extension', 'https://appscdn.joomla.org/webapps/jedapps/webinstaller.xml', 1, 0, ''),
(10, 'Gallery`s Update', 'extension', 'http://ordasoft.com/xml_update/osgallery_update.xml', 1, 0, '');

-- --------------------------------------------------------

--
-- Table structure for table `t41uf_update_sites_extensions`
--

CREATE TABLE `t41uf_update_sites_extensions` (
  `update_site_id` int(11) NOT NULL DEFAULT '0',
  `extension_id` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci COMMENT='Links extensions to update sites';

--
-- Dumping data for table `t41uf_update_sites_extensions`
--

INSERT INTO `t41uf_update_sites_extensions` (`update_site_id`, `extension_id`) VALUES
(1, 700),
(3, 802),
(4, 10000),
(5, 10000),
(6, 10013),
(7, 28),
(8, 801),
(9, 10013),
(10, 10020);

-- --------------------------------------------------------

--
-- Table structure for table `t41uf_usergroups`
--

CREATE TABLE `t41uf_usergroups` (
  `id` int(10) UNSIGNED NOT NULL COMMENT 'Primary Key',
  `parent_id` int(10) UNSIGNED NOT NULL DEFAULT '0' COMMENT 'Adjacency List Reference Id',
  `lft` int(11) NOT NULL DEFAULT '0' COMMENT 'Nested set lft.',
  `rgt` int(11) NOT NULL DEFAULT '0' COMMENT 'Nested set rgt.',
  `title` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT ''
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `t41uf_usergroups`
--

INSERT INTO `t41uf_usergroups` (`id`, `parent_id`, `lft`, `rgt`, `title`) VALUES
(1, 0, 1, 20, 'Public'),
(2, 1, 6, 17, 'Registered'),
(3, 2, 7, 14, 'Author'),
(4, 3, 8, 11, 'Editor'),
(5, 4, 9, 10, 'Publisher'),
(6, 1, 2, 5, 'Manager'),
(7, 6, 3, 4, 'Administrator'),
(8, 1, 18, 19, 'Super Users');

-- --------------------------------------------------------

--
-- Table structure for table `t41uf_users`
--

CREATE TABLE `t41uf_users` (
  `id` int(11) NOT NULL,
  `name` varchar(400) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `username` varchar(150) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `email` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `password` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `block` tinyint(4) NOT NULL DEFAULT '0',
  `sendEmail` tinyint(4) DEFAULT '0',
  `registerDate` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `lastvisitDate` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `activation` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `params` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `lastResetTime` datetime NOT NULL DEFAULT '0000-00-00 00:00:00' COMMENT 'Date of last password reset',
  `resetCount` int(11) NOT NULL DEFAULT '0' COMMENT 'Count of password resets since lastResetTime',
  `otpKey` varchar(1000) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '' COMMENT 'Two factor authentication encrypted keys',
  `otep` varchar(1000) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '' COMMENT 'One time emergency passwords',
  `requireReset` tinyint(4) NOT NULL DEFAULT '0' COMMENT 'Require user to reset password on next login'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `t41uf_users`
--

INSERT INTO `t41uf_users` (`id`, `name`, `username`, `email`, `password`, `block`, `sendEmail`, `registerDate`, `lastvisitDate`, `activation`, `params`, `lastResetTime`, `resetCount`, `otpKey`, `otep`, `requireReset`) VALUES
(239, 'Super User', 'admin', 'armekh@gmail.com', '$2y$10$JZg30qXYTclOXJ.qHoz5q.PC1ARkqeHRRfQw6K836KpUh1WQvgrZy', 0, 1, '2018-06-26 11:45:53', '2018-07-06 07:32:07', '0', '', '0000-00-00 00:00:00', 0, '', '', 0);

-- --------------------------------------------------------

--
-- Table structure for table `t41uf_user_keys`
--

CREATE TABLE `t41uf_user_keys` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` varchar(150) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `series` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `invalid` tinyint(4) NOT NULL,
  `time` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL,
  `uastring` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `t41uf_user_notes`
--

CREATE TABLE `t41uf_user_notes` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `catid` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `subject` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `body` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `state` tinyint(3) NOT NULL DEFAULT '0',
  `checked_out` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `checked_out_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `created_user_id` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `created_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `modified_user_id` int(10) UNSIGNED NOT NULL,
  `modified_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `review_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `publish_up` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `publish_down` datetime NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `t41uf_user_profiles`
--

CREATE TABLE `t41uf_user_profiles` (
  `user_id` int(11) NOT NULL,
  `profile_key` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `profile_value` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `ordering` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci COMMENT='Simple user profile storage table';

-- --------------------------------------------------------

--
-- Table structure for table `t41uf_user_usergroup_map`
--

CREATE TABLE `t41uf_user_usergroup_map` (
  `user_id` int(10) UNSIGNED NOT NULL DEFAULT '0' COMMENT 'Foreign Key to #__users.id',
  `group_id` int(10) UNSIGNED NOT NULL DEFAULT '0' COMMENT 'Foreign Key to #__usergroups.id'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `t41uf_user_usergroup_map`
--

INSERT INTO `t41uf_user_usergroup_map` (`user_id`, `group_id`) VALUES
(239, 8);

-- --------------------------------------------------------

--
-- Table structure for table `t41uf_utf8_conversion`
--

CREATE TABLE `t41uf_utf8_conversion` (
  `converted` tinyint(4) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `t41uf_utf8_conversion`
--

INSERT INTO `t41uf_utf8_conversion` (`converted`) VALUES
(2);

-- --------------------------------------------------------

--
-- Table structure for table `t41uf_viewlevels`
--

CREATE TABLE `t41uf_viewlevels` (
  `id` int(10) UNSIGNED NOT NULL COMMENT 'Primary Key',
  `title` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `ordering` int(11) NOT NULL DEFAULT '0',
  `rules` varchar(5120) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT 'JSON encoded access control.'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `t41uf_viewlevels`
--

INSERT INTO `t41uf_viewlevels` (`id`, `title`, `ordering`, `rules`) VALUES
(1, 'Public', 0, '[1]'),
(2, 'Registered', 2, '[6,2,8]'),
(3, 'Special', 3, '[6,3,8]'),
(5, 'Guest', 1, '[9]'),
(6, 'Super Users', 4, '[8]');

-- --------------------------------------------------------

--
-- Table structure for table `t41uf_weblinks`
--

CREATE TABLE `t41uf_weblinks` (
  `id` int(10) UNSIGNED NOT NULL,
  `catid` int(11) NOT NULL DEFAULT '0',
  `title` varchar(250) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `alias` varchar(400) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL DEFAULT '',
  `url` varchar(250) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `description` mediumtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `hits` int(11) NOT NULL DEFAULT '0',
  `state` tinyint(1) NOT NULL DEFAULT '0',
  `checked_out` int(11) NOT NULL DEFAULT '0',
  `checked_out_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `ordering` int(11) NOT NULL DEFAULT '0',
  `access` int(11) NOT NULL DEFAULT '1',
  `params` mediumtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `language` char(7) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `created` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `created_by` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `created_by_alias` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `modified` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `modified_by` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `metakey` mediumtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `metadesc` mediumtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `metadata` mediumtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `featured` tinyint(3) UNSIGNED NOT NULL DEFAULT '0' COMMENT 'Set if link is featured.',
  `xreference` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT 'A reference to enable linkages to external data sets.',
  `publish_up` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `publish_down` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `version` int(10) UNSIGNED NOT NULL DEFAULT '1',
  `images` mediumtext COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `t41uf_assets`
--
ALTER TABLE `t41uf_assets`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `idx_asset_name` (`name`),
  ADD KEY `idx_lft_rgt` (`lft`,`rgt`),
  ADD KEY `idx_parent_id` (`parent_id`);

--
-- Indexes for table `t41uf_associations`
--
ALTER TABLE `t41uf_associations`
  ADD PRIMARY KEY (`context`,`id`),
  ADD KEY `idx_key` (`key`);

--
-- Indexes for table `t41uf_banners`
--
ALTER TABLE `t41uf_banners`
  ADD PRIMARY KEY (`id`),
  ADD KEY `idx_state` (`state`),
  ADD KEY `idx_own_prefix` (`own_prefix`),
  ADD KEY `idx_banner_catid` (`catid`),
  ADD KEY `idx_language` (`language`),
  ADD KEY `idx_metakey_prefix` (`metakey_prefix`(100));

--
-- Indexes for table `t41uf_banner_clients`
--
ALTER TABLE `t41uf_banner_clients`
  ADD PRIMARY KEY (`id`),
  ADD KEY `idx_own_prefix` (`own_prefix`),
  ADD KEY `idx_metakey_prefix` (`metakey_prefix`(100));

--
-- Indexes for table `t41uf_banner_tracks`
--
ALTER TABLE `t41uf_banner_tracks`
  ADD PRIMARY KEY (`track_date`,`track_type`,`banner_id`),
  ADD KEY `idx_track_date` (`track_date`),
  ADD KEY `idx_track_type` (`track_type`),
  ADD KEY `idx_banner_id` (`banner_id`);

--
-- Indexes for table `t41uf_categories`
--
ALTER TABLE `t41uf_categories`
  ADD PRIMARY KEY (`id`),
  ADD KEY `cat_idx` (`extension`,`published`,`access`),
  ADD KEY `idx_access` (`access`),
  ADD KEY `idx_checkout` (`checked_out`),
  ADD KEY `idx_left_right` (`lft`,`rgt`),
  ADD KEY `idx_language` (`language`),
  ADD KEY `idx_path` (`path`(100)),
  ADD KEY `idx_alias` (`alias`(100));

--
-- Indexes for table `t41uf_contact_details`
--
ALTER TABLE `t41uf_contact_details`
  ADD PRIMARY KEY (`id`),
  ADD KEY `idx_access` (`access`),
  ADD KEY `idx_checkout` (`checked_out`),
  ADD KEY `idx_state` (`published`),
  ADD KEY `idx_catid` (`catid`),
  ADD KEY `idx_createdby` (`created_by`),
  ADD KEY `idx_featured_catid` (`featured`,`catid`),
  ADD KEY `idx_language` (`language`),
  ADD KEY `idx_xreference` (`xreference`);

--
-- Indexes for table `t41uf_content`
--
ALTER TABLE `t41uf_content`
  ADD PRIMARY KEY (`id`),
  ADD KEY `idx_access` (`access`),
  ADD KEY `idx_checkout` (`checked_out`),
  ADD KEY `idx_state` (`state`),
  ADD KEY `idx_catid` (`catid`),
  ADD KEY `idx_createdby` (`created_by`),
  ADD KEY `idx_featured_catid` (`featured`,`catid`),
  ADD KEY `idx_language` (`language`),
  ADD KEY `idx_xreference` (`xreference`),
  ADD KEY `idx_alias` (`alias`(191));

--
-- Indexes for table `t41uf_contentitem_tag_map`
--
ALTER TABLE `t41uf_contentitem_tag_map`
  ADD UNIQUE KEY `uc_ItemnameTagid` (`type_id`,`content_item_id`,`tag_id`),
  ADD KEY `idx_tag_type` (`tag_id`,`type_id`),
  ADD KEY `idx_date_id` (`tag_date`,`tag_id`),
  ADD KEY `idx_core_content_id` (`core_content_id`);

--
-- Indexes for table `t41uf_content_frontpage`
--
ALTER TABLE `t41uf_content_frontpage`
  ADD PRIMARY KEY (`content_id`);

--
-- Indexes for table `t41uf_content_rating`
--
ALTER TABLE `t41uf_content_rating`
  ADD PRIMARY KEY (`content_id`);

--
-- Indexes for table `t41uf_content_types`
--
ALTER TABLE `t41uf_content_types`
  ADD PRIMARY KEY (`type_id`),
  ADD KEY `idx_alias` (`type_alias`(100));

--
-- Indexes for table `t41uf_extensions`
--
ALTER TABLE `t41uf_extensions`
  ADD PRIMARY KEY (`extension_id`),
  ADD KEY `element_clientid` (`element`,`client_id`),
  ADD KEY `element_folder_clientid` (`element`,`folder`,`client_id`),
  ADD KEY `extension` (`type`,`element`,`folder`,`client_id`);

--
-- Indexes for table `t41uf_fields`
--
ALTER TABLE `t41uf_fields`
  ADD PRIMARY KEY (`id`),
  ADD KEY `idx_checkout` (`checked_out`),
  ADD KEY `idx_state` (`state`),
  ADD KEY `idx_created_user_id` (`created_user_id`),
  ADD KEY `idx_access` (`access`),
  ADD KEY `idx_context` (`context`(191)),
  ADD KEY `idx_language` (`language`);

--
-- Indexes for table `t41uf_fields_categories`
--
ALTER TABLE `t41uf_fields_categories`
  ADD PRIMARY KEY (`field_id`,`category_id`);

--
-- Indexes for table `t41uf_fields_groups`
--
ALTER TABLE `t41uf_fields_groups`
  ADD PRIMARY KEY (`id`),
  ADD KEY `idx_checkout` (`checked_out`),
  ADD KEY `idx_state` (`state`),
  ADD KEY `idx_created_by` (`created_by`),
  ADD KEY `idx_access` (`access`),
  ADD KEY `idx_context` (`context`(191)),
  ADD KEY `idx_language` (`language`);

--
-- Indexes for table `t41uf_fields_values`
--
ALTER TABLE `t41uf_fields_values`
  ADD KEY `idx_field_id` (`field_id`),
  ADD KEY `idx_item_id` (`item_id`(191));

--
-- Indexes for table `t41uf_finder_filters`
--
ALTER TABLE `t41uf_finder_filters`
  ADD PRIMARY KEY (`filter_id`);

--
-- Indexes for table `t41uf_finder_links`
--
ALTER TABLE `t41uf_finder_links`
  ADD PRIMARY KEY (`link_id`),
  ADD KEY `idx_type` (`type_id`),
  ADD KEY `idx_title` (`title`(100)),
  ADD KEY `idx_md5` (`md5sum`),
  ADD KEY `idx_url` (`url`(75)),
  ADD KEY `idx_published_list` (`published`,`state`,`access`,`publish_start_date`,`publish_end_date`,`list_price`),
  ADD KEY `idx_published_sale` (`published`,`state`,`access`,`publish_start_date`,`publish_end_date`,`sale_price`);

--
-- Indexes for table `t41uf_finder_links_terms0`
--
ALTER TABLE `t41uf_finder_links_terms0`
  ADD PRIMARY KEY (`link_id`,`term_id`),
  ADD KEY `idx_term_weight` (`term_id`,`weight`),
  ADD KEY `idx_link_term_weight` (`link_id`,`term_id`,`weight`);

--
-- Indexes for table `t41uf_finder_links_terms1`
--
ALTER TABLE `t41uf_finder_links_terms1`
  ADD PRIMARY KEY (`link_id`,`term_id`),
  ADD KEY `idx_term_weight` (`term_id`,`weight`),
  ADD KEY `idx_link_term_weight` (`link_id`,`term_id`,`weight`);

--
-- Indexes for table `t41uf_finder_links_terms2`
--
ALTER TABLE `t41uf_finder_links_terms2`
  ADD PRIMARY KEY (`link_id`,`term_id`),
  ADD KEY `idx_term_weight` (`term_id`,`weight`),
  ADD KEY `idx_link_term_weight` (`link_id`,`term_id`,`weight`);

--
-- Indexes for table `t41uf_finder_links_terms3`
--
ALTER TABLE `t41uf_finder_links_terms3`
  ADD PRIMARY KEY (`link_id`,`term_id`),
  ADD KEY `idx_term_weight` (`term_id`,`weight`),
  ADD KEY `idx_link_term_weight` (`link_id`,`term_id`,`weight`);

--
-- Indexes for table `t41uf_finder_links_terms4`
--
ALTER TABLE `t41uf_finder_links_terms4`
  ADD PRIMARY KEY (`link_id`,`term_id`),
  ADD KEY `idx_term_weight` (`term_id`,`weight`),
  ADD KEY `idx_link_term_weight` (`link_id`,`term_id`,`weight`);

--
-- Indexes for table `t41uf_finder_links_terms5`
--
ALTER TABLE `t41uf_finder_links_terms5`
  ADD PRIMARY KEY (`link_id`,`term_id`),
  ADD KEY `idx_term_weight` (`term_id`,`weight`),
  ADD KEY `idx_link_term_weight` (`link_id`,`term_id`,`weight`);

--
-- Indexes for table `t41uf_finder_links_terms6`
--
ALTER TABLE `t41uf_finder_links_terms6`
  ADD PRIMARY KEY (`link_id`,`term_id`),
  ADD KEY `idx_term_weight` (`term_id`,`weight`),
  ADD KEY `idx_link_term_weight` (`link_id`,`term_id`,`weight`);

--
-- Indexes for table `t41uf_finder_links_terms7`
--
ALTER TABLE `t41uf_finder_links_terms7`
  ADD PRIMARY KEY (`link_id`,`term_id`),
  ADD KEY `idx_term_weight` (`term_id`,`weight`),
  ADD KEY `idx_link_term_weight` (`link_id`,`term_id`,`weight`);

--
-- Indexes for table `t41uf_finder_links_terms8`
--
ALTER TABLE `t41uf_finder_links_terms8`
  ADD PRIMARY KEY (`link_id`,`term_id`),
  ADD KEY `idx_term_weight` (`term_id`,`weight`),
  ADD KEY `idx_link_term_weight` (`link_id`,`term_id`,`weight`);

--
-- Indexes for table `t41uf_finder_links_terms9`
--
ALTER TABLE `t41uf_finder_links_terms9`
  ADD PRIMARY KEY (`link_id`,`term_id`),
  ADD KEY `idx_term_weight` (`term_id`,`weight`),
  ADD KEY `idx_link_term_weight` (`link_id`,`term_id`,`weight`);

--
-- Indexes for table `t41uf_finder_links_termsa`
--
ALTER TABLE `t41uf_finder_links_termsa`
  ADD PRIMARY KEY (`link_id`,`term_id`),
  ADD KEY `idx_term_weight` (`term_id`,`weight`),
  ADD KEY `idx_link_term_weight` (`link_id`,`term_id`,`weight`);

--
-- Indexes for table `t41uf_finder_links_termsb`
--
ALTER TABLE `t41uf_finder_links_termsb`
  ADD PRIMARY KEY (`link_id`,`term_id`),
  ADD KEY `idx_term_weight` (`term_id`,`weight`),
  ADD KEY `idx_link_term_weight` (`link_id`,`term_id`,`weight`);

--
-- Indexes for table `t41uf_finder_links_termsc`
--
ALTER TABLE `t41uf_finder_links_termsc`
  ADD PRIMARY KEY (`link_id`,`term_id`),
  ADD KEY `idx_term_weight` (`term_id`,`weight`),
  ADD KEY `idx_link_term_weight` (`link_id`,`term_id`,`weight`);

--
-- Indexes for table `t41uf_finder_links_termsd`
--
ALTER TABLE `t41uf_finder_links_termsd`
  ADD PRIMARY KEY (`link_id`,`term_id`),
  ADD KEY `idx_term_weight` (`term_id`,`weight`),
  ADD KEY `idx_link_term_weight` (`link_id`,`term_id`,`weight`);

--
-- Indexes for table `t41uf_finder_links_termse`
--
ALTER TABLE `t41uf_finder_links_termse`
  ADD PRIMARY KEY (`link_id`,`term_id`),
  ADD KEY `idx_term_weight` (`term_id`,`weight`),
  ADD KEY `idx_link_term_weight` (`link_id`,`term_id`,`weight`);

--
-- Indexes for table `t41uf_finder_links_termsf`
--
ALTER TABLE `t41uf_finder_links_termsf`
  ADD PRIMARY KEY (`link_id`,`term_id`),
  ADD KEY `idx_term_weight` (`term_id`,`weight`),
  ADD KEY `idx_link_term_weight` (`link_id`,`term_id`,`weight`);

--
-- Indexes for table `t41uf_finder_taxonomy`
--
ALTER TABLE `t41uf_finder_taxonomy`
  ADD PRIMARY KEY (`id`),
  ADD KEY `parent_id` (`parent_id`),
  ADD KEY `state` (`state`),
  ADD KEY `ordering` (`ordering`),
  ADD KEY `access` (`access`),
  ADD KEY `idx_parent_published` (`parent_id`,`state`,`access`);

--
-- Indexes for table `t41uf_finder_taxonomy_map`
--
ALTER TABLE `t41uf_finder_taxonomy_map`
  ADD PRIMARY KEY (`link_id`,`node_id`),
  ADD KEY `link_id` (`link_id`),
  ADD KEY `node_id` (`node_id`);

--
-- Indexes for table `t41uf_finder_terms`
--
ALTER TABLE `t41uf_finder_terms`
  ADD PRIMARY KEY (`term_id`),
  ADD UNIQUE KEY `idx_term` (`term`),
  ADD KEY `idx_term_phrase` (`term`,`phrase`),
  ADD KEY `idx_stem_phrase` (`stem`,`phrase`),
  ADD KEY `idx_soundex_phrase` (`soundex`,`phrase`);

--
-- Indexes for table `t41uf_finder_terms_common`
--
ALTER TABLE `t41uf_finder_terms_common`
  ADD KEY `idx_word_lang` (`term`,`language`),
  ADD KEY `idx_lang` (`language`);

--
-- Indexes for table `t41uf_finder_tokens`
--
ALTER TABLE `t41uf_finder_tokens`
  ADD KEY `idx_word` (`term`),
  ADD KEY `idx_context` (`context`);

--
-- Indexes for table `t41uf_finder_tokens_aggregate`
--
ALTER TABLE `t41uf_finder_tokens_aggregate`
  ADD KEY `token` (`term`),
  ADD KEY `keyword_id` (`term_id`);

--
-- Indexes for table `t41uf_finder_types`
--
ALTER TABLE `t41uf_finder_types`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `title` (`title`);

--
-- Indexes for table `t41uf_languages`
--
ALTER TABLE `t41uf_languages`
  ADD PRIMARY KEY (`lang_id`),
  ADD UNIQUE KEY `idx_sef` (`sef`),
  ADD UNIQUE KEY `idx_langcode` (`lang_code`),
  ADD KEY `idx_access` (`access`),
  ADD KEY `idx_ordering` (`ordering`);

--
-- Indexes for table `t41uf_menu`
--
ALTER TABLE `t41uf_menu`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `idx_client_id_parent_id_alias_language` (`client_id`,`parent_id`,`alias`(100),`language`),
  ADD KEY `idx_componentid` (`component_id`,`menutype`,`published`,`access`),
  ADD KEY `idx_menutype` (`menutype`),
  ADD KEY `idx_left_right` (`lft`,`rgt`),
  ADD KEY `idx_language` (`language`),
  ADD KEY `idx_alias` (`alias`(100)),
  ADD KEY `idx_path` (`path`(100));

--
-- Indexes for table `t41uf_menu_types`
--
ALTER TABLE `t41uf_menu_types`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `idx_menutype` (`menutype`);

--
-- Indexes for table `t41uf_messages`
--
ALTER TABLE `t41uf_messages`
  ADD PRIMARY KEY (`message_id`),
  ADD KEY `useridto_state` (`user_id_to`,`state`);

--
-- Indexes for table `t41uf_messages_cfg`
--
ALTER TABLE `t41uf_messages_cfg`
  ADD UNIQUE KEY `idx_user_var_name` (`user_id`,`cfg_name`);

--
-- Indexes for table `t41uf_modules`
--
ALTER TABLE `t41uf_modules`
  ADD PRIMARY KEY (`id`),
  ADD KEY `published` (`published`,`access`),
  ADD KEY `newsfeeds` (`module`,`published`),
  ADD KEY `idx_language` (`language`);

--
-- Indexes for table `t41uf_modules_menu`
--
ALTER TABLE `t41uf_modules_menu`
  ADD PRIMARY KEY (`moduleid`,`menuid`);

--
-- Indexes for table `t41uf_newsfeeds`
--
ALTER TABLE `t41uf_newsfeeds`
  ADD PRIMARY KEY (`id`),
  ADD KEY `idx_access` (`access`),
  ADD KEY `idx_checkout` (`checked_out`),
  ADD KEY `idx_state` (`published`),
  ADD KEY `idx_catid` (`catid`),
  ADD KEY `idx_createdby` (`created_by`),
  ADD KEY `idx_language` (`language`),
  ADD KEY `idx_xreference` (`xreference`);

--
-- Indexes for table `t41uf_os_gallery`
--
ALTER TABLE `t41uf_os_gallery`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `t41uf_os_gallery_categories`
--
ALTER TABLE `t41uf_os_gallery_categories`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_gal_id` (`fk_gal_id`);

--
-- Indexes for table `t41uf_os_gallery_connect`
--
ALTER TABLE `t41uf_os_gallery_connect`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_cat_id` (`fk_cat_id`),
  ADD KEY `fk_gal_img_id` (`fk_gal_img_id`);

--
-- Indexes for table `t41uf_os_gallery_img`
--
ALTER TABLE `t41uf_os_gallery_img`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `t41uf_overrider`
--
ALTER TABLE `t41uf_overrider`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `t41uf_postinstall_messages`
--
ALTER TABLE `t41uf_postinstall_messages`
  ADD PRIMARY KEY (`postinstall_message_id`);

--
-- Indexes for table `t41uf_redirect_links`
--
ALTER TABLE `t41uf_redirect_links`
  ADD PRIMARY KEY (`id`),
  ADD KEY `idx_old_url` (`old_url`(100)),
  ADD KEY `idx_link_modifed` (`modified_date`);

--
-- Indexes for table `t41uf_schemas`
--
ALTER TABLE `t41uf_schemas`
  ADD PRIMARY KEY (`extension_id`,`version_id`);

--
-- Indexes for table `t41uf_session`
--
ALTER TABLE `t41uf_session`
  ADD PRIMARY KEY (`session_id`),
  ADD KEY `userid` (`userid`),
  ADD KEY `time` (`time`);

--
-- Indexes for table `t41uf_tags`
--
ALTER TABLE `t41uf_tags`
  ADD PRIMARY KEY (`id`),
  ADD KEY `tag_idx` (`published`,`access`),
  ADD KEY `idx_access` (`access`),
  ADD KEY `idx_checkout` (`checked_out`),
  ADD KEY `idx_left_right` (`lft`,`rgt`),
  ADD KEY `idx_language` (`language`),
  ADD KEY `idx_path` (`path`(100)),
  ADD KEY `idx_alias` (`alias`(100));

--
-- Indexes for table `t41uf_template_styles`
--
ALTER TABLE `t41uf_template_styles`
  ADD PRIMARY KEY (`id`),
  ADD KEY `idx_template` (`template`),
  ADD KEY `idx_home` (`home`);

--
-- Indexes for table `t41uf_ucm_base`
--
ALTER TABLE `t41uf_ucm_base`
  ADD PRIMARY KEY (`ucm_id`),
  ADD KEY `idx_ucm_item_id` (`ucm_item_id`),
  ADD KEY `idx_ucm_type_id` (`ucm_type_id`),
  ADD KEY `idx_ucm_language_id` (`ucm_language_id`);

--
-- Indexes for table `t41uf_ucm_content`
--
ALTER TABLE `t41uf_ucm_content`
  ADD PRIMARY KEY (`core_content_id`),
  ADD KEY `tag_idx` (`core_state`,`core_access`),
  ADD KEY `idx_access` (`core_access`),
  ADD KEY `idx_language` (`core_language`),
  ADD KEY `idx_modified_time` (`core_modified_time`),
  ADD KEY `idx_created_time` (`core_created_time`),
  ADD KEY `idx_core_modified_user_id` (`core_modified_user_id`),
  ADD KEY `idx_core_checked_out_user_id` (`core_checked_out_user_id`),
  ADD KEY `idx_core_created_user_id` (`core_created_user_id`),
  ADD KEY `idx_core_type_id` (`core_type_id`),
  ADD KEY `idx_alias` (`core_alias`(100)),
  ADD KEY `idx_title` (`core_title`(100)),
  ADD KEY `idx_content_type` (`core_type_alias`(100));

--
-- Indexes for table `t41uf_ucm_history`
--
ALTER TABLE `t41uf_ucm_history`
  ADD PRIMARY KEY (`version_id`),
  ADD KEY `idx_ucm_item_id` (`ucm_type_id`,`ucm_item_id`),
  ADD KEY `idx_save_date` (`save_date`);

--
-- Indexes for table `t41uf_updates`
--
ALTER TABLE `t41uf_updates`
  ADD PRIMARY KEY (`update_id`);

--
-- Indexes for table `t41uf_update_sites`
--
ALTER TABLE `t41uf_update_sites`
  ADD PRIMARY KEY (`update_site_id`);

--
-- Indexes for table `t41uf_update_sites_extensions`
--
ALTER TABLE `t41uf_update_sites_extensions`
  ADD PRIMARY KEY (`update_site_id`,`extension_id`);

--
-- Indexes for table `t41uf_usergroups`
--
ALTER TABLE `t41uf_usergroups`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `idx_usergroup_parent_title_lookup` (`parent_id`,`title`),
  ADD KEY `idx_usergroup_title_lookup` (`title`),
  ADD KEY `idx_usergroup_adjacency_lookup` (`parent_id`),
  ADD KEY `idx_usergroup_nested_set_lookup` (`lft`,`rgt`) USING BTREE;

--
-- Indexes for table `t41uf_users`
--
ALTER TABLE `t41uf_users`
  ADD PRIMARY KEY (`id`),
  ADD KEY `idx_name` (`name`(100)),
  ADD KEY `idx_block` (`block`),
  ADD KEY `username` (`username`),
  ADD KEY `email` (`email`);

--
-- Indexes for table `t41uf_user_keys`
--
ALTER TABLE `t41uf_user_keys`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `series` (`series`),
  ADD UNIQUE KEY `series_3` (`series`),
  ADD KEY `user_id` (`user_id`);

--
-- Indexes for table `t41uf_user_notes`
--
ALTER TABLE `t41uf_user_notes`
  ADD PRIMARY KEY (`id`),
  ADD KEY `idx_user_id` (`user_id`),
  ADD KEY `idx_category_id` (`catid`);

--
-- Indexes for table `t41uf_user_profiles`
--
ALTER TABLE `t41uf_user_profiles`
  ADD UNIQUE KEY `idx_user_id_profile_key` (`user_id`,`profile_key`);

--
-- Indexes for table `t41uf_user_usergroup_map`
--
ALTER TABLE `t41uf_user_usergroup_map`
  ADD PRIMARY KEY (`user_id`,`group_id`);

--
-- Indexes for table `t41uf_viewlevels`
--
ALTER TABLE `t41uf_viewlevels`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `idx_assetgroup_title_lookup` (`title`);

--
-- Indexes for table `t41uf_weblinks`
--
ALTER TABLE `t41uf_weblinks`
  ADD PRIMARY KEY (`id`),
  ADD KEY `idx_access` (`access`),
  ADD KEY `idx_checkout` (`checked_out`),
  ADD KEY `idx_state` (`state`),
  ADD KEY `idx_catid` (`catid`),
  ADD KEY `idx_createdby` (`created_by`),
  ADD KEY `idx_featured_catid` (`featured`,`catid`),
  ADD KEY `idx_language` (`language`),
  ADD KEY `idx_xreference` (`xreference`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `t41uf_assets`
--
ALTER TABLE `t41uf_assets`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT 'Primary Key', AUTO_INCREMENT=120;

--
-- AUTO_INCREMENT for table `t41uf_banners`
--
ALTER TABLE `t41uf_banners`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `t41uf_banner_clients`
--
ALTER TABLE `t41uf_banner_clients`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `t41uf_categories`
--
ALTER TABLE `t41uf_categories`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;

--
-- AUTO_INCREMENT for table `t41uf_contact_details`
--
ALTER TABLE `t41uf_contact_details`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `t41uf_content`
--
ALTER TABLE `t41uf_content`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=37;

--
-- AUTO_INCREMENT for table `t41uf_content_types`
--
ALTER TABLE `t41uf_content_types`
  MODIFY `type_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10005;

--
-- AUTO_INCREMENT for table `t41uf_extensions`
--
ALTER TABLE `t41uf_extensions`
  MODIFY `extension_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10022;

--
-- AUTO_INCREMENT for table `t41uf_fields`
--
ALTER TABLE `t41uf_fields`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `t41uf_fields_groups`
--
ALTER TABLE `t41uf_fields_groups`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `t41uf_finder_filters`
--
ALTER TABLE `t41uf_finder_filters`
  MODIFY `filter_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `t41uf_finder_links`
--
ALTER TABLE `t41uf_finder_links`
  MODIFY `link_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `t41uf_finder_taxonomy`
--
ALTER TABLE `t41uf_finder_taxonomy`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `t41uf_finder_terms`
--
ALTER TABLE `t41uf_finder_terms`
  MODIFY `term_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `t41uf_finder_types`
--
ALTER TABLE `t41uf_finder_types`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `t41uf_languages`
--
ALTER TABLE `t41uf_languages`
  MODIFY `lang_id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `t41uf_menu`
--
ALTER TABLE `t41uf_menu`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=182;

--
-- AUTO_INCREMENT for table `t41uf_menu_types`
--
ALTER TABLE `t41uf_menu_types`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `t41uf_messages`
--
ALTER TABLE `t41uf_messages`
  MODIFY `message_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `t41uf_modules`
--
ALTER TABLE `t41uf_modules`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=136;

--
-- AUTO_INCREMENT for table `t41uf_newsfeeds`
--
ALTER TABLE `t41uf_newsfeeds`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `t41uf_os_gallery`
--
ALTER TABLE `t41uf_os_gallery`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `t41uf_os_gallery_categories`
--
ALTER TABLE `t41uf_os_gallery_categories`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `t41uf_os_gallery_connect`
--
ALTER TABLE `t41uf_os_gallery_connect`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `t41uf_os_gallery_img`
--
ALTER TABLE `t41uf_os_gallery_img`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `t41uf_overrider`
--
ALTER TABLE `t41uf_overrider`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT COMMENT 'Primary Key';

--
-- AUTO_INCREMENT for table `t41uf_postinstall_messages`
--
ALTER TABLE `t41uf_postinstall_messages`
  MODIFY `postinstall_message_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `t41uf_redirect_links`
--
ALTER TABLE `t41uf_redirect_links`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `t41uf_tags`
--
ALTER TABLE `t41uf_tags`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `t41uf_template_styles`
--
ALTER TABLE `t41uf_template_styles`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;

--
-- AUTO_INCREMENT for table `t41uf_ucm_content`
--
ALTER TABLE `t41uf_ucm_content`
  MODIFY `core_content_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `t41uf_ucm_history`
--
ALTER TABLE `t41uf_ucm_history`
  MODIFY `version_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `t41uf_updates`
--
ALTER TABLE `t41uf_updates`
  MODIFY `update_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `t41uf_update_sites`
--
ALTER TABLE `t41uf_update_sites`
  MODIFY `update_site_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT for table `t41uf_usergroups`
--
ALTER TABLE `t41uf_usergroups`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT 'Primary Key', AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `t41uf_users`
--
ALTER TABLE `t41uf_users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=240;

--
-- AUTO_INCREMENT for table `t41uf_user_keys`
--
ALTER TABLE `t41uf_user_keys`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `t41uf_user_notes`
--
ALTER TABLE `t41uf_user_notes`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `t41uf_viewlevels`
--
ALTER TABLE `t41uf_viewlevels`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT 'Primary Key', AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `t41uf_weblinks`
--
ALTER TABLE `t41uf_weblinks`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
